import { createSwitchNavigator } from "@react-navigation/compat";
import { NavigationContainer } from "@react-navigation/native";
import {
  CardStyleInterpolators,
  createStackNavigator,
} from "@react-navigation/stack";
import { useSelector } from "react-redux";
import { Loading, navigationRef } from "./components";
import {
  ACTIVATION_STACK,
  AUTH_STACK,
  HOME_STACK,
  REGISTRATION_STACK,
  SHARED_STACKS,
  SPLASH_STACK,
  WELCOME_STACK,
} from "./helpers";
import { RootState } from "./models";
import {
  AccountScreen,
  AkteScreen,
  ChangePasswordScreen,
  FilePreviewScreen,
  HomeScreen,
  PrivacyProtection,
  Unsecured,
  WorkflowFormula,
} from "./screens";

const StackSplash = createStackNavigator();
const StackOnBoarding = createStackNavigator();
const StackWelcome = createStackNavigator();
const StackAuth = createStackNavigator();
const StackRegistration = createStackNavigator();
const StackProvider = createStackNavigator();
const StackActivation = createStackNavigator();
const StackPublic = createStackNavigator();

const optionScreen = {
  headerShown: false,
  gestureDirection: "horizontal",
  animation: "fade",
  cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
};

function SharedGroup(Stack) {
  return (
    <Stack.Group>
      <Stack.Screen
        name={SHARED_STACKS.FORMULA_SCREEN}
        component={WorkflowFormula.FormProcessScreen}
      />
      <StackProvider.Screen
        name={SHARED_STACKS.FORMULA_SCREEN_SUCCESS}
        component={WorkflowFormula.FormSuccessScreen}
      />
      <StackProvider.Screen
        name={SHARED_STACKS.FORMULA_SCREEN_ANSWER}
        component={WorkflowFormula.FormAnswerScreen}
      />
      <Stack.Screen
        name={SHARED_STACKS.PREVIEW_SCREEN}
        component={FilePreviewScreen}
      />
      <Stack.Screen
        name={SHARED_STACKS.PROVIDER_SCREEN}
        component={AccountScreen}
      />
      <StackProvider.Screen
        name={SHARED_STACKS.WEBVIEW_SCREEN}
        component={Unsecured.WebViewScreen}
      />
    </Stack.Group>
  );
}

function Splash() {
  return (
    <StackSplash.Navigator screenOptions={optionScreen as any}>
      <StackSplash.Screen
        name={SPLASH_STACK.SPLASH_SCREEN}
        component={Unsecured.SplashScreen}
      />
    </StackSplash.Navigator>
  );
}

function Public() {
  return (
    <StackPublic.Navigator screenOptions={optionScreen as any}>
      {SharedGroup(StackPublic)}
    </StackPublic.Navigator>
  );
}

function OnBoarding() {
  return (
    <StackOnBoarding.Navigator screenOptions={optionScreen as any}>
      <StackOnBoarding.Screen
        name={AUTH_STACK.ON_BOARDING_SCREEN}
        component={Unsecured.OnBoardingScreen}
      />
    </StackOnBoarding.Navigator>
  );
}

function Welcome() {
  return (
    <StackWelcome.Navigator screenOptions={optionScreen as any}>
      <StackWelcome.Screen
        name={WELCOME_STACK.WELCOME_SCREEN}
        component={Unsecured.WelcomeScreen}
      />
    </StackWelcome.Navigator>
  );
}

function Auth() {
  return (
    <StackAuth.Navigator
      screenOptions={optionScreen as any}
      initialRouteName={AUTH_STACK.SIGN_IN_SCREEN}
    >
      <StackAuth.Screen
        name={AUTH_STACK.SIGN_IN_SCREEN}
        component={Unsecured.SignInScreen}
      />
      {/* <StackAuth.Screen
        name={AUTH_STACK.GAIN_ACCESS_SCREEN}
        component={Unsecured.GainAccessScreen}
      /> */}
      <StackAuth.Screen
        name={AUTH_STACK.PIN_SCREEN}
        component={Unsecured.PinScreen}
      />
      <StackAuth.Screen
        name={AUTH_STACK.FORGOT_SCREEN}
        component={Unsecured.ForgotScreen}
      />
      <StackAuth.Screen
        name={AUTH_STACK.FORGOT_USERNAME_SCREEN}
        component={Unsecured.ForgotUsernameScreen}
      />
      <StackAuth.Screen
        name={AUTH_STACK.FORGOT_PASSWORD_SCREEN}
        component={Unsecured.ForgotPasswordScreen}
      />
      <StackAuth.Screen
        name={AUTH_STACK.RESET_PASSWORD_SCREEN}
        component={Unsecured.ResetPasswordScreen}
      />
    </StackAuth.Navigator>
  );
}

function Activation() {
  return (
    <StackActivation.Navigator screenOptions={optionScreen as any}>
      <StackActivation.Screen
        name={ACTIVATION_STACK.PUSHNOTIF_SCREEN}
        component={PrivacyProtection.PushNotificationScreen}
      />
      <StackActivation.Screen
        name={ACTIVATION_STACK.BIOMETRIC_SCREEN}
        component={PrivacyProtection.BiometricActivationScreen}
      />
    </StackActivation.Navigator>
  );
}

function Registration() {
  return (
    <StackRegistration.Navigator screenOptions={optionScreen as any}>
      <StackRegistration.Screen
        name={REGISTRATION_STACK.STARTED_SCREEN}
        component={Unsecured.RegistrationStartedScreen}
      />
      <StackRegistration.Screen
        name={REGISTRATION_STACK.USERNAME_SCREEN}
        component={Unsecured.RegistrationUsernameScreen}
      />
      <StackRegistration.Screen
        name={REGISTRATION_STACK.PASSWORD_SCREEN}
        component={Unsecured.RegistrationPasswordScreen}
      />
      <StackRegistration.Screen
        name={REGISTRATION_STACK.CONGRATS_SCREEN}
        component={Unsecured.CongratsScreen}
      />
      {SharedGroup(StackPublic)}
    </StackRegistration.Navigator>
  );
}

function HomeStack() {
  return (
    <StackProvider.Navigator
      screenOptions={optionScreen as any}
      initialRouteName={HOME_STACK.HOME_SCREEN}
    >
      <StackProvider.Screen
        name={HOME_STACK.HOME_SCREEN}
        component={HomeScreen}
      />
      {/* <StackProvider.Screen
        name="NotificationHandler"
        component={NotificationHandler}
      /> */}

      {/* <StackProvider.Screen name="ScreenSearch" component={ScreenSearch} /> */}
      {/* <StackProvider.Screen
        name="ScreenNotification"
        component={ScreenNotification}
      /> */}

      <StackProvider.Screen
        name={HOME_STACK.MESSAGE_SCREEN}
        component={AkteScreen}
      />
      <StackProvider.Screen
        name={HOME_STACK.COMPOSE_MESSAGE_SCREEN}
        component={AkteScreen.Message.ComposeMessageScreen}
      />
      <StackProvider.Screen
        name={HOME_STACK.CHANGE_PASSWORD_SCREEN}
        component={ChangePasswordScreen}
      />
      {SharedGroup(StackProvider)}
    </StackProvider.Navigator>
  );
}

const AppNavigator = createSwitchNavigator(
  {
    Splash: Splash,
    OnBoarding: OnBoarding,
    Welcome: Welcome,
    Auth: Auth,
    Registration: Registration,
    Home: HomeStack,
    Activation: Activation,
    Public: Public,
  },
  {
    initialRouteName: "Splash",
    backBehavior: "initialRoute",
  }
);

export function Route() {
  const isLoading = useSelector(
    (state: RootState) => state.esy.appState.isLoading
  );

  return (
    <NavigationContainer independent={true} ref={navigationRef}>
      <AppNavigator screenProps={optionScreen} />
      {isLoading ? <Loading /> : null}
    </NavigationContainer>
  );
}
