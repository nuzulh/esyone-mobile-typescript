import { Text } from "react-native";
import { IconButton } from "./icon-button.component";

export declare type TextIconButtonProps = {
  icon: any;
  text: string;
  onPress?: () => void;
};

export function TextIconButton({ icon, text, onPress }: TextIconButtonProps) {
  return (
    <IconButton icon={icon} onPress={onPress}>
      <Text
        style={{ fontSize: 14, fontFamily: "Lato-Regular", color: "#030D12" }}
      >
        {text}
      </Text>
    </IconButton>
  );
}
