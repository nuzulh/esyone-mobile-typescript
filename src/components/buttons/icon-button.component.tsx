import { Image } from "react-native";
import { Button } from "./button.component";

export declare type IconButtonProps = {
  icon: any;
  children?: any;
  onPress?: () => void;
  style?: any;
};

export function IconButton({
  icon,
  children,
  onPress,
  style = {},
}: IconButtonProps) {
  return (
    <Button onPress={onPress}>
      <Image source={icon} style={{ height: 20, width: 20, ...style }} />
      {children}
    </Button>
  );
}
