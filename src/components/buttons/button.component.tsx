import { Text, TouchableOpacity, Image } from "react-native";
import { useContext } from "react";
import { AssetsService, Services } from "../../services";

export declare type ButtonProps = {
  children: any;
  disabled?: boolean;
  style?: any;
  onPress?: () => void;
};

export function Button({
  children,
  disabled,
  style = {},
  onPress,
}: ButtonProps) {
  return (
    <TouchableOpacity
      style={{
        flexDirection: "row",
        justifyContent: "center",
        marginTop: 0,
        marginRight: 0,
        marginLeft: 0,
        paddingHorizontal: 10,
        paddingVertical: 18,
        width: "100%",
        ...style,
      }}
      disabled={disabled}
      onPress={onPress}
    >
      {children}
    </TouchableOpacity>
  );
}

function PrimaryButton(props: ButtonProps) {
  return (
    <Button
      {...props}
      style={{
        backgroundColor: props.disabled ? "#c6c6c6" : "#404040",
        borderRadius: 5,
        borderColor: props.disabled ? "#a6a6a6" : "#303030",
        borderWidth: 1,
        ...(props.style || {}),
      }}
    >
      {props.children}
    </Button>
  );
}

PrimaryButton.Text = function ({
  text,
  style = {},
}: {
  text: string;
  style?: any;
}) {
  return (
    <Text
      style={{
        textAlign: "center",
        fontSize: 14,
        fontFamily: "Lato-Bold",
        color: "white",
        ...style,
      }}
    >
      {text}
    </Text>
  );
};

function SecondaryButton(props: ButtonProps) {
  return (
    <Button
      {...props}
      style={{
        borderColor: "#6b7280",
        borderWidth: 1,
        backgroundColor: "#f9fafb",
        borderRadius: 5,
        paddingVertical: 18,
        // backgroundColor: "#DFDFDF",
        ...(props.style || {}),
      }}
    >
      {props.children}
    </Button>
  );
}

SecondaryButton.Text = function ({
  text,
  style = {},
}: {
  text: string;
  style?: any;
}) {
  return (
    <Text
      style={{
        textAlign: "center",
        fontSize: 14,
        fontFamily: "Lato-Bold",
        color: "#1f2937",
        // backgroundColor: "#DFDFDF",
        ...style,
      }}
    >
      {text}
    </Text>
  );
};

function BackButton(props: {
  onPress: () => void;
}) {
  const services = useContext(Services);

  return (
    <Button
      onPress={props.onPress}
      style={{
        margin: 16,
        width: "15%",
      }}
    >
      <Image
        source={services.assetsService.getIcons("iconArrowLeft")}
        style={{ width: 24, height: 24 }}
      />
    </Button>
  );
}

Button.Primary = PrimaryButton;
Button.Secondary = SecondaryButton;
Button.Back = BackButton;
