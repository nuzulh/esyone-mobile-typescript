import { createContext, useReducer } from "react";
import { Akte, Message } from "../../models";
import { MetaFile } from "../../services";

declare type AkteState = {
  akte: Akte;
  isMenuVisible: boolean;
  selectedMessage?: Message;
  selectedFile?: MetaFile;
};

export const AkteContext = createContext(
  {} as {
    selectMessage: (message: Message) => void;
    selectFile: (file: MetaFile) => void;
    reset: () => void;
    showMenu: (isVisible: boolean) => void;
  } & AkteState
);

export function createAkteContext(akte: Akte) {
  const [state, dispatch] = useReducer(reducer, {
    akte,
    isMenuVisible: false,
    selectedMessage: null,
    selectedFile: null,
  });

  return {
    ...state,
    selectMessage: (message: Message) =>
      dispatch({ type: "SELECTED_MESSAGE", payload: message }),
    selectFile: (file: MetaFile) =>
      dispatch({ type: "SELECTED_FILE", payload: file }),
    reset: () => dispatch({ type: "RESET" }),
    showMenu: (isVisible) =>
      dispatch({ type: "SET_MENU_VISIBLE", payload: isVisible }),
  };
}

function reducer(state, action): AkteState {
  switch (action.type) {
    case "SET_MENU_VISIBLE":
      return { ...state, isMenuVisible: action.payload };
    case "SELECTED_MESSAGE":
      return { ...state, selectedMessage: action.payload };
    case "SELECTED_FILE":
      return { ...state, selectedFile: action.payload };
    case "RESET":
      return {
        ...state,
        isMenuVisible: false,
        selectedMessage: null,
        selectedFile: null,
      };
    default:
      return state;
  }
}
