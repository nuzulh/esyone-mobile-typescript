import { Text, View } from "react-native";
import { AkteAssignee } from "../../models";
import { CircleImage } from "../image";

export declare type AssigneeImageProps = {
  assignee: AkteAssignee;
  fallback?: any; // fallback source
};

export function AssigneeImage({ assignee, fallback }: AssigneeImageProps) {
  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "flex-start",
        width: "90%",
        alignSelf: "center",
      }}
    >
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <CircleImage
          size={52}
          style={{ alignSelf: "flex-start", marginTop: 5 }}
          source={
            assignee.profilePictureUrl
              ? { uri: assignee.profilePictureUrl }
              : fallback
          }
        />
        <View style={{ flexDirection: "row" }}>
          <Text
            style={{
              textAlign: "left",
              fontSize: 14,
              marginVertical: 10,
              marginHorizontal: 10,
            }}
          >
            {assignee.firstName && assignee.lastName
              ? assignee.firstName + " " + assignee.lastName
              : assignee.lastName}
          </Text>
        </View>
      </View>
    </View>
  );
}
