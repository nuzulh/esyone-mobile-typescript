import { TabActions, useNavigation } from "@react-navigation/native";
import { Text, TouchableOpacity, View } from "react-native";
import { useDispatch } from "react-redux";
import { navigateToAction } from "../../../helpers";
import { Akte } from "../../../models";

export declare type AkteHasAufgabeAlertProps = {
  akte: Akte;
};

export function AkteHasAufgabeAlert({ akte }: AkteHasAufgabeAlertProps) {
  // const dispatch = useDispatch();
  const navigation = useNavigation();

  if (!akte.hasOpenAufgabe) return null;

  return (
    <View
      style={{
        width: "95%",
        backgroundColor: "#EB5757",
        alignSelf: "center",
        margin: 10,
        borderRadius: 10,
      }}
    >
      <View
        style={{
          paddingTop: 7,
          paddingRight: 7,
          paddingBottom: 7,
          paddingLeft: 20,
        }}
      >
        <Text
          numberOfLines={1}
          style={[
            {
              fontSize: 14,
              fontFamily: "Lato-Regular",
              marginVertical: 2,
            },
            {
              color: "white",
            },
          ]}
        >
          {akte.hasOpenAufgabe
            ? "Ihre Mithilfe wird benötigt"
            : "Status ihres Falles"}
        </Text>
        <Text
          style={{
            fontSize: 14,
            fontFamily: "Lato-Regular",
            color: "white",
            marginVertical: 15,
          }}
        >
          {`Um Ihren Fall weiterverfolgen zu können, benötigen wir noch einige Angaben von Ihnen.`}
        </Text>
      </View>
      <TouchableOpacity
        onPress={
          () => navigation.dispatch(TabActions.jumpTo("Aufgaben"))
          // dispatch(
          //   navigateToAction({
          //     navigateTo: {
          //       screen: "Aufgaben",
          //     },
          //   })
          // )
        }
        style={{
          backgroundColor: "white",
          borderRadius: 10,
          alignSelf: "center",
          justifyContent: "center",
          paddingVertical: 10,
          marginBottom: 15,
          width: "90%",
        }}
      >
        <Text
          numberOfLines={1}
          style={{
            fontSize: 14,
            fontFamily: "Lato-Regular",
            color: "black",
            marginVertical: 2,
            alignSelf: "center",
          }}
        >
          {"Zu Ihrer Aufgabe"}
        </Text>
      </TouchableOpacity>
    </View>
  );
}
