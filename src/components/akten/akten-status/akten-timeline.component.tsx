import { Image, Platform, Text, TouchableOpacity, View } from "react-native";
import { useEsyState, useToggle } from "../../../hooks";
import { Akte } from "../../../models";

export declare type AktenTimelineProps = {
  akte: Akte;
  expandIcon?: any;
  hideIcon?: any;
};

export function AktenTimeline({
  akte,
  expandIcon,
  hideIcon,
}: AktenTimelineProps) {
  const { value: isExpand, toggle: toggleExpand } = useToggle(true);
  const phases = useEsyState(
    (state) => state.akten.akten[akte.id]
      ? state.akten.akten[akte.id].phases
      : []
  );
  const CASE_CLOSED = "Fall Abgeschlossen";

  // console.log(JSON.stringify(phases, null, 2));

  if (!phases || phases.length === 0) return null;

  return (
    <View
      style={{
        width: "95%",
        alignSelf: "center",
        paddingVertical: 10,
        marginVertical: 5,
        marginHorizontal: 5,
        borderTopEndRadius: 10,
        borderTopStartRadius: 10,
        borderBottomEndRadius: 10,
        borderBottomStartRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
          width: 1,
          height: Platform.OS == "ios" ? 3 : 1,
        },
        shadowOpacity: Platform.OS == "ios" ? 0.1 : 0.4,
        shadowRadius: 3,
        elevation: Platform.OS == "ios" ? 1 : 5,
        backgroundColor: "#FFFFFF",
      }}
    >
      <View
        style={{
          width: "90%",
          alignSelf: "center",
          margin: 10,
          borderRadius: 10,
          backgroundColor: "white",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <View>
          <Text
            numberOfLines={1}
            style={[
              {
                fontSize: 14,
                fontFamily: "Lato-Regular",
                marginVertical: 2,
              },
              { color: "black" },
            ]}
          >
            Status
          </Text>
        </View>
        <TouchableOpacity onPress={toggleExpand}>
          <Image
            source={!isExpand ? expandIcon : hideIcon}
            style={{ height: 20, width: 20, resizeMode: "contain" }}
          />
        </TouchableOpacity>
      </View>

      {(isExpand
        ? phases
        : phases.filter((p) => p.states.length > 0).slice(-1))
        .map((phase, phaseIdx) => (
          <View
            key={phaseIdx}
            style={{
              borderLeftColor: "#7A7A7A",
              borderStyle: Platform.OS === "ios" ? "solid" : "dashed",
              borderLeftWidth: phase.name !== CASE_CLOSED ? 1 : 0,
              marginLeft: phase.name !== CASE_CLOSED ? 28 : 29,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "center",
                marginLeft: 20,
                marginBottom: phase.name === CASE_CLOSED
                  && phase.states.length === 0 ? 0 : 8,
              }}
            >
              <DrawTimelineStatus
                statusType={
                  phase.states.length === 0
                    ? "empty"
                    : ![...phase.states].pop().isInProgress
                      || phase.name === CASE_CLOSED
                      ? "done"
                      : "none"
                }
              />
              <Text
                numberOfLines={1}
                style={{
                  fontSize: 14,
                  fontFamily: "Lato-Regular",
                }}
              >
                {phase.name}
              </Text>
            </View>

            {phase.states.length > 0 ? (
              isExpand
                ? phase.states
                : phase.states.slice(-1)
            ).map((state, stateIdx) => (
              <View
                key={stateIdx}
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  alignItems: "center",
                  marginLeft: 20,
                  marginBottom: stateIdx === phase.states.length - 1 && isExpand
                    ? 12
                    : phase.name !== CASE_CLOSED && isExpand
                      ? 6
                      : 0,
                }}
              >
                <DrawTimelineStatus
                  statusType={
                    state.isInProgress
                      && (stateIdx === phase.states.length - 1 || !isExpand)
                      && phase.name !== CASE_CLOSED
                      ? "inprogress"
                      : "none"
                  }
                />
                <Text
                  numberOfLines={1}
                  style={{
                    fontSize: 11,
                    fontFamily: "Lato-Regular",
                    color: "#7A7A7A",
                  }}
                >
                  {state.statusText}
                </Text>
              </View>
            )) : null}
          </View>
        ))}
    </View>
  );
}

function DrawTimelineStatus({
  statusType,
}: {
  statusType: "done" | "inprogress" | "empty" | "none";
}) {
  if (statusType === "none") return null;

  return (
    <View
      style={[
        {
          zIndex: 103,
          position: "absolute",
          left: Platform.OS === "ios" ? -28 : -29,
          width: 16,
          height: 16,
          borderRadius: 16,
        },
        {
          backgroundColor: statusType === "done"
            ? "green"
            : statusType === "inprogress"
              ? "orange"
              : "rgba(224, 224, 224, 1)"
        },
      ]}
    ></View>
  );
}
