import {
  Dimensions,
  Image,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useSelector } from "react-redux";
import { formatDateAndTimeCasesDetail, localization } from "../../../helpers";
import { useToggle } from "../../../hooks";
import { Akte, RootState } from "../../../models";

const { height } = Dimensions.get("window");

export declare type AktenInfoProps = {
  akte: Akte;
  expandIcon: any;
  hideIcon: any;
};

export function AktenInfo({ akte, expandIcon, hideIcon }: AktenInfoProps) {
  const { value: isExpand, toggle: toggleExpand } = useToggle(true);

  return (
    <View
      style={[
        {
          width: "95%",
          alignSelf: "center",
          marginVertical: 5,
          marginHorizontal: 5,
          borderTopEndRadius: 10,
          borderTopStartRadius: 10,
          borderBottomEndRadius: 10,
          borderBottomStartRadius: 10,
          shadowColor: "#000",
          shadowOffset: {
            width: 1,
            height: Platform.OS == "ios" ? 3 : 1,
          },
          shadowOpacity: Platform.OS == "ios" ? 0.1 : 0.4,
          shadowRadius: 3,
          elevation: Platform.OS == "ios" ? 1 : 5,
        },
        { backgroundColor: "#FFFFFF" },
      ]}
    >
      <View
        style={{
          width: "95%",
          alignSelf: "center",
          margin: 10,
          borderRadius: 10,
          backgroundColor: "white",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <View
          style={{
            paddingTop: 7,
            paddingRight: 7,
            paddingBottom: 7,
            paddingLeft: 10,
          }}
        >
          <Text
            numberOfLines={1}
            style={[
              {
                fontSize: 14,
                fontFamily: "Lato-Regular",
                marginVertical: 2,
              },
              { color: "black" },
            ]}
          >
            Akte Info
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => toggleExpand()}
          style={{
            paddingTop: 7,
            paddingRight: 7,
            paddingBottom: 7,
            paddingLeft: 20,
            height: "70%",
          }}
        >
          <Image
            source={!isExpand ? expandIcon : hideIcon}
            style={{ height: 20, width: 20, resizeMode: "contain" }}
          />
        </TouchableOpacity>
      </View>
      {isExpand ? <AktenInfoDetail akte={akte} /> : null}
    </View>
  );
}

function AktenInfoDetail(props: { akte: Akte }) {
  const language = useSelector(
    (state: RootState) => state.esy.settings.language
  );

  return (
    <>
      <View
        style={{
          alignSelf: "flex-start",
          margin: 10,
          borderRadius: 10,
          backgroundColor: "white",
          flexDirection: "column",
        }}
      >
        <View>
          <Text
            numberOfLines={1}
            style={[
              {
                fontSize: 14,
                fontFamily: "Lato-Regular",
                marginVertical: 2,
                marginLeft: 10,
              },
              { color: "black" },
            ]}
          >
            {localization.language.CreateOn}
          </Text>
        </View>
        <View>
          <Text
            numberOfLines={1}
            style={[
              {
                fontSize: 14,
                fontFamily: "Lato-Regular",
                marginVertical: 2,
                marginLeft: 10,
              },
              { color: "black" },
            ]}
          >
            {formatDateAndTimeCasesDetail(language, props.akte.creationDate)}
          </Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <View
          style={{
            alignSelf: "flex-start",
            margin: 10,
            borderRadius: 10,
            backgroundColor: "white",
            flexDirection: "column",
          }}
        >
          <View>
            <Text
              numberOfLines={1}
              style={[
                {
                  fontSize: 14,
                  fontFamily: "Lato-Regular",
                  marginVertical: 2,
                  marginLeft: 10,
                },
                { color: "black" },
              ]}
            >
              {localization.language.News}
            </Text>
          </View>
          <View>
            <Text
              numberOfLines={1}
              style={[
                {
                  fontSize: 14,
                  fontFamily: "Lato-Regular",
                  marginVertical: 2,
                  marginLeft: 10,
                },
                { color: "black" },
              ]}
            >
              {localization.language.IntoThisFile}
            </Text>
          </View>
          <View>
            <Text
              numberOfLines={1}
              style={[
                {
                  fontSize: 14,
                  fontFamily: "Lato-Regular",
                  marginVertical: 2,
                  marginLeft: 10,
                },
                { color: "#7A7A7A" },
              ]}
            >
              {props.akte.totalUnreadMessage || 0}
            </Text>
          </View>
        </View>
        <View
          style={{
            alignSelf: "flex-start",
            margin: 10,
            borderRadius: 10,
            backgroundColor: "white",
            flexDirection: "column",
          }}
        >
          <View>
            <Text
              numberOfLines={1}
              style={[
                {
                  fontSize: 14,
                  fontFamily: "Lato-Regular",
                  marginVertical: 2,
                },
                { color: "white" },
              ]}
            >
              Nachrichten
            </Text>
          </View>
          <View>
            <Text
              numberOfLines={1}
              style={[
                {
                  fontSize: 14,
                  fontFamily: "Lato-Regular",
                  marginVertical: 2,
                  marginHorizontal: 20,
                },
                { color: "black" },
              ]}
            >
              {localization.language.InSubFolder}
            </Text>
          </View>
          <View>
            <Text
              numberOfLines={1}
              style={[
                {
                  fontSize: 14,
                  fontFamily: "Lato-Regular",
                  marginVertical: 2,
                  marginHorizontal: 20,
                },
                { color: "#7A7A7A" },
              ]}
            >
              {props.akte.folderCount || 0}
            </Text>
          </View>
        </View>
      </View>
    </>
  );
}
