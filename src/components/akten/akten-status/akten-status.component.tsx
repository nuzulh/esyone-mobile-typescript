import { useState } from "react";
import { Image, Platform, Text, TouchableOpacity, View } from "react-native";

export declare type AktenStatusProps = {
  expandIcon: any;
  hideIcon: any;
};

export function AktenStatus({ expandIcon, hideIcon }: AktenStatusProps) {
  const [isExpand, setExpand] = useState(true);

  return (
    <View
      style={[
        {
          paddingVertical: 10,
          marginTop: 5,
          marginHorizontal: 5,
          borderTopEndRadius: 10,
          borderTopStartRadius: 10,
          borderBottomEndRadius: isExpand ? 0 : 10,
          borderBottomStartRadius: isExpand ? 0 : 10,
          shadowColor: "#000",
          shadowOffset: {
            width: 1,
            height: Platform.OS == "ios" ? 3 : 1,
          },
          shadowOpacity: Platform.OS == "ios" ? 0.1 : 0.4,
          shadowRadius: 3,
          elevation: Platform.OS == "ios" ? 1 : 5,
        },
        { backgroundColor: "#FFFFFF" },
      ]}
    >
      <View
        style={{
          width: "90%",
          alignSelf: "center",
          margin: 10,
          borderRadius: 10,
          backgroundColor: "white",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <View>
          <Text
            numberOfLines={1}
            style={[
              {
                fontSize: 14,
                fontFamily: "Lato-Regular",
                marginVertical: 2,
              },
              { color: "white" },
            ]}
          >
            Status
          </Text>
        </View>
        <TouchableOpacity onPress={() => setExpand(!isExpand)}>
          <Image
            source={!isExpand ? hideIcon : expandIcon}
            style={{ height: 20, width: 20, resizeMode: "contain" }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}
