import { ActivityIndicator, Text, TouchableOpacity, View } from "react-native";
import { styles } from "./akten-list-footer.style";

export declare type AktenListFooterProps = {
  isLoading?: boolean;
  onPress?: () => void;
};

export function AktenListFooter({ isLoading, onPress }: AktenListFooterProps) {
  if (!isLoading) return null;

  return (
    <View style={styles.footer}>
      <TouchableOpacity activeOpacity={0.9} onPress={onPress}>
        <Text style={styles.btnText}>Loading</Text>
        <ActivityIndicator color="black" style={{ marginLeft: 8 }} />
      </TouchableOpacity>
    </View>
  );
}
