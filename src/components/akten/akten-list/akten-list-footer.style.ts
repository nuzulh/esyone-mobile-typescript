import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  footer: {
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  btnText: {
    color: "black",
    fontSize: 15,
    textAlign: "center",
  },
});
