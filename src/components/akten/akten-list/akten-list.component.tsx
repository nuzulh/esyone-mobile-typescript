import { FlatList, View, RefreshControl } from "react-native";
import { useDispatch } from "react-redux";
import { fetchAllAkteAction } from "../../../helpers";
// import { useAkten } from "../../../hooks";
import { Account, Akte } from "../../../models";
import { AktenListItem } from "../akten-list-item";
import { AktenListFooter } from "./akten-list-footer.component";
import { useMemo, useState } from "react";
import { AktenListHeader } from "./akten-list-header.component";

export declare type AktenListProps = {
  ref?: any;
  icon?: any;
  fallback?: any; // fallback component
  placeholder?: any;
  akten: Akte[];
  isInProgress?: boolean;
  onAktePress?: (account: Account, akte: Akte) => void;
  onTaskPress?: (account: Account, akte: Akte) => void;
  onFetchNext?: () => void;
};

export function AktenList({
  ref,
  icon,
  fallback,
  placeholder,
  akten,
  isInProgress,
  onAktePress,
  onTaskPress,
  onFetchNext,
}: AktenListProps) {
  const dispatch = useDispatch();
  const [queryText, setQueryText] = useState(null);
  const selectedAkten = useMemo(
    () =>
      queryText && queryText !== ""
        ? akten.filter(
          ({ header, creatorFirstName, creatorLastName }) =>
            header.toLowerCase().includes(queryText.toLowerCase()) ||
            creatorFirstName.toLowerCase().includes(queryText.toLowerCase()) ||
            creatorLastName.toLowerCase().includes(queryText.toLowerCase())
        )
        : akten,
    [queryText, akten]
  );

  if (!isInProgress && (!akten || akten.length == 0)) return fallback;

  // if (isInProgress) return placeholder;

  return (
    <FlatList
      ref={ref}
      data={selectedAkten}
      refreshControl={
        <RefreshControl
          refreshing={isInProgress}
          onRefresh={() => dispatch(fetchAllAkteAction())}
        />
      }
      showsVerticalScrollIndicator={true}
      // onEndReached={onFetchNext}
      onEndReachedThreshold={0.1}
      keyExtractor={(item) => item.id}
      ListHeaderComponent={
        akten.length > 2
          ? <AktenListHeader onSearch={(query) => setQueryText(query)} />
          : <View style={{ marginTop: 10 }}></View>
      }
      // ListFooterComponent={
      //   <AktenListFooter isLoading={isInProgress} onPress={onFetchNext} />
      // }
      renderItem={({ item }) => (
        <AktenListItem
          icon={icon}
          akte={item}
          onAktePress={onAktePress}
          onTaskPress={onTaskPress}
        />
      )}
    />
  );
}
