import { View } from "react-native";
import { Input } from "../../inputs";
import { localization } from "../../../helpers";

declare type AktenListHeaderProps = {
  onSearch: (query: string) => void;
};

export function AktenListHeader({ onSearch }: AktenListHeaderProps) {
  return (
    <View style={{ margin: 10 }}>
      <Input
        type="text"
        placeholder={localization.language.SearchCases}
        onChange={(text) => onSearch(text)}
        style={{ width: "100%" }}
      />
    </View>
  );
}
