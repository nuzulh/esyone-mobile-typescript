import { useContext } from "react";
import { Image, Platform, Text, TouchableOpacity, View } from "react-native";
import FastImage from "react-native-fast-image";
import { useSelector } from "react-redux";
import { localization, parseInitial } from "../../../helpers";
import { useImage } from "../../../hooks/image";
import { Account, Akte, RootState } from "../../../models";
import { Services } from "../../../services";
import { AktenListItemHighlight, UnreadMessageHighlight } from "./akten-list-item-highlight.component";

export declare type AktenListItemProps = {
  icon?: any;
  akte: Akte;
  onTaskPress: (account: Account, akte: Akte) => void;
  onAktePress: (account: Account, akte: Akte) => void;
};

export function AktenListItem({
  icon,
  akte,
  onTaskPress,
  onAktePress,
}: AktenListItemProps) {
  const services = useContext(Services);
  const accounts = useSelector((state: RootState) => state.esy.auth?.accounts);
  const { imageUri, headers } = useImage(() =>
    services.accountService.getMainLogo(akte.accountId)
  );

  // console.log("akte list item called", imageUri);

  if (!accounts) return null;

  const name =
    (akte.creatorFirstName ? akte.creatorFirstName + " " : "") +
    akte.creatorLastName;
  const initials = parseInitial(
    (akte.creatorFirstName ? akte.creatorFirstName + " " : "") +
    (akte.creatorLastName ? akte.creatorLastName : "")
  );
  const provider = accounts[akte.accountId]; //dataProvider.filter(x => x.id == aktenItem.accountId);

  if (!provider) return null;

  return (
    <>
      {/* {index == 0 && <Text numberOfLines={1} style={{marginVertical: 2,marginVertical:20,marginHorizontal:10,fontSize: 21,color: '#404040',lineHeight: 32,fontWeight: '600',}}>{Language.TabCases}</Text>}  */}
      <View
        style={{
          width: "100%",
          paddingLeft: 5,
          paddingRight: 5,
          marginTop: 0,
          paddingBottom: 5,
          borderRadius: 10,
          marginBottom: 10,
        }}
      >
        <View
          style={[
            {
              margin: 5,
              borderRadius: 10,
              flexDirection: "column",
              shadowColor: "#000",
              shadowOffset: { width: 1, height: Platform.OS == "ios" ? 3 : 1 },
              shadowOpacity: Platform.OS == "ios" ? 0.1 : 0.4,
              shadowRadius: 3,
              elevation: Platform.OS == "ios" ? 1 : 5,
            },
            { backgroundColor: "#FFFFFF", paddingVertical: 20 },
          ]}
        >
          <TouchableOpacity
            onPress={() => onAktePress(provider, akte)}
            style={{ flexDirection: "row" }}
          >
            <View
              style={{
                width: "22%",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  paddingLeft: 7,
                  paddingTop: 7,
                  marginRight: 0,
                  borderRadius: 3,
                  alignItems: "center",
                  justifyContent: "center" /* , backgroundColor: '#0F0' */,
                }}
              >
                <View
                  style={[
                    {
                      height: 48,
                      width: 48,
                      borderRadius: 48,
                      justifyContent: "center",
                      position: "absolute",
                      top: -20,
                    },
                    { backgroundColor: provider.mainColor },
                  ]}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      fontFamily: "Lato-Regular",
                      color: "#ffffff",
                      textAlign: "center",
                    }}
                  >
                    {initials}
                  </Text>
                </View>
                <FastImage
                  source={{ uri: imageUri, headers }}
                  style={{
                    height: 48,
                    width: 48,
                    borderRadius: 48,
                    position: "absolute",
                    top: -20,
                  }}
                  resizeMode={FastImage.resizeMode.contain}
                />
              </View>
            </View>
            <View style={{ width: "60%" }}>
              <View
                style={{ paddingTop: 20, paddingRight: 7, paddingBottom: 7 }}
              >
                <Text
                  numberOfLines={1}
                  style={[
                    {
                      fontSize: 14,
                      fontFamily: "Lato-Bold",
                      marginVertical: 2,
                    },
                    { color: "#404040" },
                  ]}
                >
                  {provider.name}
                </Text>
                <View style={{ flexDirection: "row" }}>
                  {/* <Text
                    numberOfLines={1}
                    style={{
                      fontSize: 12,
                      fontFamily: "Lato-Regular",
                      color: "#7A7A7A",
                      marginVertical: 2,
                    }}
                  >
                    {akte.aktenzeichen && `${akte.aktenzeichen} `}
                  </Text> */}
                  <Text
                    numberOfLines={2}
                    style={[
                      {
                        fontSize: 12,
                        fontFamily: "Lato-Regular",
                        marginVertical: 2,
                        flex: 1,
                        flexWrap: "wrap",
                        color: "#404040",
                      },
                    ]}
                  >
                    {akte.header}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                width: "17%",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image
                source={icon}
                style={{
                  resizeMode: "contain",
                  height: 25,
                  width: 25,
                }}
              ></Image>
            </View>
          </TouchableOpacity>
          <View style={{ marginLeft: 20, marginTop: 10 }}>
            <View style={{ paddingRight: 7, paddingBottom: 7 }}>
              {akte.lastState !== null ? (
                <>
                  <View style={{ flexDirection: "row" }}>
                    <Text
                      numberOfLines={1}
                      style={{
                        fontSize: 12,
                        fontFamily: "Lato-Regular",
                        color: "white",
                        marginVertical: 2,
                      }}
                    >
                      <AktenListItemHighlight status={akte.lastState?.name} />
                    </Text>
                    <Text
                      numberOfLines={1}
                      style={{
                        fontSize: 12,
                        fontFamily: "Lato-Regular",
                        color: "white",
                        marginVertical: 2,
                        marginLeft: 6,
                      }}
                    >
                      <UnreadMessageHighlight totalUnreadMessage={akte.totalUnreadMessage} />
                    </Text>
                  </View>
                  {akte.lastState?.name == "Fall In Bearbeitung" && (
                    <Text
                      numberOfLines={1}
                      style={{
                        fontSize: 12,
                        fontFamily: "Lato-Regular",
                        color: "black",
                        marginVertical: 2,
                      }}
                    >
                      {akte.lastState?.name == "Fall In Bearbeitung" &&
                        akte.lastState?.statusText}
                    </Text>
                  )}
                </>
              ) : (
                <View style={{ flexDirection: "row" }}>
                  
                </View>
              )}
            </View>
          </View>
          {akte.lastState !== null && (
            <>
              {akte.hasOpenAufgabe && (
                <View
                  style={{
                    width: "90%",
                    backgroundColor: akte.hasOpenAufgabe
                      ? "#EB5757"
                      : "#F5F5F5",
                    alignSelf: "center",
                    margin: 10,
                    borderRadius: 10,
                    paddingBottom: 20,
                  }}
                >
                  <View
                    style={{
                      paddingTop: 7,
                      paddingRight: 7,
                      paddingBottom: 7,
                      paddingLeft: 20,
                    }}
                  >
                    <Text
                      numberOfLines={1}
                      style={[
                        {
                          fontSize: 14,
                          fontFamily: "Lato-Regular",
                          marginVertical: 2,
                        },
                        { color: akte.hasOpenAufgabe ? "white" : "black" },
                      ]}
                    >
                      {akte.hasOpenAufgabe
                        ? localization.language.YourHelpIsNeeded
                        : "Status ihres Falles"}
                    </Text>
                    <Text
                      style={{
                        fontSize: 14,
                        fontFamily: "Lato-Regular",
                        color: akte.hasOpenAufgabe ? "white" : "black",
                        marginVertical: 15,
                      }}
                    >
                      {akte.hasOpenAufgabe
                        ? localization.language.InOrdertoBeable
                        : "lhr Fall wird aktuel von uns bearbeitet"}
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => onTaskPress(provider, akte)}
                    style={{
                      backgroundColor: "white",
                      borderRadius: 10,
                      height: 40,
                      width: "90%",
                      alignSelf: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Text
                      numberOfLines={1}
                      style={{
                        fontSize: 12,
                        fontFamily: "Lato-Regular",
                        color: "black",
                        marginVertical: 2,
                        alignSelf: "center",
                      }}
                    >
                      {akte.hasOpenAufgabe
                        ? localization.language.ToYourTask
                        : "Zu ihren Fall"}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
            </>
          )}
        </View>
      </View>
    </>
  );
}
