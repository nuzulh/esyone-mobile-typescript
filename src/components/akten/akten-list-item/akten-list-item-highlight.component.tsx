import { Text } from "react-native";
import { localization } from "../../../helpers";

export declare type AktenListItemHighlightProps = {
  status: string;
};

export function AktenListItemHighlight({
  status,
}: AktenListItemHighlightProps) {
  if (!status) return null;

  return (
    <Text
      style={{
        paddingHorizontal: 10,
        backgroundColor: status == "Fall Abgeschlossen" ? "green" : "#EF7E1B",
      }}
    >
      {status}
    </Text>
  );

  // const tags = parseStatus(status).split(" ");

  // return (
  //   <>
  //     {tags.map((tag, i) => (
  //       <Text key={i}>
  //         <Text
  //           style={{
  //             backgroundColor:
  //               status == "Fall Abgeschlossen" ? "green" : "#EF7E1B",
  //           }}
  //         >
  //           {tag}{" "}
  //         </Text>{" "}
  //       </Text>
  //     ))}
  //   </>
  // );
}

export declare type UnreadMessageHighlightProps = {
  totalUnreadMessage: number;
};

export function UnreadMessageHighlight({
  totalUnreadMessage,
}: UnreadMessageHighlightProps) {
  return (
    <Text
      style={{
        paddingHorizontal: 10,
        backgroundColor: "#F574AB",
      }}
    >
      {totalUnreadMessage > 0 ? localization.language.UnreadMessage : ""}
    </Text>
  );
}

function parseStatus(status) {
  switch (status) {
    case "Fall Gemeldet":
      return localization.language.CaseReported;
    case "Fall Wird Geprüft":
      return localization.language.CaseUnderReview;
    case "Fall In Bearbeitung":
      return localization.language.CaseInProgress;
    case "Fall Abgeschlossen":
      return localization.language.CaseClosed;
    default:
      return "";
  }
}
