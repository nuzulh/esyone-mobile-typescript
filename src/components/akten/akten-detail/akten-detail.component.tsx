import { useContext } from "react";
import {
  ScrollView,
  StyleProp,
  View,
  ViewStyle,
  RefreshControl,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { fetchSingleAkteAction } from "../../../helpers";
import { Account, Akte, RootState } from "../../../models";
import { Services } from "../../../services";
import { ShimmerPlaceholder } from "../../shimmer-placeholder.component";
import { AktenInfo, AktenTimeline } from "../akten-status";
import { AkteHasAufgabeAlert } from "../alerts";
import { AktenDetailCollaboratorInfo } from "./akten-detail-collaborator-info.component";
import { AccountDetailCollaborator } from "./akten-detail-collaborator.component";

export declare type AktenDetailProps = {
  akte: Akte;
  account: Account;
  // fallbackIcon?: any;
  // expandIcon?: any;
  // hideIcon?: any;
  style?: StyleProp<ViewStyle>;
};

export function AktenDetail({
  akte,
  account,
  // fallbackIcon,
  // expandIcon,
  // hideIcon,
  style = {},
}: AktenDetailProps) {
  const services = useContext(Services);
  const dispatch = useDispatch();
  const isLoadingBackground = useSelector(
    (state: RootState) => state.esy.appState.isLoadingBackground
  );

  return (
    <>
      {/* <ShimmerPlaceholder
        visible={true}
        style={{
          width: "100%",
          paddingLeft: 5,
          paddingRight: 5,
          marginTop: 0,
          paddingBottom: 5,
          borderRadius: 10,
          ...(style as any),
        }}
      >
        
      </ShimmerPlaceholder> */}
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={isLoadingBackground}
            onRefresh={() => dispatch(fetchSingleAkteAction(account, akte))}
          />
        }
        style={{
          width: "100%",
          paddingLeft: 5,
          paddingRight: 5,
          marginTop: 0,
          paddingBottom: 5,
          borderRadius: 10,
          ...(style as any),
        }}
      >
        <View
          style={[
            {
              margin: 5,
              borderRadius: 10,
              flexDirection: "column",
            },
          ]}
        >
          <AccountDetailCollaborator
            akte={akte}
            account={account}
            fallbackIcon={services.assetsService.getIcons("anonymous")}
          />
        </View>
        <AkteHasAufgabeAlert akte={akte} />
        <AktenTimeline
          akte={akte}
          expandIcon={services.assetsService.getIcons("iconUp")}
          hideIcon={services.assetsService.getIcons("iconDown")}
        />
        <AktenInfo
          akte={akte}
          expandIcon={services.assetsService.getIcons("iconUp")}
          hideIcon={services.assetsService.getIcons("iconDown")}
        />
      </ScrollView>
    </>
  );
}
