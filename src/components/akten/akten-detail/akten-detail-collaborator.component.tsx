import { useContext, useState } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { useSelector } from "react-redux";
import { localization } from "../../../helpers";
import { usePromise } from "../../../hooks";
import { Account, Akte, RootState } from "../../../models";
import { Services } from "../../../services";
import { AccountImage } from "../../account";
import { CircleFrame } from "../../frame";
import { CircleImage } from "../../image";
import { AktenDetailCollaboratorInfo } from "./akten-detail-collaborator-info.component";

export declare type AccountDetailCollaboratorProps = {
  akte: Akte;
  account: Account;
  fallbackIcon?: any;
  onAccountPress?: () => void;
  onCollaboratorPress?: () => void;
};

export function AccountDetailCollaborator({
  akte,
  account,
  fallbackIcon,
  onAccountPress,
  onCollaboratorPress,
}: AccountDetailCollaboratorProps) {
  const [isInfoVisible, setInfoVisible] = useState(false);
  const assignees = useSelector((state: RootState) =>
    state.esy.akten.akten[akte.id]
      ? state.esy.akten.akten[akte.id].assignees
      : []
  );
  // const services = useContext(Services);
  // const { result: assignees } = usePromise(
  //   () =>
  //     services.aktenService
  //       .getOne(akte.accountId, akte.id)
  //       .then((res) => res.assignees),
  //   [],
  //   [akte]
  // );

  return (
    <>
      <AktenDetailCollaboratorInfo
        assignees={assignees}
        fallbackIcon={fallbackIcon}
        isVisible={isInfoVisible}
        onClose={() => setInfoVisible(false)}
      />
      <View style={{ flexDirection: "row" }}>
        <View style={{ width: "50%", paddingVertical: 10 }}>
          <Text
            style={{
              fontSize: 14,
              fontFamily: "Lato-Bold",
              color: "black",
              textAlign: "left",
              paddingLeft: 7,
            }}
          >
            {localization.language.CasesDetailAccount}
          </Text>
          <TouchableOpacity onPress={onAccountPress}>
            <AccountImage account={account} />
          </TouchableOpacity>
        </View>
        <View style={{ width: "50%", paddingVertical: 10 }}>
          <Text
            style={{
              fontSize: 14,
              fontFamily: "Lato-Bold",
              color: "black",
              textAlign: "left",
              paddingLeft: 7,
            }}
          >
            {localization.language.CasesDetailEditor}
          </Text>
          <TouchableOpacity
            onPress={() => setInfoVisible(true)}
            style={{
              paddingLeft: 7,
              paddingTop: 7,
              marginRight: 0,
              borderRadius: 3,
              alignItems: "center" /* \, backgroundColor: '#0F0' */,
              flexDirection: "row",
            }}
          >
            {assignees.slice(0, 3).map((item, i) => (
              <CircleFrame
                key={i}
                style={{
                  zIndex: assignees.length - i,
                  marginLeft: i == 0 ? 0 : i == 1 ? -10 : -20,
                }}
              >
                <CircleImage
                  source={
                    item.profilePictureUrl
                      ? { uri: item.profilePictureUrl }
                      : fallbackIcon
                  }
                />
              </CircleFrame>
            ))}
            <CircleFrame
              style={{ zIndex: 0, marginLeft: -10 }}
              isVisible={assignees.length >= 4}
            >
              <CircleFrame
                size={48}
                style={{
                  backgroundColor: account.mainColor,
                  flexDirection: "row",
                }}
              >
                <Text style={{ color: "white", fontSize: 20 }}>
                  {assignees.length - 3}
                </Text>
                <Text style={{ color: "white", fontSize: 20 }}>+</Text>
              </CircleFrame>
            </CircleFrame>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}
