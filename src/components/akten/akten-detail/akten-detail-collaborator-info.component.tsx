import { Modal, Text, TouchableOpacity, View } from "react-native";
import { AkteAssignee } from "../../../models";
import { AssigneeImage } from "../assignee-image";

export declare type AktenDetailCollaboratorInfoProps = {
  assignees: AkteAssignee[];
  isVisible?: boolean;
  fallbackIcon?: any;
  onClose?: () => void;
};

export function AktenDetailCollaboratorInfo({
  assignees,
  isVisible,
  fallbackIcon,
  onClose,
}: AktenDetailCollaboratorInfoProps) {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={isVisible}
      onRequestClose={onClose}
    >
      <TouchableOpacity
        onPress={onClose}
        style={{
          backgroundColor: "rgba(52, 52, 52, 0.8)",
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <View
          style={{
            backgroundColor: "white",
            alignSelf: "center",
            width: "90%",
            borderRadius: 20,
            paddingVertical: 20,
          }}
        >
          <Text
            style={{
              textAlign: "left",
              fontSize: 20,
              marginVertical: 10,
              marginHorizontal: 10,
              fontWeight: "bold",
              marginLeft: 20,
            }}
          >
            Diese Akte wird geteilt mit
          </Text>
          {assignees.map((assignee, i) => (
            <AssigneeImage
              assignee={assignee}
              key={i}
              fallback={fallbackIcon}
            />
          ))}
        </View>
      </TouchableOpacity>
    </Modal>
  );
}
