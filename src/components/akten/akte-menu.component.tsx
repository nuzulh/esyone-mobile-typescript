import { IconProp } from "@fortawesome/fontawesome-svg-core";
import {
  faArrowLeft,
  faDownload,
  faStar,
  faStarHalfAlt,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useContext } from "react";
import { TouchableOpacity, View } from "react-native";
import { useDispatch } from "react-redux";
import {
  downloadAttachmentAction,
  downloadAttachmentFromMessage,
  toggleFavoriteMessage,
} from "../../helpers";
import { AkteContext } from "./akte.context";

export function AkteMenu() {
  const akteContext = useContext(AkteContext);
  const dispatch = useDispatch();

  const handleDownloadFile = () => {
    dispatch(downloadAttachmentAction(akteContext.selectedFile));
    akteContext.reset();
  };

  const handleDownloadAttachment = () => {
    dispatch(
      downloadAttachmentFromMessage(
        akteContext.akte,
        akteContext.selectedMessage
      )
    );
    akteContext.reset();
  };

  const handleMarkFavorite = () => {
    dispatch(
      toggleFavoriteMessage(akteContext.akte, akteContext.selectedMessage)
    );
    akteContext.reset();
  };

  return (
    <View
      style={{
        padding: 10,
        height: 60,
        backgroundColor: "#404040",
        flexDirection: "row",
      }}
    >
      <TouchableOpacity
        style={{ width: "65%", justifyContent: "center", marginLeft: 15 }}
        onPress={() => akteContext.reset()}
      >
        <FontAwesomeIcon icon={faArrowLeft} color={"#FFFFFF"} size={18} />
      </TouchableOpacity>
      <View
        style={{
          width: "30%",
          flexDirection: "row",
          justifyContent: "flex-end",
        }}
      >
        <AkteMenuItem
          isVisible={!!akteContext.selectedMessage}
          icon={
            akteContext.selectedMessage
              ? akteContext.selectedMessage.isFavorite
                ? faStarHalfAlt
                : faStar
              : faStar
          }
          onClick={handleMarkFavorite}
        />
        <AkteMenuItem
          isVisible={
            akteContext.selectedMessage
              ? akteContext.selectedMessage.attachments &&
                akteContext.selectedMessage.attachments.length > 0
                ? true
                : false
              : false
          }
          icon={faDownload}
          onClick={handleDownloadAttachment}
        />
        <AkteMenuItem
          isVisible={!!akteContext.selectedFile}
          icon={faDownload}
          onClick={handleDownloadFile}
        />
      </View>
    </View>
  );
}

function AkteMenuItem({
  isVisible,
  icon,
  onClick,
}: {
  isVisible: boolean;
  icon: IconProp;
  onClick: () => void;
}) {
  if (!isVisible) return null;

  return (
    <TouchableOpacity
      style={{
        width: "30%",
        marginHorizontal: 5,
        justifyContent: "center",
        alignItems: "center",
      }}
      onPress={onClick}
    >
      <FontAwesomeIcon icon={icon} size={20} color="#ffffff" />
    </TouchableOpacity>
  );
}
