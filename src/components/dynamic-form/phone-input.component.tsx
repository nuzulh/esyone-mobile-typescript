import React, { useContext, useMemo, useState } from "react";
import { Controller } from "react-hook-form";
import { StyleSheet, TextInput, View } from "react-native";
import CountryPicker from "rn-country-picker";
import { parseMobileNumber, validatePhoneNumber } from "../../helpers";
import { Services } from "../../services";

declare type PhoneInputProps = {
  control: any;
  name: string;
  defaultValue?: any;
  rules?: any;
};

export default function PhoneInput({
  control,
  name,
  defaultValue,
  rules,
}: PhoneInputProps) {
  const services = useContext(Services);
  const [isFocused, setFocus] = useState(false);
  const { defaultCode, defaultPhone } = useMemo(() => {
    if (!defaultValue) return {};

    const code = defaultValue?.split(" ")[0]?.replace("+", "");
    const phone = defaultValue
      ?.replace("+", "")
      ?.replace(code, "")
      ?.replace(" ", "");
    return { defaultCode: code, defaultPhone: phone };
  }, []);
  const [code, setCode] = useState(defaultCode || "49");
  const [phone, setPhone] = useState(defaultPhone || "");

  // console.log(code, phone);

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={{
        validate: {
          isPhone: (value) =>
            validatePhoneNumber(value)
              ? true
              : "Bitte geben Sie eine gültige Mobilnummer an",
        },
        ...rules,
      }}
      render={({ field: { onChange, onBlur, value } }) => (
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginTop: 10,
            marginBottom: 0,
            padding: 0,
            borderRadius: 5,
            backgroundColor: "#ffffff",
            borderWidth: isFocused ? 1.5 : 1,
            borderColor: isFocused ? "#404040" : "#ADADAD",
          }}
        >
          <CountryPicker
            disable={false}
            animationType={"slide"}
            containerStyle={styles.pickerStyle}
            pickerTitleStyle={styles.pickerTitleStyle}
            dropDownImage={services.assetsService.getIcons("iconUp")}
            selectedCountryTextStyle={styles.selectedCountryTextStyle}
            countryNameTextStyle={styles.countryNameTextStyle}
            pickerTitle={"Country Picker"}
            searchBarPlaceHolder={"Search......"}
            hideCountryFlag={false}
            hideCountryCode={true}
            searchBarStyle={styles.searchBarStyle}
            countryCode={code}
            selectedValue={(value) => {
              setCode(value);
              onChange(`+${value} ${parseMobileNumber(phone)}`);
            }}
          />
          <TextInput
            style={{ flex: 1, padding: 12 }}
            onChangeText={(text) => {
              setPhone(text);
              onChange(`+${code} ${parseMobileNumber(text)}`);
            }}
            value={phone}
            secureTextEntry={false}
            keyboardType="phone-pad"
            onFocus={() => setFocus(true)}
            onBlur={() => {
              onBlur();
              setFocus(false);
            }}
            placeholder={"15123457879"}
            placeholderTextColor="#ADADAD"
          />
        </View>
      )}
    />
  );
}

const styles = StyleSheet.create({
  pickerTitleStyle: {
    justifyContent: "center",
    flexDirection: "row",
    alignSelf: "center",
    fontWeight: "bold",
    flex: 1,
    marginLeft: 10,
    fontSize: 16,
    color: "#000",
  },
  pickerStyle: {
    width: "20%",
    justifyContent: "center",
    borderColor: "#303030",
    backgroundColor: "white",
  },
  selectedCountryTextStyle: {
    paddingLeft: 5,
    paddingRight: 5,
    color: "#000",
    textAlign: "right",
  },

  countryNameTextStyle: {
    paddingLeft: 10,
    color: "#000",
    textAlign: "right",
  },

  searchBarStyle: {
    flex: 1,
    justifyContent: "center",
    flexDirection: "row",
    marginLeft: 8,
    marginRight: 10,
  },
});
