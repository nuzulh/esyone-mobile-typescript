import { Controller } from "react-hook-form";
import { Text, View } from "react-native";

// declare type RadioInputProps = {
//   control: any;
//   name: string;
//   defaultValue: any;
//   rules?: any;
// };

export function RadioInput() {
  return (
    <View
      style={{
        backgroundColor: "#ffffff",
        paddingLeft: 24,
        paddingRight: 24,
      }}
    >
      <Text
        numberOfLines={1}
        style={[
          {
            fontSize: 14,
            fontFamily: "Lato-Bold",
            marginTop: 16,
            marginBottom: 12,
          },
          { color: "#404040" },
        ]}
      >
        unsupported form element type 'radio'
      </Text>
    </View>
  );
}
