import { TouchableOpacity, View } from "react-native";
import { Insurance, WorkflowFormElement, WorkflowFormular } from "../../models";
import Label from "./label.component";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faPlus, faTrash } from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";
import { FormElement } from ".";
import { Control } from "react-hook-form";
import { FileUploadInfo } from "../../services";

declare type RepeaterElementProps = {
  control: Control;
  element: WorkflowFormElement;
  isProtected: boolean;
  insurances: Insurance[];
  uploadInfo?: FileUploadInfo;
  workflowFormular?: WorkflowFormular;
};

export default function RepeaterElement({
  control,
  element,
  isProtected,
  insurances,
  uploadInfo,
  workflowFormular,
}: RepeaterElementProps) {
  const [currentSize, setCurrentSize] = useState<number>(
    workflowFormular?.fieldValues
      ? Object.keys(workflowFormular.fieldValues).filter(
        (key) => key.startsWith("isVisible_repeater")
      ).length
      : 1
  );

  const addRepeaterForm = () => setCurrentSize((prev) => prev + 1);

  const removeRepeaterForm = (
    newElement: WorkflowFormElement,
    repeaterIdx: number,
  ) => {
    if (currentSize > 1) {
      control.unregister(`isVisible_repeater_${repeaterIdx}_${element.name}`);
      newElement.formElements.forEach((el) => {
        if (el.name.startsWith(`repeater_${repeaterIdx}`))
          control.unregister(el.name);
      });
      setCurrentSize((prev) => prev - 1);
    }
  };

  const recreateFormElement = (form: WorkflowFormElement, index: number) => ({
    ...form,
    name: form.name.startsWith("file")
      ? `${form.name}_repeater_${index}`
      : `repeater_${index}_${form.name}`,
    value: form.name.startsWith("file")
      ? `${form.name}_repeater_${index}`
      : `repeater_${index}_${form.name}`,
  });

  return (
    <View
      style={{
        borderWidth: 0.5,
        borderColor: "#ADADAD",
        borderRadius: 5,
        paddingHorizontal: 12,
        marginTop: 10,
      }}
    >
      {Array.from(Array(currentSize).keys()).map((repeaterIdx) => (
        element.formElements.map((el, index) => {
          let newElement = {} as WorkflowFormElement;

          Object.keys(el).forEach((key) => {
            newElement[key] = el[key];
            if (el.formElements.length > 0)
              newElement["formElements"] = el.formElements.map(
                (prev) => recreateFormElement(prev, repeaterIdx)
              );
            else newElement = recreateFormElement(el, repeaterIdx);

            control.register(`isVisible_repeater_${repeaterIdx}_${element.name}`, {
              value: true,
            });
          });

          // console.log(JSON.stringify(newElement, null, 2));

          return (
            <View
              style={{
                borderBottomWidth: 0.5,
                borderColor: "#ADADAD",
                paddingBottom: 12,
              }}
              key={index}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Label style={{ paddingRight: 12 }}>
                  {el.text} {repeaterIdx + 1}
                </Label>
                {currentSize > 1 && currentSize === (repeaterIdx + 1) ? (
                  <TouchableOpacity
                    style={{
                      paddingVertical: 8,
                      paddingHorizontal: 12,
                      marginTop: 12,
                    }}
                    onPress={() => removeRepeaterForm(newElement, repeaterIdx)}
                  >
                    <FontAwesomeIcon icon={faTrash} size={14} />
                  </TouchableOpacity>
                ) : null}
              </View>
              <FormElement
                element={newElement}
                isProtected={isProtected}
                insurances={insurances}
                uploadInfo={uploadInfo}
              />
            </View>
          );
        })
      ))}
      {currentSize < element.size ? (
        <TouchableOpacity
          style={{
            alignItems: "center",
            justifyContent: "center",
            paddingVertical: 16,
            width: "100%",
          }}
          onPress={addRepeaterForm}
        >
          <FontAwesomeIcon icon={faPlus} size={12} />
        </TouchableOpacity>
      ) : null}
    </View>
  );
}
