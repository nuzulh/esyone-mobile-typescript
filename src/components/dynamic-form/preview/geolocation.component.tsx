import { Text, View } from "react-native";
import { styles } from "./preview.style";

export function GeolocationPreview({
  label,
  latitude,
  longitude,
}: {
  label: string;
  latitude: number;
  longitude: number;
}) {
  return (
    <View style={styles.wrapItem}>
      <Text numberOfLines={1} style={styles.itemTitle}>
        {label}
      </Text>
      <Text style={styles.itemDesc}>
        lat: {latitude} lng: {longitude}
      </Text>
    </View>
  );
}
