import { Text, View } from "react-native";
import { styles } from "./preview.style";

export function DisplayPreview({ label }: { label: string }) {
  return (
    <View style={styles.wrapItem}>
      <Text style={styles.itemDesc}>{label}</Text>
    </View>
  );
}
