import { Text, View } from "react-native";
import { WorkflowAnswer, WorkflowFormElementAnswer } from "../../../models";
import { CheckboxPreview } from "./checkbox-preview.component";
import { DatePreview } from "./date-preview.component";
import { DisplayPreview } from "./display-preview.component";
import { FilePreview } from "./file-preview.component";
import { HeadlinePreview } from "./headline-preview.component";
import { InputPreview } from "./input-preview.component";
import { styles } from "./preview.style";
import { RatingPreview } from "./rating-preview.component";
import { SignaturePreview } from "./signature-preview.component";
import { TimePreview } from "./time-preview.component";

// export * from "./checkbox-preview.component";
// export * from "./date-preview.component";
// export * from "./file-preview.component";
// export * from "./geolocation.component";
// export * from "./input-preview.component";
// export * from "./preview.style";
// export * from "./rating-preview.component";
// export * from "./signature-preview.component";
// export * from "./time-preview.component";

declare type DynamicFormPreviewProps = {
  answer: WorkflowAnswer;
};

export function DynamicFormPreview({ answer }: DynamicFormPreviewProps) {
  // console.log(answer);

  // return null;
  return (
    <>
      {answer.formValues.map((element, i) => (
        <FormElementPreview key={i} element={element} />
      ))}
    </>
  );
}

function FormElementPreview({
  element,
}: {
  element: WorkflowFormElementAnswer;
}) {
  switch (element.type) {
    case "headline":
      return <HeadlinePreview label={element.label} />;
    case "display":
      return <DisplayPreview label={element.label} />;
    case "input":
      return <InputPreview label={element.label} value={element.value} />;
    case "date":
      return <DatePreview label={element.label} value={element.value} />;
    case "time":
      return <TimePreview label={element.label} value={element.value} />;
    case "number":
      return <InputPreview label={element.label} value={element.value} />;
    case "mobilenumber":
      return <InputPreview label={element.label} value={element.value} />;
    case "geolocation":
      return null;
    case "rating":
      return <RatingPreview label={element.label} value={element.value} />;
    case "textarea":
      return <InputPreview label={element.label} value={element.value} />;
    case "file":
      return <FilePreview label={element.label} files={element.value} />;
    case "signature":
      return <SignaturePreview label={element.label} files={element.value} />;
    case "simplechoice":
      return <InputPreview label={element.label} value={element.value} />;
    case "checkbox":
      return <CheckboxPreview label={element.label} value={element.value} />;
    case "multiplechoice":
      return <InputPreview label={element.label} value={element.value} />;
    case "inputregex":
      return <InputPreview label={element.label} value={element.value} />;
    default:
      return (
        <View style={styles.wrapItem}>
          <Text style={styles.itemTitle}>{element.label}</Text>
          <Text style={styles.itemDesc}>{JSON.stringify(element.value)}</Text>
        </View>
      );
  }
}
