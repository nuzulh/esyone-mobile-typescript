import { useContext } from "react";
import { Image, Text, View } from "react-native";
import { AccountContext } from "../../../context";
import { useFileReader, usePromise } from "../../../hooks";
import { MetaFile, Services, VirtualMetaFile } from "../../../services";
import { UnknownError } from "../../errors";
import { styles } from "./preview.style";

export function SignaturePreview({
  label,
  files,
}: {
  label: string;
  files: MetaFile[];
}) {
  const { account } = useContext(AccountContext);
  const services = useContext(Services);
  const fetchResult = usePromise<VirtualMetaFile[]>(
    () =>
      files
        ? Promise.all(
            files.map((file) =>
              services.filesService
                .downloadFile(
                  account.id,
                  typeof file.id === "string" ? parseInt(file.id) : file.id
                )
                .then((metaFile) =>
                  services.filesService.readFile(metaFile, "base64")
                )
            )
          )
        : Promise.resolve([]),
    [] as VirtualMetaFile[],
    [files]
  );

  // console.log("SignaturePreview", fetchResult);

  if (fetchResult.error) return <UnknownError />;

  return (
    <View style={styles.wrapItem}>
      <Text numberOfLines={1} style={styles.itemTitle}>
        {label}
      </Text>
      {fetchResult.result.map((file, index) => (
        <View
          key={index}
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "flex-start",
            marginBottom: 10,
          }}
        >
          <Image
            source={{
              uri: `data:image/png;base64,${file.content}`,
            }}
            style={{
              height: 100,
              width: 100,
              resizeMode: "contain",
            }}
          />
        </View>
      ))}
    </View>
  );
}
