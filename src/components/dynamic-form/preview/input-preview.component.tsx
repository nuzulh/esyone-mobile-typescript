import { Text, View } from "react-native";
import { styles } from "./preview.style";

export function InputPreview({
  label,
  value,
}: {
  label: string;
  value: string;
}) {
  return (
    <View style={{ marginHorizontal: 16, marginBottom: 10 }}>
      <Text style={styles.itemTitle}>{label}</Text>
      <Text style={styles.itemDesc}>{value}</Text>
    </View>
  );
}
