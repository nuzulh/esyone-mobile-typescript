import { Text, View } from "react-native";
import { styles } from "./preview.style";

export function CheckboxPreview({
  label,
  value,
}: {
  label: string;
  value: boolean;
}) {
  return (
    <View style={styles.wrapItem}>
      <Text numberOfLines={1} style={styles.itemTitle}>
        {label}
      </Text>
      <Text style={styles.itemDesc}>{value ? "Ja" : "Nein"}</Text>
    </View>
  );
}
