import { useContext } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { useDispatch } from "react-redux";
import { AccountContext } from "../../../context";
import {
  createGetFileIcon,
  HOME_STACK,
  navigateToAction,
  previewAttachmentAction,
  previewFileAction,
} from "../../../helpers";
import { MetaFile, Services } from "../../../services";
import { DocumentListItem } from "../../document-list-item.component";
import { styles } from "./preview.style";

export function FilePreview({
  label,
  files,
}: {
  label?: string;
  files: MetaFile[];
}) {
  const { account } = useContext(AccountContext);
  const dispatch = useDispatch();

  if (!files || files.length === 0) return null;
  if (!account) return null;

  return (
    <View style={styles.wrapItem}>
      {label ? (
        <Text numberOfLines={1} style={styles.itemTitle}>
          {label}
        </Text>
      ) : null}
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          flexWrap: "wrap",
          marginTop: 10,
        }}
      >
        {files.map((file, index) => (
          <DocumentListItem
            style={{}}
            key={index}
            file={file}
            onPress={(file) => dispatch(previewFileAction(account, file))}
          />
        ))}
      </View>
    </View>
  );
}
