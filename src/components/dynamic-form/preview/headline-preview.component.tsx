import { Text, View } from "react-native";
import { styles } from "./preview.style";

export function HeadlinePreview({ label }: { label: string }) {
  return (
    <View style={styles.wrapItem}>
      <Text style={styles.itemTitle}>{label}</Text>
    </View>
  );
}
