import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  wrapItem: {
    marginHorizontal: 16,
    marginBottom: 10,
  },
  itemTitle: {
    color: "#404040",
    fontSize: 17,
    fontWeight: "700",
    lineHeight: 28,
    fontFamily: "Lato-Regular",
    // textTransform: "capitalize",
  },
  itemDesc: {
    color: "#404040",
    fontSize: 17,
    fontWeight: "400",
    lineHeight: 28,
    fontFamily: "Lato-Regular",
  },
});
