import { useContext, useMemo } from "react";
import { Services } from "../../../services";

export function useRatingPreview() {
  const services = useContext(Services);
  const dataRating = useMemo(
    () => [
      {
        key: "positiv",
        name: "Excellent",
        status: false,
        icon: services.assetsService.getIcons("ratingExcellent"),
      },
      {
        key: "neutral",
        name: "Average",
        status: false,
        icon: services.assetsService.getIcons("ratingAverage"),
      },
      {
        key: "negativ",
        name: "Poor",
        status: false,
        icon: services.assetsService.getIcons("ratingPoor"),
      },
    ],
    []
  );

  const getRating = (answer: string) =>
    dataRating.find((item) => item.key === answer);

  return { getRating };
}
