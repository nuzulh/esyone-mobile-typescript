import moment from "moment";
import { Text, View } from "react-native";
import { useSelector } from "react-redux";
import { useEsyState } from "../../../hooks";
import { RootState } from "../../../models";
import { styles } from "./preview.style";

export function DatePreview({ label, value }: { label: string; value: Date }) {
  const language = useEsyState((state) => state.settings.language);

  return (
    <View style={styles.wrapItem}>
      <Text numberOfLines={1} style={styles.itemTitle}>
        {label}
      </Text>
      <Text style={styles.itemDesc}>
        {moment(value).format("dddd, D. MMMM YYYY")}
      </Text>
    </View>
  );
}
