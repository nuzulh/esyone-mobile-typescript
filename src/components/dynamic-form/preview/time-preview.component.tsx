import moment from "moment";
import { Text, View } from "react-native";
import { styles } from "./preview.style";

export function TimePreview({ label, value }: { label: string; value: Date }) {
  return (
    <View style={styles.wrapItem}>
      <Text numberOfLines={1} style={styles.itemTitle}>
        {label}
      </Text>
      <Text style={styles.itemDesc}>{moment(value).format("HH:mm")}</Text>
    </View>
  );
}
