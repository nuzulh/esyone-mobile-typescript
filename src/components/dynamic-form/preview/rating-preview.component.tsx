import { useMemo } from "react";
import { Image, Text, View } from "react-native";
import { styles } from "./preview.style";
import { useRatingPreview } from "./rating-preview.hook";

export function RatingPreview({
  label,
  value,
}: {
  label: string;
  value: string;
}) {
  const { getRating } = useRatingPreview();
  const rating = useMemo(() => getRating(value), [value]);

  if (!rating)
    return (
      <View style={styles.wrapItem}>
        <Text numberOfLines={1} style={styles.itemTitle}>
          {label}
        </Text>
        <Text style={styles.itemDesc}>Not Available ({value})</Text>
      </View>
    );

  return (
    <View style={styles.wrapItem}>
      <Text numberOfLines={1} style={styles.itemTitle}>
        {label}
      </Text>
      <View
        style={{
          height: 100,
          flexDirection: "row",
          alignItems: "center",
          marginTop: 10,
          marginBottom: 10,
          justifyContent: "space-between",
        }}
      >
        <View
          style={{
            borderWidth: 1,
            borderColor: "#e0e0e0",
            borderRadius: 5,
            justifyContent: "center",
            alignItems: "center",
            paddingVertical: 12,
          }}
        >
          <Image source={rating.icon} style={{ width: 48, height: 48 }} />
          <Text
            style={{
              fontSize: 14,
              fontFamily: "Lato-Regular",
              fontWeight: "400",
              lineHeight: 20,
              marginTop: 10,
            }}
          >
            {rating.name}
          </Text>
        </View>
      </View>
    </View>
  );
}
