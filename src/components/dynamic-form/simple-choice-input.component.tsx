import { Controller } from "react-hook-form";
import { SegmentedControls } from "react-native-radio-buttons";

declare type Option = {
  label: string;
  value: number;
};

declare type SimpleChoiceInputProps = {
  options: Option[];
  control: any;
  name: string;
  defaultValue?: any;
  rules?: any;
};

export function SimpleChoiceInput({
  options = [
    { label: "Nein", value: 0 },
    { label: "Ja", value: 1 },
  ],
  control,
  name,
  defaultValue,
  rules,
}: SimpleChoiceInputProps) {
  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={rules}
      render={({ field: { onChange, value } }) => (
        <SegmentedControls
          containerStyle={{ marginTop: 10 }}
          tint={"#404040"}
          backTint={"#ffffff"}
          options={options}
          onSelection={(selectedOption: Option) =>
            onChange(selectedOption.value)
          }
          selectedOption={options.find((option) => option.value === value)}
          extractText={(option: Option) => option.label}
          // renderContainer={RadioButtons.renderVerticalContainer}
        />
      )}
    />
  );
}
