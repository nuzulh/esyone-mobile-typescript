import React from "react";
import { Controller } from "react-hook-form";
import { Platform, TextInput, View } from "react-native";

declare type TextAreaProps = {
  control: any;
  name: string;
  defaultValue?: any;
  rules?: any;
  placeholder?: string;
};

export default function TextAreaInput({
  control,
  name,
  defaultValue,
  rules,
  placeholder,
}: TextAreaProps) {
  const [isFocused, setFocus] = React.useState(false);

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={rules}
      render={({ field: { onChange, onBlur, value } }) => (
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginTop: 10,
            marginBottom: 0,
            padding: 0,
            borderRadius: 5,
            backgroundColor: "#ffffff",
            borderWidth: isFocused ? 1.5 : 1,
            borderColor: isFocused ? "#404040" : "#ADADAD",
          }}
        >
          <TextInput
            style={[
              Platform.OS === "ios" ? {
                paddingTop: 20,
                paddingBottom: 20,
                paddingHorizontal: 20,
                height: 100,
                flex: 1,
              } : {
                maxHeight: 120,
                padding: 12,
                flex: 1
              },
            ]}
            multiline={true}
            numberOfLines={4}
            onChangeText={(text) => onChange(text)}
            value={value}
            secureTextEntry={false}
            // autoCompleteType="password"
            onFocus={() => setFocus(true)}
            onBlur={() => {
              onBlur();
              setFocus(false);
            }}
            placeholder={placeholder}
            placeholderTextColor="#ADADAD"
          />
        </View>
      )}
    />
  );
}
