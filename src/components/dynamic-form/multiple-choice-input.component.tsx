import { useContext } from "react";
import { Controller } from "react-hook-form";
import { Image, Text, View } from "react-native";
import SelectDropdown from "react-native-select-dropdown";
import { Services } from "../../services";

declare type MultipleChoiceInputProps = {
  data: MultipleChoiceItem[];
  control: any;
  name: string;
  defaultValue?: any;
  rules?: any;
};

export declare type MultipleChoiceItem = {
  label: string;
  value: string;
};

export function MultipleChoiceInput({
  data = [],
  control,
  name,
  defaultValue,
  rules,
}: MultipleChoiceInputProps) {
  const services = useContext(Services);

  if (!data || data.length === 0)
    return (
      <View
        style={{
          flexDirection: "column",
          alignItems: "center",
          marginTop: 10,
          marginBottom: 0,
          borderWidth: 1,
          borderRadius: 10,
        }}
      >
        <Text
          numberOfLines={0}
          style={[
            { fontSize: 14, fontFamily: "Lato-Regular", padding: 10 },
            { color: "#404040" },
          ]}
        >
          No data for select options
        </Text>
      </View>
    );

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={rules}
      render={({ field: { onChange } }) => (
        <SelectDropdown
          data={data}
          buttonStyle={{
            width: "100%",
            height: 50,
            backgroundColor: "#FFF",
            borderRadius: 8,
            borderWidth: 1,
            borderColor: "#444",
            marginTop: 10,
            marginBottom: 0,
          }}
          defaultButtonText={defaultValue || "Bitte auswählen"}
          renderDropdownIcon={(isOpened) => {
            return (
              <Image
                source={
                  isOpened
                    ? services.assetsService.getIcons("iconDown")
                    : services.assetsService.getIcons("iconUp")
                }
                style={{ height: 18, width: 18 }}
              />
            );
          }}
          dropdownIconPosition={"right"}
          dropdownStyle={{ backgroundColor: "#EFEFEF" }}
          rowTextStyle={{ color: "#444", textAlign: "left", fontSize: 14 }}
          buttonTextStyle={{
            color: "#444",
            textAlign: "left",
            fontSize: 14,
          }}
          onSelect={(item) => onChange(item.value)}
          buttonTextAfterSelection={(item) => {
            return item.label;
          }}
          rowTextForSelection={(item) => {
            return item.label;
          }}
        />
      )}
    />
  );
}
