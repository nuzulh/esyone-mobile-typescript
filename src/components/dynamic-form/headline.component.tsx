import React from "react";
import { Text, View } from "react-native";

declare type HeadlineProps = {
  children: any;
};

export default function Headline({ children }: HeadlineProps) {
  return (
    <Text
      numberOfLines={0}
      style={[
        { fontSize: 22, fontFamily: "Lato-Bold", marginTop: 16 },
        { color: "#404040" },
      ]}
    >
      {children}
    </Text>
  );
}
