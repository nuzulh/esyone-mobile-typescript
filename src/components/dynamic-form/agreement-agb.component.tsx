import CheckBox from "@react-native-community/checkbox";
import { Controller } from "react-hook-form";
import { Platform, Text, View } from "react-native";

declare type AgreementAGBProps = {
  control: any;
  name: string;
  defaultValue?: any;
  rules?: any;
  visible?: boolean;
};

export function AgreementAGB({
  control,
  name,
  defaultValue,
  rules,
  visible = true,
}: AgreementAGBProps) {
  if (!visible) return null;

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={rules}
      render={({ field: { onChange, value } }) => (
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            alignItems: "flex-start",
            justifyContent: "space-between",
            marginBottom: 12,
          }}
        >
          <CheckBox
            disabled={false}
            onValueChange={(value) => onChange(value)}
            value={value}
            style={{ left: 0, marginRight: Platform.OS === "ios" ? 10 : 0 }}
          />
          <Text
            style={{
              color: "#404040",
              fontWeight: "400",
              fontSize: 14,
              lineHeight: 20,
              fontFamily: "Lato-Regular",
              textAlign: "justify",
              flexWrap: "wrap",
              flex: 1,
              top: 3,
            }}
          >
            Die Nutzungbedingungen für das vorliegende Angebot habe ich
            verstanden und bin damit einverstanden.
          </Text>
        </View>
      )}
    />
  );
}
