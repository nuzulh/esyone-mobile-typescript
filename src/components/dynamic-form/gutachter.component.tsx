import { Control, useForm } from "react-hook-form";
import { Gutachter, WorkflowFormElement } from "../../models";
import { IconButton } from "../buttons";
import { View } from "react-native";
import { MultipleChoiceInput } from "./multiple-choice-input.component";
import { useContext, useState } from "react";
import { Services } from "../../services";
import { useEsyState } from "../../hooks";
import { useDispatch } from "react-redux";
import { hideLoadingAction, showLoadingAction } from "../../helpers";
import { Form } from "../form";

declare type GutachterInputProps = {
  element: WorkflowFormElement;
  formControl: Control;
};

export default function GutachterInput({
  element,
  formControl,
}: GutachterInputProps) {
  const {
    control,
    watch,
    setError,
    clearErrors,
    formState: { errors },
  } = useForm();
  const postalCode = watch("postalCodeSearch");
  const dispatch = useDispatch();
  const services = useContext(Services);
  const accountId = useEsyState((state) => state.auth.profile.accountId);

  const [gutachters, setGutachters] = useState<Gutachter[] | null>(null);

  async function onSearch() {
    dispatch(showLoadingAction());
    try {
      setGutachters(null);
      const gutachter = await services.accountService.getGutachter(
        parseInt(accountId),
        postalCode
      );
      setGutachters(gutachter.participants);
      clearErrors("postalCodeSearch");

    } catch (error) {
      setError("postalCodeSearch", {
        message: "Bitte geben Sie eine gültige Postleitzahl ein.",
      });

    } finally {
      dispatch(hideLoadingAction());
    }
  }

  return (
    <Form.Field>
      <Form.Label>{element.text}</Form.Label>
      <View
        style={{
          width: "100%",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <View style={{ width: "85%" }}>
          <Form.Input
            control={control}
            name="postalCodeSearch"
            placeholder="Postleitzahl"
          />
        </View>
        <View style={{ width: "10%" }}>
          <IconButton
            icon={services.assetsService.getIcons("iconSearch")}
            onPress={onSearch}
          />
        </View>
      </View>
      <Form.ErrorMessage errors={errors} name="postalCodeSearch" />
      {gutachters && gutachters.length > 0 ? (
        <Form.Field>
          <Form.Label>Gutachter</Form.Label>
          <MultipleChoiceInput
            data={gutachters.map(
              (gutachter) => ({
                label: gutachter.name,
                value: gutachter.id.toString(),
              })
            )}
            control={formControl}
            name={element.name}
          />
        </Form.Field>
      ) : null}
    </Form.Field>
  );
}
