import React from "react";
import { Text, View } from "react-native";

declare type DisplayProps = {
  children: any;
};

export default function Display({ children }: DisplayProps) {
  return (
    <Text
      numberOfLines={0}
      style={[
        { fontSize: 14, fontFamily: "Lato-Regular", marginTop: 16 },
        { color: "#404040" },
      ]}
    >
      {children}
    </Text>
  );
}
