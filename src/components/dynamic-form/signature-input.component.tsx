import React from "react";
import { Controller } from "react-hook-form";
import { Image, Platform, Text, TouchableOpacity, View } from "react-native";
import BottomSheet from "react-native-gesture-bottom-sheet";
import SignatureScreen from "react-native-signature-canvas";

declare type SignatureInputProps = {
  control: any;
  name: string;
  defaultValue?: any;
  rules?: any;
};

export default function SignatureInput({
  control,
  name,
  defaultValue,
  rules,
}: SignatureInputProps) {
  const sheetRef = React.useRef(null);
  const signatureRef = React.useRef(null);

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={rules}
      render={({ field: { onChange, value, ref } }) => (
        <>
          <BottomSheet
            hasDraggableIcon
            draggable={false}
            ref={sheetRef}
            height={320}
          >
            <View
              style={{
                flexDirection: "column",
                paddingHorizontal: 24,
              }}
            >
              <View style={{ marginBottom: 31 }}>
                <Text
                  style={{
                    color: "#404040",
                    fontSize: 14,
                    fontWeight: "600",
                    lineHeight: 20,
                    marginBottom: 5,
                  }}
                >
                  Bitte hier unterschreibenss
                </Text>
                <View
                  style={{
                    height: 140,
                    borderWidth: 1,
                    borderColor: "#ADADAD",
                    borderRadius: 3,
                    backgroundColor: "red",
                  }}
                >
                  <SignatureScreen
                    ref={signatureRef}
                    onBegin={() => {}}
                    onEnd={() => {}}
                    onOK={(value) => onChange(value)}
                    webStyle={`.m-signature-pad--footer {display: none; margin: 0px;}`}
                  />
                </View>
              </View>
              <TouchableOpacity
                onPress={() => {
                  signatureRef.current?.readSignature();
                  sheetRef.current?.close();
                }}
                style={{
                  borderWidth: 1,
                  borderColor: "#404040",
                  borderRadius: 10,
                  marginTop: 10,
                  marginBottom: 0,
                }}
              >
                <Text
                  style={{
                    color: "#404040",
                    fontSize: 14,
                    fontWeight: "600",
                    lineHeight: 20,
                    textAlign: "center",
                    paddingVertical: 15,
                  }}
                >
                  Hinzufügen
                </Text>
              </TouchableOpacity>
            </View>
          </BottomSheet>
          <TouchableOpacity
            onPress={() => {
              sheetRef.current?.show();
            }}
            style={{
              height: 100,
              borderWidth: 1,
              borderColor: "black",
              borderRadius: 10,
              marginTop: 10,
              marginBottom: 0,
            }}
          >
            <>
              {/* <Text style={{ color: 'white' }}>x</Text> */}
              {value ? (
                <Image
                  resizeMode={Platform.OS == "ios" ? "contain" : "contain"}
                  style={{ width: "100%", height: 180 }}
                  source={{ uri: value }}
                />
              ) : null}
            </>
          </TouchableOpacity>
        </>
      )}
    />
  );
}
