import React, { useContext, useState, useEffect } from "react";
import { Controller } from "react-hook-form";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { Overlay } from "react-native-elements";
import { selectCamera } from "../../helpers/natives/camera-selector";
import { selectFiles } from "../../helpers/natives/file-selector";
import { selectPhotos } from "../../helpers/natives/photo-selector";
import { useToggle } from "../../hooks";
import { FileUploadInfo, MetaFile, Services } from "../../services";
import { localization } from "../../helpers";
import { useSelector } from "react-redux";

declare type FileInputProps = {
  control: any;
  name: string;
  defaultValue?: any;
  rules?: any;
  options?: {
    elementId?: any;
  };
  uploadInfo: FileUploadInfo;
};

declare type FileMenuProps = {
  isVisible: boolean;
  onClose: () => void;
  selectFilePress: () => void;
  selectPhotoPress: () => void;
  selectCameraPress: () => void;
};

declare type FilePreviewProps = {
  files: MetaFile[];
  onDelete: (file: MetaFile, index: number) => void;
};

export default function FileInput({
  control,
  name,
  defaultValue,
  rules,
  options = {},
  uploadInfo,
}: FileInputProps) {
  const { value: isMenuVisible, toggle } = useToggle(false);
  const services = useContext(Services);
  const [fileTemp, setFileTemp] = useState([]);
  const [errorMessages, setErrorMessages] = useState([]);

  const validateFiles = (files: MetaFile[]) => {
    setErrorMessages([]);
    files.forEach((file) => {
      if (file.size / 1000 > uploadInfo.maxUploadSizeInKB)
        setErrorMessages(
          (prev) => [
            ...prev,
            localization.language.FileTooLarge
              .replace("{{fileName}}", file.fileName)
              .replace("{{maxSize}}", `${uploadInfo.maxUploadSizeInKB / 1000}`)
          ]
        );
      if (uploadInfo.defaultDisallowedUploadFormats.includes(file.extension))
        setErrorMessages(
          (prev) => [
            ...prev,
            localization.language.InvalidFileExtension
              .replace("{{fileName}}", file.fileName)
          ]
        );
    });
  };

  const handleSelect =
    (cb: (files: any) => void) =>
      (files: Partial<MetaFile>[], error: Error) => {
        if (error) {
          services.logService.error(error);
          return;
        }

        const temp = [...fileTemp];

        files.map((file) => ({
          ...file,
          tags: { elementId: options.elementId },
        }));

        files.map((file) => {
          temp.push({
            ...file,
            tags: { elementId: options.elementId },
          });
          setFileTemp((fileTemp) => [
            ...fileTemp,
            {
              ...file,
              tags: { elementId: options.elementId },
            },
          ]);
        });
        cb(temp);
        validateFiles(temp);
        toggle();
      };

  const handleDelete =
    (files: MetaFile[], cb: (files: any) => void) =>
      (file: MetaFile, index: number) => {
        if (index < 0) return;

        files.splice(index, 1);
        cb([...files]);
        validateFiles([...files]);
      };

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={{
        ...rules,
        validate: {
          filesValidation: () => errorMessages.length === 0,
        },
      }}
      render={({ field: { onChange, value } }) => (
        <>
          <TouchableOpacity
            onPress={() => toggle()}
            style={{
              borderRadius: 10,
              backgroundColor: "white",
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.23,
              shadowRadius: 2.62,
              elevation: 4,
              marginTop: 4,
            }}
          >
            <View
              style={{
                height: 50,
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "row",
              }}
            >
              <Image
                source={services.assetsService.getIcons("iconAttachment")}
                style={{ height: 20, width: 20 }}
              />
              <Text
                style={[
                  { fontSize: 14, fontFamily: "Lato-Bold" },
                  { color: "#404040" },
                ]}
              >
                {localization.language.AddFilesOrImage}
              </Text>
            </View>
          </TouchableOpacity>
          <FilePreview
            files={fileTemp}
            onDelete={handleDelete(fileTemp, onChange)}
          />
          {errorMessages.length > 0 ? (
            <Text
              style={{
                fontSize: 14,
                fontFamily: "Lato-Bold",
                color: "#bc3640",
                marginTop: 5,
              }}
            >
              {errorMessages.join("\n")}
            </Text>
          ) : null}
          <FileMenu
            isVisible={isMenuVisible}
            onClose={() => toggle()}
            selectFilePress={() => selectFiles(handleSelect(onChange))}
            selectPhotoPress={() => selectPhotos(handleSelect(onChange))}
            selectCameraPress={() => selectCamera(handleSelect(onChange))}
          />
        </>
      )}
    />
  );
}

function FilePreview({ files, onDelete }: FilePreviewProps) {
  const services = useContext(Services);

  if (!files) return null;

  // console.log(files, "ini file preview");

  return (
    <>
      {files.map((file, index) => (
        <View
          key={index}
          style={{
            height: file.contentType.startsWith("image") ? 165 : 65,
            marginVertical: 10,
            backgroundColor: "#FFFFFF",
            borderWidth: 1,
            borderColor: "#f5f5f5",
            flexDirection: "column",
            shadowColor: "#000",
            justifyContent: "center",
            alignItems: "center",
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.23,
            shadowRadius: 2.62,
            borderRadius: 5,
            elevation: 4,
          }}
        >
          {file.contentType.startsWith("image") ? (
            <View
              style={{
                height: 100,
                width: "100%",
                borderTopLeftRadius: 5,
                borderTopRightRadius: 5,
              }}
            >
              <Image
                source={{ uri: file.filePath }}
                style={{
                  height: 100,
                  width: "100%",
                  borderTopLeftRadius: 5,
                  borderTopRightRadius: 5,
                }}
                resizeMode="cover"
              />
            </View>
          ) : null}

          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "center",
              padding: 12,
              alignItems: "center",
            }}
          >
            <View style={{ width: "10%" }}>
              <Image
                source={
                  !file.contentType.startsWith("image")
                    ? services.assetsService.getIcons("iconFileBlack")
                    : services.assetsService.getIcons("iconImageBlack")
                }
                style={{ height: 20, width: 20 }}
              />
            </View>
            <View style={{ flex: 1 }}>
              <Text
                numberOfLines={1}
                style={{
                  color: "#404040",
                  fontSize: 14,
                  lineHeight: 20,
                }}
              >
                {file.fileName}
              </Text>
              <Text
                style={{
                  color: "#ADADAD",
                  fontSize: 11,
                  lineHeight: 13,
                }}
              >
                {(file.size / 1000000).toFixed(2)} MB
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => onDelete(file, index)}
              style={{ width: "10%" }}
            >
              <Image
                source={services.assetsService.getIcons("iconCloseBlack")}
                style={{ height: 20, width: 20 }}
              />
            </TouchableOpacity>
          </View>
        </View>
      ))}
    </>
  );
}

function FileMenu({
  isVisible,
  onClose,
  selectFilePress,
  selectPhotoPress,
  selectCameraPress,
}: FileMenuProps) {
  const services = useContext(Services);

  return (
    <Overlay
      isVisible={isVisible}
      overlayStyle={{
        width: "90%",
        position: "absolute",
        bottom: 80,
        justifyContent: "flex-end",
      }}
      onBackdropPress={onClose}
    >
      <View style={{ flexDirection: "row" }}>
        <TouchableOpacity
          onPress={selectFilePress}
          style={{ padding: 10, marginHorizontal: 5, alignItems: "center" }}
        >
          <Image
            source={services.assetsService.getIcons("iconFileBlack")}
            style={{ height: 20, width: 20, marginVertical: 10 }}
          />
          <Text
            style={{
              fontSize: 13,
              fontFamily: "Lato-Regular",
              color: "#030D12B3",
            }}
          >
            {localization.language.Files}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={selectPhotoPress}
          style={{ padding: 10, marginHorizontal: 5, alignItems: "center" }}
        >
          <Image
            source={services.assetsService.getIcons("iconImageBlack")}
            style={{ height: 20, width: 20, marginVertical: 10 }}
          />
          <Text
            style={{
              fontSize: 13,
              fontFamily: "Lato-Regular",
              color: "#030D12B3",
            }}
          >
            {localization.language.Gallery}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={selectCameraPress}
          style={{ padding: 10, marginHorizontal: 5, alignItems: "center" }}
        >
          <Image
            source={services.assetsService.getIcons("iconCamera")}
            style={{ height: 20, width: 20, marginVertical: 10 }}
          />
          <Text
            style={{
              fontSize: 13,
              fontFamily: "Lato-Regular",
              color: "#030D12B3",
            }}
          >
            {localization.language.Camera}
          </Text>
        </TouchableOpacity>
      </View>
    </Overlay>
  );
}
