import { useState } from "react";
import { ScrollView, Text, View } from "react-native";
import Modal from "react-native-modal";
import { useDispatch } from "react-redux";
import { previewAgb, previewAgbText, previewDatenschutz, previewDatenschutzText } from "../../helpers";
import { Account } from "../../models";

declare type AgreementProps = {
  account: Account;
  children: any;
  isVisible?: boolean;
};

declare type AgreementTOCProps = {
  content: string;
  isVisible?: boolean;
  onClose?: () => void;
};

export function Agreement({ account, children, isVisible }: AgreementProps) {
  const dispatch = useDispatch();
  // const [content, setContent] = useState("");
  // const [isModalVisible, setModalVisible] = useState(false);

  if (!account) return null;
  if (!isVisible) return null;

  return (
    <>
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          marginHorizontal: 24,
          marginBottom: 4,
          marginTop: 32,
        }}
      >
        <View>
          <Text
            style={{
              color: "#404040",
              fontWeight: "600",
              fontSize: 21,
              lineHeight: 32,
              fontFamily: "Lato-Regular",
            }}
          >
            Datenschutz und AGB
          </Text>
        </View>
        <View style={{ marginTop: 8, marginBottom: 16 }}>
          <Text
            style={{
              color: "#404040",
              fontWeight: "400",
              fontSize: 14,
              lineHeight: 20,
              fontFamily: "Lato-Regular",
            }}
          >
            Hier finden Sie unsere Regelungen zu{" "}
            <Text
              onPress={() => {
                // setModalVisible(true);
                // setContent(account.esyThingDatenschutz);

                dispatch(previewDatenschutzText(account));
              }}
              style={{ color: "#429F98" }}
            >
              Datenschutz
            </Text>{" "}
            sowie die{" "}
            <Text
              onPress={() => {
                // setModalVisible(true);
                // setContent(account.esyThingAgb);

                dispatch(previewAgbText(account));
              }}
              style={{ color: "#429F98" }}
            >
              AGB
            </Text>
          </Text>
        </View>
        {children}
      </View>
      {/* <AgreementTOC
        content={content}
        isVisible={isModalVisible}
        onClose={() => setModalVisible(false)}
      /> */}
    </>
  );
}

function AgreementTOC({
  content,
  isVisible = false,
  onClose = () => {},
}: AgreementTOCProps) {
  return (
    <Modal
      statusBarTranslucent={true}
      isVisible={isVisible}
      onBackButtonPress={onClose}
      onBackdropPress={onClose}
    >
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <View
          style={{
            backgroundColor: "white",
            alignSelf: "center",
            height: "80%",
            width: "90%",
            borderRadius: 20,
          }}
        >
          <ScrollView
            style={{ paddingHorizontal: 24, marginTop: 10 }}
            showsVerticalScrollIndicator={true}
          >
            <Text
              style={{
                fontWeight: "400",
                fontSize: 14,
                lineHeight: 20,
                color: "#404040",
                textAlign: "justify",
              }}
            >
              {content}
            </Text>
          </ScrollView>
        </View>
      </View>
    </Modal>
  );
}
