import React from "react";
import { Text } from "react-native";

declare type LabelProps = {
  children: any;
  isRequired?: boolean;
  style?: any;
};

export default function Label({
  children,
  isRequired,
  style = {},
}: LabelProps) {
  return (
    <Text
      style={{
        fontSize: 14,
        fontFamily: "Lato-Bold",
        marginTop: 16,
        color: "#404040",
        ...style,
      }}
    >
      {children}
      {isRequired ? <Text style={{ color: "#bc363f" }}>*</Text> : null}
    </Text>
  );
}
