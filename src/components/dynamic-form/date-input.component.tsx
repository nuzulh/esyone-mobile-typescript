import moment from "moment";
import { Controller } from "react-hook-form";
import {
  Image,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  Platform,
} from "react-native";
import { useToggle } from "../../hooks";
import Modal from "react-native-modal";
import DatePicker from "react-native-date-picker";
import { useContext } from "react";
import { Services } from "../../services";

declare type DateInputProps = {
  control: any;
  name: string;
  defaultValue?: Date;
  rules?: any;
};

export default function DateInput({
  control,
  name,
  defaultValue,
  rules,
}: DateInputProps) {
  const { value: isModalActive, toggle } = useToggle(false);
  const services = useContext(Services);

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue || new Date()}
      rules={rules}
      render={({ field: { onChange, value } }) => {
        // console.log(value);
        return (
          <>
            <TouchableOpacity
              onPress={() => toggle()}
              style={{
                width: "100%",
                flexDirection: "row",
                alignItems: "center",
                marginTop: 10,
                marginBottom: 0,
                padding: 10,
                borderRadius: 4,
                backgroundColor: "#ffffff",
                borderWidth: 1,
                borderColor: "#ADADAD",
                justifyContent: "space-between",
              }}
            >
              <Image
                source={services.assetsService.getIcons("date")}
                style={{ width: 24, height: 24 }}
              />
              <Text
                style={{
                  width: "70%",
                  color: "#ADADAD",
                  fontSize: 14,
                  lineHeight: 20,
                  fontWeight: "400",
                }}
              >
                {value == undefined
                  ? "tt.mm.jjjj"
                  : moment(value).format("DD.MM.yyyy")}
              </Text>
              <Image
                source={services.assetsService.getIcons("down")}
                style={{ width: 24, height: 24 }}
              />
              {/* <DatePicker mode="date" date={enteredValue} onDateChange={(date) => { handleChange(entry, date) }}/> */}
            </TouchableOpacity>
            <Modal
              isVisible={isModalActive}
              onBackButtonPress={() => toggle()}
              onBackdropPress={() => toggle()}
            >
              <TouchableWithoutFeedback
                onPress={() => toggle()}
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <View
                  style={{
                    backgroundColor: "white",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "40%",
                    width: Platform.OS === "android" ? "97%" : "100%",
                    borderRadius: 20,
                  }}
                >
                  <DatePicker
                    textColor="black"
                    mode="date"
                    date={new Date(value)}
                    onDateChange={(date) => onChange(date)}
                  />
                </View>
              </TouchableWithoutFeedback>
            </Modal>
          </>
        );
      }}
    />
  );
}
