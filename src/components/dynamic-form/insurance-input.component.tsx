import { useForm } from "react-hook-form";
import { Insurance, WorkflowFormElement } from "../../models";
import { Form } from "../form";
import { SimpleChoiceInput } from "./simple-choice-input.component";
import { FormElements } from ".";
import { FileUploadInfo } from "../../services";

declare type InsuranceInputProps = {
  element: WorkflowFormElement;
  insurances: Insurance[];
  uploadInfo?: FileUploadInfo;
};

export default function InsuranceInput({
  element,
  insurances,
  uploadInfo,
}: InsuranceInputProps) {
  const { control, watch } = useForm<{ isShow: 0 | 1; }>({
    defaultValues: {
      isShow: 0,
    },
  });
  const isShow = watch("isShow");

  return (
    <>
      <Form.Field>
        <Form.Label>Besitzen Sie eine Rechtsschutzversicherung?</Form.Label>
        <SimpleChoiceInput
          control={control}
          name="isShow"
          defaultValue={isShow}
          options={[
            { label: "Nein", value: 0 },
            { label: "Ja", value: 1 },
          ]}
        />
      </Form.Field>
      {isShow ? (
        <FormElements
          elements={element.formElements}
          insurances={insurances}
          isProtected={true}
          uploadInfo={uploadInfo}
        />
      ) : null}
    </>
  );
}
