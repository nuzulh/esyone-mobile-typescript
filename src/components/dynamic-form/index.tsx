import { useCallback, useContext, useEffect, useState } from "react";
import { FormProvider, useForm, useFormContext } from "react-hook-form";
import { Text, TouchableOpacity, View } from "react-native";
import { useDispatch } from "react-redux";
import {
  HOME_STACK,
  localization,
  navigateToAction,
  redirectExternallinkAction,
  saveFormularDraft,
  submitWorkflow,
  validatePhoneNumber,
} from "../../helpers";
import { useAnmeldeValue, useDebounce, usePromise } from "../../hooks";
import { Account, Insurance } from "../../models";
import { WorkflowFormElement, WorkflowFormular, WorkflowProcess } from "../../models/workflow";
import { FileUploadInfo, Services } from "../../services";
import { AgreementAGB } from "./agreement-agb.component";
import { AgreementDatenschutz } from "./agreement-datenschutz.component";
import { Agreement } from "./agreement.component";
import { CheckboxInput } from "./checkbox-input.component";
import DateInput from "./date-input.component";
import Display from "./display.component";
import ErrorMessage from "./error.component";
import Field from "./field.component";
import FileInput from "./file-input.component";
import GeolocationInput from "./geolocation-input.component";
import Headline from "./headline.component";
import Input from "./input.component";
import Label from "./label.component";
import {
  MultipleChoiceInput,
  MultipleChoiceItem,
} from "./multiple-choice-input.component";
import NumberInput from "./number-input.component";
import PhoneInput from "./phone-input.component";
import { RadioInput } from "./radio-input.component";
import RatingInput from "./rating-input.component";
import SignatureInput from "./signature-input.component";
import { SimpleChoiceInput } from "./simple-choice-input.component";
import TextAreaInput from "./text-area-input.component";
import TimeInput from "./time-input.component";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faMinus, faPlus, faTrash } from "@fortawesome/free-solid-svg-icons";
import RepeaterElement from "./repeater.component";
import GutachterInput from "./gutachter.component";
import InsuranceInput from "./insurance-input.component";
import { debounce } from "redux-saga/effects";

export * from "./preview";

const REQUIRED_TYPE = {
  NOTEMPTY: "NOTEMPTY", //
  UPLOAD: "UPLOAD", //
  CUSTOMFORMTIME: "CUSTOMFORMTIME", //
  CUSTOMFORMDATE: "CUSTOMFORMDATE", //
  NUMERIC: "NUMERIC", //
  SIGNATURENOTEMPTY: "SIGNATURENOTEMPTY", //
  MOBILENUMBER: "MOBILENUMBER", //
  CUSTOMFORMGEOLOCATION: "CUSTOMFORMGEOLOCATION", //
  CUSTOMFORMRADIONOTEMPTY: "CUSTOMFORMRADIONOTEMPTY", //
  REGCHECK: "REGCHECK", //
  CHECKBOXREQUIRED: "CHECKBOXREQUIRED",
};

const ERROR_MESSAGES = {
  REQUIRED: "This field is required",
  INVALID_PATTERN: "Invalid pattern",
  INVALID_PHONE_NUMBER: "Invalid phone number",
};

declare type DynamicFormProps = {
  account: Account;
  insurances: Insurance[];
  workflowProcess: WorkflowProcess;
  accessToken: string;
  uploadInfo?: FileUploadInfo;
  workflowFormular?: WorkflowFormular;
};

declare type FormElementsProps = {
  insurances: Insurance[];
  elements: WorkflowFormElement[];
  isProtected?: boolean;
  uploadInfo?: FileUploadInfo;
  workflowFormular?: WorkflowFormular;
};

declare type FormElementProps = {
  element: WorkflowFormElement;
  isProtected: boolean;
  insurances: Insurance[];
  uploadInfo?: FileUploadInfo;
  workflowFormular?: WorkflowFormular;
};

export function DynamicForm({
  account,
  insurances,
  workflowProcess,
  accessToken,
  uploadInfo,
  workflowFormular,
}: DynamicFormProps) {
  const dispatch = useDispatch();
  const methods = useForm({
    defaultValues: workflowFormular?.fieldValues,
  });
  const { control, handleSubmit, watch } = methods;
  const fieldValues = watch();
  const agb = fieldValues["agb"]; //watch("agb");
  const datenschutz = fieldValues["datenschutz"]; //watch("datenschutz");
  const services = useContext(Services);

  useDebounce(
    () => dispatch(
      saveFormularDraft({
        workflowId: workflowProcess.workflowId,
        fieldValues,
      })
    ),
    500,
    [fieldValues]
  );

  // console.log(JSON.stringify(workflowProcess, null, 2));

  const onSubmit = () => {
    const executor = handleSubmit((data) => {
      if (!data) {
        services.logService.error(
          new Error("DynamicForm onSubmit data is null")
        );
        return;
      }

      const tmp = {};

      Object.keys(data).forEach((key) => {
        if (key === "agb" || key === "datenschutz") return;
        if (data[key] === undefined || data[key] === null) return;

        tmp[key] = data[key];
      });

      // console.log(JSON.stringify(tmp, null, 2));

      dispatch(
        submitWorkflow(account, workflowProcess, accessToken, tmp, uploadInfo)
      );
    });

    executor().catch((err) => console.log(err));
  };

  if (!workflowProcess || !workflowProcess.formElement) return null;

  // console.log(
  //   "isDisabled",
  //   (workflowProcess.showAgb ? !!agb : false) &&
  //     (workflowProcess.showDatenschutz ? !!datenschutz : false)
  // );

  return (
    <FormProvider {...methods}>
      <View style={{ paddingHorizontal: 24 }}>
        <FormElements
          insurances={insurances}
          elements={workflowProcess.formElement.formElements}
          isProtected={
            workflowProcess.formElement.type === "rechtsschutzformular"
          }
          uploadInfo={uploadInfo}
          workflowFormular={workflowFormular}
        />
      </View>
      <Agreement
        account={account}
        isVisible={workflowProcess.showAgb || workflowProcess.showDatenschutz}
      >
        <AgreementAGB
          visible={workflowProcess.showAgb}
          control={control}
          name="agb"
          rules={{ required: ERROR_MESSAGES.REQUIRED }}
        />
        <AgreementDatenschutz
          visible={workflowProcess.showDatenschutz}
          control={control}
          name="datenschutz"
          rules={{ required: ERROR_MESSAGES.REQUIRED }}
        />
      </Agreement>
      <View
        style={{
          width: "100%",
          position: "absolute",
          bottom: 0,
          paddingTop: 0,
          paddingBottom: 32,
          paddingHorizontal: 24,
          backgroundColor: "white",
        }}
      >
        <TouchableOpacity
          onPress={onSubmit}
          disabled={
            (workflowProcess.showAgb ? !agb : false) &&
            (workflowProcess.showDatenschutz ? !datenschutz : false)
          }
          style={{
            backgroundColor:
              (workflowProcess.showAgb ? !!agb : true) &&
                (workflowProcess.showDatenschutz ? !!datenschutz : true)
                ? "#404040"
                : "#d9d9d9",
            borderRadius: 10,
            paddingVertical: 15,
          }}
        >
          <Text
            style={{
              fontSize: 14,
              lineHeight: 20,
              fontWeight: "600",
              fontFamily: "Lato-Regular",
              textAlign: "center",
              color: "#FFFFFF",
            }}
          >
            {localization.language.Send}
          </Text>
        </TouchableOpacity>
      </View>
    </FormProvider>
  );
}

export function FormElements({
  insurances,
  elements,
  isProtected,
  uploadInfo,
  workflowFormular,
}: FormElementsProps) {
  if (!elements || elements.length === 0) return null;

  return (
    <>
      {elements.map((element, index) => (
        <FormElement
          element={element}
          isProtected={isProtected}
          insurances={insurances}
          uploadInfo={uploadInfo}
          workflowFormular={workflowFormular}
          key={index}
        />
      ))}
    </>
  );
}

export function FormElement({
  element,
  isProtected,
  insurances,
  uploadInfo,
  workflowFormular,
}: FormElementProps) {
  const isRequired = REQUIRED_TYPE[element.requiredType];
  const anmeldeValue = useAnmeldeValue(element);
  const defaultValue = anmeldeValue
    ? anmeldeValue
    : workflowFormular?.fieldValues[element.name];
  const {
    control,
    formState: { errors },
  } = useFormContext();

  // console.log(errors);

  switch (element.type) {
    case "headline":
      return (
        <Field>
          <Headline>{element.text}</Headline>
        </Field>
      );
    case "display":
      return (
        <Field>
          <Display>{element.text}</Display>
        </Field>
      );
    case "inputregex":
      return (
        <Field>
          <Label isRequired={isRequired}>{element.text}</Label>
          <Input
            control={control}
            name={element.name}
            defaultValue={defaultValue}
            rules={{
              required: isRequired ? ERROR_MESSAGES.REQUIRED : false,
              pattern: {
                value: new RegExp(element.regEx),
                message: ERROR_MESSAGES.INVALID_PATTERN,
              },
            }}
          />
          <ErrorMessage errors={errors} name={element.name} />
        </Field>
      );
    case "input":
      return (
        <Field>
          <Label isRequired={isRequired}>{element.text}</Label>
          <Input
            control={control}
            name={element.name}
            defaultValue={defaultValue}
            rules={{
              required: isRequired ? ERROR_MESSAGES.REQUIRED : false,
            }}
          />
          <ErrorMessage errors={errors} name={element.name} />
        </Field>
      );
    case "date":
      return (
        <Field>
          <Label isRequired={isRequired}>{element.text}</Label>
          <DateInput
            control={control}
            name={element.name}
            rules={{
              required: isRequired ? ERROR_MESSAGES.REQUIRED : false,
            }}
          />
          <ErrorMessage errors={errors} name={element.name} />
        </Field>
      );
    case "time":
      return (
        <Field>
          <Label isRequired={isRequired}>{element.text}</Label>
          <TimeInput
            control={control}
            name={element.name}
            rules={{
              required: isRequired ? ERROR_MESSAGES.REQUIRED : false,
            }}
          />
          <ErrorMessage errors={errors} name={element.name} />
        </Field>
      );
    case "number":
      return (
        <Field>
          <Label isRequired={isRequired}>{element.text}</Label>
          <NumberInput
            control={control}
            name={element.name}
            defaultValue={defaultValue}
            rules={{
              required: isRequired ? ERROR_MESSAGES.REQUIRED : false,
            }}
          />
          <ErrorMessage errors={errors} name={element.name} />
        </Field>
      );
    case "mobilenumber":
      return (
        <Field>
          <Label isRequired={isRequired}>{element.text}</Label>
          <PhoneInput
            control={control}
            name={element.name}
            defaultValue={defaultValue}
            rules={{
              required: isRequired ? ERROR_MESSAGES.REQUIRED : false,
              validate: {
                validPhoneNumber: (value) =>
                  !isRequired
                    ? true
                    : validatePhoneNumber(value)
                      ? true
                      : ERROR_MESSAGES.INVALID_PHONE_NUMBER,
              },
            }}
          />
          <ErrorMessage errors={errors} name={element.name} />
        </Field>
      );
    case "geolocation":
      return null;
    // return (
    //   <Field flex={false}>
    //     <Label isRequired={isRequired}>{element.text}</Label>
    //     <GeolocationInput
    //       control={control}
    //       name={element.name}
    //       rules={{
    //         required: isRequired ? ERROR_MESSAGES.REQUIRED : false,
    //       }}
    //     />
    //     <ErrorMessage errors={errors} name={element.name} />
    //   </Field>
    // );
    case "rating":
      return (
        <Field flex={false}>
          <Label isRequired={isRequired}>{element.text}</Label>
          <RatingInput
            control={control}
            name={element.name}
            defaultValue={defaultValue}
            rules={{ required: isRequired ? ERROR_MESSAGES.REQUIRED : false }}
          />
          <ErrorMessage errors={errors} name={element.name} />
        </Field>
      );
    case "textarea":
      return (
        <Field>
          <Label isRequired={isRequired}>{element.text}</Label>
          <TextAreaInput
            control={control}
            name={element.name}
            defaultValue={defaultValue}
            rules={{ required: isRequired ? ERROR_MESSAGES.REQUIRED : false }}
          />
          <ErrorMessage errors={errors} name={element.name} />
        </Field>
      );
    case "signature":
      return (
        <Field flex={false}>
          <Label isRequired={isRequired}>{element.text}</Label>
          <SignatureInput
            control={control}
            name={element.name}
            rules={{ required: isRequired ? ERROR_MESSAGES.REQUIRED : false }}
          />
          <ErrorMessage errors={errors} name={element.name} />
        </Field>
      );
    case "file":
      return (
        <Field flex={false}>
          <Label isRequired={isRequired}>{element.text}</Label>
          <FileInput
            control={control}
            name={element.name}
            defaultValue={defaultValue}
            rules={{ required: isRequired ? ERROR_MESSAGES.REQUIRED : false }}
            options={{ elementId: element.id }}
            uploadInfo={uploadInfo}
          />
          <ErrorMessage errors={errors} name={element.name} />
        </Field>
      );
    case "simplechoice":
      return (
        <Field>
          <Label isRequired={isRequired}>{element.text}</Label>
          <SimpleChoiceInput
            control={control}
            name={element.name}
            defaultValue={defaultValue}
            options={[
              { label: "Nein", value: 0 },
              { label: "Ja", value: 1 },
            ]}
            rules={{ required: isRequired ? ERROR_MESSAGES.REQUIRED : false }}
          />
          <ErrorMessage errors={errors} name={element.name} />
        </Field>
      );
    case "checkbox":
      return (
        <Field>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              flex: 1,
            }}
          >
            <CheckboxInput
              control={control}
              name={element.name}
              defaultValue={defaultValue}
              rules={{ required: isRequired ? ERROR_MESSAGES.REQUIRED : false }}
            />
            <Label
              isRequired={isRequired}
              style={{ marginTop: 0, marginBottom: 0 }}
            >
              {element.text}
            </Label>
          </View>
        </Field>
      );
    case "multiplechoice":
      let data = [] as MultipleChoiceItem[];
      if (!isProtected)
        data = element.formElements.map((element) => ({
          label: element.text,
          value: element.text,
        }));
      else
        data = insurances.map((insurance) => ({
          label: insurance.name,
          value: insurance.foreignId,
        }));

      return (
        <Field flex={false}>
          <Label isRequired={isRequired}>{element.text}</Label>
          <MultipleChoiceInput
            control={control}
            name={element.name}
            defaultValue={defaultValue}
            data={data}
            rules={{ required: isRequired ? ERROR_MESSAGES.REQUIRED : false }}
          />
          <ErrorMessage errors={errors} name={element.name} />
        </Field>
      );
    case "rechtsschutzformular":
      return (
        // <FormElements
        //   elements={element.formElements}
        //   insurances={insurances}
        //   isProtected={true}
        //   uploadInfo={uploadInfo}
        // />
        <InsuranceInput
          element={element}
          insurances={insurances}
          uploadInfo={uploadInfo}
        />
      );
    case "radio":
      return (
        <Field flex={false}>
          <RadioInput />
        </Field>
      );
    case "repeater":
      return (
        <RepeaterElement
          control={control}
          element={element}
          isProtected={isProtected}
          insurances={insurances}
          uploadInfo={uploadInfo}
          workflowFormular={workflowFormular}
        />
      );
    case "gutachterchoice":
      return (
        <GutachterInput
          element={element}
          formControl={control}
        />
      );
    case "externallink":
      const dispatch = useDispatch();
      dispatch(
        // navigateToAction({
        //   navigateTo: {
        //     screen: HOME_STACK.WEBVIEW_SCREEN,
        //     params: {
        //       url: element.text,
        //       title: element.text.replace(/^https?:\/\//, ""),
        //     },
        //   },
        // })
        redirectExternallinkAction(
          element.text,
          element.text.replace(/^https?:\/\//, "")
        )
      );
      return null;
    // case "anmeldeformular":
    //   return <View></View>;
    // case "anmeldeformularlong":
    //   return <View></View>;
    default:
      return (
        // <View>
        <FormElements
          elements={element.formElements}
          isProtected={isProtected}
          insurances={insurances}
          uploadInfo={uploadInfo}
          workflowFormular={workflowFormular}
        />
        // </View>
      );
  }
}
