import { useMemo } from "react";
import { Text } from "react-native";
import { ErrorMessage as RHErrorMessage } from "@hookform/error-message";

declare type ErrorMessageProps = {
  errors: any;
  name: string;
};

export default function ErrorMessage({ errors, name }: ErrorMessageProps) {
  // console.log(name, errors);

  return (
    <RHErrorMessage
      name={name}
      errors={errors}
      render={(props) => {
        // console.log(props);
        const { message, messages } = props;
        if (message)
          return (
            <Text
              style={{
                fontSize: 14,
                fontFamily: "Lato-Bold",
                color: "#bc3640",
                marginTop: 5,
              }}
            >
              {message}
            </Text>
          );

        return messages
          ? Object.entries(messages).map(([type, m]) => (
              <Text
                key={type}
                style={{
                  fontSize: 14,
                  fontFamily: "Lato-Bold",
                  color: "#bc3640",
                  marginTop: 5,
                }}
              >
                {m}
              </Text>
            ))
          : null;
      }}
    />
  );

  // const errorsAsList = useMemo(() => {
  //   if (!errors) return [];
  //   return Object.keys(errors).map((key) => errors[key]);
  // }, [errors]);

  // return errorsAsList.map((error: any, index: number) => (
  // <Text
  //   key={index}
  //   style={[{ fontSize: 14, fontFamily: "Lato-Bold" }, { color: "#bc3640" }]}
  // >
  //   {error.type === "required"
  //     ? "Pflichtfeld"
  //     : error.message
  //     ? error.message
  //     : "Ungültige Eingabe"}
  // </Text>
  // ));
}
