import CheckBox from "@react-native-community/checkbox";
import { Controller } from "react-hook-form";
import { Platform } from "react-native";

declare type CheckboxInputProps = {
  control: any;
  name: string;
  defaultValue?: any;
  rules?: any;
};

export function CheckboxInput({
  control,
  name,
  defaultValue,
  rules,
}: CheckboxInputProps) {
  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={rules}
      render={({ field: { onChange, value } }) => (
        <CheckBox
          disabled={false}
          onValueChange={(value) => onChange(value)}
          value={value}
          style={{
            marginTop: 5,
            marginBottom: 0,
            marginLeft: 0,
            marginRight: Platform.OS === "ios" ? 10 : 0,
          }}
          // renderContainer={RadioButtons.renderVerticalContainer}
        />
      )}
    />
  );
}
