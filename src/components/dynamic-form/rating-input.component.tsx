import React, { useContext, useMemo } from "react";
import { Controller } from "react-hook-form";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { Services } from "../../services";

declare type RatingInputProps = {
  name: string;
  control: any;
  // categories: Array<{
  //   label: string;
  //   sentiment: string;
  //   icon: any;
  // }>;
  defaultValue?: any;
  rules?: any;
};

export default function RatingInput({
  name,
  control,
  defaultValue,
  // categories = [],
  rules,
}: RatingInputProps) {
  const services = useContext(Services);
  const categories = useMemo(
    () => [
      {
        sentiment: "positiv",
        label: "Excellent",
        icon: services.assetsService.getIcons("ratingExcellent"),
      },
      {
        sentiment: "neutral",
        label: "Average",
        icon: services.assetsService.getIcons("ratingAverage"),
      },
      {
        sentiment: "negativ",
        label: "Poor",
        icon: services.assetsService.getIcons("ratingPoor"),
      },
    ],
    []
  );

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={rules}
      render={({ field: { onChange, value } }) => (
        <View
          style={{
            height: 100,
            flexDirection: "row",
            alignItems: "center",
            marginTop: 10,
            marginBottom: 0,
            justifyContent: "space-between",
          }}
        >
          {categories.map((category, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => onChange(category.sentiment)}
              style={{
                borderWidth: 1,
                borderColor:
                  value === category.sentiment ? "#429F98" : "#e0e0e0",
                borderRadius: 5,
                justifyContent: "center",
                alignItems: "center",
                paddingHorizontal: 25,
                paddingVertical: 12,
                marginLeft: index % 3 !== 0 ? 5 : 0,
              }}
            >
              <Image source={category.icon} style={{ width: 48, height: 48 }} />
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: "Lato-Regular",
                  fontWeight: "400",
                  lineHeight: 20,
                }}
              >
                {category.label}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      )}
    />
  );
}
