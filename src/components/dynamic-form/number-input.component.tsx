import React from "react";
import { Controller } from "react-hook-form";
import { TextInput, View } from "react-native";
import { validateNumber } from "../../helpers";

declare type NumberInputProps = {
  name: string;
  control: any;
  defaultValue?: any;
  rules?: any;
};

export default function NumberInput({
  name,
  control,
  defaultValue,
  rules,
}: NumberInputProps) {
  const [isFocused, setFocus] = React.useState(false);

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={{
        validate: {
          isNumber: (value) =>
            !value
              ? true
              : validateNumber(value)
              ? true
              : "Please enter a valid number",
        },
        ...rules,
      }}
      render={({ field: { onBlur, onChange, value } }) => (
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginTop: 10,
            marginBottom: 0,
            padding: 0,
            borderRadius: 5,
            backgroundColor: "#ffffff",
            borderWidth: isFocused ? 1.5 : 1,
            borderColor: isFocused ? "#404040" : "#ADADAD",
          }}
        >
          <TextInput
            style={{ flex: 1, padding: 12 }}
            keyboardType="number-pad"
            onChangeText={(text) => onChange(text)}
            value={value}
            secureTextEntry={false}
            // autoCompleteType="password"
            onFocus={() => setFocus(true)}
            onBlur={() => {
              onBlur();
              setFocus(false);
            }}
            placeholder={""}
            placeholderTextColor="#ADADAD"
          />
        </View>
      )}
    />
  );
}
