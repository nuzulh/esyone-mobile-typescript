import React, { useContext } from "react";
import {
  Image,
  Platform,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { Controller } from "react-hook-form";
import { useToggle } from "../../hooks";
import moment from "moment";
import Modal from "react-native-modal";
import DatePicker from "react-native-date-picker";
import { Services } from "../../services";

declare type TimeInputProps = {
  control: any;
  name: string;
  defaultValue?: any;
  rules?: any;
};

export default function TimeInput({
  control,
  name,
  defaultValue,
  rules,
}: TimeInputProps) {
  const { value: isModalActive, toggle } = useToggle(false);
  const services = useContext(Services);

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue || new Date()}
      rules={rules}
      render={({ field: { onChange, value } }) => (
        <>
          <TouchableOpacity
            onPress={() => toggle()}
            style={{
              width: "50%",
              flexDirection: "row",
              alignItems: "center",
              marginTop: 10,
              marginBottom: 0,
              padding: 10,
              borderRadius: 5,
              backgroundColor: "#ffffff",
              borderWidth: 1,
              borderColor: "#ADADAD",
              justifyContent: "space-between",
            }}
          >
            <Image
              source={services.assetsService.getIcons("timer")}
              style={{ width: 24, height: 24 }}
            />
            <Text
              style={{
                color: "#ADADAD",
                fontSize: 14,
                lineHeight: 20,
                fontWeight: "400",
              }}
            >
              {moment(value).format("HH:mm")}
            </Text>
            <Image
              source={services.assetsService.getIcons("down")}
              style={{ width: 24, height: 24 }}
            />
          </TouchableOpacity>
          <Modal
            isVisible={isModalActive}
            onBackButtonPress={() => toggle()}
            onBackdropPress={() => toggle()}
          >
            <TouchableWithoutFeedback
              onPress={() => toggle()}
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  backgroundColor: "white",
                  justifyContent: "center",
                  alignItems: "center",
                  height: "40%",
                  width: Platform.OS === "android" ? "90%" : "100%",
                  borderRadius: 20,
                }}
              >
                <DatePicker
                  textColor="black"
                  locale="fr"
                  is24hourSource="locale"
                  mode="time"
                  date={new Date(value)}
                  onDateChange={(date) => onChange(date)}
                />
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        </>
      )}
    />
  );
}
