import React, { useMemo } from "react";
import { Controller } from "react-hook-form";
import { View } from "react-native";
import { LeafletView } from "react-native-leaflet-view";

declare type GeolocationInputProps = {
  control: any;
  name: string;
  defaultValue?: any;
  rules?: any;
};

export default function GeolocationInput({
  control,
  name,
  defaultValue,
  rules,
}: GeolocationInputProps) {
  const [isMapReady, setMapReady] = React.useState(false);
  const markerCoordinate = useMemo(
    () =>
      defaultValue || {
        lat: 37.78825,
        lng: -122.4324,
      },
    []
  );

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={markerCoordinate}
      rules={rules}
      render={({ field: { onChange } }) => (
        <View style={{ height: 200, borderRadius: 5 }}>
          <LeafletView
            onMessageReceived={(message) => {
              if (message.msg === "MAP_READY") setMapReady(true);

              if (isMapReady && message.event === "onMoveEnd") {
                // setTimeout(() => {
                // setMarker({
                //   lat: message.payload.mapCenterPosition.lat,
                //   lng: message.payload.mapCenterPosition.lng,
                // });
                // set to handleChange
                onChange({
                  lat: message.payload.mapCenterPosition.lat,
                  lng: message.payload.mapCenterPosition.lng,
                });
                // }, 3000);
              }
            }}
            mapMarkers={[
              {
                position: markerCoordinate,
                icon: "📍",
                size: [32, 32],
                iconAnchor: [0, 32],
              },
            ]}
            mapCenterPosition={markerCoordinate}
          />
        </View>
      )}
    />
  );
}
