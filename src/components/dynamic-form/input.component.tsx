import React from "react";
import { Controller } from "react-hook-form";
import { Text, TextInput, View } from "react-native";

declare type InputProps = {
  name: string;
  control: any;
  defaultValue?: any;
  rules?: any;
  placeholder?: string;
  secured?: boolean;
};

export default function Input({
  name,
  control,
  defaultValue,
  rules,
  placeholder,
  secured,
}: InputProps) {
  const [isFocused, setFocus] = React.useState(false);

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={rules}
      render={({ field: { onChange, onBlur, value } }) => (
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginTop: 10,
            marginBottom: 0,
            padding: 0,
            borderRadius: 5,
            backgroundColor: "#ffffff",
            borderWidth: isFocused ? 1.5 : 1,
            borderColor: isFocused ? "#404040" : "#ADADAD",
          }}
        >
          <TextInput
            style={{ flex: 1, padding: 12 }}
            onChangeText={(text) => onChange(text)}
            value={value}
            secureTextEntry={secured}
            // autoCompleteType="password"
            onFocus={() => setFocus(true)}
            onBlur={() => {
              onBlur();
              setFocus(false);
            }}
            placeholder={placeholder}
            placeholderTextColor="#ADADAD"
          />
        </View>
      )}
    />
  );
}
