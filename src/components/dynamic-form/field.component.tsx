import React from "react";
import { View } from "react-native";

declare type FieldProps = {
  flex?: boolean;
  style?: any;
  // isFocused: boolean;
  children: any;
};

export default function Field({ flex = true, style, children }: FieldProps) {
  let defaultStyle: any = {
    // backgroundColor: "#ffffff",
    // paddingLeft: 24,
    // paddingRight: 24,
    paddingBottom: 10,
    // borderColor: isFocused === true ? "#404040" : "#ADADAD",
  };

  if (flex)
    defaultStyle = {
      ...defaultStyle,
      flexDirection: "column",
      alignItems: "flex-start",
      justifyContent: "center",
    };

  if (style)
    defaultStyle = {
      ...defaultStyle,
      ...style,
    };

  return <View style={defaultStyle}>{children}</View>;
}
