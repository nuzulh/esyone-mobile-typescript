import React from "react";
import { Text, View } from "react-native";

declare type InfoProps = {
  value: string;
};

export default function Info({ value }: InfoProps) {
  if (!value) return null;

  return (
    <View>
      <Text>{value}</Text>
    </View>
  );
}
