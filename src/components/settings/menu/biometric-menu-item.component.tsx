import { Alert, Switch, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { localization, updateBiometricSettings } from "../../../helpers";
import { useBiometric } from "../../../hooks";
import { RootState } from "../../../models";

export function BiometricMenuItem() {
  const settings = useSelector((state: RootState) => state.esy.settings);
  const { validate } = useBiometric();
  const dispatch = useDispatch();

  const toggleBioPress = () => {
    if (settings.isBiometricEnabled) {
      dispatch(updateBiometricSettings(false));
      return;
    }

    validate((status) => {
      // console.log(status);
      if (status.isError)
        return Alert.alert(
          "Unable to enable biometric login",
          "Ensure biometric feature is enabled on your device"
        );

      // enable biometric
      dispatch(updateBiometricSettings(true));
    });
  };

  return (
    <View
      style={{
        flexDirection: "row",
        borderBottomColor: "#030D1233",
        borderBottomWidth: 0.5,
      }}
    >
      <View style={{ width: "60%", justifyContent: "center" }}>
        <Text
          style={{
            fontSize: 13,
            fontFamily: "Lato-Regular",
            color: "#030D12",
          }}
        >
          {localization.language.ScreenProfile_Biometric}
        </Text>
      </View>
      <View style={{ width: "40%", padding: 10, alignItems: "flex-end" }}>
        <Switch
          trackColor={{ false: "#A0A0A0", true: "#404040" }}
          thumbColor={settings.isBiometricEnabled ? "#F7F7FC" : "#F7F7FC"}
          ios_backgroundColor="#707070"
          style={{ transform: [{ scaleX: 0.8 }, { scaleY: 0.8 }] }}
          onValueChange={toggleBioPress}
          value={settings.isBiometricEnabled}
        />
      </View>
    </View>
  );
}
