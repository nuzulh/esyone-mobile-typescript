import { Switch, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  disableFcmAction,
  enableFcmAction,
  localization,
} from "../../../helpers";
import { RootState } from "../../../models";

export function PushNotificationMenuItem() {
  const settings = useSelector((state: RootState) => state.esy.settings);
  const dispatch = useDispatch();

  const togglePushNotification = () => {
    if (settings.isPushNotificationEnabled) {
      dispatch(disableFcmAction());
      return;
    }

    dispatch(enableFcmAction());
  };

  return (
    <View
      style={{
        flexDirection: "row",
        borderBottomColor: "#030D1233",
        borderBottomWidth: 0.5,
      }}
    >
      <View style={{ width: "60%", justifyContent: "center" }}>
        <Text
          style={{
            fontSize: 13,
            fontFamily: "Lato-Regular",
            color: "#030D12",
          }}
        >
          {localization.language.ScreenProfile_PushNotification}
        </Text>
      </View>
      <View style={{ width: "40%", padding: 10, alignItems: "flex-end" }}>
        <Switch
          trackColor={{ false: "#A0A0A0", true: "#404040" }}
          thumbColor={
            settings.isPushNotificationEnabled ? "#F7F7FC" : "#F7F7FC"
          }
          ios_backgroundColor="#707070"
          style={{ transform: [{ scaleX: 0.8 }, { scaleY: 0.8 }] }}
          onValueChange={togglePushNotification}
          value={settings.isPushNotificationEnabled}
        />
      </View>
    </View>
  );
}
