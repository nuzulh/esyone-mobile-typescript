import { useContext } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { localization, updateLanguageAction } from "../../../helpers";
import { RootState } from "../../../models";
import { Services } from "../../../services";

export function LanguageMenuItem() {
  const services = useContext(Services);
  const settings = useSelector((state: RootState) => state.esy.settings);
  const dispatch = useDispatch();

  return (
    <TouchableOpacity
      onPress={() =>
        dispatch(updateLanguageAction(settings.language === "de" ? "en" : "de"))
      }
      style={{
        flexDirection: "row",
        borderBottomColor: "#030D1233",
        borderBottomWidth: 0.5,
      }}
    >
      <View style={{ width: "60%", justifyContent: "center" }}>
        <Text
          style={{
            fontSize: 13,
            fontFamily: "Lato-Regular",
            color: "#030D12",
          }}
        >
          {localization.language.ScreenProfile_Language}
        </Text>
      </View>
      <View
        style={{
          width: "40%",
          padding: 20,
          justifyContent: "flex-end",
          alignItems: "flex-end",
          flexDirection: "row",
        }}
      >
        <Text
          style={{
            fontSize: 13,
            fontFamily: "Lato-Bold",
            color: "#404040",
            marginRight: 15,
          }}
        >
          {settings.language == "de" ? "Deutsch" : "English"}
        </Text>
        <Image
          source={services.assetsService.getIcons("iconChevronRightBlack")}
          style={{ height: 12, width: 12 }}
        />
      </View>
    </TouchableOpacity>
  );
}
