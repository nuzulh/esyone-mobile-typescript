import { useContext } from "react";
import { Alert, Image, Text, TouchableOpacity, View } from "react-native";
import { clearFileCache, localization } from "../../../helpers";
import { Services } from "../../../services";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../models";

export function ClearCachedMenuItem() {
  const settings = useSelector((state: RootState) => state.esy.settings);
  const dispatch = useDispatch();
  const services = useContext(Services);

  return (
    <TouchableOpacity
      onPress={() => {
        Alert.alert(
          localization.language.ScreenProfile_ClearCachePrompt,
          "",
          [
            {
              text: localization.language.ScreenProfile_ClearCachePrompt_No,
              onPress: () => {},
            },
            {
              text: localization.language.ScreenProfile_ClearCachePrompt_Yes,
              onPress: () => dispatch(clearFileCache()),
            },
          ],
          { cancelable: true }
        );
      }}
    >
      <View
        style={{
          flexDirection: "row",
          borderBottomColor: "#030D1233",
          borderBottomWidth: 0.5,
        }}
      >
        <View style={{ width: "60%", justifyContent: "center" }}>
          <Text
            style={{
              fontSize: 13,
              fontFamily: "Lato-Regular",
              color: "#030D12",
            }}
          >
            {localization.language.ScreenProfile_ClearCache}
          </Text>
        </View>
        <View style={{ width: "40%", padding: 20, alignItems: "flex-end" }}>
          <Image
            source={services.assetsService.getIcons("iconChevronRightBlack")}
            style={{ height: 12, width: 12 }}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
}
