import { useContext } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { HOME_STACK, localization, navigateToAction } from "../../../helpers";
import { Services } from "../../../services";
import { RootState } from "../../../models";

export function ChangePasswordMenuItem() {
  const settings = useSelector((state: RootState) => state.esy.settings);

  const services = useContext(Services);
  const dispatch = useDispatch();

  return (
    <TouchableOpacity
      onPress={() =>
        dispatch(
          navigateToAction({
            navigateTo: {
              screen: HOME_STACK.CHANGE_PASSWORD_SCREEN,
            },
          })
        )
      }
    >
      <View
        style={{
          flexDirection: "row",
          borderBottomColor: "#030D1233",
          borderBottomWidth: 0.5,
        }}
      >
        <View style={{ width: "60%", justifyContent: "center" }}>
          <Text
            style={{
              fontSize: 13,
              fontFamily: "Lato-Regular",
              color: "#030D12",
            }}
          >
            {localization.language.ScreenProfile_ChangePassword}
          </Text>
        </View>
        <View style={{ width: "40%", padding: 20, alignItems: "flex-end" }}>
          <Image
            source={services.assetsService.getIcons("iconChevronRightBlack")}
            style={{ height: 12, width: 12 }}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
}
