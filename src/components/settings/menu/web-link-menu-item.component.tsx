import { useContext } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { useDispatch } from "react-redux";
import { HOME_STACK, localization, navigateToAction } from "../../../helpers";
import { Services } from "../../../services";

declare type WebLinkMenuItemProps = {
  title: string;
  url: string;
};



export function WebLinkMenuItem({ title, url }: WebLinkMenuItemProps) {
  const services = useContext(Services);
  const dispatch = useDispatch();

  return (
    <TouchableOpacity
      onPress={() =>
        dispatch(
          navigateToAction({
            navigateTo: {
              screen: HOME_STACK.WEBVIEW_SCREEN,
              params: {
                url,
                title,
              },
            },
          })
        )
      }
    >
      <View
        style={{
          flexDirection: "row",
          borderBottomColor: "#030D1233",
          borderBottomWidth: 0.5,
        }}
      >
        <View style={{ width: "60%", justifyContent: "center" }}>
          <Text
            style={{
              fontSize: 13,
              fontFamily: "Lato-Regular",
              color: "#030D12",
            }}
          >
            {title}
          </Text>
        </View>
        <View style={{ width: "40%", padding: 20, alignItems: "flex-end" }}>
          <Image
            source={services.assetsService.getIcons("iconChevronRightBlack")}
            style={{ height: 12, width: 12 }}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
}
