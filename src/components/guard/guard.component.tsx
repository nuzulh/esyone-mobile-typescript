export declare type GuardComponentProps = {
  navigation: any;
  component: any;
};

export function GuardComponent({ component, ...props }: GuardComponentProps) {
  const Component = component;

  return <Component {...props} />;
}
