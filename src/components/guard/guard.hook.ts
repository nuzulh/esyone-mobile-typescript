import { useDispatch, useSelector } from "react-redux";
// import { EsyState } from "../../models/store";
import { useNavigation } from "@react-navigation/native";
import { Services } from "../../services";
import { useContext } from "react";
import { RootState } from "../../models";
import { usePromise } from "../../hooks";

export function useGuard() {
  const dispatch = useDispatch();
  const services = useContext(Services);
  // const authStatus = useSelector((state: Store) => state.auth);
  const currentLink = useSelector(
    (state: RootState) => state.esy.linking.currentLink
  );
  // const currentFlow = useSelector((state: Store) => state.currentFlow);
  const navigation: any = useNavigation();
  const { result, error } = usePromise(
    () => services.guardService.evaluate(currentLink),
    null,
    [currentLink]
  );

  dispatch(result);

  // if (result && !result.isPassed)
  //   navigation.navigate(
  //     result.navigateTo.stack,
  //     result.navigateTo.screen
  //       ? { screen: result.navigateTo.screen }
  //       : result.navigateTo.params,
  //     result.navigateTo.screen ? null : result.navigateTo.params
  //   );

  if (error) services.logService.error(error);

  // if(currentLink && currentLink !== "") handleLinking(currentLink)
  return { result, error };
}
