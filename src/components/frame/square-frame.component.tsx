import { StyleProp, View, ViewStyle } from "react-native";

export declare type SquareFrameProps = {
  isVisible?: boolean;
  size?: number;
  roundEdge?: "none" | "sm" | "md" | "lg",
  style?: StyleProp<ViewStyle>;
  children: any;
};

export function SquareFrame({ isVisible = true, size = 52, roundEdge = "none", style = {}, children }: SquareFrameProps) {
  if(!isVisible) return null;

  return (
    <View
      style={{
        height: size,
        width: size,
        borderRadius: roundEdge === "none" ? 0 : roundEdge === "sm" ? 2 : roundEdge === "md" ? 4 : 8,
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
        ...(style as any)
      }}
    ></View>
  );
}
