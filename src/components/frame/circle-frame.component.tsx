import { StyleProp, View, ViewStyle } from "react-native";

export declare type CircleFrameProps = {
  isVisible?: boolean;
  size?: number;
  style?: StyleProp<ViewStyle>
  children: any;
}

export function CircleFrame({ isVisible = true, size = 52, style = {}, children }: CircleFrameProps) {
  if(!isVisible) return null;

  return (
    <View
      style={{
        height: size,
        width: size,
        borderRadius: size,
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
        ...(style as any),
      }}
    >
      {children}
    </View>
  );
}
