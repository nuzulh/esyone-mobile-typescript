import { View, StatusBar, SafeAreaView as RNSafeAreaView } from "react-native";

declare type SafeAreaViewProps = {
  color?: string;
};

export function SafeAreaView({ color = "white" }: SafeAreaViewProps) {
  return (
    <View>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <RNSafeAreaView style={{ backgroundColor: color }} />
    </View>
  );
}
