import { useContext } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { createGetFileIcon, formatTimelineDate } from "../helpers";
import { useEsyState } from "../hooks";
import { MetaFile, Services } from "../services";

export function DocumentListItem({
  file,
  children,
  style = {},
  onPress,
}: {
  file: MetaFile;
  children?: any;
  style?: any;
  onPress: (file: MetaFile) => void;
}) {
  const isImage = !(
    file.fileName.toLowerCase().substring(file.fileName.lastIndexOf(".") + 1) !=
      "png" &&
    file.fileName.toLowerCase().substring(file.fileName.lastIndexOf(".") + 1) !=
      "jpg" &&
    file.fileName.toLowerCase().substring(file.fileName.lastIndexOf(".") + 1) !=
      "jpeg"
  );
  const language = useEsyState((state) => state.settings.language);
  const services = useContext(Services);
  const getFileIcon = createGetFileIcon(services.assetsService);

  return (
    <TouchableOpacity
      onPress={() => onPress(file)}
      style={{
        flex: 1,
        // marginLeft: 20,
        // marginRight: 20,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        gap: 10,
        // marginVertical: 5,
        borderBottomColor: "#030D1233",
        borderBottomWidth: 0.5,
        ...style,
      }}
    >
      <View style={{ paddingVertical: 10, paddingLeft: 0, paddingRight: 20 }}>
        {!isImage ? (
          <Image
            source={getFileIcon(file.fileName)}
            style={{ height: 40, width: 40, resizeMode: "contain" }}
          />
        ) : (
          <Image
            source={services.assetsService.getIcons("iconImage")}
            style={{ height: 40, width: 40, resizeMode: "contain" }}
          />
        )}
      </View>
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          gap: 5,
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <View style={{ marginVertical: 3, flexShrink: 1 }}>
          <Text
            numberOfLines={1}
            style={{ fontSize: 13, fontFamily: "Lato-Bold", color: "#030D12" }}
          >
            {file.fileName}
          </Text>
        </View>
        {children}
      </View>
    </TouchableOpacity>
  );
}
