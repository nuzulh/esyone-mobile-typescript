import { useContext } from "react";
import { Image, Text, View } from "react-native";
import { Services } from "../services";

export function Loading({ text, images, CH, CW, RM }: any) {
  const services = useContext(Services);

  return (
    <View
      style={{
        position: "absolute",
        height: "100%",
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#00000099",
        zIndex: 200,
      }}
    >
      <View
        style={{
          backgroundColor: "#ffffff",
          padding: 5,
          borderRadius: 10,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {images ? (
          <Image
            source={images}
            style={{ width: CW, height: CH, resizeMode: RM }}
          />
        ) : (
          <Image
            source={services.assetsService.getIcons("loading")}
            style={{ width: 80, height: 80 }}
          />
        )}
        {text && <Text>{text}</Text>}
      </View>
    </View>
  );
}
