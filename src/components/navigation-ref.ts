import {
  createNavigationContainerRef,
  NavigationContainerRef,
  NavigationContainerRefWithCurrent,
} from "@react-navigation/native";

export declare type NavigationRef = NavigationContainerRefWithCurrent<any> & {
  navigate(stack: string, screen?: any, params?: any);
};

export const navigationRef: NavigationRef = createNavigationContainerRef();
