import { useContext, useState } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { Services } from "../services";

declare type FlagProps = {
  countryId: string;
  onChange: (countryId: string) => void;
  style?: any;
};

export function Flag({ countryId, onChange, style }: FlagProps) {
  const services = useContext(Services);
  const [changeFlag, setChangeFlag] = useState(false);

  // const pressFlag = () => {
  //   changeFlag == false ? setChangeFlag(true) : setChangeFlag(false);
  // };

  const onChangeLanguage = (value) => {
    onChange(value);
    setChangeFlag(false);
  };

  return (
    <View
      style={[
        {
          position: "absolute",
          top: 25,
          zIndex: 102,
        },
        style ? style : {},
      ]}
    >
      <View
        style={{
          padding: 15,
          flexDirection: "row",
        }}
      >
        {changeFlag == false ? (
          <TouchableOpacity
            onPress={() => setChangeFlag(!changeFlag)}
            style={{
              flexDirection: "row",
              borderRadius: 10,
              borderWidth: 1,
              borderColor: "#E0E0E0",
              alignSelf: "flex-start",
              padding: 8,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#FFF",
            }}
          >
            <Image
              source={
                countryId === "en"
                  ? services.assetsService.getIcons("iconEn")
                  : services.assetsService.getIcons("iconDe")
              }
              style={{
                height: 20,
                width: 20,
                borderRadius: 10,
              }}
            />
            <View style={{ marginLeft: 8 }}>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: "Lato-Bold",
                  color: "black",
                }}
              >
                {countryId === "en" ? "EN" : "DE"}
              </Text>
            </View>
          </TouchableOpacity>
        ) : (
          <FlagOptions onPress={(value) => onChangeLanguage(value)} />
        )}
      </View>
    </View>
  );
}

function FlagOptions({ onPress }: { onPress: (countryId: string) => void; }) {
  const services = useContext(Services);
  return (
    <View style={{ flexDirection: "row" }}>
      <FlagOption
        countryId="de"
        icon={services.assetsService.getIcons("iconDe")}
        label="DE"
        onPress={onPress}
      />
      <FlagOption
        countryId="en"
        icon={services.assetsService.getIcons("iconEn")}
        label="EN"
        onPress={onPress}
      />
    </View>
  );
}

function FlagOption({
  countryId,
  icon,
  label,
  onPress,
}: {
  countryId: string;
  icon: any;
  label: string;
  onPress: (countryId: string) => void;
}) {
  return (
    <TouchableOpacity
      onPress={() => onPress(countryId)}
      style={{
        marginRight: 8,
        flexDirection: "row",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#E0E0E0",
        alignSelf: "flex-start",
        padding: 8,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#FFF",
      }}
    >
      <Image
        source={icon}
        style={{
          height: 20,
          width: 20,
          borderRadius: 10,
        }}
      />
      <View style={{ marginLeft: 8 }}>
        <Text
          style={{
            fontSize: 14,
            fontFamily: "Lato-Bold",
            color: "black",
          }}
        >
          {label}
        </Text>
      </View>
    </TouchableOpacity>
  );
}
