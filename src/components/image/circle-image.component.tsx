import { ImageStyle, StyleProp } from "react-native";
import FastImage from "react-native-fast-image";

export declare type CircleImageProps = {
  isVisible?: boolean;
  size?: number;
  style?: StyleProp<ImageStyle>;
  source: any;
};

export function CircleImage({
  isVisible = true,
  size = 48,
  style = {},
  source,
}: CircleImageProps) {
  if (!isVisible) return null;

  return (
    <FastImage
      source={source}
      style={{
        height: size,
        width: size,
        borderRadius: size,
        ...(style as any),
      }}
      resizeMode={FastImage.resizeMode.cover}
    />
  );
}
