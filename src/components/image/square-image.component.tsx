import { ImageStyle, StyleProp } from "react-native";
import FastImage from "react-native-fast-image";

export declare type SquareImageProps = {
  isVisible?: boolean;
  size?: number;
  roundEdge?: "none" | "sm" | "md" | "lg";
  style?: StyleProp<ImageStyle>;
  source: any;
};

export function SquareImage({
  isVisible = true,
  size = 48,
  roundEdge = "none",
  style = {},
  source,
}: SquareImageProps) {
  if(!isVisible) return null;

  return (
    <FastImage
      source={source}
      style={{
        height: size,
        width: size,
        borderRadius: roundEdge === "none" ? 0 : roundEdge === "sm" ? 2 : roundEdge === "md" ? 4 : 8,
        ...(style as any)
      }}
      resizeMode={FastImage.resizeMode.contain}
    />
  );
}
