import { useContext, useState } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { Overlay } from "react-native-elements";
import { selectCamera } from "../../helpers/natives/camera-selector";
import { selectFiles } from "../../helpers/natives/file-selector";
import { selectPhotos } from "../../helpers/natives/photo-selector";
import { MetaFile, Services } from "../../services";
import { localization } from "../../helpers";

declare type AttachmentOverlayProps = {
  isVisible: boolean;
  onClose: () => void;
  onSelect: (files: MetaFile[]) => void;
  onError: (error: Error) => void;
};

export function AttachmentOverlay({
  isVisible,
  onClose,
  onSelect,
  onError,
}: AttachmentOverlayProps) {
  const services = useContext(Services);
  return (
    <Overlay
      isVisible={isVisible}
      overlayStyle={{
        width: "90%",
        position: "absolute",
        bottom: 20,
        justifyContent: "flex-end",
        borderRadius: 20,
        padding: 24,
      }}
      onBackdropPress={onClose}
    >
      <Text
        style={{
          color: "#404040",
          width: "100%",
          fontFamily: "Lato-Bold",
          fontSize: 20,
        }}
      >
        {localization.language.Addattachment}
      </Text>
      <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
        <TouchableOpacity
          onPress={() =>
            selectFiles((files: MetaFile[], error) => {
              if (error) {
                onError(error);
                return;
              }

              onSelect(files);
            })
          }
          style={{
            padding: 10,
            marginHorizontal: 5,
            alignItems: "center",
            flex: 1,
          }}
        >
          <Image
            source={services.assetsService.getIcons("iconFileBlack")}
            style={{ height: 20, width: 20, marginVertical: 10 }}
          />
          <Text
            style={{
              fontSize: 13,
              fontFamily: "Lato-Regular",
              color: "#030D12B3",
            }}
          >
            {localization.language.Files}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            selectPhotos((files: MetaFile[], error) => {
              if (error) {
                onError(error);
                return;
              }

              onSelect(files);
            })
          }
          style={{
            padding: 10,
            marginHorizontal: 5,
            alignItems: "center",
            flex: 1,
          }}
        >
          <Image
            source={services.assetsService.getIcons("iconImageBlack")}
            style={{ height: 20, width: 20, marginVertical: 10 }}
          />
          <Text
            style={{
              fontSize: 13,
              fontFamily: "Lato-Regular",
              color: "#030D12B3",
            }}
          >
            {localization.language.Gallery}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            selectCamera((files: MetaFile[], error) => {
              if (error) {
                onError(error);
                return;
              }

              onSelect(files);
            })
          }
          style={{
            padding: 10,
            marginHorizontal: 5,
            alignItems: "center",
            flex: 1,
          }}
        >
          <Image
            source={services.assetsService.getIcons("iconCamera")}
            style={{ height: 20, width: 20, marginVertical: 10 }}
          />
          <Text
            style={{
              fontSize: 13,
              fontFamily: "Lato-Regular",
              color: "#030D12B3",
            }}
          >
            {localization.language.Camera}
          </Text>
        </TouchableOpacity>

        {/* {scannerPress ? (
          <TouchableOpacity
            onPress={scannerPress}
            style={{
              padding: 10,
              marginHorizontal: 5,
              alignItems: "center",
              flex: 1,
            }}
          >
            <Image
              source={require("../assets/icon/icon-scanner.png")}
              style={{ height: 20, width: 20, marginVertical: 10 }}
            />
            <Text
              style={{
                fontSize: 13,
                fontFamily: "Lato-Regular",
                color: "#030D12B3",
              }}
            >
              Camera
            </Text>
          </TouchableOpacity>
        ) : null} */}
      </View>
    </Overlay>
  );
}
