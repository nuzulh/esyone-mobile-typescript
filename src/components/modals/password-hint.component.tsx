import {
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import Modal from "react-native-modal";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faWindowClose } from "@fortawesome/free-solid-svg-icons";
import { localization } from "../../helpers";

export declare type PasswordHintModalProps = {
  isVisible: boolean;
  setIsVisible: any;
};

export function PasswordHintModal({
  isVisible,
  setIsVisible,
}: PasswordHintModalProps) {
  return (
    <Modal
      style={{ flex: 1, margin: 0, backgroundColor: "rgba(0,0,0,0.7)" }}
      onBackdropPress={() => setIsVisible(false)}
      isVisible={isVisible}
    >
      <View
        style={{ borderRadius: 10, margin: 10, backgroundColor: "#fafafa" }}
      >
        <View style={{ padding: 10, alignItems: "center" }}>
          <Text
            style={{
              marginLeft: 10,
              fontSize: 18,
              fontFamily: "Lato-Bold",
              color: "#404040",
            }}
          >
            {localization.language.Passwordhint}
          </Text>
        </View>
        <View
          style={{
            flexDirection: "column",
            borderTopColor: "#404040",
            borderTopWidth: 1,
            marginLeft: 20,
            marginRight: 20,
          }}
        >
          <View style={{ margin: 10 }}>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  padding: 5,
                  color: "#404040",
                }}
              >
                •
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  padding: 5,
                  color: "#404040",
                }}
              >
                {localization.language.Thepasswordmusthaveatleast}
              </Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                  color: "#404040",
                }}
              >
                1.
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  padding: 5,
                  color: "#404040",
                }}
              >
                {localization.language.Itmustcontainanuppercaseletter}
              </Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                  color: "#404040",
                }}
              >
                2.
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  padding: 5,
                  color: "#404040",
                }}
              >
                {localization.language.Itmustcontainanlowercaseletter}
              </Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                  color: "#404040",
                }}
              >
                3.
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  padding: 5,
                  color: "#404040",
                }}
              >
                {localization.language.Itmustcontainannumber}
              </Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                  color: "#404040",
                }}
              >
                4.
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  padding: 5,
                  color: "#404040",
                }}
              >
                {localization.language.Itmustcontainanumlautspecialcharacter}
              </Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  padding: 5,
                  color: "#404040",
                }}
              >
                •
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  padding: 5,
                  color: "#404040",
                }}
              >
                {localization.language.Thepasswordmustnotbeatrivialpassword}
              </Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                  color: "#404040",
                }}
              >
                1.
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Regular",
                  padding: 5,
                  color: "#404040",
                }}
              >
                {localization.language.LengthPassword}
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            justifyContent: "flex-end",
            alignItems: "center",
            marginBottom: 10,
          }}
        >
          <TouchableOpacity
            onPress={() => setIsVisible(false)}
          >
            <FontAwesomeIcon icon={faWindowClose} size={40} color="#404040" />
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}
