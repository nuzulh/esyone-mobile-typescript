import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useNavigation } from "@react-navigation/native";
import { useContext } from "react";
import { Image, TouchableOpacity, View } from "react-native";
import { Services } from "../../services";

export declare type OfflineHeaderProps = {
  // applicationIcon: any;
  // offlineIcon: any;
  showLogo?: boolean;
  showBackButton?: boolean;
  children?: any; // when not offline
  control?: any;
};

export function HeaderOffline({
  // applicationIcon,
  // offlineIcon,
  showLogo = true,
  showBackButton = false,
  children,
  control,
}: OfflineHeaderProps) {
  const navigation = useNavigation();
  const services = useContext(Services);

  return (
    <>
      <View
        style={{
          width: showLogo ? "30%" : "70%",
          justifyContent: "flex-start",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        {showBackButton ? (
          <TouchableOpacity
            style={{
              width: "10%",
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => navigation.goBack()}
          >
            <FontAwesomeIcon icon={faArrowLeft} color={"#404040"} size={18} />
          </TouchableOpacity>
        ) : null}
        {children}
      </View>
      {showLogo ? (
        <View
          style={{
            width: "40%",
            justifyContent: "center",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Image
            source={services.assetsService.getIcons("logoOriginal")}
            style={{ height: 48, width: 48, resizeMode: "contain" }}
          />
        </View>
      ) : null}
      <View
        style={{
          width: "30%",
          justifyContent: "flex-end",
          flexDirection: "row",
          gap: 5,
          alignItems: "center",
        }}
      >
        {control}
        <Image
          source={services.assetsService.getIcons("iconOffline")}
          style={{ height: 20, width: 20, resizeMode: "contain" }}
        />
      </View>
    </>
  );
}
