import { Platform, View } from "react-native";
import { useSelector } from "react-redux";
import { RootState } from "../../models";

export declare type HeaderProps = {
  children: any;
};

export function Header({ children }: HeaderProps) {
  return (
    <View
      style={{
        padding: 15,
        height: 70,
        backgroundColor: "#FFFFFF",
        flexDirection: "row",
        justifyContent: "space-between",
        shadowColor: "#000",
        shadowOffset: { width: 1, height: Platform.OS == "ios" ? 5 : 1 },
        shadowOpacity: Platform.OS == "ios" ? 0.1 : 0.4,
        shadowRadius: 3,
        elevation: Platform.OS == "ios" ? 1 : 5,
        zIndex: 101,
      }}
    >
      {children}
    </View>
  );
}

Header.Offline = ({ children }) => {
  const isOffline = useSelector(
    (state: RootState) => state.esy.appState?.isOffline
  );

  if (!isOffline) return null;

  return children;
};

Header.Online = ({ children }) => {
  const isOffline = useSelector(
    (state: RootState) => state.esy.appState?.isOffline
  );

  if (isOffline) return null;

  return children;
};
