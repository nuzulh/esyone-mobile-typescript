import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useNavigation } from "@react-navigation/native";
import { useContext } from "react";
import { Image, TouchableOpacity, View } from "react-native";
import { Services } from "../../services";

export declare type HeaderOnlineProps = {
  // applicationIcon: any;
  showLogo?: boolean;
  showBackButton?: boolean;
  children?: any;
  control?: any;
};

export function HeaderOnline({
  showLogo = true,
  showBackButton = false,
  children,
  control,
}: HeaderOnlineProps) {
  const navigation = useNavigation();
  const services = useContext(Services);

  return (
    <>
      <View
        style={{
          width: showLogo ? "30%" : "70%",
          justifyContent: "flex-start",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        {showBackButton ? (
          <TouchableOpacity
            style={{
              width: "10%",
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => navigation.goBack()}
          >
            <FontAwesomeIcon icon={faArrowLeft} color={"#404040"} size={18} />
          </TouchableOpacity>
        ) : null}
        {children}
      </View>
      {showLogo ? (
        <View
          style={{
            width: "40%",
            justifyContent: "center",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Image
            source={services.assetsService.getIcons("logoOriginal")}
            style={{ height: 48, width: 48, resizeMode: "contain" }}
          />
        </View>
      ) : null}
      <View
        style={{
          width: "30%",
          justifyContent: "flex-end",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        {control}
      </View>
    </>
  );

  // return (
  //   <View
  //     style={{
  //       width: "100%",
  //       justifyContent: "center",
  //       flexDirection: "row",
  //       alignItems: "center",
  //     }}
  //   >
  //     <Image
  //       source={applicationIcon}
  //       style={{ height: 48, width: 48, resizeMode: "contain" }}
  //     />
  //   </View>
  // );
}
