import { useMemo, useRef, useState } from "react";
import { Dimensions, FlatList, View } from "react-native";

declare type CarouselProps = {
  data: any[];
  hasDots?: boolean;
  keyExtractor: (item: any, index: number) => string;
  renderItem: (item: any) => JSX.Element;
};

export function Carousel({
  data,
  hasDots,
  keyExtractor,
  renderItem,
}: CarouselProps) {
  const windowWidth = useMemo(() => Dimensions.get("window").width, []);
  const [sliderState, setSliderState] = useState({
    item: 0,
    offset: 0,
  });
  const scrollViewRef = useRef(null);

  const setSliderPage = (event) => {
    const item = Math.round(
      event.nativeEvent.contentOffset.x / (windowWidth - 48)
    );

    setSliderState({
      item: item,
      offset: item * windowWidth,
    });
  };

  return (
    <>
      <FlatList
        style={{ marginBottom: 16 }}
        data={data}
        horizontal
        showsHorizontalScrollIndicator={false}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        onScroll={(event) => {
          setSliderPage(event);
        }}
        ref={scrollViewRef}
        getItemLayout={(_, index) => ({
          length: windowWidth,
          offset: windowWidth * index,
          index,
        })}
        contentContainerStyle={{ paddingHorizontal: 16 }}
      />
      {hasDots ? (
        <View
          style={{
            width: "100%",
            alignContent: "center",
            justifyContent: "center",
            flexDirection: "row",
          }}
        >
          {data.map((_, i) => {
            return (
              <View
                key={i}
                style={{
                  marginLeft: 10,
                  height: 10,
                  width: 10,
                  borderRadius: 10 / 2,
                  backgroundColor: "#404040",
                  opacity: sliderState.item === i ? 1 : 0.2,
                }}
              />
            );
          })}
        </View>
      ) : null}
    </>
  );
}
