import ErrorMessage from "../dynamic-form/error.component";
import Field from "../dynamic-form/field.component";
import Input from "../dynamic-form/input.component";
import Label from "../dynamic-form/label.component";
import TextArea from "../dynamic-form/text-area-input.component";
import { Email } from "./email.component";
import { Password } from "./password.component";

export const Form = {
  Field,
  Label,
  Input,
  TextArea,
  Email,
  Password,
  ErrorMessage,
};
