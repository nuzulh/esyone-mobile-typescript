import { validateEmail } from "../../helpers";
import Input from "../dynamic-form/input.component";

declare type EmailProps = {
  name: string;
  control: any;
  defaultValue?: any;
  rules?: any;
  required?: boolean;
  placeholder?: string;
};

export function Email({
  name,
  control,
  defaultValue,
  required,
  rules = { validate: {} },
  placeholder,
}: EmailProps) {
  return (
    <Input
      control={control}
      name={name}
      defaultValue={defaultValue}
      placeholder={placeholder}
      rules={{
        ...rules,
        validate: {
          ...rules.validate,
          validEmail: (value) =>
            !required
              ? true
              : validateEmail(value)
              ? true
              : "Invalid email address",
        },
      }}
    />
  );
}
