import { faEye, faEyeSlash, faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import React from "react";
import { Controller } from "react-hook-form";
import { Text, TextInput, TouchableOpacity, View } from "react-native";

declare type PasswordProps = {
  name: string;
  control: any;
  defaultValue?: any;
  rules?: any;
  placeholder?: string;
  icon?: boolean;
  setValueicon?: any;
};

export function Password({
  name,
  control,
  defaultValue,
  rules,
  placeholder,
  icon,
  setValueicon,
}: PasswordProps) {
  const [isFocused, setFocus] = React.useState(false);
  const [hidePassword, setHidePassword] = React.useState(true);

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={rules}
      render={({ field: { onChange, onBlur, value } }) => (
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginTop: 10,
            marginBottom: 0,
            padding: 0,
            borderRadius: 5,
            backgroundColor: "#ffffff",
            borderWidth: isFocused ? 1.5 : 1,
            borderColor: isFocused ? "#404040" : "#ADADAD",
          }}
        >
          <TextInput
            style={{ flex: 1, padding: 12 }}
            onChangeText={(text) => onChange(text)}
            value={value}
            secureTextEntry={hidePassword}
            // autoCompleteType="password"
            onFocus={() => setFocus(true)}
            onBlur={() => {
              onBlur();
              setFocus(false);
            }}
            placeholder={placeholder}
            placeholderTextColor="#ADADAD"
          />
          {icon && (
            <TouchableOpacity
              onPress={() => setValueicon(true)}
              style={{ paddingRight: 20 }}
            >
              <FontAwesomeIcon icon={faInfoCircle} size={24} color="gray" />
            </TouchableOpacity>
          )}

          <TouchableOpacity
            onPress={() => setHidePassword(!hidePassword)}
            style={{ paddingRight: 20 }}
          >
            {hidePassword ? (
              <FontAwesomeIcon icon={faEye} size={24} color="gray" />
            ) : (
              <FontAwesomeIcon icon={faEyeSlash} size={24} color="gray" />
            )}
          </TouchableOpacity>
        </View>
      )}
    />
  );
}
