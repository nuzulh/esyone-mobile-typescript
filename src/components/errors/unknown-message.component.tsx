import { Text, View } from "react-native";

export function UnknownMessage() {
  return (
    <View>
      <Text>Oops... Cannot Parse Message!</Text>
    </View>
  );
}
