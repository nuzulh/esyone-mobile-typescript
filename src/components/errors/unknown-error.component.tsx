import { Text, View } from "react-native";
import { SafeAreaView } from "../safe-area-view.component";

export function UnknownError() {
  return (
    <View>
      <SafeAreaView />
      <Text>Oops... Something wrong!</Text>
    </View>
  );
}
