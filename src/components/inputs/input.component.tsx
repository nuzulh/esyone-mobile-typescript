import { useState } from "react";
import { TextInput } from "react-native";

export declare type InputProps = {
  type: "text" | "password" | "email" | "number";
  defaultValue?: any;
  placeholder?: string;
  onChange?: (value: any) => void;
  style?: any;
  clearButtonMode?: "never" | "while-editing" | "unless-editing" | "always";
  ref?: React.LegacyRef<TextInput>;
};

export function Input({
  type,
  defaultValue,
  placeholder,
  onChange,
  style = {},
  clearButtonMode,
  ref,
}: InputProps) {
  const [value, setValue] = useState(defaultValue);

  const validate = (value: any) => {
    if (onChange) onChange(value);
  };

  return (
    <TextInput
      style={{
        backgroundColor: "#fff",
        borderWidth: 1,
        borderColor: "#ADADAD",
        height: 50,
        padding: 10,
        borderRadius: 10,
        ...style,
      }}
      onChangeText={validate}
      value={value}
      placeholder={placeholder}
      placeholderTextColor={"#030D124D"}
      clearButtonMode={clearButtonMode}
      ref={ref}
    />
  );
}
