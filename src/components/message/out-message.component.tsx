import { useContext } from "react";
import { FlatList, Image, Text, TouchableOpacity, View } from "react-native";
import FastImage from "react-native-fast-image";
import ParsedText from "react-native-parsed-text";
import { useDispatch } from "react-redux";
import { formatTimelineDate, previewAttachmentAction } from "../../helpers";
import { useEsyState, useMessageParser } from "../../hooks";
import { Akte, Message } from "../../models";
import { Services } from "../../services";
import { UnknownMessage } from "../errors";
import { Attachment } from "./attachment.component";
import { Avatar } from "./avatar.component";

declare type OutMessageProps = {
  akte: Akte;
  message: Message;
  isSelected?: boolean;
  onLongPress: () => void;
};

export function OutMessage({
  akte,
  message,
  isSelected,
  onLongPress,
}: OutMessageProps) {
  const dispatch = useDispatch();
  const services = useContext(Services);
  const { specs } = useMessageParser();
  const language = useEsyState((state) => state.settings.language);
  const account = useEsyState((state) => state.auth.accounts[akte.accountId]);

  if (!message) return <UnknownMessage />;

  return (
    <TouchableOpacity
      onLongPress={onLongPress}
      style={
        isSelected
          ? {
              flexDirection: "row",
              margin: 8,
              padding: 5,
              backgroundColor: "#429F9833",
            }
          : { flexDirection: "row", margin: 8, padding: 5 }
      }
    >
      <View style={{ width: "85%", height: "100%" }}>
        <View
          style={{ padding: 10, backgroundColor: "#F5F5F5", borderRadius: 8 }}
        >
          <View
            style={{
              flexDirection: "row",
              marginVertical: 5,
              marginHorizontal: 5,
            }}
          >
            {message.attachments && message.attachments.length > 0 ? (
              <View
                style={{
                  padding: 7,
                  marginRight: 5,
                  marginBottom: 10,
                  borderRadius: 3,
                  alignItems: "center",
                  backgroundColor: "#030D1226",
                }}
              >
                <Image
                  source={services.assetsService.getIcons("iconAttachment")}
                  style={{ height: 20, width: 20 }}
                />
              </View>
            ) : null}
          </View>

          {message.header != "-" ? (
            <View style={{ marginBottom: 10, marginHorizontal: 5 }}>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Bold",
                  color: "#030D12B3",
                }}
              >
                {message.header}
              </Text>
            </View>
          ) : null}

          {message.message ? (
            <View style={{ marginBottom: 10, marginHorizontal: 5 }}>
              <ParsedText
                style={{
                  fontSize: 14,
                  fontFamily: "Lato-Regular",
                  color: "#030D1280",
                  textAlign: "justify",
                }}
                parse={specs}
              >
                {message.message}
              </ParsedText>
            </View>
          ) : null}

          <FlatList
            key={"#"}
            style={{ marginVertical: 10 }}
            numColumns={2}
            data={message.attachments || []}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (
              <Attachment
                attachment={item}
                onPress={(attachment) =>
                  dispatch(previewAttachmentAction(akte, attachment))
                }
              />
            )}
          />

          <View
            style={{
              justifyContent: "flex-end",
              alignItems: "flex-end",
              flexDirection: "row",
            }}
          >
            {message.isFavorite == true ? (
              <Image
                source={services.assetsService.getIcons("iconStar")}
                style={{ height: 12, width: 12, marginHorizontal: 5 }}
              />
            ) : null}
            <Text
              style={{
                fontSize: 10,
                fontFamily: "Lato-Regular",
                color: "#404040",
              }}
            >
              {formatTimelineDate(language, message.creationDate)}
            </Text>
          </View>
        </View>
      </View>
      <Avatar account={account} message={message} isOwnMessage={true} />
    </TouchableOpacity>
  );
}
