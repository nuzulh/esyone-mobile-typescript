import { useContext } from "react";
import { Image, Text, View } from "react-native";
import FastImage from "react-native-fast-image";
import { getInitials } from "../../helpers";
import { useFileReader, usePromise } from "../../hooks";
import { Account, Message, MessageAttachment } from "../../models";
import { Services } from "../../services";
import { UnknownError } from "../errors";
import { useAvatar } from "./avatar.hook";

declare type AvatarProps = {
  account: Account;
  message: Message;
  isOwnMessage?: boolean;
};

export function Avatar({ account, message, isOwnMessage }: AvatarProps) {
  /* const services = useContext(Services);
  const fetchResponse = usePromise(
    () =>
      isOwnMessage
        ? services.usersService.getProfilePicture(account.id, message.creatorId)
        : services.usersService.getProfilePictureOfOther(
            account.id,
            message.creatorId
          ),
    null,
    [account, message]
  );
  const readResponse = useFileReader(
    fetchResponse.result ? fetchResponse.result.filePath : null,
    "base64",
    [fetchResponse.result]
  );

  if (fetchResponse.error || readResponse.error) return <UnknownError />; */
  const { content, error } = useAvatar(account, message, isOwnMessage);

  if (error) return <UnknownError />;

  if (!content)
    return (
      <View
        style={{
          width: "15%",
          alignItems: "center" /*, backgroundColor: '#FF0000'*/,
        }}
      >
        <View
          style={[
            {
              height: 48,
              width: 48,
              borderRadius: 48,
              justifyContent: "center",
              position: "absolute",
              top: 0,
              left: 6,
            },
            { backgroundColor: "#CCCCCC" },
          ]}
        >
          <Text
            style={{
              fontSize: 20,
              fontFamily: "Lato-Regular",
              color: "#ffffff",
              textAlign: "center",
            }}
          >
            {getInitials(message.creatorName)}
          </Text>
        </View>
      </View>
    );

  // console.log(readResponse.content);

  return (
    <View
      style={{
        width: "15%",
        alignItems: "center" /*, backgroundColor: '#FF0000'*/,
      }}
    >
      <Image
        source={{ uri: "data:image/png;base64," + content }}
        style={{ height: 48, width: 48, borderRadius: 48 }}
        resizeMode={FastImage.resizeMode.contain}
      />
    </View>
  );
}
