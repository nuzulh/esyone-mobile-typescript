import { useContext } from "react";
import { Akte, Message } from "../../models";
import { AkteContext } from "../akten/akte.context";
import { InMessage } from "./in-message.component";
import { OutMessage } from "./out-message.component";

declare type MessageViewProps = {
  akte: Akte;
  message: Message;
  isSelected?: boolean;
  // onPress: () => void;
  // onLongPress: () => void;
};

export function MessageView({ akte, message, isSelected }: MessageViewProps) {
  const akteContext = useContext(AkteContext);

  const handleLongPress = () => {
    // akteContext.selectMessage(message);
    // akteContext.showMenu(true);
  };

  if (message.isOwnMessage)
    return (
      <OutMessage
        akte={akte}
        message={message}
        onLongPress={handleLongPress}
        isSelected={akteContext.selectedMessage?.id == message.id}
      />
    );

  return (
    <InMessage
      akte={akte}
      message={message}
      onLongPress={handleLongPress}
      onPress={() => {}}
      isSelected={akteContext.selectedMessage?.id == message.id}
    />
  );
}
