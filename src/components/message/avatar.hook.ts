import { useContext, useEffect, useState } from "react";
import RNFetchBlob from "rn-fetch-blob";
import { Account, Message } from "../../models";
import { MetaFile, Services } from "../../services";

export function useAvatar(
  account: Account,
  message: Message,
  isOwnMessage?: boolean
) {
  const [content, setContent] = useState<string>(null);
  const [error, setError] = useState<Error>(null);
  const services = useContext(Services);

  useEffect(() => {
    if (!account || !message) return;
    if (!account.id || !message.creatorId) return;
    if (content) return;

    (async () => {
      let metaFile: MetaFile;

      try {
        if (isOwnMessage)
          metaFile = await services.usersService.getProfilePicture(
            account.id,
            message.creatorId
          );
        else
          metaFile = await services.usersService.getProfilePictureOfOther(
            account.id,
            message.creatorId
          );

        const content = await RNFetchBlob.fs.readFile(
          metaFile.filePath,
          "base64"
        );

        setContent(content);
      } catch (error) {
        console.error(error);
        setError(error);
      }
    })();
  }, [account, message]);

  return { content, error };
}
