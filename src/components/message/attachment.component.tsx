import { useContext } from "react";
import { Image, Text, TouchableOpacity } from "react-native";
import { createGetFileIcon } from "../../helpers";
import { MessageAttachment } from "../../models";
import { Services } from "../../services";

declare type AttachmentProps = {
  attachment: MessageAttachment;
  onPress: (attachment: MessageAttachment) => void;
};

export function Attachment({ attachment, onPress }: AttachmentProps) {
  const services = useContext(Services);
  const getFileIcon = createGetFileIcon(services.assetsService);

  return (
    <TouchableOpacity
      onPress={() => onPress(attachment)}
      style={{ width: "45%", margin: 2, alignItems: "center" }}
    >
      <Image
        source={getFileIcon(attachment.fileName)}
        style={{ height: 70, width: 70, resizeMode: "contain" }}
      />
      <Text
        numberOfLines={1}
        style={{
          fontSize: 14,
          fontFamily: "Lato-Regular",
          color: "#030D1280",
        }}
      >
        {attachment.fileName}
      </Text>
    </TouchableOpacity>
  );
}
