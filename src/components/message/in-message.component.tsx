import { useContext } from "react";
import { FlatList, Image, Text, TouchableOpacity, View } from "react-native";
import ParsedText from "react-native-parsed-text";
import { useDispatch } from "react-redux";
import {
  formatTimelineDate,
  localization,
  previewAttachmentAction,
  readMessageAction,
} from "../../helpers";
import { useEsyState, useMessageParser } from "../../hooks";
import { Account, Akte, Message } from "../../models";
import { Services } from "../../services";
import { Attachment } from "./attachment.component";
import { Avatar } from "./avatar.component";

declare type InMessageProps = {
  akte: Akte;
  message: Message;
  isSelected?: boolean;
  onPress: () => void;
  onLongPress: () => void;
};

export function InMessage({
  akte,
  message,
  isSelected,
  onPress,
  onLongPress,
}: InMessageProps) {
  const services = useContext(Services);
  const account = useEsyState((state) => state.auth.accounts[akte.accountId]);
  const language = useEsyState((state) => state.settings.language);
  const { specs } = useMessageParser();
  const dispatch = useDispatch();

  // console.log(message);

  return (
    <TouchableOpacity
      onPress={onPress}
      onLongPress={onLongPress}
      style={
        isSelected
          ? {
            flexDirection: "row",
            margin: 8,
            padding: 5,
            backgroundColor: "#E0E0E0",
          }
          : { flexDirection: "row", margin: 8, padding: 5 }
      }
    >
      <Avatar account={account} message={message} isOwnMessage={false} />
      <View style={{ width: "85%", height: "100%" }}>
        <View
          style={[
            { padding: 10, backgroundColor: "#030D120D", borderRadius: 8 },
            message.read == false
              ? { borderColor: "#F574AB", borderWidth: 1 }
              : null,
          ]}
        >
          <Text
            numberOfLines={1}
            style={{
              fontSize: 15,
              fontFamily: "Lato-Bold",
              color: "#030D12E6",
              marginHorizontal: 5,
            }}
          >
            {message.creatorName}
          </Text>
          <View
            style={{
              flexDirection: "row",
              marginVertical: 5,
              marginHorizontal: 5,
              flexWrap: "wrap",
            }}
          >
            {message.attachments && message.attachments.length > 0 ? (
              <View
                style={{
                  padding: 7,
                  marginRight: 5,
                  marginBottom: 5,
                  borderRadius: 3,
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: "#030D1226",
                }}
              >
                <Image
                  source={services.assetsService.getIcons("iconAttachment")}
                  style={{ height: 15, width: 15 }}
                />
              </View>
            ) : null}
            {message.read
              ? message.labels.map((item, index) => (
                <View
                  key={"view" + index}
                  style={{
                    padding: 7,
                    marginBottom: 5,
                    marginRight: 5,
                      /*width: '20%',*/ borderRadius: 3,
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: "#030D1226",
                  }}
                >
                  <Text
                    key={"text" + index}
                    style={{
                      fontSize: 10,
                      fontFamily: "Lato-Regular",
                      color: "#030D12",
                    }}
                  >
                    {item}
                  </Text>
                </View>
              ))
              : null}
          </View>

          {message.read == false ? (
            <View
              style={{
                flexDirection: "row",
                marginVertical: 5,
                marginHorizontal: 5,
              }}
            >
              <Text
                style={{
                  fontSize: 10,
                  fontFamily: "Lato-Bold",
                  color: "#000000",
                }}
              >
                {message.header}
              </Text>
            </View>
          ) : null}

          {message.read ? (
            <View
              style={{ marginTop: 5, marginBottom: 5, marginHorizontal: 5 }}
            >
              <Text
                numberOfLines={0}
                style={{
                  fontSize: 15,
                  fontFamily: "Lato-Bold",
                  color: "#030D12B3",
                }}
              >
                {message.header}
              </Text>
            </View>
          ) : null}

          {message.read ? (
            <View style={{ marginBottom: 10, marginHorizontal: 5 }}>
              <ParsedText
                style={{
                  fontSize: 14,
                  fontFamily: "Lato-Regular",
                  color: "#030D1280",
                  textAlign: "justify",
                }}
                parse={specs}
              >
                {message.message}
              </ParsedText>
            </View>
          ) : null}

          {message.read ? (
            <FlatList
              style={{ marginVertical: 10 }}
              numColumns={2}
              data={message.attachments || []}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item }) => (
                <Attachment
                  attachment={item}
                  onPress={(attachment) =>
                    dispatch(previewAttachmentAction(akte, attachment))
                  }
                />
              )}
            />
          ) : null}

          <View
            style={{
              marginVertical: 5,
              marginHorizontal: 5,
              flexDirection: "row",
              alignItems: "flex-end",
              justifyContent: "space-between",
            }}
          >
            {!message.read ? (
              <TouchableOpacity
                style={[{ width: "50%" }]}
                onPress={() => dispatch(readMessageAction(akte, message))}
              >
                <View
                  style={{
                    padding: 7,
                    marginRight: 5,
                    marginBottom: 5,
                    width: "100%",
                    borderRadius: 3,
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: "#F574AB",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 16,
                      fontFamily: "Lato-Regular",
                      color: "#FFFFFF",
                      fontWeight: "bold",
                    }}
                  >
                    {localization.language.Read}
                  </Text>
                </View>
              </TouchableOpacity>
            ) : (
              <View style={[{ width: "50%" }]} />
            )}
            <View style={{ flexDirection: "row" }}>
              {message.isFavorite == true ? (
                <Image
                  source={services.assetsService.getIcons("iconStar")}
                  style={{ height: 12, width: 12, marginHorizontal: 5 }}
                />
              ) : null}
              <Text
                style={{
                  fontSize: 10,
                  fontFamily: "Lato-Regular",
                  color: "#404040",
                }}
              >
                {formatTimelineDate(language, message.creationDate)}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}
