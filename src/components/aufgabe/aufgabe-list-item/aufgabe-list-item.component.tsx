import moment from "moment";
import { useContext } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { Aufgabe } from "../../../models";
import { Services } from "../../../services";

export declare type AufgabeListItemProps = {
  aufgabe: Aufgabe;
  onAufgabePress?: (aufgabe: Aufgabe) => void;
};

export function AufgabeListItem({
  aufgabe,
  onAufgabePress = () => {},
}: AufgabeListItemProps) {
  const isExpired =
    aufgabe.isClosed && Date.now() > aufgabe.validUntil.getTime();
  const services = useContext(Services);

  return (
    <TouchableOpacity
      onPress={() => (!isExpired ? onAufgabePress(aufgabe) : null)}
      style={{
        flexDirection: "row",
        backgroundColor: "#F5F5F5",
        borderRadius: 5,
        width: "100%",
        height: 72,
        marginVertical: 10,
        paddingTop: 8,
      }}
    >
      <View
        style={{
          backgroundColor: "#E0E0E0",
          width: 40,
          height: 40,
          borderRadius: 100,
          justifyContent: "center",
          alignItems: "center",
          marginLeft: 10,
          marginTop: 3,
        }}
      >
        {aufgabe.hasAnswer && !isExpired ? (
          <Image
            source={services.assetsService.getIcons("check")}
            style={{ height: 20, width: 30, resizeMode: "contain" }}
          />
        ) : (
          <Image
            source={services.assetsService.getIcons("alertTriangle")}
            style={{ height: 20, width: 30, resizeMode: "contain" }}
          />
        )}
      </View>
      <View style={{ flexDirection: "column", marginLeft: 10 }}>
        <View style={{ alignSelf: "flex-start" }}>
          <Text
            style={{
              fontWeight: "600",
              fontSize: 14,
              lineHeight: 20,
              marginBottom: 5,
            }}
            numberOfLines={1}
          >
            {aufgabe.assistantName}
          </Text>
        </View>
        <View />
        <View
          style={{
            backgroundColor: isExpired
              ? "#E0E0E0"
              : !aufgabe.hasAnswer
              ? "#EB5757"
              : "#219653",
            borderRadius: 5,
            paddingVertical: 5,
            height: 24,
            paddingHorizontal: 10,
            alignSelf: "flex-start",
          }}
        >
          <Text
            style={{
              color: isExpired ? "black" : "white",
              fontSize: 12,
              fontWeight: "400",
            }}
          >
            {isExpired
              ? "deaktiviert "
              : aufgabe.hasAnswer
              ? "erfasst am "
              : "bis "}

            {!isExpired
              ? aufgabe.hasAnswer
                ? moment(aufgabe.answerCreationDate).format("DD.MM.YY, HH:mm")
                : moment(aufgabe.validUntil).format("DD.MM.YY, HH:mm")
              : moment(aufgabe.validUntil).format("DD.MM.YY, HH:mm")}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}
