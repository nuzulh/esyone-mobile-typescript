import { ScrollView, RefreshControl, Platform } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { fetchSingleAkteAction } from "../../../helpers";
import { Aufgabe, RootState } from "../../../models";
import { AufgabeListItem } from "../aufgabe-list-item";

export declare type AufgabeListProps = {
  aufgaben: Aufgabe[];
  onAufgabePress: (aufgabe: Aufgabe) => void;
  onRefresh: () => void;
};

export function AufgabeList({
  aufgaben,
  onAufgabePress,
  onRefresh,
}: AufgabeListProps) {
  const isLoadingBackground = useSelector(
    (state: RootState) => state.esy.appState.isLoadingBackground
  );

  return (
    <ScrollView
      refreshControl={
        <RefreshControl
          refreshing={isLoadingBackground}
          onRefresh={() => onRefresh()}
        />
      }
      style={
        Platform.OS === "ios"
          ? {}
          : { marginBottom: 70 }
      }
      contentContainerStyle={{ minHeight: "100%" }}
      showsVerticalScrollIndicator={true}
    >
      {aufgaben.map((aufgabe, index) => (
        <AufgabeListItem
          aufgabe={aufgabe}
          onAufgabePress={onAufgabePress}
          key={index}
        />
      ))}
    </ScrollView>
  );
}
