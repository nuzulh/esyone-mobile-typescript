import { Dimensions, StyleSheet } from "react-native";

const { width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    paddingTop: 24,
  },
  titlePage: {
    fontSize: 21,
    color: "#404040",
    lineHeight: 32,
    fontWeight: "600",
  },
  item: {
    backgroundColor: "#f9c2ff",
    marginVertical: 8,
    width: width - 48,
    marginRight: 12,
  },
  title: {
    fontSize: 32,
  },
});
