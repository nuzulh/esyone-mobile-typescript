import { useContext } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import FastImage from "react-native-fast-image";
import { useDispatch, useSelector } from "react-redux";
import { getTextColor, loadWorkflow, shareAssistant } from "../../../helpers";
import { Account, Assistant, RootState } from "../../../models";
import { Services } from "../../../services";
import { Carousel } from "../../carousel";
import { AccountImage } from "../account-image";
import { styles } from "./account-list-item.style";

declare type AccountListItemProps = {
  account: Account;
  onPress: (account: Account) => void;
};

declare type AccountAssistantProps = {
  account: Account;
  assistant: Assistant;
  // onPress: (assistant: Assistant) => void;
};

export function AccountListItem({ account, onPress }: AccountListItemProps) {
  const services = useContext(Services);

  // console.log("account", account.secondaryLogoUrl);

  return (
    <View>
      <TouchableOpacity
        onPress={() => onPress(account)}
        style={{
          flexDirection: "row",
          alignContent: "flex-start",
          marginTop: 16,
          marginBottom: 12,
          marginHorizontal: 16,
        }}
      >
        <View
          style={{
            marginRight: 16,
            borderRadius: 100,
            height: 40,
            width: 40,
          }}
        >
          {/* <FastImage
            source={
              !account.primaryLogoUrl && !account.secondaryLogoUrl
                ? services.assetsService.getIcons("iconWebsitelink")
                : { uri: account.primaryLogoUrl || account.secondaryLogoUrl }
            }
            style={{
              height: 40,
              width: 40,
              borderRadius: 100,
            }} */}
          <AccountImage account={account} size={40} isIconOnly={true} />
          {/* /> */}
        </View>
        <Text
          numberOfLines={2}
          style={{
            fontSize: 17,
            color: "#404040",
            textAlign: "center",
            alignSelf: "center",
            lineHeight: 28,
            fontWeight: "700",
          }}
        >
          {account.name}
        </Text>
      </TouchableOpacity>
      <Carousel
        data={account.assistants.sort(function (x, y) {
          return x.isMainService === y.isMainService
            ? 0
            : x.isMainService
              ? -1
              : 1;
        })}
        hasDots={true}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <AccountAssistant account={account} assistant={item} />
        )}
      />
    </View>
  );
}

function AccountAssistant({
  account,
  assistant,
}: // onPress,
  AccountAssistantProps) {
  const dispatch = useDispatch();
  const accessToken = useSelector(
    (state: RootState) => state.esy.auth.session.accessToken
  );
  const services = useContext(Services);

  return (
    <View
      style={[
        styles.item,
        {
          backgroundColor: "white",
          shadowColor: "#000",
          shadowOffset: { width: 1, height: 1 },
          shadowOpacity: 0.4,
          shadowRadius: 3,
          elevation: 5,
          borderRadius: 10,
          height: 285,
        },
      ]}
    >
      <TouchableOpacity
        onPress={() =>
          dispatch(loadWorkflow(account.id, assistant.id, accessToken))
        }
        style={{
          justifyContent: "center",
          alignItems: "center",
          height: "50%",
        }}
      >
        {assistant.headerLogoUrl ? (
          <Image
            style={{
              width: "100%",
              height: 145,
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
              resizeMode: "cover",
            }}
            source={{
              uri: assistant.headerLogoUrl,
            }}
          />
        ) : (
          <View
            style={{
              backgroundColor: account.mainColor,
              width: "100%",
              height: 145,
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text
              style={{
                color: getTextColor(account.mainColor),
                fontSize: 40,
                fontWeight: "700",
              }}
            >
              {assistant.name
                .toString()
                .slice(0, 2)
                .toLowerCase()
                .replace(/\w/, (firstLetter) => firstLetter.toUpperCase())}
            </Text>
          </View>
        )}
      </TouchableOpacity>
      <View style={{ paddingHorizontal: 16, flex: 1 }}>
        <TouchableOpacity
          onPress={() =>
            dispatch(loadWorkflow(account.id, assistant.id, accessToken))
          }
        >
          <View style={{ marginTop: 8, height: "50%" }}>
            <Text
              style={{
                color: "#404040",
                lineHeight: 28,
                fontSize: 17,
                fontWeight: "700",
              }}
              numberOfLines={1}
            >
              {assistant.name}
            </Text>
            <Text
              style={{
                color: "#7A7A7A",
                fontSize: 14,
                fontWeight: "400",
                lineHeight: 20,
              }}
              numberOfLines={2}
            >
              {assistant.description.toString()}
            </Text>
          </View>
        </TouchableOpacity>
        <View
          style={{
            borderWidth: 1,
            borderColor: "#F5F5F5",
            width: "100%",
            marginVertical: 10,
          }}
        />
        <TouchableOpacity
          onPress={() => dispatch(shareAssistant(assistant))}
          style={{ width: "100%", flexDirection: "row-reverse" }}
        >
          <View>
            <Image
              source={services.assetsService.getIcons("shareAndroid")}
              style={{
                height: 24,
                width: 24,
              }}
            />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}
