import { FlatList, View, RefreshControl } from "react-native";
import { Account } from "../../../models";
import { AccountListItem } from "../account-list-item";
import { useEsyState } from "../../../hooks";
import { useDispatch } from "react-redux";
import { fetchAllAccountAction } from "../../../helpers";

declare type AccountListProps = {
  accounts: Account[];
  onPress: (account: Account) => void;
};

export function AccountList({ accounts, onPress }: AccountListProps) {
  const dispatch = useDispatch();
  const isLoadingBackground = useEsyState(
    (state) => state.appState.isLoadingBackground
  );

  return (
    <FlatList
      data={accounts}
      refreshControl={
        <RefreshControl
          refreshing={isLoadingBackground}
          onRefresh={() => dispatch(fetchAllAccountAction())}
        />
      }
      showsVerticalScrollIndicator={true}
      keyExtractor={(item) => item.id}
      ListHeaderComponent={<View style={{ marginTop: 10 }}></View>}
      ListFooterComponent={<View style={{ marginBottom: 10 }}></View>}
      renderItem={({ item }) => (
        <AccountListItem account={item} onPress={onPress} />
      )}
    />
  );
}
