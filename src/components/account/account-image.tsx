import { useContext } from "react";
import { StyleProp, Text, View, ViewStyle } from "react-native";
import FastImage from "react-native-fast-image";
import { useImage } from "../../hooks";
import { Account } from "../../models";
import { Services } from "../../services";
import { CircleImage } from "../image";

export declare type AccountImageProps = {
  isIconOnly?: boolean;
  size?: number;
  fontSize?: number;
  style?: StyleProp<ViewStyle>;
  account: Account;
};

export function AccountImage({
  size = 48,
  isIconOnly = false,
  fontSize = 14,
  style = {},
  account,
}: AccountImageProps) {
  const services = useContext(Services);
  const { imageUri, headers } = useImage(() =>
    services.accountService.getMainLogo(account.id)
  );

  return (
    <View
      style={{
        paddingLeft: 7,
        paddingTop: 7,
        marginRight: 0,
        borderRadius: 3,
        alignItems: "center" /* , backgroundColor: '#0F0' */,
        flexDirection: "row",
        ...(style as any),
      }}
    >
      <View style={{ width: "30%" }}>
        <CircleImage
          size={size}
          source={{ uri: imageUri, headers }}
          style={{ alignSelf: "flex-start" }}
        />
      </View>
      {!isIconOnly ? (
        <View style={{ width: "70%" }}>
          <Text
            // numberOfLines={1}
            style={[
              {
                fontSize,
                fontFamily: "Lato-Bold",
                marginVertical: 2,
              },
              { color: "#404040" },
            ]}
          >
            {account.name}
          </Text>
        </View>
      ) : null}
    </View>
  );
}
