import { useContext } from "react";
import { Image, Linking, Text, TouchableOpacity, View } from "react-native";
import { Account } from "../../models";
import { Services } from "../../services";

declare type AccountContactProps = {
  account: Account;
};

export function AccountContact({ account }: AccountContactProps) {
  const services = useContext(Services);

  // console.log(account.contact);
  // console.log(account.website, account.phone);

  return (
    <View
      style={{
        flexDirection: "column",
        paddingHorizontal: 16,
        paddingVertical: 12,
        backgroundColor: "#FFFFFF",
      }}
    >
      {account.contact && account.contact.website ? (
        <TouchableOpacity
          onPress={() => {
            Linking.openURL(`${account.contact.website}`);
          }}
        >
          <View style={{ flexDirection: "row", marginVertical: 12 }}>
            <View
              style={{
                width: "15%",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image
                source={services.assetsService.getIcons("iconWebsitelink")}
                style={{ height: 24, width: 24, resizeMode: "contain" }}
              />
            </View>
            <View style={{ width: "85%", justifyContent: "center" }}>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: "Lato-Regular",
                  color: "#030D12B3",
                }}
              >
                {account.contact.website}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      ) : null}
      {account.contact && account.contact.phone ? (
        <TouchableOpacity
          onPress={() => {
            Linking.openURL(`tel:${account.contact.phone}`);
          }}
        >
          <View style={{ flexDirection: "row", marginVertical: 12 }}>
            <View
              style={{
                width: "15%",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image
                source={services.assetsService.getIcons("iconPhone")}
                style={{ height: 24, width: 24, resizeMode: "contain" }}
              />
            </View>
            <View style={{ width: "85%", justifyContent: "center" }}>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: "Lato-Regular",
                  color: "#030D12B3",
                }}
              >
                {account.contact.phone}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      ) : null}
      {account.mobileNumber ? (
        <TouchableOpacity
          onPress={() => {
            Linking.openURL(`tel:${account.mobileNumber}`);
          }}
        >
          <View style={{ flexDirection: "row", marginVertical: 12 }}>
            <View
              style={{
                width: "15%",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image
                source={services.assetsService.getIcons("iconMobilenumber")}
                style={{ height: 24, width: 24, resizeMode: "contain" }}
              />
            </View>
            <View style={{ width: "85%", justifyContent: "center" }}>
              <Text
                style={{
                  fontSize: 13,
                  fontFamily: "Lato-Regular",
                  color: "#030D12B3",
                }}
              >
                {account.mobileNumber}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      ) : null}
      {account.contact && account.contact.fax ? (
        <View style={{ flexDirection: "row", marginVertical: 12 }}>
          <View
            style={{
              width: "15%",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Image
              source={services.assetsService.getIcons("iconFax")}
              style={{ height: 24, width: 24, resizeMode: "contain" }}
            />
          </View>
          <View style={{ width: "85%", justifyContent: "center" }}>
            <Text
              style={{
                fontSize: 13,
                fontFamily: "Lato-Regular",
                color: "#030D12B3",
              }}
            >
              {account.contact.fax}
            </Text>
          </View>
        </View>
      ) : null}
      {account.contact && account.contact.street ? (
        <View style={{ flexDirection: "row", marginVertical: 12 }}>
          <View
            style={{
              width: "15%",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Image
              source={services.assetsService.getIcons("iconAdress")}
              style={{ height: 24, width: 24, resizeMode: "contain" }}
            />
          </View>
          <View style={{ width: "85%", justifyContent: "center" }}>
            <Text
              style={{
                fontSize: 14,
                fontFamily: "Lato-Regular",
                color: "#030D12B3",
              }}
            >
              {account.contact.street +
                (account.contact.city ? ", " + account.contact.city : "") +
                (account.contact.postalCode
                  ? " " + account.contact.postalCode
                  : "")}
            </Text>
          </View>
        </View>
      ) : null}
    </View>
  );
}
