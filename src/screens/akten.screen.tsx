import React, { useCallback, useContext } from "react";
import { useFocusEffect } from "@react-navigation/native";
import { Text, View, Image, ScrollView, RefreshControl } from "react-native";
import { SafeAreaView } from "../components/safe-area-view.component";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../models";
import {
  fetchAllAkteAction,
  HOME_STACK,
  localization,
  navigateToAction,
  previewAkte,
} from "../helpers";
import {
  AktenList,
  Header,
  HeaderOffline,
  HeaderOnline,
  ShimmerPlaceholder,
} from "../components";
import { Services } from "../services";

export declare type AktenScreenProps = {
  styles?: any;
};

export function AktenScreen({
  styles = {
    flex: 1,
    backgroundColor: "#F2F2F2",
  },
}: AktenScreenProps) {
  const dispatch = useDispatch();
  const akten = useSelector((state: RootState) => state.esy.akten);
  const isLoadingBackground = useSelector(
    (state: RootState) => state.esy.appState.isLoadingBackground
  );

  // const isFetching = useSelector(
  //   (state: RootState) => state.esy.akten.isFetching
  // );

  const services = useContext(Services);

  // useFocusEffect(
  // useCallback(() => {
  // dispatch(fetchAllAkteAction());
  // }, []);
  // );

  // console.log(akten.aktenAsList.map((akte) => akte.isEsyAkte));

  return (
    <View style={{ ...styles, backgroundColor: "white" }}>
      <SafeAreaView />
      <Header>
        <Header.Offline>
          <HeaderOffline showBackButton={false} />
        </Header.Offline>
        <Header.Online>
          <HeaderOnline showBackButton={false} />
        </Header.Online>
      </Header>
      {/* <ShimmerPlaceholder
        style={{
          width: "100%",
        }}
        visible={!isLoadingBackground}
      /> */}
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          justifyContent: "flex-start",
          backgroundColor: "#0000",
          zIndex: 100,
        }}
      >
        <View
          style={{
            /*height: '25%',*/ flex: 1,
            padding: 0,
            backgroundColor: "#0000",
          }}
        >
          <AktenList
            akten={akten.aktenAsList}
            isInProgress={isLoadingBackground}
            icon={services.assetsService.getIcons("iconArrowRight")}
            fallback={
              <AktenFallback
                refreshing={isLoadingBackground}
                onRefresh={() => dispatch(fetchAllAkteAction())}
              />
            }
            // placeholder={<AktenPlaceholder />}
            // onFetchNext={() => dispatch(fetchAllAkteAction())}
            onAktePress={(provider, akte) =>
              dispatch(previewAkte(provider, akte))
            }
            onTaskPress={(provider, akte) =>
              dispatch(previewAkte(provider, akte, "Aufgaben"))
            }
          />
        </View>
      </View>
    </View>
  );
}

function AktenFallback({ refreshing, onRefresh }: {
  refreshing: boolean,
  onRefresh: () => void,
}) {
  const services = useContext(Services);

  return (
    <ScrollView
      contentContainerStyle={{
        flexGrow: 1,
        alignItems: "center",
        justifyContent: "center",
      }}
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
        />
      }
    >
      <Image
        source={services.assetsService.getIcons("noDataImage")}
        style={{ width: 300, height: 300, resizeMode: "contain" }}
      />
      <Text style={{ textAlign: "center" }}>
        {localization.language.YouHaveNoCases}
      </Text>
    </ScrollView>
  );
}

function AktenPlaceholder() {
  return (
    <View
      style={{
        flexGrow: 1,
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Text style={{ textAlign: "center" }}>Loading</Text>
    </View>
  );
}
