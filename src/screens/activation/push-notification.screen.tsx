import { useContext } from "react";
import { Image, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form } from "../../components";
import { Flag } from "../../components/flag.component";
import { SafeAreaView } from "../../components/safe-area-view.component";
import {
  AVAILABLE_KEYS,
  PUBLIC_STACK,
  STACKS,
  config,
  enableFcmAction,
  localization,
  navigateToAction,
  startActivations,
  updateLanguageAction,
  updatePushNotificationSupport,
} from "../../helpers";
import { RootState } from "../../models";
import { Services } from "../../services";

export function PushNotificationScreen() {
  const dispatch = useDispatch();
  const services = useContext(Services);
  const settingsState = useSelector((state: RootState) => state.esy.settings);

  const enablePushNotification = () => {
    // dispatch(updatePushNotificationSupport(true));
    dispatch(enableFcmAction());
    dispatch(startActivations());
  };

  const skipPushNotification = () => {
    dispatch(updatePushNotificationSupport(false));
    dispatch(startActivations());
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#FFFFFF",
      }}
    >
      <SafeAreaView />
      <View style={{ flex: 2, marginTop: "25%", alignItems: "center" }}>
        <Image
          source={services.assetsService.getIcons("assetB11")}
          style={{ height: 300, width: "100%", resizeMode: "contain" }}
        />
        <View style={{ marginTop: 20 }}>
          <Text
            style={{
              color: "#404040",
              fontSize: 24,
              fontFamily: "Lato-Bold",
              textAlign: "center",
            }}
          >
            {localization.language.ScreenRegistrationActivationPushNotif}
          </Text>
          <Text
            style={{
              color: "#429F98",
              fontWeight: "400",
              fontSize: 14,
              lineHeight: 20,
              fontFamily: "Lato-Regular",
              marginTop: 20,
              textAlign: "center",
            }}
            onPress={() => dispatch(
              navigateToAction({
                navigateTo: {
                  stack: STACKS.PUBLIC_STACK,
                  screen: PUBLIC_STACK.WEBVIEW_SCREEN,
                  params: {
                    url: config.get(AVAILABLE_KEYS.DEVELOPER_PRIVACY_POLICY_URL),
                    title: localization.language.ScreenRegistrationActivationPrivacyPolicy,
                  },
                },
              })
            )}
          >
            {localization.language.ScreenRegistrationActivationPrivacyPolicy}
          </Text>
        </View>
      </View>
      <View
        style={{
          marginBottom: 5,
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Form.Field style={{ width: "70%" }}>
          <Button.Primary onPress={enablePushNotification}>
            <Button.Primary.Text
              text={
                localization.language
                  .ScreenRegistrationActivationPushNotif_ButtonYes
              }
            />
          </Button.Primary>
        </Form.Field>
        <Form.Field style={{ width: "70%" }}>
          <Button.Secondary onPress={skipPushNotification}>
            <Button.Secondary.Text
              text={
                localization.language
                  .ScreenRegistrationActivationPushNotif_ButtonNo
              }
            />
          </Button.Secondary>
        </Form.Field>
      </View>
      <SafeAreaView />
      <Flag
        countryId={settingsState.language}
        onChange={(countryId) => dispatch(updateLanguageAction(countryId))}
      />
    </View>
  );
}
