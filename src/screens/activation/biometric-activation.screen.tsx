import { useContext } from "react";
import { Alert, Image, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form } from "../../components";
import { Flag } from "../../components/flag.component";
import { SafeAreaView } from "../../components/safe-area-view.component";
import {
  localization,
  startActivations,
  updateBiometricSettings,
  updateLanguageAction,
} from "../../helpers";
import { useBiometric } from "../../hooks";
import { RootState } from "../../models";
import { Services } from "../../services";

export function BiometricActivationScreen() {
  const dispatch = useDispatch();
  const services = useContext(Services);
  const settingsState = useSelector((state: RootState) => state.esy.settings);
  const { validate } = useBiometric();

  const enableBioPress = () =>
    validate((status) => {
      // console.log(status);
      if (status.isError)
        return Alert.alert(
          "Unable to enable biometric login",
          "Ensure biometric feature is enabled on your device"
        );

      // enable biometric
      dispatch(updateBiometricSettings(true));
      dispatch(startActivations());
    });

  const skipBioPress = () => {
    dispatch(updateBiometricSettings(false));
    dispatch(startActivations());
  };

  return (
    <View style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
      <SafeAreaView />
      <View style={{ flex: 2, marginTop: "25%", alignItems: "center" }}>
        <Image
          source={services.assetsService.getIcons("assetB7")}
          style={{ height: 300, width: "100%", resizeMode: "contain" }}
        />
        <View style={{ marginTop: 20 }}>
          <Text
            style={{
              color: "#404040",
              fontSize: 24,
              fontFamily: "Lato-Bold",
              textAlign: "center",
            }}
          >
            {settingsState.biometricType == ""
              ? localization.language.ActivateYourBiometricDefault
              : settingsState.biometricType === "FaceID"
              ? localization.language.ActivateYourBiometricnGesichtserkennung
              : localization.language.ActivateYourBiometricFingerabdruck}
            {/* {Language.ActivateYourBiometricDefault} */}
          </Text>
        </View>
      </View>
      <View
        style={{
          marginBottom: 5,
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Form.Field style={{ width: "70%" }}>
          <Button.Primary onPress={enableBioPress}>
            <Button.Primary.Text
              text={
                localization.language
                  .ScreenRegistrationActivationBiometric_ButtonYes
              }
            />
          </Button.Primary>
        </Form.Field>
        <Form.Field style={{ width: "70%" }}>
          <Button.Secondary onPress={skipBioPress}>
            <Button.Secondary.Text
              text={
                localization.language
                  .ScreenRegistrationActivationBiometric_ButtonNo
              }
            />
          </Button.Secondary>
        </Form.Field>
      </View>
      {/* {biometricStatus.isInProgress ? <Loading /> : null} */}
      <SafeAreaView />
      <Flag
        countryId={settingsState.language}
        onChange={(countryId) => dispatch(updateLanguageAction(countryId))}
      />
    </View>
  );
}
