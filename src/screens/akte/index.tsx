import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useNavigation } from "@react-navigation/native";
import { useContext, useMemo } from "react";
import {
  Dimensions,
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { SafeAreaView } from "../../components/safe-area-view.component";
import { deserialize, downloadAkteAsZip, localization } from "../../helpers";
import { Account, Akte, RootState } from "../../models";
import { Services } from "../../services";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { OverviewAkteScreen } from "./overview-akte.screen";
import KeyboardSpacer from "react-native-keyboard-spacer";
import { AufgabenAkteScreen } from "./aufgaben-akte.screen";
import { FilesAkteScreen } from "./files-akte.screen";
import { FoldersAkteScreen } from "./folders-akte.screen";
import { useEsyState } from "../../hooks";
import { MessagesScreen } from "./messages.screen";
import * as MessageScreens from "./message";
import {
  AkteContext,
  createAkteContext,
} from "../../components/akten/akte.context";
import { AkteMenu } from "../../components/akten/akte-menu.component";

const TabMessage = createMaterialTopTabNavigator();

export function AkteScreen({ route }: any) {
  let { provider, akte, initialTab, parentPaths } = (
    route.params ? deserialize(route.params) : {}
  ) as {
    provider: Account;
    akte: Akte;
    initialTab: string;
    parentPaths: string[];
  };
  akte = useEsyState((state) => state.akten.akten[akte.id] || akte);
  const akteContext = createAkteContext(akte);

  // const aufgaben = useSelector((state: RootState) =>
  //   state.esy.akten.akten[akte.id]
  //     ? state.esy.akten.akten[akte.id].aufgaben
  //     : []
  // );
  const windowWidth = Dimensions.get("window").width;
  const windowHeight = Dimensions.get("window").height;

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#FFFFFF",
      }}
    >
      <AkteContext.Provider value={akteContext}>
        <SafeAreaView />
        {akteContext.isMenuVisible ? (
          <AkteMenu />
        ) : (
          <AkteHeader
            akte={akte}
            parentPaths={parentPaths}
            account={provider}
          />
        )}

        <TabMessage.Navigator
          backBehavior="history"
          initialRouteName={initialTab != null ? initialTab : "CasesDetail"}
          // tabBar = {(props) => {
          //     return(
          //         <CustomTabBar {...props} />
          //     );
          // }}
          screenOptions={{
            tabBarPressColor: "white",
            tabBarScrollEnabled: true,
            tabBarIndicatorStyle: {
              backgroundColor: "#404040",
              height: 4,
              width: 42.5,
              left: 22.5,
            },
            tabBarLabelStyle: {
              fontSize: windowHeight * 0.018,
              margin: 0,
              padding: 0,
              textTransform: "capitalize",
            },
            tabBarItemStyle: { width: windowWidth * 0.25, padding: 0 },
            tabBarStyle: { backgroundColor: "white" },
            lazy: true,
          }}
          sceneContainerStyle={{ backgroundColor: "white" }}
        >
          {!parentPaths ? (
            <TabMessage.Screen
              name="CasesDetail"
              options={{
                tabBarLabel: localization.language.TabAktenCasesDetail,
                lazy: true,
              }}
            >
              {(_) => <OverviewAkteScreen akte={akte} account={provider} />}
            </TabMessage.Screen>
          ) : null}
          {!parentPaths ? (
            <TabMessage.Screen
              name="Aufgaben"
              options={{
                tabBarLabel: localization.language.TabAktenTask,
                tabBarBadge: () => {
                  return (
                    <TabBadge
                      count={
                        akte.aufgaben.filter(
                          (aufgabe) =>
                            !aufgabe.hasAnswer &&
                            Date.now() < aufgabe.validUntil.getTime()
                        ).length
                      }
                    />
                  );
                },
              }}
            >
              {(_) => (
                <AufgabenAkteScreen
                  akte={akte}
                  account={provider}
                  aufgaben={akte.aufgaben.sort((a, b) => {
                    const aActive =
                      !a.hasAnswer && Date.now() < a.validUntil.getTime();

                    const bActive =
                      !b.hasAnswer && Date.now() < b.validUntil.getTime();

                    if (aActive && !bActive) return -1;
                    else if (!aActive && bActive) return 1;
                    else return 0;
                  })}
                />
              )}
            </TabMessage.Screen>
          ) : null}

          <TabMessage.Screen
            name="Messages"
            options={{
              tabBarLabel: localization.language.TabAktenUpdates,
              tabBarBadge: () => {
                return <TabBadge count={akte.totalUnreadMessage} />;
              },
            }}
          >
            {(_) => <MessagesScreen account={provider} akte={akte} />}
          </TabMessage.Screen>
          <TabMessage.Screen
            name="Files"
            options={{ tabBarLabel: localization.language.TabAktenData }}
          >
            {(_) => <FilesAkteScreen account={provider} akte={akte} />}
          </TabMessage.Screen>
          <TabMessage.Screen
            name="Folder"
            options={{
              tabBarLabel: localization.language.TabAktenFolders,
              tabBarBadge: () => {
                return <TabBadge count={akte.totalUnreadMessageInFolder} />;
              },
            }}
          >
            {(_) => (
              <FoldersAkteScreen
                account={provider}
                akte={akte}
                parentPaths={parentPaths}
              />
            )}
          </TabMessage.Screen>
        </TabMessage.Navigator>
        {Platform.OS === "ios" ? <KeyboardSpacer /> : null}
      </AkteContext.Provider>
    </View>
  );
}

AkteScreen.Message = MessageScreens;

function AkteHeader({
  account,
  akte,
  parentPaths,
}: {
  account: Account;
  akte: Akte;
  parentPaths: any[];
}) {
  const isOffline = useSelector(
    (state: RootState) => state.esy.appState?.isOffline
  );
  const navigation = useNavigation();
  const services = useContext(Services);
  const creatorName = useMemo(
    () =>
      [akte.creatorFirstName, akte.creatorLastName]
        .filter((item) => !!item)
        .join(" "),
    [akte]
  );
  const dispath = useDispatch();

  return (
    <>
      <View
        style={{
          padding: 10,
          height: 60,
          backgroundColor: "#FFFFFF",
          flexDirection: "row",
        }}
      >
        <TouchableOpacity
          style={{
            width: "10%",
            justifyContent: "center",
            alignItems: "center",
          }}
          onPress={() => navigation.goBack()}
        >
          <FontAwesomeIcon icon={faArrowLeft} color={"#404040"} size={18} />
        </TouchableOpacity>
        <View
          style={{
            width:
              isOffline && parentPaths && parentPaths.length > 0
                ? "70%"
                : "80%",
            justifyContent: "center",
            marginLeft: 3,
            marginRight: 3,
          }}
        >
          <Text
            numberOfLines={1}
            style={{
              fontSize: 15,
              fontFamily: "Lato-Regular",
              color: "#404040",
            }}
          >
            {akte.header}
          </Text>
          <Text
            numberOfLines={1}
            style={{
              fontSize: 10,
              fontFamily: "Lato-Regular",
              color: "#404040",
            }}
          >
            {localization.language.CreatorName}: {creatorName}
          </Text>
        </View>
        <View
          style={{
            width:
              isOffline && parentPaths && parentPaths.length > 0
                ? "30%"
                : "20%",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          {parentPaths && parentPaths.length > 0 ? (
            <TouchableOpacity
              onPress={() => {
                /* home press */
              }}
              style={{
                width: isOffline ? "50%" : "100%",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image
                source={services.assetsService.getIcons("iconHome")}
                style={{
                  height: 20,
                  width: 20,
                  marginRight: Platform.OS === "ios" ? 50 : 0,
                }}
              />
            </TouchableOpacity>
          ) : null}
          {isOffline ? (
            <View
              style={{
                width: "50%",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image
                source={services.assetsService.getIcons("iconOffline")}
                style={{ height: 20, width: 20, resizeMode: "contain" }}
              />
            </View>
          ) : (
            <TouchableOpacity
              style={{
                width: "50%",
                justifyContent: "center",
                alignItems: "center",
              }}
              onPress={() => dispath(downloadAkteAsZip(account.id, akte.id))}
            >
              <Image
                source={services.assetsService.getIcons("iconDownload")}
                style={{ height: 20, width: 20, resizeMode: "contain" }}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
      {parentPaths && parentPaths.length > 0 ? (
        <View>
          <ScrollView
            style={{ marginLeft: 10, marginTop: 10, flexDirection: "row" }}
            horizontal={true}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
          >
            <Text
              numberOfLines={1}
              style={{
                fontSize: 13,
                fontFamily: "Lato-Regular",
                color: "#030D12B3",
              }}
            >
              {account.name + " > "}
            </Text>
            {parentPaths.map((path, i) => (
              <Text
                key={i}
                numberOfLines={1}
                style={{
                  fontSize: 13,
                  fontFamily: "Lato-Regular",
                  color: "#D389BB",
                  paddingRight: 10,
                }}
              >
                {path}
              </Text>
            ))}
          </ScrollView>
        </View>
      ) : null}
    </>
  );
}

function TabBadge({ count }) {
  if (count) {
    return (
      <View
        style={{
          backgroundColor: "#F574AB",
          padding: 2,
          borderRadius: 100,
          width: 15,
          height: 15,
          marginTop: 10,
          marginLeft: 0,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Text
          style={{
            color: "#FFFFFF",
            fontSize: 8,
            fontFamily: "Lato-Regular",
            fontWeight: "bold",
          }}
        >
          {count != "-" ? count : ""}
        </Text>
      </View>
    );
  } else {
    return null;
  }
}
