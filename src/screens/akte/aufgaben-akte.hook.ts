import { useMemo, useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import {
  fetchSingleAkteAction,
  loadAufgabe,
  loadWorkflowAnswer,
} from "../../helpers";
import { Account, Akte, Aufgabe } from "../../models";
import { localization } from "../../helpers";

declare type FilterItem = { id: number; status: boolean; text: string };

export function useAufgabenAkte(
  akte: Akte,
  account: Account,
  aufgaben: Aufgabe[]
) {
  const dispatch = useDispatch();

  const QUICK_FILTER: Array<FilterItem> = [
    {
      id: 1,
      status: true,
      text: localization.language.All,
    },
    {
      id: 2,
      status: false,
      text: localization.language.OpenAufabe,
    },
    {
      id: 3,
      status: false,
      text: localization.language.Closed,
    },
    {
      id: 4,
      status: false,
      text: localization.language.ExpiredAufgabe,
    },
  ];
  const [quickFilter, setQuickFilter] = useState(QUICK_FILTER);
  const selectedFilter = useMemo(
    () => quickFilter.find((item) => item.status),
    [quickFilter]
  );

  const sortedAufgaben = aufgaben.sort((a, b) => {
    return (
      Number(a.hasAnswer) - Number(b.hasAnswer) ||
      Number(!a.isClosed) - Number(!b.isClosed)
    );
  });

  const availableAufgaben = useMemo(() => {
    // console.log("selectedFilter", selectedFilter)
    const filterOpen = sortedAufgaben.filter(
      (item) => !item.hasAnswer && Date.now() < item.validUntil.getTime()
    );

    const filterClosed = sortedAufgaben.filter(
      (item) => item.hasAnswer && !item.isClosed
    );

    const filterExpired = sortedAufgaben.filter(
      (item) => item.isClosed && Date.now() > item.validUntil.getTime()
    );

    switch (selectedFilter.id) {
      case 1:
        return [...filterOpen, ...filterClosed, ...filterExpired];
      case 2:
        return filterOpen;
      case 3:
        return filterClosed;
      case 4:
        return filterExpired;
      default:
        return [...filterOpen, ...filterClosed, ...filterExpired];
    }
  }, [selectedFilter, aufgaben, sortedAufgaben]);

  const handlePress = (aufgabe: Aufgabe) => {
    if (aufgabe.hasAnswer)
      dispatch(loadWorkflowAnswer(aufgabe.workflowId, account));
    else dispatch(loadAufgabe(account, aufgabe));
  };

  const handleRefresh = () => dispatch(fetchSingleAkteAction(account, akte));

  const handleFilter = (item: FilterItem) =>
    setQuickFilter(
      quickFilter.map((entry) => ({
        ...entry,
        status: entry.id === item.id ? true : false,
      }))
    );

  return {
    data: availableAufgaben,
    handlePress,
    handleRefresh,
    handleFilter,
    quickFilter,
  };
}
