import {
  faArrowLeft,
  faDownload,
  faPlus,
  faStar,
  faStarHalf,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useContext, useState } from "react";
import { FlatList, Image, TouchableOpacity, View } from "react-native";
import { FAB } from "react-native-elements";
import { useDispatch } from "react-redux";
import { MessageView, UnknownError } from "../../components";
import {
  fetchSingleAkteAction,
  HOME_STACK,
  navigateToAction,
} from "../../helpers";
import { Account, Akte, Message } from "../../models";
import { Services } from "../../services";

declare type MessagesScreenProps = {
  account: Account;
  akte: Akte;
};

export function MessagesScreen({ account, akte }: MessagesScreenProps) {
  const services = useContext(Services);
  const dispatch = useDispatch();
  const [selectedMessage, setSelectedMessage] = useState<Message | null>(null);

  if (!akte) return <UnknownError />;

  return (
    <View style={{ flex: 1, backgroundColor: "#ffffff" }}>
      {/* <MessageHeader
        isVisible={true}
        message={selectedMessage}
        markFavorite={(message) => {}}
        download={(message) => {}}
        onClose={() => {}}
      /> */}
      <FlatList
        data={akte.messages || []}
        initialNumToRender={10}
        inverted
        // contentContainerStyle={{ flexDirection: "column-reverse" }}
        showsVerticalScrollIndicator={true}
        renderItem={({ item }) => <MessageView akte={akte} message={item} />}
        keyExtractor={(item) => item.id}
        onScrollEndDrag={() => dispatch(fetchSingleAkteAction(account, akte))}
        onEndReachedThreshold={0.5}
        // onEndReached={loadMoreOlderMessages}
        // onStartReached={loadMoreRecentMessages}
        // ListHeaderComponent={renderLoader}
        // ListFooterComponent={renderFooterLoader}
        windowSize={200}
      // ref={listRef}
      // onScroll={(e) => {
      //   var ne = e.nativeEvent;
      //   if (
      //     ne.layoutMeasurement.height + ne.contentOffset.y >=
      //     ne.contentSize.height - 20
      //   ) {
      //     loadMoreOlderMessages();
      //   }
      // }}
      // onContentSizeChange={() => listRef.current.scrollToEnd()} // scroll end
      />
      <FAB
        placement="right"
        color="#404040"
        icon={
          <FontAwesomeIcon
            icon={faPlus}
            size={20}
            color="#ffffff"
            style={{ justifyContent: "center" }}
          />
          // <Image
          //   source={services.assetsService.getIcons("iconAddWhite")}
          //   style={{ width: 20, height: 20, justifyContent: "center" }}
          // />
        }
        onPress={
          () =>
            dispatch(
              navigateToAction({
                navigateTo: {
                  screen: HOME_STACK.COMPOSE_MESSAGE_SCREEN,
                  params: {
                    account,
                    akte,
                  },
                },
              })
            )
          // navigation.navigate('ScreenMessageAdd', {
          //   header,
          //   nameServiceProvider,
          //   nestCounter,
          //   CustomerId: CustomerId,
          //   AkteIdHash: AkteIdHash,
          //   accessToken: accessToken,
          //   loadData: getData,
          // })
        }
      />
    </View>
  );
}

export function MessageHeader({
  message,
  isVisible,
  markFavorite,
  download,
  onClose,
}: {
  message: Message;
  isVisible: boolean;
  markFavorite: (message: Message, isFavorite: boolean) => void;
  download: (message: Message) => void;
  onClose: () => void;
}) {
  const [messageIdFavorit, setMessageIdFavorit] = useState(false);

  if (!isVisible) return null;

  return (
    <View
      style={{
        padding: 10,
        height: 60,
        backgroundColor: "#404040",
        flexDirection: "row",
      }}
    >
      <TouchableOpacity
        style={{ width: "65%", justifyContent: "center", marginLeft: 15 }}
        onPress={onClose}
      >
        <FontAwesomeIcon icon={faArrowLeft} color={"#FFFFFF"} size={18} />
      </TouchableOpacity>
      <View
        style={{
          width: "30%",
          flexDirection: "row",
          justifyContent: "flex-end",
        }}
      >
        {messageIdFavorit == false ? (
          <TouchableOpacity
            style={{
              width: "30%",
              marginHorizontal: 5,
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => markFavorite(message, true)}
          >
            <FontAwesomeIcon icon={faStar} size={20} color="#ffffff" />
            {/* <Image
              source={require("../../../assets/icon/icon-star-white.png")}
              style={{ height: 20, width: 20 }}
            /> */}
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={{
              width: "30%",
              marginHorizontal: 5,
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => markFavorite(message, false)}
          >
            <FontAwesomeIcon icon={faStarHalf} size={20} color="#ffffff" />
            {/* <Image
              source={require("../../../assets/icon/icon-unstar-white.png")}
              style={{ height: 20, width: 20 }}
            /> */}
          </TouchableOpacity>
        )}
        <TouchableOpacity
          style={{
            width: "30%",
            marginHorizontal: 5,
            justifyContent: "center",
            alignItems: "center",
          }}
          onPress={() => download(message)}
        >
          <FontAwesomeIcon icon={faDownload} size={20} color="#ffffff" />
          {/* <Image
            source={require("../../../assets/icon/icon-download-white.png")}
            style={{ height: 20, width: 20 }}
          /> */}
        </TouchableOpacity>
      </View>
    </View>
  );
}
