import { View } from "react-native";
import { AktenDetail } from "../../components";
import { SafeAreaView } from "../../components/safe-area-view.component";
import { Account, Akte } from "../../models";

declare type OverviewAkteScreenProps = {
  akte: Akte;
  account: Account;
};

export function OverviewAkteScreen({ akte, account }: OverviewAkteScreenProps) {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#F2F2F2",
      }}
    >
      <SafeAreaView />
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          justifyContent: "flex-start",
          backgroundColor: "#0000",
        }}
      >
        <View
          style={{
            flex: 1,
            padding: 0,
            backgroundColor: "#0000",
          }}
        >
          <AktenDetail akte={akte} account={account} />
        </View>
      </View>
    </View>
  );
}
