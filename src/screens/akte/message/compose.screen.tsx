import {
  faArrowLeft,
  faPaperPlane,
  faTimesCircle,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useNavigation } from "@react-navigation/native";
import { useContext, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import {
  FlatList,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableOpacity,
  View,
  Keyboard,
  TouchableWithoutFeedback,
  Text,
} from "react-native";
import { FAB } from "react-native-elements";
import { useDispatch } from "react-redux";
import {
  AttachmentOverlay,
  DocumentListItem,
  Form,
  Header,
  HeaderOffline,
  HeaderOnline,
} from "../../../components";
import { SafeAreaView } from "../../../components/safe-area-view.component";
import {
  deserialize,
  HOME_STACK,
  localization,
  navigateToAction,
  previewFileAction,
  submitMessageAction,
} from "../../../helpers";
import { useEsyState, useFileUploadValidation } from "../../../hooks";
import { Account, Akte, Message } from "../../../models";
import { MetaFile, Services } from "../../../services";
import { useSelector } from "react-redux";
import { RootState } from "../../../models";

export function ComposeMessageScreen({ route }) {
  let { account, akte } = (route.params ? deserialize(route.params) : {}) as {
    account: Account;
    akte: Akte;
  };
  const profile = useEsyState((state) => state.auth.profile);
  const toastRef = useEsyState((state) => state.appState.toastRef);
  const {
    control,
    handleSubmit,
    watch,
    setValue,
    formState: { errors },
  } = useForm();
  const [attachmentOverlay, setAttachmentOverlay] = useState(false);
  const attachments: MetaFile[] = watch("attachments", []);
  const {
    filesErrorMessages,
    totalFileSize,
    uploadInfo,
  } = useFileUploadValidation(account, attachments);
  const dispatch = useDispatch();

  // console.log(attachments);
  const sendMessage = (data: {
    subject: string;
    message: string;
    attachments: MetaFile[];
  }) => {
    if (filesErrorMessages.length > 0) return;

    if (totalFileSize > uploadInfo.totalFileSizeLimitInKb) {
      toastRef.show(
        `The sum of file sizes in this form cannot be larger than ${uploadInfo.totalFileSizeLimitInKb / 1000} MB`
      );
      throw new Error("the sum of file sizes in this form are too large");
    }

    const message: Partial<Message> = {
      header: data.subject,
      message: data.message,
      attachments: [],
      belegdatum: new Date(),
      creationDate: new Date(),
      creatorId: profile.userId,
      creatorName: [profile.firstName, profile.lastName]
        .filter((value) => !!value)
        .join(" "),
      isOwnMessage: true,
      read: true,
    };

    dispatch(
      submitMessageAction(message as Message, account, akte, attachments)
    );
  };

  const keyboardState = useSelector((state: RootState) => state.esy.keyboard);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <KeyboardAvoidingView
        onStartShouldSetResponder={() => true}
        onResponderGrant={() => console.log("view pressed")}
        style={{ flex: 1, backgroundColor: "#FFFFFF" }}
        behavior={Platform.OS === "ios" ? "padding" : null}
      >
        <SafeAreaView />
        <Header>
          <Header.Offline>
            <HeaderOffline
              showBackButton={true}
              control={
                <UploadAttachment onPress={() => setAttachmentOverlay(true)} />
              }
            />
          </Header.Offline>
          <Header.Online>
            <HeaderOnline
              showBackButton={true}
              control={
                <UploadAttachment onPress={() => setAttachmentOverlay(true)} />
              }
            />
          </Header.Online>
        </Header>
        <View style={{ flex: 1 }}>
          <View>
            <View style={{ padding: 16, paddingBottom: 0 }}>
              <Form.Field>
                <Form.Label isRequired={true}>
                  {localization.language.ScreenMessage_LabelSubject}
                </Form.Label>
                <Form.Input
                  placeholder={localization.language.ScreePlaceholderAddSubject}
                  control={control}
                  name="subject"
                  rules={{
                    required: "Subject is required",
                  }}
                />
                <Form.ErrorMessage errors={errors} name="subject" />
              </Form.Field>

              <Form.Field>
                <Form.Label isRequired={true}>
                  {localization.language.ScreenMessage_LabelMessage}
                </Form.Label>
                <Form.TextArea
                  placeholder={localization.language.ScreePlaceholderMessage}
                  control={control}
                  name="message"
                  rules={{ required: "Message is required" }}
                />
                <Form.ErrorMessage errors={errors} name="message" />
              </Form.Field>
            </View>
            <View style={{ padding: 16 }}>
              <FlatList
                data={attachments}
                showsVerticalScrollIndicator={false}
                key={"_"}
                keyExtractor={(item) => "_" + item.id.toString()}
                renderItem={({ item }) => (
                  <DocumentListItem
                    style={{}}
                    file={item}
                    onPress={() =>
                      dispatch(
                        navigateToAction({
                          navigateTo: {
                            screen: HOME_STACK.PREVIEW_SCREEN,
                            params: {
                              metaFile: item,
                            },
                          },
                        })
                      )
                    }
                  >
                    <TouchableOpacity
                      onPress={() =>
                        setValue(
                          "attachments",
                          attachments.filter(
                            (attachment) => attachment.id != item.id
                          )
                        )
                      }
                    >
                      <FontAwesomeIcon
                        icon={faTrash}
                        size={15}
                        color="#404040"
                      />
                    </TouchableOpacity>
                  </DocumentListItem>
                )}
              />
              {filesErrorMessages.length > 0 ? (
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: "Lato-Bold",
                    color: "#bc3640",
                    marginTop: 5,
                  }}
                >
                  {filesErrorMessages.join("\n")}
                </Text>
              ) : null}
            </View>
          </View>
        </View>

        <Controller
          name="attachments"
          control={control}
          defaultValue={[]}
          render={({ field: { value, onChange } }) => (
            <AttachmentOverlay
              isVisible={attachmentOverlay}
              onClose={() => setAttachmentOverlay(false)}
              onSelect={(files) => {
                onChange([...value, ...files]);
                setAttachmentOverlay(false);
              }}
              onError={(error) => {
                console.log(error);
              }}
            />
          )}
        />
        <FAB
          style={{ bottom: keyboardState.isVisible ? Platform.OS === 'ios' ? 350 : 0 : 0 }}
          placement="right"
          color="#404040"
          icon={
            <FontAwesomeIcon
              icon={faPaperPlane}
              size={20}
              color="#ffffff"
              style={{ justifyContent: "center" }}
            />
          }
          onPress={handleSubmit(sendMessage)}
        // disabled={
        //   (!subject && !message && fileAttach.length < 1) || !isConnected
        // }
        />
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
}

function UploadAttachment({ onPress }: { onPress: () => void; }) {
  const services = useContext(Services);

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        width: "50%",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Image
        source={services.assetsService.getIcons("iconAttachment")}
        style={{ height: 20, width: 20, resizeMode: "contain" }}
      />
    </TouchableOpacity>
  );
}
