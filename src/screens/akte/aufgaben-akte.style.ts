import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  wrapStatus: {
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: "#E0E0E0",
    borderRadius: 100,
    marginRight: 12,
  },
  textStatus: {
    fontSize: 14,
    fontWeight: "400",
    lineHeight: 20,
    color: "#7A7A7A",
  },
  wrapContent: {
    backgroundColor: "#F5F5F5",
    height: 72,
    borderRadius: 5,
    marginVertical: 10,
    flexDirection: "row",
    alignItems: "center",
  },
});
