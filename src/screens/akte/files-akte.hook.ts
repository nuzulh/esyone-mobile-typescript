import { useMemo, useState } from "react";
import { localization } from "../../helpers";
import { MessageAttachment } from "../../models";

declare type FilterItem = { id: number; status: boolean; text: string; };

export function useFilesAkteFilter(attachments: MessageAttachment[]) {
  const QUICK_FILTER: FilterItem[] = [
    {
      id: 1,
      status: true,
      text: localization.language.All,
    },
    {
      id: 2,
      status: false,
      text: localization.language.images,
    },
    {
      id: 3,
      status: false,
      text: localization.language.documents,
    },
  ];
  const [quickFilter, setQuickFilter] = useState(QUICK_FILTER);
  const selectedFilter = useMemo(
    () => quickFilter.find((item) => item.status),
    [quickFilter]
  );
  const filteredAttachments = useMemo(
    () => {
      switch (selectedFilter.id) {
        case 1:
          return attachments;
        case 2:
          return attachments.filter(
            (item) => isImageFile(item.fileName)
          );
        case 3:
          return attachments.filter(
            (item) => !isImageFile(item.fileName)
          );
        default:
          return attachments;
      }
    },
    [selectedFilter, attachments]
  );
  const handleFilter = (item: FilterItem) => setQuickFilter(
    quickFilter.map((entry) => ({
      ...entry,
      status: entry.id === item.id ? true : false,
    }))
  );

  return {
    filteredAttachments,
    handleFilter,
    quickFilter,
  };
}

function isImageFile(fileName: string): boolean {
  const IMAGE_EXT = ["gif", "jpg", "png", "jpeg"];

  return IMAGE_EXT.includes(
    fileName.split(".").pop().toLowerCase()
  );
}
