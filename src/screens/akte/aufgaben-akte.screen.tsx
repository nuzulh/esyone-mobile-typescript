import { useContext, useMemo, useState } from "react";
import { TouchableOpacity, View, Text } from "react-native";
import { useDispatch } from "react-redux";
import { AufgabeList } from "../../components";
import {
  fetchSingleAkteAction,
  loadAufgabe,
  localization,
} from "../../helpers";
import { Account, Akte, Aufgabe } from "../../models";
import { Services } from "../../services";
import { useAufgabenAkte } from "./aufgaben-akte.hook";
import { styles } from "./aufgaben-akte.style";

const BGCOLOR_STATUS_ACTIVE = "#7A7A7A";
const BGCOLOR_STATUS_NONACTIVE = "#FAFAFA";
const COLOR_STATUS_ACTIVE = "#7A7A7A";

declare type AufgabenAkteScreenProps = {
  akte: Akte;
  account: Account;
  aufgaben: Aufgabe[];
};

export function AufgabenAkteScreen({
  akte,
  account,
  aufgaben,
}: AufgabenAkteScreenProps) {
  const { data, handleFilter, handleRefresh, handlePress, quickFilter } =
    useAufgabenAkte(akte, account, aufgaben);

  // console.log(localization.language.All);

  return (
    <View style={{ marginHorizontal: 10 }}>
      <View style={{ marginVertical: 16, width: "100%", flexDirection: "row" }}>
        {quickFilter.map((item, index) => {
          return (
            <TouchableOpacity
              key={index}
              onPress={() => handleFilter(item)}
              style={{
                ...styles.wrapStatus,
                backgroundColor: item.status
                  ? BGCOLOR_STATUS_ACTIVE
                  : BGCOLOR_STATUS_NONACTIVE,
              }}
            >
              <Text
                style={{
                  ...styles.textStatus,
                  color: item.status ? "white" : COLOR_STATUS_ACTIVE,
                }}
              >
                {item.text}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
      <AufgabeList
        aufgaben={data}
        onAufgabePress={handlePress}
        onRefresh={handleRefresh}
      />
      {/* <ScrollView style={{ marginBottom: 70 }}></ScrollView> */}
    </View>
  );
}
