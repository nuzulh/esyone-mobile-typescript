import { useContext, useMemo, useState } from "react";
import {
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  View,
  RefreshControl,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { IconButton, Input, ShimmerPlaceholder } from "../../components";
import { SafeAreaView } from "../../components/safe-area-view.component";
import {
  fetchSingleAkteAction,
  HOME_STACK,
  localization,
  navigateToAction,
} from "../../helpers";
import { useToggle } from "../../hooks";
import { Account, Akte, RootState } from "../../models";
import { Services } from "../../services";

declare type FoldersAkteScreenProps = {
  akte: Akte;
  account: Account;
  parentPaths: any[];
};

export function FoldersAkteScreen({
  akte,
  account,
  parentPaths,
}: FoldersAkteScreenProps) {
  const dispatch = useDispatch();
  const { value: isList, toggle: toggleMode } = useToggle(true);
  const [queryText, setQueryText] = useState(null);
  const folders = useSelector((state: RootState) =>
    state.esy.akten.akten[akte.id]
      ? state.esy.akten.akten[akte.id].children
      : []
  );
  const selectedFolders = useMemo(
    () =>
      queryText && queryText !== ""
        ? folders.filter((folder) => folder.header.includes(queryText))
        : folders,
    [queryText, folders]
  );
  const services = useContext(Services);

  return (
    <View style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
      <View>
        <SafeAreaView />
        <View
          style={{
            marginLeft: 20,
            marginRight: 10,
            marginTop: 10,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Input
            type="text"
            placeholder={localization.language.SearchFolders}
            onChange={(text) => setQueryText(text)}
            style={{ width: "90%" }}
          />
          <View style={{ width: "8%" }}>
            <IconButton
              icon={
                isList
                  ? services.assetsService.getIcons("iconList")
                  : services.assetsService.getIcons("iconGrid")
              }
              onPress={() => toggleMode()}
            />
          </View>
        </View>
        <View>
          <FolderItems
            account={account}
            akte={akte}
            isList={isList}
            folders={selectedFolders}
            onPress={(item) =>
              dispatch(
                navigateToAction({
                  navigateTo: {
                    screen: HOME_STACK.MESSAGE_SCREEN,
                    params: {
                      provider: account,
                      akte: item,
                      parentPaths: parentPaths
                        ? [...parentPaths, item.header]
                        : [item.header],
                      initialTab: "CasesDetail",
                    },
                  },
                })
              )
            }
          />
        </View>
      </View>
    </View>
  );
}

function FolderItems({ account, akte, isList, folders, onPress }) {
  const dispatch = useDispatch();
  const isLoadingBackground = useSelector(
    (state: RootState) => state.esy.appState.isLoadingBackground
  );
  if (!folders || folders.length == 0)
    return (
      <View
        style={{
          flexGrow: 1,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ textAlign: "center" }}>
          {localization.language.TabAktenFolderEmpty}
        </Text>
      </View>
    );

  if (isList)
    return (
      <FlatList
        refreshControl={
          <RefreshControl
            refreshing={isLoadingBackground}
            onRefresh={() => dispatch(fetchSingleAkteAction(account, akte))}
          />
        }
        key={"_"}
        keyExtractor={(item) => "_" + item.id.toString()}
        //data={DataAusblenden}
        data={folders}
        renderItem={({ item }) => (
          <FolderListItem item={item} onPress={() => onPress(item)} />
        )}
      />
    );

  return (
    <FlatList
      refreshControl={
        <RefreshControl
          refreshing={isLoadingBackground}
          onRefresh={() => dispatch(fetchSingleAkteAction(account, akte))}
        />
      }
      contentContainerStyle={{ paddingBottom: 20 }}
      key={"#"}
      keyExtractor={(item) => "#" + item.id.toString()}
      //data={DataAusblenden}
      data={folders}
      numColumns={4}
      renderItem={({ item }) => (
        <FolderGridItem item={item} onPress={() => onPress(item)} />
      )}
      style={{ marginLeft: 20, marginRight: 20 }}
    />
  );
}

function FolderListItem({ item, onPress }) {
  const services = useContext(Services);

  return (
    <View
      style={{
        marginLeft: 20,
        marginRight: 20,
        flexDirection: "row",
        marginVertical: 5,
        borderBottomColor: "#030D1233",
        borderBottomWidth: 0.5,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <View style={{ width: "15%", justifyContent: "center" }}>
        <ShimmerPlaceholder visible={true} style={{ height: 50, width: 50 }}>
          <Image
            source={services.assetsService.getIcons("iconFolderGreen")}
            style={{ height: 40, width: 40, resizeMode: "contain" }}
          />
        </ShimmerPlaceholder>
      </View>
      <View style={{ width: "85%", padding: 10, justifyContent: "center" }}>
        <ShimmerPlaceholder visible={true} style={{ marginVertical: 3 }}>
          <TouchableOpacity onPress={() => onPress(item)}>
            <Text
              numberOfLines={1}
              style={{
                fontSize: 13,
                fontFamily: "Lato-Bold",
                color: "#030D12",
              }}
            >
              {item.header}
            </Text>
            {item.totalUnreadMessage > 0 ? (
              <Text
                style={{
                  fontSize: 13,
                  fontFamily: "Lato-Bold",
                  color: "#CA2211",
                }}
              >
                {item.totalUnreadMessage} Unread
              </Text>
            ) : null}
          </TouchableOpacity>
        </ShimmerPlaceholder>
      </View>
    </View>
  );
}

function FolderGridItem({ item, onPress }) {
  const services = useContext(Services);

  return (
    <View
      style={{
        alignItems: "center",
        width: "22%",
        marginLeft: 5,
        marginRight: 5,
      }}
    >
      <ShimmerPlaceholder
        visible={true}
        style={{ height: 64, width: 64, marginVertical: 5 }}
      >
        <TouchableOpacity onPress={() => onPress(item)}>
          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <Image
              source={services.assetsService.getIcons("iconFolderGreen")}
              style={{ height: 64, width: 64, resizeMode: "contain" }}
            />
          </View>
        </TouchableOpacity>
      </ShimmerPlaceholder>
      <Text
        numberOfLines={2}
        style={{
          fontSize: 10,
          fontFamily: "Lato-Bold",
          color: "#404040",
          textAlign: "center",
        }}
      >
        {item.header}
      </Text>
    </View>
  );
}
