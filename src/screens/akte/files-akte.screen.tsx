import { useState, useMemo, useContext } from "react";
import {
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  View,
  RefreshControl,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { IconButton, Input, ShimmerPlaceholder } from "../../components";
import {
  createGetFileIcon,
  fetchSingleAkteAction,
  formatTimelineDate,
  localization,
  previewAttachmentAction,
} from "../../helpers";
import { useToggle } from "../../hooks";
import { Account, Akte, RootState } from "../../models";
import { Services } from "../../services";
import { useFilesAkteFilter } from "./files-akte.hook";
import { styles } from "./aufgaben-akte.style";

declare type FilesAkteScreenProps = {
  account: Account;
  akte: Akte;
};

export function FilesAkteScreen({ account, akte }: FilesAkteScreenProps) {
  const { value: isList, toggle: toggleMode } = useToggle(true);
  const [queryText, setQueryText] = useState(null);
  const attachments = useSelector((state: RootState) =>
    state.esy.akten.akten[akte.id]
      ? state.esy.akten.akten[akte.id].attachments
      : []
  );
  const selectedAttachments = useMemo(
    () =>
      queryText && queryText !== ""
        ? attachments.filter(({ fileName }) => fileName.includes(queryText))
        : attachments,
    [queryText, attachments]
  );
  const services = useContext(Services);
  const {
    filteredAttachments,
    handleFilter,
    quickFilter,
  } = useFilesAkteFilter(selectedAttachments);

  return (
    <View style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
      <View>
        <View
          style={{
            marginLeft: 20,
            marginRight: 10,
            marginTop: 10,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Input
            type="text"
            placeholder={localization.language.SearchFiles}
            onChange={(text) => setQueryText(text)}
            style={{ width: "90%" }}
          />
          <View style={{ width: "8%" }}>
            <IconButton
              icon={
                isList
                  ? services.assetsService.getIcons("iconList")
                  : services.assetsService.getIcons("iconGrid")
              }
              onPress={() => toggleMode()}
            />
          </View>
        </View>
        <View style={{ marginBottom: 120 }}>
          <View
            style={{
              marginVertical: 10,
              marginHorizontal: 16,
              width: "100%",
              flexDirection: "row",
            }}
          >
            {quickFilter.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  onPress={() => handleFilter(item)}
                  style={{
                    ...styles.wrapStatus,
                    zIndex: 104,
                    backgroundColor: item.status
                      ? "#7A7A7A"
                      : "#FAFAFA",
                  }}
                >
                  <Text
                    style={{
                      ...styles.textStatus,
                      color: item.status ? "white" : "#7A7A7A",
                    }}
                  >
                    {item.text}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </View>
          <DocumentItems
            account={account}
            akte={akte}
            isList={isList}
            files={filteredAttachments}
          />
        </View>
      </View>
      {/* {isFetching ? <Loading /> : null} */}
    </View>
  );
}

function DocumentItems({ account, akte, isList, files }) {
  const dispatch = useDispatch();
  const isLoadingBackground = useSelector(
    (state: RootState) => state.esy.appState.isLoadingBackground
  );

  if (!files || files.length == 0)
    return (
      <View
        style={{
          flexGrow: 1,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ textAlign: "center" }}>
          {localization.language.TabAktenDataEmpty}
        </Text>
      </View>
    );

  if (isList)
    return (
      <FlatList
        refreshControl={
          <RefreshControl
            refreshing={isLoadingBackground}
            onRefresh={() => dispatch(fetchSingleAkteAction(account, akte))}
          />
        }
        showsVerticalScrollIndicator={true}
        key={"_"}
        keyExtractor={(item) => "_" + item.id.toString()}
        data={files}
        renderItem={({ item }) => (
          <DocumentListItem
            file={item}
            onPress={() => dispatch(previewAttachmentAction(akte, item))}
          />
        )}
      />
    );

  return (
    <FlatList
      refreshControl={
        <RefreshControl
          refreshing={isLoadingBackground}
          onRefresh={() => dispatch(fetchSingleAkteAction(account, akte))}
        />
      }
      contentContainerStyle={{ paddingBottom: 20 }}
      showsVerticalScrollIndicator={true}
      key={"#"}
      keyExtractor={(item) => "#" + item.id.toString()}
      data={files}
      numColumns={4}
      style={{ marginLeft: 10, marginRight: 20 }}
      renderItem={({ item }) => (
        <DocumentGridItem
          file={item}
          onPress={() => dispatch(previewAttachmentAction(akte, item))}
        />
      )}
    />
  );
}

function DocumentListItem({ file, onPress }) {
  const isImage = !(
    file.fileName.toLowerCase().substring(file.fileName.lastIndexOf(".") + 1) !=
    "png" &&
    file.fileName.toLowerCase().substring(file.fileName.lastIndexOf(".") + 1) !=
    "jpg"
  );
  const language = useSelector(
    (state: RootState) => state.esy.settings.language
  );
  const services = useContext(Services);
  const getFileIcon = createGetFileIcon(services.assetsService);

  return (
    <TouchableOpacity
      onPress={() => onPress(file)}
      style={{
        marginLeft: 20,
        marginRight: 20,
        flexDirection: "row",
        marginVertical: 5,
        borderBottomColor: "#030D1233",
        borderBottomWidth: 0.5,
      }}
    >
      <View style={{ width: "15%", justifyContent: "center" }}>
        <ShimmerPlaceholder visible={true} style={{ height: 50, width: 50 }}>
          {!isImage ? (
            <Image
              source={getFileIcon(file.fileName)}
              style={{ height: 40, width: 40, resizeMode: "contain" }}
            />
          ) : (
            <Image
              source={services.assetsService.getIcons("iconImage")}
              style={{ height: 40, width: 40, resizeMode: "contain" }}
            />
          )}
        </ShimmerPlaceholder>
      </View>
      <View style={{ width: "85%", padding: 10, justifyContent: "center" }}>
        <ShimmerPlaceholder visible={true} style={{ marginVertical: 3 }}>
          <Text
            numberOfLines={1}
            style={{ fontSize: 13, fontFamily: "Lato-Bold", color: "#030D12" }}
          >
            {file.fileName}
          </Text>
        </ShimmerPlaceholder>
        <ShimmerPlaceholder visible={true} style={{ marginVertical: 3 }}>
          <Text
            style={{
              fontSize: 10,
              fontFamily: "Lato-Regular",
              color: "#030D1280",
            }}
          >
            {formatTimelineDate(language, file.creationDate)}
          </Text>
        </ShimmerPlaceholder>
      </View>
    </TouchableOpacity>
  );
}

function DocumentGridItem({ file, onPress }) {
  const isImage = !(
    file.fileName.toLowerCase().substring(file.fileName.lastIndexOf(".") + 1) !=
    "png" &&
    file.fileName.toLowerCase().substring(file.fileName.lastIndexOf(".") + 1) !=
    "jpg"
  );
  const services = useContext(Services);
  const getFileIcon = createGetFileIcon(services.assetsService);

  return (
    <TouchableOpacity
      onPress={() => onPress(file)}
      style={{
        width: "22%",
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 5,
        marginRight: 5,
        flexDirection: "column",
        alignContent: "center",
        justifyContent: "center",
      }}
    >
      <ShimmerPlaceholder
        visible={true}
        style={{ height: 85, width: 85, marginVertical: 0 }}
      >
        <View style={{ alignItems: "center" }}>
          {!isImage ? (
            <Image
              source={getFileIcon(file.fileName)}
              style={{ height: 80, width: 80, resizeMode: "contain" }}
            />
          ) : (
            <Image
              source={services.assetsService.getIcons("iconImage")}
              style={{ height: 80, width: 80, resizeMode: "contain" }}
            />
          )}
        </View>
      </ShimmerPlaceholder>
      <Text
        numberOfLines={2}
        style={{
          fontSize: 10,
          fontFamily: "Lato-Bold",
          color: "#030D12B3",
          textAlign: "center",
        }}
      >
        {file.fileName}
      </Text>
    </TouchableOpacity>
  );
}
