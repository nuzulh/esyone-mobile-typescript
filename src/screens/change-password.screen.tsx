import { useForm } from "react-hook-form";
import { KeyboardAvoidingView, Platform, ScrollView, View } from "react-native";
import {
  Button,
  Form,
  Header,
  HeaderOffline,
  HeaderOnline,
  PasswordHintModal,
} from "../components";
import { useDispatch } from "react-redux";
import { authChangePasswordAction, localization } from "../helpers";
import { SafeAreaView } from "../components/safe-area-view.component";
import { useState } from "react";

export function ChangePasswordScreen() {
  const dispatch = useDispatch();
  const {
    control,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const password = watch("password");
  const newPassword = watch("newPassword");
  const [modalHintPassword, setmodalHintPassword] = useState(true);

  return (
    <View style={{ flex: 1, backgroundColor: "white" }}>
      <SafeAreaView />
      <Header>
        <Header.Offline>
          <HeaderOffline showBackButton={true} />
        </Header.Offline>
        <Header.Online>
          <HeaderOnline showBackButton={true} />
        </Header.Online>
      </Header>

      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{
          flex: 1,
          backgroundColor: "#FFFFFF",
        }}
      >
        <PasswordHintModal
          isVisible={modalHintPassword}
          setIsVisible={setmodalHintPassword}
        />
        <ScrollView
          contentContainerStyle={{ paddingBottom: 150 }}
          scrollEnabled={true}
        >
          <View style={{ paddingHorizontal: 24 }}>
            <Form.Field>
              <Form.Label>{localization.language.CurrentPassword}</Form.Label>
              <Form.Password
                control={control}
                name="password"
                placeholder={localization.language.CurrentPassword}
                rules={{ required: "Current Password Is Required" }}
              />
              <Form.ErrorMessage errors={errors} name="password" />
            </Form.Field>
            <Form.Field>
              <Form.Label>{localization.language.NewPassword}</Form.Label>
              <Form.Password
                icon={true}
                setValueicon={setmodalHintPassword}
                control={control}
                name="newPassword"
                placeholder={localization.language.NewPassword}
                rules={{
                  required: "New Password Is Required",
                  validate: {
                    matchesPreviousPassword: (value: string) => {
                      return password !== value
                        ? true
                        : "Use different password than old password";
                    },
                  },
                }}
              />
              <Form.ErrorMessage errors={errors} name="newPassword" />
            </Form.Field>
            <Form.Field>
              <Form.Label>
                {localization.language.ConfirmNewPassword}
              </Form.Label>
              <Form.Password
                control={control}
                name="newPasswordConfirmation"
                placeholder={localization.language.ConfirmNewPassword}
                rules={{
                  required: "New Password Confirmation Is Required",
                  validate: {
                    matchesNewPassword: (value: string) => {
                      return newPassword === value
                        ? true
                        : "Password doesn't match";
                    },
                  },
                }}
              />
              <Form.ErrorMessage
                errors={errors}
                name="newPasswordConfirmation"
              />
            </Form.Field>
            <Form.Field style={{ marginTop: 20 }}>
              <Button.Primary
                // onPress={handleSubmit((data) => console.log(data))}
                onPress={handleSubmit((data: any) => {
                  if (!data) return;
                  dispatch(
                    authChangePasswordAction(
                      data.password,
                      data.newPassword,
                      data.newPasswordConfirmation,
                    )
                  );
                })}
              >
                <Button.Primary.Text
                  text={localization.language.ChangePassword}
                />
              </Button.Primary>
            </Form.Field>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
}
