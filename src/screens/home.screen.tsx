import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {
  createStackNavigator,
  CardStyleInterpolators,
} from "@react-navigation/stack";
import { useContext } from "react";
import { Image } from "react-native";
import { localization } from "../helpers";
import { Services } from "../services";
import { AccountsScreen } from "./accounts.screen";
import { AktenScreen } from "./akten.screen";
import { SettingsScreen } from "./settings.screen";

const Tab = createBottomTabNavigator();
const OPTIONS_SCREEN = {
  headerShown: false,
  gestureDirection: "horizontal",
  animation: "fade",
  cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
};

export function HomeScreen() {
  const services = useContext(Services);

  // if (!services.assetsService) return null;

  return (
    <Tab.Navigator
      screenOptions={{
        ...OPTIONS_SCREEN,
        tabBarActiveTintColor: "#000000",
        tabBarInactiveTintColor: "#0000004D",
        tabBarShowLabel: true,
        tabBarLabelStyle: {
          fontSize: 12,
        },
        tabBarLabelPosition: "below-icon",
        tabBarStyle: [
          {
            display: "flex",
          },
          null,
        ],
      }}
    >
      <Tab.Screen
        name="Cases"
        component={AktenScreen}
        options={{
          tabBarLabel: localization.language.TabCases,
          tabBarIcon: ({ focused, color, size }) =>
            focused ? (
              <Image
                source={services.assetsService.getIcons("iconCases")}
                style={{ width: 24, height: 24 }}
              />
            ) : (
              <Image
                source={services.assetsService.getIcons("iconCasesUnfocused")}
                style={{ width: 24, height: 24 }}
              />
            ),
        }}
      />
      <Tab.Screen
        name="Explore"
        component={AccountsScreen}
        options={{
          tabBarLabel: localization.language.TabExplore,
          tabBarIcon: ({ focused, color, size }) =>
            focused ? (
              <Image
                source={services.assetsService.getIcons("iconSearch")}
                style={{ width: 24, height: 24 }}
              />
            ) : (
              <Image
                source={services.assetsService.getIcons("iconSearchUnfocused")}
                style={{ width: 24, height: 24 }}
              />
            ),
        }}
      />
      <Tab.Screen
        name="Account"
        component={SettingsScreen}
        options={{
          tabBarLabel: localization.language.TabAccount,
          tabBarIcon: ({ focused, color, size }) =>
            focused ? (
              <Image
                source={services.assetsService.getIcons("iconAccount")}
                style={{ width: 24, height: 24 }}
              />
            ) : (
              <Image
                source={services.assetsService.getIcons("iconAccountUnfocused")}
                style={{ width: 24, height: 24 }}
              />
            ),
        }}
      />
    </Tab.Navigator>
  );
}
