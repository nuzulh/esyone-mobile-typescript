import { useContext, useMemo } from "react";
import { View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  AccountList,
  Header,
  HeaderOffline,
  HeaderOnline,
} from "../components";
import { SafeAreaView } from "../components/safe-area-view.component";
import { previewAccount } from "../helpers";
import { RootState } from "../models";
import { Services } from "../services";

export function AccountsScreen() {
  const dispatch = useDispatch();
  const accountsMap = useSelector(
    (state: RootState) => state.esy.auth.accounts
  );
  const accounts = useMemo(() => Object.values(accountsMap), [accountsMap]);
  const services = useContext(Services);

  return (
    <View style={{ flex: 1, backgroundColor: "white" }}>
      <SafeAreaView />
      <Header>
        <Header.Offline>
          <HeaderOffline showBackButton={true} />
        </Header.Offline>
        <Header.Online>
          <HeaderOnline showBackButton={true} />
        </Header.Online>
      </Header>
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          justifyContent: "flex-start",
          backgroundColor: "#0000",
          zIndex: 100,
          marginBottom: 20,
        }}
      >
        <View
          style={{
            /*height: '25%',*/ flex: 1,
            padding: 0,
            backgroundColor: "#0000",
          }}
        >
          <AccountList
            accounts={accounts}
            onPress={(account) => dispatch(previewAccount(account))}
          />
        </View>
      </View>
    </View>
  );
}
