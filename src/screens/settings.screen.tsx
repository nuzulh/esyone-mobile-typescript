import { useContext, useState } from "react";
import {
  Modal,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  Header,
  HeaderOffline,
  HeaderOnline,
  CircleImage,
  Button,
} from "../components";
import {
  AVAILABLE_KEYS,
  authUpdateProfileImage,
  config,
  localization,
} from "../helpers";
import { Profile, RootState } from "../models";
import { Services } from "../services";
import Settings from "../components/settings";
import { SafeAreaView } from "../components/safe-area-view.component";
import { Overlay } from "react-native-elements";
import { selectCamera } from "../helpers/natives/camera-selector";
import { MetaFile } from "../services";
import { selectPhotosOne } from "../helpers/natives/photo-selector-one";

export function SettingsScreen() {
  const [isLicensesVisible, setLicencesVisible] = useState(false);
  const [isVisibleChangeProfile, setVisibleChangeProfile] = useState(false);
  const services = useContext(Services);

  const profile: Profile = useSelector(
    (state: RootState) => state.esy.auth.profile
  );

  const accountId: Profile = useSelector(
    (state: RootState) => state.esy.auth.profile
  );

  const settings = useSelector((state: RootState) => state.esy.settings);

  const dispatch = useDispatch();


  return (
    <View style={{ flex: 1, backgroundColor: "white" }}>
      <SafeAreaView />

      <Header>
        <Header.Offline>
          <HeaderOffline showBackButton={true} />
        </Header.Offline>
        <Header.Online>
          <HeaderOnline showBackButton={true} />
        </Header.Online>
      </Header>
      <ScrollView>
        <View
          style={{
            marginLeft: 20,
            marginRight: 20,
            flexDirection: "row",
            borderBottomColor: "#030D1233",
            borderBottomWidth: 0.5,
          }}
        >
          <TouchableOpacity
            onPress={() => {
              setVisibleChangeProfile(true);
            }}
            style={{ width: "15%", justifyContent: "center" }}
          >
            <CircleImage
              size={48}
              source={
                profile && profile.profilePicture
                  ? { uri: `file://${profile.profilePicture.filePath}` }
                  : services.assetsService.getIcons("iconWebsitelink")
              }
            />
          </TouchableOpacity>
          <View style={{ width: "85%", padding: 20 }}>
            <Text
              numberOfLines={1}
              style={{
                fontSize: 20,
                fontFamily: "Lato-Bold",
                color: "#404040",
              }}
            >
              {profile.pregivenUsername}
            </Text>
            <Text
              numberOfLines={1}
              style={{
                fontSize: 14,
                fontFamily: "Lato-Regular",
                color: "#404040",
              }}
            >
              {profile.email}
            </Text>
          </View>
        </View>
        <View
          style={{
            marginLeft: 20,
            marginRight: 20,
            marginTop: 20,
            paddingBottom: 10,
          }}
        >
          <View style={{ marginTop: 20 }}>
            <Text
              style={{
                fontSize: 20,
                fontFamily: "Lato-Bold",
                color: "#404040",
              }}
            >
              {localization.language.ScreenProfile_Settings}
            </Text>
          </View>
          <Settings.Menu.LanguageMenuItem />
          <Settings.Menu.BiometricMenuItem />
          <Settings.Menu.PushNotificationMenuItem />
          <Settings.Menu.ChangePasswordMenuItem />
          <Settings.Menu.ClearCachedMenuItem />
          <View style={{ marginTop: 20 }}>
            <Text
              style={{
                fontSize: 20,
                fontFamily: "Lato-Bold",
                color: "#404040",
              }}
            >
              About
            </Text>
          </View>
          <Settings.Menu.WebLinkMenuItem
            title={localization.language.ScreenProfile_PrivacyPolicy}
            url={"https://www.e-consult.de/datenschutz/"}
          />
          <Settings.Menu.WebLinkMenuItem
            title={localization.language.ScreenProfile_TermsAndConditions}
            url={"https://www.e-consult.de/AGB/"}
          />
          <Settings.Menu.WebLinkMenuItem
            title={localization.language.ScreenProfile_Imprint}
            url={"https://www.e-consult.de/impressum/"}
          />
          <Settings.Menu.LogoutMenuItem />
          <View style={{ marginTop: 20, marginBottom: 20 }}>
            <Text
              style={{
                fontSize: 10,
                fontFamily: "Lato-Bold",
                color: "#404040",
              }}
            >
              Version {config.get(AVAILABLE_KEYS.APPLICATION_VERSION)}
            </Text>
          </View>
          <TouchableOpacity
            style={{ marginTop: 0, marginBottom: 20 }}
            onPress={() => setLicencesVisible(true)}
          >
            <Text
              style={{
                fontSize: 13,
                fontFamily: "Lato-Bold",
                color: "#404040",
              }}
            >
              Third-party Licenses
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
      <Licenses
        isVisible={isLicensesVisible}
        onClose={() => setLicencesVisible(false)}
      />
      <FileMenu
        isVisible={isVisibleChangeProfile}
        onClose={() => setVisibleChangeProfile(false)}
        selectCameraPress={() =>
          selectCamera((files: MetaFile[], error) => {
            if (error) {
              console.log(error);
              return;
            }
            dispatch(authUpdateProfileImage(files));
            setVisibleChangeProfile(false);
          })
        }
        selectPhotoPress={() =>
          selectPhotosOne((files: MetaFile[], error) => {
            if (error) {
              console.log(error);
              return;
            }
            dispatch(authUpdateProfileImage(files));
            setVisibleChangeProfile(false);
          })
        }
      />
    </View>
  );
}

function FileMenu({
  isVisible,
  onClose = () => {},
  selectPhotoPress = () => {},
  selectCameraPress = () => {},
}: {
  isVisible?: boolean;
  onClose?: () => void;
  selectPhotoPress?: () => void;
  selectCameraPress?: () => void;
}) {
  const services = useContext(Services);

  return (
    <Overlay
      isVisible={isVisible}
      overlayStyle={{
        width: "80%",
        position: "absolute",
        top: 150,
        justifyContent: "center",
        alignItems: "center",
      }}
      onBackdropPress={onClose}
    >
      <Text>{localization.language.ScreenProfile_ChangeProfilePicture}</Text>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <TouchableOpacity
          onPress={selectPhotoPress}
          style={{ padding: 10, marginHorizontal: 5, alignItems: "center" }}
        >
          <Image
            source={services.assetsService.getIcons("iconImageBlack")}
            style={{ height: 20, width: 20, marginVertical: 10 }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={selectCameraPress}
          style={{ padding: 10, marginHorizontal: 5, alignItems: "center" }}
        >
          <Image
            source={services.assetsService.getIcons("iconCamera")}
            style={{ height: 20, width: 20, marginVertical: 10 }}
          />
        </TouchableOpacity>
      </View>
    </Overlay>
  );
}

function Licenses({
  isVisible,
  onClose = () => {},
}: {
  isVisible?: boolean;
  onClose?: () => void;
}) {
  return (
    <Modal
      presentationStyle="pageSheet"
      visible={isVisible}
      onRequestClose={onClose}
      style={{ backgroundColor: "black" }}
    >
      <View
        style={{
          height: "100%",
          alignContent: "center",
          justifyContent: "center",
          paddingTop: 0,
        }}
      >
        <ScrollView style={{ padding: 20, marginBottom: 20 }}>
          <Text
            style={{
              color: "#404040",
              fontSize: 24,
              fontFamily: "Lato-Regular",
              textAlign: "center",
              paddingBottom: 20,
            }}
          >
            Licenses
          </Text>
          <Text>{config.get(AVAILABLE_KEYS.LICENCES)}</Text>
        </ScrollView>
        <View style={{ paddingHorizontal: 20, paddingBottom: 10 }}>
          <Button.Primary onPress={onClose}>
            <Button.Primary.Text text="Ok" />
          </Button.Primary>
        </View>
      </View>
    </Modal>
  );
}
