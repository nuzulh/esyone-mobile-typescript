import { useContext } from "react";
import { Image, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "../../components";
import { Flag } from "../../components/flag.component";
import { SafeAreaView } from "../../components/safe-area-view.component";
import {
  AUTH_STACK,
  deserialize,
  localization,
  navigateToAction,
  STACKS,
  startRegistrationAction,
  updateLanguageAction,
} from "../../helpers";
import { RootState } from "../../models";
import { Services } from "../../services";

export function WelcomeScreen({ route }) {
  const routeParams = route.params ? deserialize(route.params) : {};
  const dispatch = useDispatch();
  const services = useContext(Services);
  const authState = useSelector((state: RootState) => state.esy.auth);
  const language = useSelector(
    (state: RootState) => state.esy.settings.language
  );

  const onButtonPress = () => {
    if (routeParams.id) {
      dispatch(
        startRegistrationAction(routeParams.id)
      );
    } else {
      dispatch(
        navigateToAction({
          navigateTo: {
            stack: STACKS.AUTH_STACK,
            screen: AUTH_STACK.SIGN_IN_SCREEN,
          },
        })
      );
    }
  };

  return (
    <View style={{ flex: 1, backgroundColor: "white" }}>
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Image
          source={services.assetsService.getIcons("onboardingB")}
          style={{ width: "100%", height: "50%", resizeMode: "contain" }}
        />
        <Text
          style={{
            fontSize: 24,
            fontFamily: "Lato-Bold",
            color: "#404040",
          }}
        >
          {localization.language.LetsGetStarted}
        </Text>
        <Text
          style={{
            fontSize: 24,
            fontFamily: "Lato-Bold",
            color: "#404040",
          }}
        >
          {authState.id}
        </Text>
      </View>
      <View
        style={{
          position: "absolute",
          bottom: 32,
          paddingHorizontal: 32,
          width: "100%",
        }}
      >
        <Button.Primary onPress={onButtonPress}>
          <Button.Primary.Text text={localization.language.LetsGo} />
        </Button.Primary>
      </View>
      <SafeAreaView />
      <Flag
        countryId={language}
        onChange={(countryId) => dispatch(updateLanguageAction(countryId))}
      />
    </View>
  );
}
