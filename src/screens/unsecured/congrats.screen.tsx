import { useContext } from "react";
import {
  View,
  Image,
  Text,
  SafeAreaView,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableOpacity,
} from "react-native";
import {
  AUTH_STACK,
  STACKS,
  deserialize,
  navigateToAction,
  updateLanguageAction,
} from "../../helpers";
import { useDispatch, useSelector } from "react-redux";
import { localization } from "../../helpers";
import { Button, UnknownError } from "../../components";
import { RootState, UserSeed } from "../../models";
import { Flag } from "../../components/flag.component";
import { Services } from "../../services";
import { useNavigation } from "@react-navigation/native";

declare type CongratsScreenProps = {
  route: any;
  style?: any;
};

export function CongratsScreen({ route, style }: CongratsScreenProps) {
  const routeParams = (route.params ? deserialize(route.params) : {}) as {
    id?: string;
    user?: UserSeed;
  };
  const dispatch = useDispatch();
  const authState = useSelector((state: RootState) => state.esy.auth);
  const language = useSelector(
    (state: RootState) => state.esy.settings.language
  );
  const services = useContext(Services);
  // const name = [routeParams?.user?.firstName, routeParams?.user?.lastName]
  //   .filter((v) => !!v)
  //   .join(" ");
  const accountName = routeParams.user?.accountName;
  if (!routeParams.user) return <UnknownError />;

  return (
    <View style={{ ...style, ...styles.containerGlobalWhite }}>
      <SafeAreaView />
      <View
        style={{ flex: 2, justifyContent: "flex-end", alignItems: "center" }}
      >
        <Text
          style={{
            fontSize: 30,
            fontFamily: "Lato-Bold",
            color: "#404040",
            textAlign: "center",
          }}
        >
          {localization.language.Congratulation}
        </Text>
      </View>
      <View style={{ alignItems: "center" }}>
        <Image
          source={services.assetsService.getIcons("assetB10")}
          style={{ resizeMode: "contain", width: "95%", height: 300 }}
        />
      </View>
      <View style={{ flex: 4, justifyContent: "center", alignItems: "center" }}>
        <Text
          style={{
            fontSize: 28,
            fontFamily: "Lato-Bold",
            color: "#404040",
            textAlign: "center",
          }}
        >
          {localization.language.EnjoyEasyAndSecureCommunicationWith}
        </Text>
        <Text
          style={{
            fontSize: 28,
            fontFamily: "Lato-Bold",
            color: "#404040",
            textAlign: "center",
          }}
        >
          {accountName}
          {" "}
          {localization.language.EnjoyEasyAndSecureCommunicationWith2}
        </Text>
      </View>
      <View
        style={{
          flex: 5,
          paddingHorizontal: 32,
          marginBottom: 32,
          justifyContent: "flex-end",
          alignItems: "center",
        }}
      >
        <Button.Primary
          onPress={() =>
            dispatch(
              navigateToAction({
                navigateTo: {
                  stack: STACKS.AUTH_STACK,
                  screen: AUTH_STACK.SIGN_IN_SCREEN,
                },
              })
            )
          }
        >
          <Button.Primary.Text text={localization.language.Next} />
        </Button.Primary>
      </View>
      <SafeAreaView />
    </View>
  );
}

const styles = StyleSheet.create({
  containerGlobal: {
    flex: 1,
    backgroundColor: "#F2F2F2",
  },
  containerProviderDetail: {
    flex: 1,
    backgroundColor: "#E5E5E5",
  },
  containerGlobalWhite: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  containerGlobalFlex: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  containerFlex2: {
    flex: 1,
  },
  viewImage: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  imageSize: {
    width: "100%",
    height: "50%",
    resizeMode: "contain",
  },
  textDesc: {
    fontSize: 24,
    fontFamily: "Lato-Bold",
    color: "#404040",
  },
});
