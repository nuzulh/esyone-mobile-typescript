import { useContext } from "react";
import { View, Image, Text, SafeAreaView, StyleSheet } from "react-native";
import {
  STACKS,
  deserialize,
  navigateToAction,
  localization,
  updateLanguageAction,
  REGISTRATION_STACK,
} from "../../helpers";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "../../components";
import { RootState } from "../../models";
import { Flag } from "../../components/flag.component";
import { Services } from "../../services";
import CheckBox from "react-native-check-box";

declare type GetStartedScreenProps = {
  route: any;
  style?: any;
};

export function GetStartedScreen({ route, style }: GetStartedScreenProps) {
  const routeParams = route.params ? deserialize(route.params) : {};
  const dispatch = useDispatch();
  const language = useSelector(
    (state: RootState) => state.esy.settings.language
  );
  const services = useContext(Services);

  return (
    <View style={styles.containerGlobalWhite}>
      <SafeAreaView />
      <View style={{ flex: 2, marginTop: "25%", alignItems: "center" }}>
        <Text
          style={{ color: "#404040", fontSize: 24, fontFamily: "Lato-Regular" }}
        >
          {localization.language.welcometo}
        </Text>
        <Text
          style={{ color: "#404040", fontSize: 30, fontFamily: "Lato-Bold" }}
        >
          e.syOne
        </Text>
        <Image
          source={services.assetsService.getIcons("assetB6")}
          style={{ height: "50%", width: "80%", resizeMode: "contain" }}
        />
        <View
          style={{
            width: "80%",
            alignItems: "flex-start",
            flexDirection: "row",
            marginTop: 10,
            marginRight: 30,
          }}
        >
          <CheckBox
            style={{ width: 35, padding: 0, marginTop: 0 }}
            onClick={() => {}}
            isChecked={false}
          />
          <View style={{ flexDirection: "column", alignItems: "flex-start" }}>
            <View style={{ marginBottom: 5 }}>
              <Text>
                {localization.language.ScreenAuthWelcomeAGB1}
                <Text
                  onPress={() => {}}
                  style={{ fontWeight: "bold", color: "#00a099" }}
                >
                  {" "}
                  {localization.language.ScreenAuthWelcomeAGBLink}{" "}
                </Text>
                {localization.language.ScreenAuthWelcomeAGB2}
                <Text style={{ fontWeight: "bold" }}> (accountName) </Text>
                {localization.language.ScreenAuthWelcomeAGB3}
              </Text>
            </View>
            <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
              <Text>
                {localization.language.ScreenAuthDatenschutz1}
                <Text
                  onPress={() => {}}
                  style={{ fontWeight: "bold", color: "#00a099" }}
                >
                  {" "}
                  {localization.language.ScreenAuthDatenschutzLink}{" "}
                </Text>
                {localization.language.ScreenAuthDatenschutz3}
              </Text>
            </View>
          </View>
        </View>
      </View>
      <View style={{ margin: 5 }}>
        <Button.Primary
          onPress={() =>
            dispatch(
              navigateToAction({
                navigateTo: {
                  stack: STACKS.REGISTRATION_STACK,
                  screen: REGISTRATION_STACK.USERNAME_SCREEN,
                },
              })
            )
          }
        >
          <Button.Primary.Text text={localization.language.GetStarted} />
        </Button.Primary>
      </View>
      <SafeAreaView />
      <Flag
        countryId={language}
        onChange={(countryId) => dispatch(updateLanguageAction(countryId))}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  containerGlobal: {
    flex: 1,
    backgroundColor: "#F2F2F2",
  },
  containerProviderDetail: {
    flex: 1,
    backgroundColor: "#E5E5E5",
  },
  containerGlobalWhite: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  containerGlobalFlex: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  containerFlex2: {
    flex: 1,
  },
  viewImage: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  imageSize: {
    width: "100%",
    height: "50%",
    resizeMode: "contain",
  },
  textDesc: {
    fontSize: 24,
    fontFamily: "Lato-Bold",
    color: "#404040",
  },
});
