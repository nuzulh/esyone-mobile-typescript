import {
  View,
  Text,
  SafeAreaView,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
} from "react-native";
import {
  deserialize,
  authRequestPinLoginAction,
  authRequestPin,
  updateLanguageAction,
} from "../../helpers";
import { useDispatch } from "react-redux";
import { localization } from "../../helpers";
import { Button } from "../../components";
import { useNavigation } from "@react-navigation/native";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import { UserSeed } from "../../models";
import { useEsyState } from "../../hooks";
import { Flag } from "../../components/flag.component";

declare type PinScreenProps = {
  route: any;
  style?: any;
};

export function PinScreen({ route, style }: PinScreenProps) {
  const routeParams = (route.params ? deserialize(route.params) : {}) as {
    id?: string;
    user?: UserSeed;
  };
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const language = useEsyState((state) => state.settings.language);

  // console.log(routeParams);
  if (!routeParams.id && !routeParams.user)
    return (
      <View>
        <Text>Oops something wrong here...</Text>
      </View>
    );

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={{
        flex: 1,
        backgroundColor: "#FFFFFF",
      }}
    >
      <SafeAreaView />
      <View style={{ flex: 1 }}>
        <Button.Back onPress={() => navigation.goBack()} />
        <Flag
          countryId={language}
          onChange={(countryId) => dispatch(updateLanguageAction(countryId))}
          style={{ top: 35 }}
        />
        <View style={{ margin: 16, marginTop: 64 }}>
          <Text
            style={{ fontSize: 24, fontFamily: "Lato-Bold", color: "#404040" }}
          >
            {localization.language.ScreenPIN_Header}
          </Text>
        </View>
        <View style={{ margin: 16, marginTop: 2 }}>
          <Text
            style={{
              fontSize: 15,
              fontFamily: "Lato-Regular",
              color: "#404040",
            }}
          >
            {localization.language.ScreenPIN_Message}
          </Text>
        </View>
        <View style={{ margin: 16, marginTop: 0 }}>
          <OTPInputView
            style={{ width: "100%", height: 100 }}
            pinCount={6}
            autoFocusOnLoad
            codeInputFieldStyle={{
              width: 45,
              height: 60,
              borderRadius: 10,
              borderWidth: 1,
              borderBottomWidth: 1,
              backgroundColor: "#F5F5F5",
              color: "#404040",
              fontFamily: "Lato-Bold",
              fontSize: 24,
            }}
            codeInputHighlightStyle={{ borderColor: "#404040" }}
            onCodeFilled={(code) =>
              dispatch(
                authRequestPinLoginAction(
                  routeParams.id,
                  code,
                  routeParams.user
                )
              )
            }
          />
        </View>
        <TouchableOpacity
          style={{ margin: 16, marginTop: 2, flexDirection: "row" }}
          onPress={() =>
            dispatch(authRequestPin(routeParams.id, undefined, true))
          }
        >
          <Text
            style={{
              fontSize: 15,
              fontFamily: "Lato-Regular",
              color: "#404040",
            }}
          >
            {localization.language.ScreenPIN_QuestionReceived}{" "}
          </Text>
          <Text
            style={{ fontSize: 15, fontFamily: "Lato-Bold", color: "#404040" }}
          >
            {localization.language.ScreenPIN_RequestNew}
          </Text>
        </TouchableOpacity>
      </View>
      <SafeAreaView />
    </KeyboardAvoidingView>
  );
}
