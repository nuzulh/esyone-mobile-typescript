import { useContext, useEffect, useRef } from "react";
import { Animated, Image, Text, View } from "react-native";
import { useDispatch } from "react-redux";
import { SafeAreaView } from "../../components/safe-area-view.component";
import { AVAILABLE_KEYS, config, startApplicationBoot } from "../../helpers";
import { Services } from "../../services";

export function SplashScreen() {
  const dispatch = useDispatch();
  const fadeAnim = useRef(new Animated.Value(0)).current;
  const services = useContext(Services);
  useEffect(() => {
    const animation = Animated.timing(fadeAnim, {
      // TODO: create fade in function
      toValue: 1,
      useNativeDriver: true,
      duration: 3500,
    });

    animation.start(() => {
      dispatch(startApplicationBoot());
      animation.stop();
    });
  }, []);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#FFFFFF",
      }}
    >
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Image
          source={services.assetsService.getIcons("splashscreen")}
          style={{
            width: "35%",
            height: "35%",
            resizeMode: "contain",
          }}
        />
        <Animated.View
          style={[
            {
              opacity: fadeAnim, // Bind opacity to animated value
            },
          ]}
        >
          <Text
            style={{
              fontSize: 12,
              fontFamily: "Lato-Regular",
              color: "#429F98",
            }}
          >
            Ver {config.get(AVAILABLE_KEYS.APPLICATION_VERSION)}
          </Text>
        </Animated.View>
      </View>
      <SafeAreaView />
      {/* {stateDataRender()} */}
    </View>
  );
}
