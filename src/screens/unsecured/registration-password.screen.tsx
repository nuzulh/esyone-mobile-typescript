import { useContext, useState } from "react";
import {
  Text,
  View,
  ScrollView,
  Image,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import {
  deserialize,
  authSubmitRegistrationAction,
  updateLanguageAction,
} from "../../helpers";
import { useDispatch, useSelector } from "react-redux";
import { localization } from "../../helpers";
import { Button, Form, PasswordHintModal } from "../../components";
import { RootState, UserSeed } from "../../models";
import { Services } from "../../services";
import { useNavigation } from "@react-navigation/native";
import { SafeAreaView } from "../../components/safe-area-view.component";
import { useForm } from "react-hook-form";
import { useEsyState } from "../../hooks";
import { Flag } from "../../components/flag.component";

declare type RegistrationPasswordScreenProps = {
  route: any;
  style?: any;
};

export function RegistrationPasswordScreen({
  route,
  style,
}: RegistrationPasswordScreenProps) {
  const routeParams = (route.params ? deserialize(route.params) : {}) as {
    user: UserSeed;
  };
  const dispatch = useDispatch();
  // const authState = useSelector((state: RootState) => state.esy.auth);
  // const language = useSelector(
  //   (state: RootState) => state.esy.settings.language
  // );
  const services = useContext(Services);
  const navigation = useNavigation();
  const {
    control,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm();
  const password = watch("password");
  const [modalHintPassword, setmodalHintPassword] = useState(true);
  const language = useEsyState((state) => state.settings.language);

  if (!routeParams.user)
    return (
      <View>
        <Text>Oops something went wrong here...</Text>
      </View>
    );

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{
          flex: 1,
          backgroundColor: "#F2F2F2",
        }}
      >
        <PasswordHintModal
          isVisible={modalHintPassword}
          setIsVisible={setmodalHintPassword}
        />
        <SafeAreaView />
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <Button.Back onPress={() => navigation.goBack()} />
          <Flag
            countryId={language}
            onChange={(countryId) => dispatch(updateLanguageAction(countryId))}
            style={{ top: 35 }}
          />
          <View style={{ alignItems: "center" }}>
            <Image
              source={services.assetsService.getIcons("assetB9")}
              style={{ resizeMode: "contain", width: 250, height: 270 }}
            />
          </View>
          <View style={{ alignItems: "center", marginTop: 5 }}>
            <Text
              style={{
                fontSize: 30,
                fontFamily: "Lato-Bold",
                color: "#404040",
                textAlign: "center",
              }}
            >
              {localization.language.JustOneMoreStepConfirmYourPassword}
            </Text>
          </View>
          <View
            style={{
              flex: 2,
              justifyContent: "flex-end",
              paddingHorizontal: 32,
              marginBottom: 22,
            }}
          >
            {/* <View style={{ flexDirection: "row", width: "100%" }}> */}

            {/* <TouchableOpacity
                onPress={() => {}}
                style={{
                  width: "15%",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <FontAwesomeIcon icon={faInfoCircle} />
              </TouchableOpacity> */}
            {/* </View> */}
            <Form.Field>
              <Form.Password
                icon={true}
                setValueicon={setmodalHintPassword}
                control={control}
                name="password"
                placeholder={localization.language.Password}
                rules={{
                  required: localization.language.PasswordRequired,
                }}
              />
              <Form.ErrorMessage errors={errors} name="password" />
            </Form.Field>
            <Form.Field>
              <Form.Password
                control={control}
                name="passwordConfirmation"
                placeholder={localization.language.ConfirmNewPassword}
                rules={{
                  required: localization.language.PasswordConfirmRequired,
                  validate: {
                    matchesPassword: (value) =>
                      password === value
                        ? true
                        : localization.language.PasswordDontMatch,
                  },
                }}
              />
              <Form.ErrorMessage errors={errors} name="passwordConfirmation" />
            </Form.Field>
            <Form.Field>
              <Button.Primary
                style={{ marginTop: 15 }}
                onPress={handleSubmit((data: any) => {
                  if (!data) return;
                  dispatch(
                    authSubmitRegistrationAction(routeParams.user.id, {
                      ...routeParams.user,
                      password: data.password,
                    })
                  );
                })}
              >
                <Button.Primary.Text text={localization.language.Next} />
              </Button.Primary>
            </Form.Field>
          </View>
        </ScrollView>
        <SafeAreaView />
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
}
