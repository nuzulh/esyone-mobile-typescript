import { useContext } from "react";
import {
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  authRequestBiometricLoginAction,
  authRequestLoginAction,
  AUTH_STACK,
  deserialize,
  localization,
  navigateToAction,
  STACKS,
  updateLanguageAction,
} from "../../helpers";
import { RootState } from "../../models";
import { Services } from "../../services";
import { Form, Button } from "../../components";
import { useForm } from "react-hook-form";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faFingerprint } from "@fortawesome/free-solid-svg-icons";
import { Flag } from "../../components/flag.component";

declare type SignInScreenProps = {
  route: any;
  style?: any;
};

export function SignInScreen({
  route,
  style = { flex: 1, backgroundColor: "#FFFFFF" },
}: SignInScreenProps) {
  const routeParams = route.params ? deserialize(route.params) : {};
  const dispatch = useDispatch();
  const services = useContext(Services);
  const keyboardState = useSelector((state: RootState) => state.esy.keyboard);
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const language = useSelector(
    (state: RootState) => state.esy.settings.language
  );

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}
    >
      <ScrollView>
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          style={style}
        >
          <Image
            source={services.assetsService.getIcons("imageLogin")}
            style={{
              width: "100%",
              height: keyboardState.isVisible
                ? 50
                : Platform.OS == "android"
                  ? 350
                  : 430,
            }}
          />
          <Flag
            countryId={language}
            onChange={(countryId) => dispatch(updateLanguageAction(countryId))}
          />
          <View style={styles.containerFlex2}>
            <View style={styles.viewTextSignin}>
              <Text style={styles.textSignin}>
                {localization.language.SignIn_Header}
              </Text>
            </View>
            <View style={styles.viewTextDescInput}>
              <Text style={styles.textDesc}>
                {localization.language.SignIn_Message}
              </Text>
            </View>
            <View style={styles.viewTextDescInput}>
              <Form.Field
                style={{ paddingLeft: 0, paddingRight: 0, paddingBottom: 0 }}
              >
                <Form.Input
                  control={control}
                  name="username"
                  placeholder={localization.language.TypeUsername}
                  rules={{
                    required: localization.language.UsernameisRequired,
                  }}
                />
                <Form.ErrorMessage errors={errors} name="username" />
              </Form.Field>
              <Form.Field style={{ paddingLeft: 0, paddingRight: 0 }}>
                <Form.Password
                  control={control}
                  name="password"
                  placeholder={localization.language.TypePassword}
                  rules={{
                    required: localization.language.PasswordisRequired,
                  }}
                />
                <Form.ErrorMessage errors={errors} name="password" />
              </Form.Field>
              <Form.Field
                style={{
                  paddingTop: 10,
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <Button.Primary
                  onPress={handleSubmit((data: any) => {
                    if (!data) return;
                    dispatch(
                      authRequestLoginAction(
                        data.username,
                        data.password,
                        routeParams.dispatch
                      )
                    );
                  })}
                  style={{ width: "82%" }}
                >
                  <Button.Primary.Text
                    text={localization.language.SignIn_LogIn}
                  />
                </Button.Primary>
                <Button.Secondary
                  style={{ width: "16%", paddingVertical: 10 }}
                  onPress={() =>
                    dispatch(
                      authRequestBiometricLoginAction(routeParams.dispatch)
                    )
                  }
                >
                  <FontAwesomeIcon
                    icon={faFingerprint}
                    size={32}
                    color="#404040"
                  />
                </Button.Secondary>
              </Form.Field>
              <TouchableOpacity
                onPress={() =>
                  dispatch(
                    navigateToAction({
                      navigateTo: {
                        stack: STACKS.AUTH_STACK,
                        screen: AUTH_STACK.FORGOT_SCREEN,
                      },
                    })
                  )
                }
              >
                <Button.Secondary.Text
                  text={localization.language.SignIn_Forgot}
                />
              </TouchableOpacity>
              {/* <Form.Field>
                <Button.Secondary
                  onPress={() =>
                    dispatch(
                      navigateToAction({
                        navigateTo: {
                          stack: STACKS.AUTH_STACK,
                          screen: AUTH_STACK.FORGOT_SCREEN,
                        },
                      })
                    )
                  }
                >
                  <Button.Secondary.Text
                    text={localization.language.SignIn_Forgot}
                  />
                </Button.Secondary>
              </Form.Field> */}
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  containerGlobal: {
    flex: 1,
    backgroundColor: "#F2F2F2",
  },
  containerProviderDetail: {
    flex: 1,
    backgroundColor: "#E5E5E5",
  },
  containerGlobalWhite: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  containerGlobalFlex: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  containerFlex2: {
    flex: 1,
  },
  viewTextSignin: {
    alignItems: "baseline",
    margin: 16,
  },
  textSignin: {
    fontSize: 21,
    fontFamily: "Lato-Bold",
    color: "#404040",
    marginTop: 20,
  },
  viewTextDescInput: {
    margin: 16,
    marginTop: 2,
    // alignItems: 'center'
  },
  textDesc: {
    fontSize: 14,
    fontFamily: "Lato-Regular",
    color: "#404040",
  },
  passwordView: {
    flex: 1,
    flexDirection: "row",
  },
  passwordIcon: {
    padding: 10,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: "#fff",
    color: "#424242",
  },
});
