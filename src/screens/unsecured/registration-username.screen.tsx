import { useContext } from "react";
import {
  Text,
  View,
  ScrollView,
  TextInput,
  Image,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
} from "react-native";
import {
  deserialize,
  navigateToAction,
  localization,
  REGISTRATION_STACK,
  authRequestUsername,
  updateLanguageAction,
} from "../../helpers";
import { useDispatch } from "react-redux";
import { Button, Form } from "../../components";
import { Services } from "../../services";
import { useNavigation } from "@react-navigation/native";
import { SafeAreaView } from "../../components/safe-area-view.component";
import { useForm } from "react-hook-form";
import { useEsyState } from "../../hooks";
import { Flag } from "../../components/flag.component";

declare type RegistrationUsernameScreenProps = {
  route: any;
  style?: any;
};

export function RegistrationUsernameScreen({
  route,
  style,
}: RegistrationUsernameScreenProps) {
  const routeParams = route.params ? deserialize(route.params) : {};
  const dispatch = useDispatch();
  const services = useContext(Services);
  const navigation = useNavigation();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const language = useEsyState((state) => state.settings.language);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={styles.containerGlobalWhite}
      >
        <SafeAreaView />
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <Button.Back onPress={() => navigation.goBack()} />
          <Flag
            countryId={language}
            onChange={(countryId) => dispatch(updateLanguageAction(countryId))}
            style={{ top: 35 }}
          />
          <View style={{ alignItems: "center", marginTop: "5%" }}>
            <Image
              source={services.assetsService.getIcons("assetB8")}
              style={{ resizeMode: "contain", width: 250, height: 300 }}
            />
          </View>
          <View style={{ alignItems: "center", marginTop: 30 }}>
            <Text
              style={{
                fontSize: 30,
                fontFamily: "Lato-Bold",
                color: "#404040",
              }}
            >
              {localization.language.SignUpNow}
            </Text>
          </View>
          <View
            style={{
              padding: 10,
              marginLeft: 20,
              marginRight: 20,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text
              style={{
                textAlign: "center",
                fontSize: 18,
                fontFamily: "Lato-Regular",
                color: "#404040",
              }}
            >
              {localization.language.andagaindirectaccesstoyouronlineAkte}
            </Text>
          </View>
          <View
            style={{
              flex: 2,
              justifyContent: "flex-end",
              paddingHorizontal: 32,
              marginBottom: 22,
            }}
          >
            <Form.Field style={{ padding: 0, marginVertical: 2 }}>
              <Form.Input
                control={control}
                name="username"
                placeholder={localization.language.PlaceHolderForgotUsername}
                rules={{
                  required: "Username is required",
                }}
              />
              <Form.ErrorMessage errors={errors} name="username" />
            </Form.Field>
            <Form.Field>
              <Button.Primary
                style={{ marginTop: 15 }}
                onPress={handleSubmit((data: any) => {
                  if (!data) return;
                  dispatch(
                    authRequestUsername({
                      ...routeParams.user,
                      username: data.username,
                    })
                  );
                })}
              >
                <Button.Primary.Text text={localization.language.Next} />
              </Button.Primary>
            </Form.Field>
          </View>
        </ScrollView>
        <SafeAreaView />
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  containerGlobal: {
    flex: 1,
    backgroundColor: "#F2F2F2",
  },
  containerProviderDetail: {
    flex: 1,
    backgroundColor: "#E5E5E5",
  },
  containerGlobalWhite: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  containerGlobalFlex: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  containerFlex2: {
    flex: 1,
  },
  viewImage: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  imageSize: {
    width: "100%",
    height: "50%",
    resizeMode: "contain",
  },
  textDesc: {
    fontSize: 24,
    fontFamily: "Lato-Bold",
    color: "#404040",
  },
});
