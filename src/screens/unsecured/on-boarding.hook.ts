import { useContext, useMemo } from "react";
import { Services } from "../../services";

export function useOnBoarding() {
  const services = useContext(Services);
  const slidesDe = useMemo(
    () => [
      {
        key: "1",
        text: "Juristische oder\nsteuerliche\nAnliegen sicher\ndigital erledigen.",
        image: services.assetsService.getIcons("assetB1"),
        backgroundColor: "#59b2ab",
      },
      {
        key: "2",
        text: "Wann und wo, das entscheiden Sie!",
        image: services.assetsService.getIcons("assetB2"),
        backgroundColor: "#febe29",
      },
      {
        key: "3",
        text: "Einfach, sicher und\nschnell kommunizieren,\nzu jeder Zeit An\njedem Ort.",
        image: services.assetsService.getIcons("assetB3"),
        backgroundColor: "#22bcb5",
      },
      {
        key: "4",
        text: "Und so funktioniert:",
        image: services.assetsService.getIcons("assetB4"),
        backgroundColor: "#22bcb5",
      },
      {
        key: "5",
        text: "Hier geht es:",
        image: services.assetsService.getIcons("assetB5"),
        backgroundColor: "#22bcb5",
      },
    ],
    []
  );

  const slidesEn = useMemo(
    () => [
      {
        key: "1",
        text: "Handle legal or tax\nissues securely and\neasily",
        image: services.assetsService.getIcons("assetB1"),
        backgroundColor: "#59b2ab",
      },
      {
        key: "2",
        text: "When and where -\nyou decide!",
        image: services.assetsService.getIcons("assetB2"),
        backgroundColor: "#febe29",
      },
      {
        key: "3",
        text: "We are experienced\nhandling secure\ncommunication.\nYour privacy is safe\nwith us.",
        image: services.assetsService.getIcons("assetB3"),
        backgroundColor: "#22bcb5",
      },
      {
        key: "4",
        text: "How it works:",
        image: services.assetsService.getIcons("assetB4"),
        backgroundColor: "#22bcb5",
      },
      {
        key: "5",
        text: "Let's Go",
        image: services.assetsService.getIcons("assetB5"),
        backgroundColor: "#22bcb5",
      },
    ],
    []
  );

  return { slidesDe, slidesEn };
}
