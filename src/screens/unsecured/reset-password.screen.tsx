import { useContext, useState } from "react";
import {
  Text,
  View,
  ScrollView,
  Image,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import {
  authCreateNewPassword,
  deserialize,
} from "../../helpers";
import { useDispatch } from "react-redux";
import { localization } from "../../helpers";
import { Button, Form } from "../../components";
import { Services } from "../../services";
import { useNavigation } from "@react-navigation/native";
import { SafeAreaView } from "../../components/safe-area-view.component";
import { useForm } from "react-hook-form";
import { PasswordHintModal } from "../../components";

declare type ResetPasswordScreenProps = {
  route: any;
  style?: any;
};

export function ResetPasswordScreen({
  route,
  style,
}: ResetPasswordScreenProps) {
  const routeParams = (route.params ? deserialize(route.params) : {}) as {
    key: string;
  };
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {
    control,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm();
  const password = watch("password");
  const [modalHintPassword, setmodalHintPassword] = useState(true);

  if (!routeParams.key)
    return (
      <View>
        <Text>Oops something went wrong here...</Text>
      </View>
    );

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{
          flex: 1,
          backgroundColor: "#F2F2F2",
        }}
      >
        <PasswordHintModal
          isVisible={modalHintPassword}
          setIsVisible={setmodalHintPassword}
        />
        <SafeAreaView />
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <Button.Back onPress={() => navigation.goBack()} />
          <View style={{ alignItems: "center", marginTop: 5 }}>
            <Text
              style={{
                fontSize: 30,
                fontFamily: "Lato-Bold",
                color: "#404040",
                textAlign: "center",
              }}
            >
              {localization.language.ResetPassword}
            </Text>
          </View>
          <View
            style={{
              flex: 2,
              justifyContent: "flex-end",
              paddingHorizontal: 32,
              marginBottom: 22,
            }}
          >
            <Form.Field>
              <Form.Password
                icon={true}
                setValueicon={setmodalHintPassword}
                control={control}
                name="password"
                placeholder={localization.language.Password}
                rules={{
                  required: localization.language.PasswordRequired,
                }}
              />
              <Form.ErrorMessage errors={errors} name="password" />
            </Form.Field>
            <Form.Field>
              <Form.Password
                control={control}
                name="passwordConfirmation"
                placeholder={localization.language.ConfirmNewPassword}
                rules={{
                  required: localization.language.PasswordConfirmRequired,
                  validate: {
                    matchesPassword: (value: string) =>
                      password === value
                        ? true
                        : localization.language.PasswordDontMatch,
                  },
                }}
              />
              <Form.ErrorMessage errors={errors} name="passwordConfirmation" />
            </Form.Field>
            <Form.Field>
              <Button.Primary
                style={{ marginTop: 15 }}
                onPress={handleSubmit((data: any) => {
                  if (!data) return;
                  dispatch(
                    authCreateNewPassword(routeParams.key, password)
                  );
                })}
              >
                <Button.Primary.Text text={localization.language.Forgot_Submit} />
              </Button.Primary>
            </Form.Field>
          </View>
        </ScrollView>
        <SafeAreaView />
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
}
