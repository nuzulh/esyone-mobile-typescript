import { useNavigation } from "@react-navigation/native";
import { useContext } from "react";
import { useForm } from "react-hook-form";
import {
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  Text,
} from "react-native";
import { useDispatch } from "react-redux";
import { Button, Form } from "../../../components";
import { SafeAreaView } from "../../../components/safe-area-view.component";
import { authCheckUsernameAction, localization } from "../../../helpers";
import { Services } from "../../../services";

export function ForgotUsernameScreen() {
  const navigation = useNavigation();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const services = useContext(Services);
  const dispatch = useDispatch();

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}
    >
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{ flex: 1, backgroundColor: "#ffff" }}
      >
        <SafeAreaView />
        <TouchableOpacity
          style={{ margin: 16, width: "100%", flexDirection: "row" }}
          onPress={() => navigation.goBack()}
        >
          <Image
            source={services.assetsService.getIcons("iconArrowLeft")}
            style={{
              width: 24,
              height: 24,
            }}
          />
          <Text
            numberOfLines={1}
            style={{
              color: "black",
              fontSize: 20,
              fontFamily: "Lato-Bold",
              marginVertical: 2,
              marginLeft: 10,
            }}
          >
            {localization.language.TitleHeaderForgotUsername}
          </Text>
        </TouchableOpacity>
        <View style={{ padding: 16, flex: 1, backgroundColor: "#ffffff" }}>
          <Form.Field>
            <Form.Label>
              {localization.language.ForgotUsername_Message}
            </Form.Label>
            <Form.Email
              control={control}
              name="email"
              placeholder={"Email"}
              required={true}
            />
            <Form.ErrorMessage errors={errors} name="email" />
          </Form.Field>
          <Form.Field>
            <Button.Primary
              onPress={handleSubmit((data: any) => {
                if (!data) return;
                dispatch(authCheckUsernameAction(data.email));
              })}
            >
              <Button.Primary.Text text={localization.language.Forgot_Submit} />
            </Button.Primary>
          </Form.Field>
        </View>
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
}
