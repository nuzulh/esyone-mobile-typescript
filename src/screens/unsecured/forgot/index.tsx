import { useNavigation } from "@react-navigation/native";
import { useContext } from "react";
import {
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { useDispatch } from "react-redux";
import { Button } from "../../../components";
import { SafeAreaView } from "../../../components/safe-area-view.component";
import {
  authCheckUsernameAction,
  AUTH_STACK,
  localization,
  navigateToAction,
  STACKS,
} from "../../../helpers";
import { Services } from "../../../services";

export * from "./forgot-username.screen";
export * from "./forgot-password.screen";

export function ForgotScreen() {
  const navigation = useNavigation();
  const services = useContext(Services);
  const dispatch = useDispatch();

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}
    >
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{ flex: 1, backgroundColor: "#f2f2f2" }}
      >
        <SafeAreaView />
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <View style={{ flex: 1, backgroundColor: "#ffffff" }}>
            <TouchableOpacity
              style={{ margin: 16, width: "15%" }}
              onPress={() => navigation.goBack()}
            >
              <Image
                source={services.assetsService.getIcons("iconArrowLeft")}
                style={{
                  width: 24,
                  height: 24,
                }}
              />
            </TouchableOpacity>

            <View style={{ margin: 16, marginTop: 64 }}>
              <Text
                style={{
                  fontFamily: "Lato-Bold",
                  fontSize: 24,
                  color: "#404040",
                }}
              >
                {localization.language.ForgotCredentials}
              </Text>
            </View>

            <View style={{ paddingHorizontal: 16 }}>
              <View style={{ marginBottom: 16, marginTop: 8 }}>
                <Text
                  style={{
                    textAlign: "justify",
                    fontFamily: "Lato-Regular",
                    fontSize: 15,
                    color: "#030D12",
                  }}
                >
                  {localization.language.ForgotUsername_ButtonHeader}
                </Text>
              </View>

              <Button.Primary
                onPress={() =>
                  dispatch(
                    navigateToAction({
                      navigateTo: {
                        stack: STACKS.AUTH_STACK,
                        screen: AUTH_STACK.FORGOT_USERNAME_SCREEN,
                      },
                    })
                  )
                }
              >
                <Button.Primary.Text
                  text={localization.language.ForgotUsername_ButtonText}
                />
              </Button.Primary>

              <View style={{ marginVertical: 16 }}>
                <Text
                  style={{
                    textAlign: "justify",
                    fontFamily: "Lato-Regular",
                    fontSize: 15,
                    color: "#030D12",
                  }}
                >
                  {localization.language.ForgotPassword_ButtonHeader}
                </Text>
              </View>

              <Button.Secondary
                onPress={() =>
                  dispatch(
                    navigateToAction({
                      navigateTo: {
                        stack: STACKS.AUTH_STACK,
                        screen: AUTH_STACK.FORGOT_PASSWORD_SCREEN,
                      },
                    })
                  )
                }
              >
                <Button.Secondary.Text
                  text={localization.language.ForgotPassword_ButtonText}
                />
              </Button.Secondary>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
}
