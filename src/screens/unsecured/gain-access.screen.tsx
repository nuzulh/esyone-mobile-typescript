// This screen has no more any implement

import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import {
  AUTH_STACK,
  REGISTRATION_STACK,
  deserialize,
  navigateToAction,
  authRequestPin,
} from "../../helpers";
import { useDispatch } from "react-redux";
import { localization } from "../../helpers";
import { Button } from "../../components";
import { useNavigation } from "@react-navigation/native";
import { SafeAreaView } from "../../components/safe-area-view.component";
import { Services } from "../../services";
import { useContext } from "react";

declare type GainAccessScreenProps = {
  route: any;
  style?: any;
};

export function GainAccessScreen({ route, style }: GainAccessScreenProps) {
  const routeParams = route.params ? deserialize(route.params) : {};
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const services = useContext(Services);

  return (
    <View style={{ ...style, ...styles.containerGlobalWhite }}>
      <SafeAreaView />
      <TouchableOpacity
        style={{ margin: 16, width: "15%" }}
        onPress={() => navigation.goBack()}
      >
        <Image
          source={services.assetsService.getIcons("iconArrowLeft")}
          style={{
            width: 24,
            height: 24,
          }}
        />
      </TouchableOpacity>
      <View style={{ flex: 1, paddingHorizontal: 16 }}>
        <View style={styles.viewTextGainAccess}>
          <Text style={styles.textGainAccess}>
            {localization.language.GainAccess}
          </Text>
        </View>
        <View style={styles.viewTextDesc}>
          <Text style={styles.textDesc}>
            {localization.language.SplashPhaseTwoDescriptionEmail}
          </Text>
        </View>
      </View>
      <View style={{ paddingHorizontal: 32, marginBottom: 32 }}>
        <Button.Primary
          onPress={() => dispatch(authRequestPin(routeParams.id))}
        >
          <Button.Primary.Text text={localization.language.GetPin} />
        </Button.Primary>
      </View>
      <SafeAreaView />
    </View>
  );
}

const styles = StyleSheet.create({
  containerGlobal: {
    flex: 1,
    backgroundColor: "#F2F2F2",
  },
  containerProviderDetail: {
    flex: 1,
    backgroundColor: "#E5E5E5",
  },
  containerGlobalWhite: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  containerGlobalFlex: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  containerFlex2: {
    flex: 1,
  },
  viewImage: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  imageSize: {
    width: "100%",
    height: "50%",
    resizeMode: "contain",
  },
  viewTextDesc: {
    margin: 16,
    marginTop: 8,
  },
  textDesc: {
    fontFamily: "Lato-Regular",
    fontSize: 15,
    color: "#030D12",
  },
  viewTextGainAccess: {
    margin: 16,
    marginTop: 64,
  },
  textGainAccess: {
    fontFamily: "Lato-Bold",
    fontSize: 24,
    color: "#404040",
  },
});
