import { useContext, useMemo, useState } from "react";
import { View, Image, Text } from "react-native";
import {
  STACKS,
  deserialize,
  navigateToAction,
  localization,
  updateLanguageAction,
  REGISTRATION_STACK,
  PUBLIC_STACK,
} from "../../helpers";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "../../components";
import { RootState, UserSeed } from "../../models";
import { Flag } from "../../components/flag.component";
import { Services, VirtualMetaFile } from "../../services";
import CheckBox from "react-native-check-box";
import { SafeAreaView } from "../../components/safe-area-view.component";

declare type RegistrationStartedScreenProps = {
  route: any;
  style?: any;
};

export function RegistrationStartedScreen({
  route,
  style,
}: RegistrationStartedScreenProps) {
  const routeParams = (route.params ? deserialize(route.params) : {}) as {
    id?: string;
    user?: UserSeed;
  };
  const dispatch = useDispatch();
  const language = useSelector(
    (state: RootState) => state.esy.settings.language
  );
  const services = useContext(Services);
  const name = [routeParams?.user?.firstName, routeParams?.user?.lastName]
    .filter((v) => !!v)
    .join(" ");
  const accountName = routeParams.user?.accountName;
  const [isAccepted, setAccepted] = useState(
    routeParams?.user?.agb || routeParams?.user?.datenschutz ? false : true
  );
  const agbFile = useMemo<Partial<VirtualMetaFile>>(
    () =>
      routeParams?.user?.agb
        ? {
          fileName: "Agb.txt",
          filePath: null,
          contentType: "text/agb",
          size: routeParams?.user?.agb.length,
          extension: "txt",
          content: routeParams?.user?.agb,
        }
        : null,
    []
  );

  const datenschutzFile = useMemo<Partial<VirtualMetaFile>>(
    () =>
      routeParams?.user?.datenschutz
        ? {
          fileName: "Datenschutz.txt",
          filePath: null,
          contentType: "text/datenschutz",
          size: routeParams?.user?.datenschutz.length,
          extension: "txt",
          content: routeParams?.user?.datenschutz,
        }
        : null,
    []
  );

  if (!routeParams?.user)
    return (
      <View style={{ flex: 1, backgroundColor: "#ffffff" }}>
        <SafeAreaView />
        <Text>Oops something Wrong Here...</Text>
      </View>
    );

  return (
    <View style={{ flex: 1, backgroundColor: "#ffffff" }}>
      <SafeAreaView />
      <View
        style={{
          flex: 2,
          marginTop: "25%",
          alignItems: "center",
          paddingHorizontal: 32,
        }}
      >
        <Text
          style={{ color: "#404040", fontSize: 24, fontFamily: "Lato-Regular" }}
        >
          {localization.language.welcometo}
        </Text>
        <Text
          style={{ color: "#404040", fontSize: 30, fontFamily: "Lato-Bold" }}
        >
          e.syOne
        </Text>
        <Image
          source={services.assetsService.getIcons("assetB6")}
          style={{ height: "50%", width: "80%", resizeMode: "contain" }}
        />
        {!routeParams.user.datenschutz || !routeParams.user.agb ? (
          <Text
            style={{
              color: "#404040",
              fontSize: 20,
              fontFamily: "Lato-Regular",
            }}
          >
            {name}
          </Text>
        ) : null}
        {routeParams.user.datenschutz || routeParams.user.agb ? (
          <View
            style={{
              alignItems: "flex-start",
              flexDirection: "row",
              marginTop: 10,
              paddingHorizontal: 32,
            }}
          >
            <CheckBox
              style={{ width: 35, padding: 0, marginTop: 0 }}
              onClick={() => setAccepted(!isAccepted)}
              isChecked={isAccepted}
            />
            <View style={{ flexDirection: "column", alignItems: "flex-start" }}>
              {routeParams.user.agb ? (
                <View style={{ marginBottom: 5 }}>
                  <Text>
                    {localization.language.ScreenAuthWelcomeAGB1}
                    <Text
                      onPress={() =>
                        dispatch(
                          navigateToAction({
                            navigateTo: {
                              stack: STACKS.REGISTRATION_STACK,
                              screen: REGISTRATION_STACK.PREVIEW_SCREEN,
                              params: {
                                metaFile: agbFile,
                              },
                            },
                          })
                        )
                      }
                      style={{ fontWeight: "bold", color: "#00a099" }}
                    >
                      {" "}
                      {localization.language.ScreenAuthWelcomeAGBLink}{" "}
                    </Text>
                    {localization.language.ScreenAuthWelcomeAGB2}
                    <Text style={{ fontWeight: "bold" }}> {accountName} </Text>
                    {localization.language.ScreenAuthWelcomeAGB3}
                  </Text>
                </View>
              ) : null}
              {routeParams.user.datenschutz ? (
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <Text>
                    {localization.language.ScreenAuthDatenschutz1}
                    <Text
                      onPress={() =>
                        dispatch(
                          navigateToAction({
                            navigateTo: {
                              stack: STACKS.REGISTRATION_STACK,
                              screen: REGISTRATION_STACK.PREVIEW_SCREEN,
                              params: {
                                metaFile: datenschutzFile,
                              },
                            },
                          })
                        )
                      }
                      style={{ fontWeight: "bold", color: "#00a099" }}
                    >
                      {" "}
                      {localization.language.ScreenAuthDatenschutzLink}{" "}
                    </Text>
                    {localization.language.ScreenAuthDatenschutz3}
                  </Text>
                </View>
              ) : null}
            </View>
          </View>
        ) : null}
      </View>
      <View style={{ paddingHorizontal: 32, marginBottom: 32 }}>
        <Button.Primary
          disabled={!isAccepted}
          onPress={() =>
            dispatch(
              navigateToAction({
                navigateTo: {
                  stack: STACKS.REGISTRATION_STACK,
                  screen: REGISTRATION_STACK.USERNAME_SCREEN,
                  params: {
                    user: routeParams.user,
                  },
                },
              })
            )
          }
        >
          <Button.Primary.Text text={localization.language.GetStarted} />
        </Button.Primary>
      </View>
      <SafeAreaView />
      <Flag
        countryId={language}
        onChange={(countryId) => dispatch(updateLanguageAction(countryId))}
      />
    </View>
  );
}
