import { faComment, faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useContext, useState } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";
import ReactNativeModal from "react-native-modal";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form } from "../../components";
import { Flag } from "../../components/flag.component";
import { SafeAreaView } from "../../components/safe-area-view.component";
import {
  localization,
  navigateToAction,
  openExternalMailAction,
  openExternalSMSAction,
  STACKS,
  updateLanguageAction,
} from "../../helpers";
import { RootState } from "../../models";
import { Services } from "../../services";
import { useOnBoarding } from "./on-boarding.hook";
import { styles } from "./on-boarding.style";

export function OnBoardingScreen() {
  const dispatch = useDispatch();
  const language = useSelector(
    (state: RootState) => state.esy.settings.language
  );
  const { slidesDe, slidesEn } = useOnBoarding();
  const [isModalVisible, setModalVisible] = useState(false);

  return (
    <View style={{ flex: 1, backgroundColor: "white" }}>
      <SafeAreaView />
      <AppIntroSlider
        renderItem={({ item }) => <SliderItemView item={item} />}
        data={language == "de" ? slidesDe : slidesEn}
        renderPagination={(index) => <SliderPagination index={index} />}
      />
      <View
        style={{
          paddingTop: 10,
          paddingBottom: 25,
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Form.Field style={{ width: "70%" }}>
          <Button.Primary onPress={() => setModalVisible(true)}>
            <Button.Primary.Text text={localization.language.ToYourSmartlink} />
          </Button.Primary>
        </Form.Field>
        <Form.Field style={{ width: "70%" }}>
          <Button.Secondary
            onPress={() =>
              dispatch(
                navigateToAction({
                  navigateTo: {
                    stack: STACKS.WELCOME_STACK,
                  },
                })
              )
            }
          >
            <Button.Secondary.Text
              text={localization.language.AlreadyHaveLogin}
            />
          </Button.Secondary>
        </Form.Field>
        {/* <TouchableOpacity
          style={styles.buttonNext}
          onPress={() => setModalVisible(true)}
        >
          <Text style={styles.textButtonSmartlink}>
            {localization.language.ToYourSmartlink}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonDoesnthavelawyer}
          onPress={() =>
            dispatch(
              navigateToAction({
                navigateTo: {
                  stack: STACKS.WELCOME_STACK,
                },
              })
            )
          }
        >
          <Text style={styles.textButtonDoesnthavelawyer}>
            {localization.language.AlreadyHaveLogin}
          </Text>
        </TouchableOpacity> */}
        {/* TODO: restore when esything.io is up
                <View style={styles.viewRowQuestion} >
                    <TouchableOpacity style={styles.marginRight3} onPress={anyQuestionPress.bind(this)}>
                        <Text style={styles.textAnyQuestion}>{Language.Anyquestions}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonAnyQuestion} onPress={anyQuestionPress.bind(this)}>
                        <Text style={styles.textAnyQuestion}>{Language.MoreatwwwesyMandantde}</Text>
                    </TouchableOpacity>
                </View>
                */}
      </View>
      <InvitationOptions
        isVisible={isModalVisible}
        onClose={() => setModalVisible(false)}
      />
      <Flag
        countryId={language}
        onChange={(countryId) => dispatch(updateLanguageAction(countryId))}
      />
    </View>
  );
}

function SliderItemView({ item }: any) {
  const services = useContext(Services);
  const checkmark = services.assetsService.getIcons("iconCheckBlack");

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.viewImage}>
        <Image style={styles.imageOnBoarding} source={item.image} />
      </View>
      <View style={{ marginTop: 10, marginBottom: 10 }}>
        <Text style={styles.textImage}>{item.text}</Text>
      </View>
      {item.key === "4" ? (
        <View style={styles.flexDirectionColumn}>
          <View style={styles.viewRowPagination}>
            <View style={styles.marginRight10}>
              <Image source={checkmark} style={styles.imageCheckmark} />
            </View>
            <View>
              <Text style={styles.textActiveIndex3}>
                {localization.language.YouwillreceiveanSMSorEmailfromyourlawyer}
              </Text>
            </View>
          </View>
          <View style={styles.viewRowPagination}>
            <View style={styles.marginRight10}>
              <Image source={checkmark} style={styles.imageCheckmark} />
            </View>
            <View>
              <Text style={styles.textActiveIndex3}>
                {localization.language.FollowtheSmartlinkintheSMSorEmail}
              </Text>
            </View>
          </View>
          <View style={styles.viewRowPagination}>
            <View style={styles.marginRight10}>
              <Image source={checkmark} style={styles.imageCheckmark} />
            </View>
            <View>
              <Text style={styles.textActiveIndex3}>
                {
                  localization.language
                    .Thatsitnowyoucouldcommunicatewithyourlawyer
                }
              </Text>
            </View>
          </View>
        </View>
      ) : null}
    </View>
  );
}

function SliderPagination({ index }: any) {
  const { slidesDe, slidesEn } = useOnBoarding();

  return (
    <View style={styles.viewRenderPagination}>
      <View style={styles.viewDotStyle}>
        {slidesDe.length > 1 &&
          slidesDe.map((_, i) => (
            <TouchableOpacity
              key={i}
              style={[
                styles.buttonDot,
                i === index ? styles.dotInactive : styles.dotActive,
              ]}
              onPress={() => this.slider?.goToSlide(i, true)}
            />
          ))}
      </View>
    </View>
  );
}

function InvitationOptions({
  isVisible,
  onClose,
}: {
  isVisible: boolean;
  onClose: () => void;
}) {
  const dispatch = useDispatch();

  return (
    <ReactNativeModal
      style={styles.modalStyle}
      swipeDirection={["down"]}
      animationIn="slideInLeft"
      onSwipeComplete={onClose}
      onBackdropPress={onClose}
      isVisible={isVisible}
    >
      <View style={styles.viewContainerModal}>
        {/*
                    <View style={styles.viewModalTitle}>
                        <Text style={styles.textTitle}>{Language.Opensmartlinkwith}</Text>
                    </View>
                    */}
        <View style={styles.viewHeightModal}>
          <View style={styles.viewRowModal}>
            <View style={styles.viewRowIconModal}>
              <View style={styles.flexDirectionRow}>
                <FontAwesomeIcon
                  icon={faEnvelope}
                  size={25}
                  color="#404040"
                  style={{ marginTop: 3 }}
                />
                {/* <Icon name="ios-mail" style={styles.iconEmailAndMessage} /> */}
                <View style={styles.marginLeft15}>
                  <Text style={styles.textTitleEmailAndMessage}>E-mail</Text>
                </View>
              </View>
            </View>
            <View style={styles.viewButtonEmailAndMessage}>
              <View style={styles.alignItemsCenter}>
                <TouchableOpacity
                  style={styles.openButton}
                  onPress={() => dispatch(openExternalMailAction())}
                >
                  <Text style={styles.openText}>
                    {localization.language.Open}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.viewRowModal}>
            <View style={styles.viewRowIconModal}>
              <View style={styles.flexDirectionRow}>
                <FontAwesomeIcon
                  icon={faComment}
                  size={25}
                  color="#404040"
                  style={{ marginTop: 3 }}
                />
                {/* <Icon
                  name="ios-chatbubbles"
                  style={styles.iconEmailAndMessage}
                /> */}
                <View style={styles.marginLeft15}>
                  <Text style={styles.textTitleEmailAndMessage}>SMS</Text>
                </View>
              </View>
            </View>
            <View style={styles.viewButtonEmailAndMessage}>
              <View style={styles.alignItemsCenter}>
                <TouchableOpacity
                  style={styles.openButton}
                  onPress={() => dispatch(openExternalSMSAction())}
                >
                  <Text style={styles.openText}>
                    {localization.language.Open}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    </ReactNativeModal>
  );
}
