import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  flex1: {
    backgroundColor: "#FFF",
    flex: 1,
  },
  marginTop30: {
    marginTop: 30,
  },
  marginLeft15: {
    marginLeft: 15,
  },
  marginRight3: {
    marginRight: 3,
  },
  marginRight10: {
    marginRight: 10,
  },
  flexDirectionColumn: {
    flexDirection: "column",
  },
  flexDirectionRow: {
    flexDirection: "row",
  },
  alignItemsCenter: {
    alignItems: "center",
  },
  modalStyle: {
    flex: 1,
    justifyContent: "flex-end",
    margin: 0,
    position: "absolute",
    bottom: 0,
  },
  viewContainerModal: {
    height: 200,
    paddingBottom: 30,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: "white",
  },
  viewModalTitle: {
    padding: 10,
    margin: 10,
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "#d3d3d350",
    borderBottomWidth: 1,
  },
  textTitle: {
    fontSize: 16,
    fontFamily: "Lato-Bold",
    color: "#00a099",
  },
  viewHeightModal: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingTop: 30,
    height: "100%",
  },
  viewRowModal: {
    height: "35%",
    flexDirection: "row",
  },
  viewRowIconModal: {
    width: "70%",
    padding: 10,
    justifyContent: "center",
  },
  iconEmailAndMessage: {
    fontSize: 25,
    color: "#404040",
    marginTop: 3,
  },
  textTitleEmailAndMessage: {
    padding: 5,
    fontSize: 16,
    fontFamily: "Lato-Bold",
    color: "#555555",
  },
  viewButtonEmailAndMessage: {
    width: "30%",
    justifyContent: "center",
  },
  openButton: {
    backgroundColor: "#404040",
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 10,
  },
  openText: {
    fontSize: 14,
    fontFamily: "Lato-Bold",
    color: "white",
  },
  viewImage: {
    /*backgroundColor: '#00FF00',*/
    width: "100%",
    height: "50%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  imageOnBoarding: {
    width: "95%",
    height: "80%",
    resizeMode: "contain",
  },
  textImage: {
    textAlign: "center",
    color: "#404040",
    fontSize: 24,
    fontFamily: "Lato-Bold",
  },
  viewRenderPagination3: {
    position: "absolute",
    top: "60%",
    bottom: 0,
    left: 0,
    right: 0,
  },
  viewRenderPagination: {
    position: "absolute",
    top: "90%",
    bottom: 0,
    left: 0,
    right: 0,
  },
  viewRowPagination: {
    flexDirection: "row",
    marginRight: 30,
    marginLeft: 30,
  },
  imageCheckmark: {
    height: 20,
    width: 20,
    resizeMode: "contain",
  },
  textActiveIndex3: {
    color: "#404040",
    fontSize: 18,
    fontFamily: "Lato-Regular",
  },
  buttonNext: {
    padding: 12,
    borderRadius: 5,
    backgroundColor: "#404040",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginLeft: 50,
    marginRight: 50,
  },
  textButton: {
    fontSize: 20,
    fontFamily: "Lato-Bold",
    color: "#f3f4f4",
  },
  textButtonSmartlink: {
    fontSize: 18,
    fontFamily: "Lato-Bold",
    color: "#f3f4f4",
  },
  buttonDoesnthavelawyer: {
    padding: 12,
    borderRadius: 5,
    backgroundColor: "#FFFF",
    borderColor: "#404040",
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 2,
    marginLeft: 50,
    marginRight: 50,
  },
  textButtonDoesnthavelawyer: {
    fontSize: 14,
    fontFamily: "Lato-Bold",
    color: "#231f20",
  },
  viewRowQuestion: {
    flexDirection: "row",
    padding: 10,
    marginLeft: 30,
    marginRight: 30,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 2,
  },
  textAnyQuestion: {
    color: "#429F98",
    fontSize: 12,
    fontFamily: "Lato-Regular",
  },
  viewDotStyle: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-end",
    marginBottom: 14,
  },
  buttonDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 4,
  },
  dotInactive: {
    backgroundColor: "#404040",
  },
  dotActive: {
    borderColor: "#404040",
    borderStyle: "solid",
    borderWidth: 1,
  },
});
