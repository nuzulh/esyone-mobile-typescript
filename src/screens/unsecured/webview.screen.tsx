import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useNavigation } from "@react-navigation/native";
import { useEffect } from "react";
import { Platform, Text, TouchableOpacity, View } from "react-native";
import WebView from "react-native-webview";
import { useDispatch } from "react-redux";
import { SafeAreaView } from "../../components/safe-area-view.component";
import {
  deserialize,
  hideLoadingAction,
  showLoadingAction,
} from "../../helpers";

export function WebViewScreen({ route }) {
  const navigation = useNavigation();
  const { url, title } = route.params ? deserialize(route.params) : ({} as any);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(showLoadingAction());
  }, []);

  const onComplete = () => dispatch(hideLoadingAction());

  return (
    <View style={{ flex: 1, backgroundColor: "#ffffff" }}>
      <SafeAreaView />
      {/* <Header
        type={"Webview"}
        headerText={header === undefined ? "Externe Afrage 2" : header}
        headerTextTitle={item}
        mainColor={mainColor}
        onPress={() => navigation.goBack()}
      /> */}
      <View
        style={[
          {
            padding: 10,
            height: 70,
            flexDirection: "row",
            shadowColor: "#000",
            shadowOffset: { width: 1, height: Platform.OS == "ios" ? 5 : 1 },
            shadowOpacity: Platform.OS == "ios" ? 0.1 : 0.4,
            shadowRadius: 3,
            elevation: Platform.OS == "ios" ? 1 : 5,
          },
          { backgroundColor: "#FFFFFF" },
        ]}
      >
        <TouchableOpacity
          style={{
            width: "10%",
            justifyContent: "center",
            alignItems: "center",
          }}
          onPress={() => navigation.goBack()}
        >
          <FontAwesomeIcon icon={faTimes} color={"#404040"} size={18} />
        </TouchableOpacity>
        <View style={{ width: "80%", justifyContent: "center" }}>
          <Text
            numberOfLines={1}
            style={{
              fontSize: 14,
              fontFamily: "Lato-Regular",
              color: "#404040",
            }}
          >
            {title}
          </Text>
          <Text
            numberOfLines={1}
            style={{
              fontSize: 10,
              fontFamily: "Lato-Regular",
              color: "#404040",
            }}
          >
            {url}
          </Text>
        </View>
      </View>
      <WebView
        source={{ uri: url }}
        style={{ flex: 1 }}
        onLoad={() => onComplete()}
        onError={() => onComplete()}
        onHttpError={() => onComplete()}
      />
    </View>
  );
}
