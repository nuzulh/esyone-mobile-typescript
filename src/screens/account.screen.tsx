import { faArrowLeft, faEllipsisV } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import CheckBox from "@react-native-community/checkbox";
import { useNavigation } from "@react-navigation/native";
import { useContext, useState } from "react";
import {
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
} from "react-native";
// import { CheckBox } from "react-native-elements";
import FastImage from "react-native-fast-image";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger,
} from "react-native-popup-menu";
import { useDispatch, useSelector } from "react-redux";
import {
  AccountContact,
  Button,
  Form,
  ShimmerPlaceholder,
} from "../components";
import { SafeAreaView } from "../components/safe-area-view.component";
import {
  activateAccount,
  deserialize,
  getTextColor,
  loadWorkflow,
  localization,
  previewAgb,
  previewAgbText,
  previewDatenschutz,
  previewDatenschutzText,
  shareAccount,
  shareAssistant,
} from "../helpers";
import { useEsyState, useImage } from "../hooks";
import { Account, RootState } from "../models";
import { Services } from "../services";

export function AccountScreen({ route }: any) {
  let { account: _account } = (
    route.params ? deserialize(route.params) : {}
  ) as {
    account: Account;
  };
  const { contact, welcomeHeadline, welcomeText } = _account;

  let account = useEsyState((state) => ({
    ...(state.auth?.accounts[_account.id] || _account),
    contact,
    welcomeHeadline,
    welcomeText,
  }));

  const dispatch = useDispatch();
  const navigation = useNavigation();
  const services = useContext(Services);
  const mainLogo = useImage(() =>
    services.accountService.getMainLogo(account.id)
  );

  const secondaryLogo = useImage(() =>
    services.accountService.getSecondaryLogo(account.id)
  );

  return (
    <View style={{ flex: 1, backgroundColor: "#E5E5E5" }}>
      <SafeAreaView />
      <View
        style={{
          padding: 15,
          height: 70,
          backgroundColor: "#FFFFFF",
          flexDirection: "row",
          justifyContent: "space-between",
          shadowColor: "#000",
          shadowOffset: { width: 1, height: Platform.OS == "ios" ? 5 : 1 },
          shadowOpacity: Platform.OS == "ios" ? 0.1 : 0.4,
          shadowRadius: 3,
          elevation: Platform.OS == "ios" ? 1 : 5,
        }}
      >
        <TouchableOpacity
          style={{
            width: "10%",
            justifyContent: "center",
            alignItems: "center",
          }}
          onPress={async () => navigation.goBack()}
        >
          <FontAwesomeIcon icon={faArrowLeft} color={"#404040"} size={18} />
        </TouchableOpacity>
        <View
          style={{
            width: "80%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <FastImage
            source={
              account.primaryLogoUrl
                ? { uri: account.primaryLogoUrl }
                : { uri: mainLogo.imageUri, headers: mainLogo.headers }
            }
            style={{
              alignContent: "center",
              backgroundColor: "#FFFFFF",
              width: 56,
              height: 56,
            }}
            resizeMode={FastImage.resizeMode.contain}
          />
        </View>
        <View
          style={{
            width: "10%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Menu>
            <MenuTrigger>
              <FontAwesomeIcon icon={faEllipsisV} color={"#404040"} size={18} />
            </MenuTrigger>
            <MenuOptions>
              <MenuOption onSelect={() => dispatch(previewAgb(account))}>
                <Text>AGB</Text>
              </MenuOption>
              <MenuOption
                onSelect={() => dispatch(previewDatenschutz(account))}
              >
                <Text>Datenschutz</Text>
              </MenuOption>
            </MenuOptions>
          </Menu>
        </View>
      </View>
      <ScrollView contentContainerStyle={{ flexGrow: 1 }} style={{ flex: 1 }}>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            paddingTop: 16,
            paddingRight: 16,
            paddingLeft: 16,
          }}
        >
          {!account.secondaryLogoUrl && !secondaryLogo.imageUri ? (
            <ShimmerPlaceholder visible={true}>
              <View
                style={{
                  height: undefined,
                  width: "100%",
                  aspectRatio: 1.778,
                  borderRadius: 15,
                }}
              ></View>
            </ShimmerPlaceholder>
          ) : (
            <FastImage
              source={
                account.secondaryLogoUrl
                  ? { uri: account.secondaryLogoUrl }
                  : {
                    uri: secondaryLogo.imageUri,
                    headers: secondaryLogo.headers,
                  }
              }
              style={{
                width: "100%",
                height: undefined,
                aspectRatio: 1.778,
                borderRadius: 15,
              }}
              resizeMode={FastImage.resizeMode.contain}
            />
          )}
        </View>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            margin: 0,
            paddingHorizontal: 16,
            paddingTop: 16,
          }}
        >
          <Text
            numberOfLines={1}
            style={[
              { flex: 1, fontSize: 17.5, fontFamily: "Lato-Bold" },
              { color: "#404040" },
            ]}
          >
            {account.name}
          </Text>
          <TouchableOpacity
            onPress={() => dispatch(shareAccount(account))}
            style={{ width: 30 }}
          >
            <FastImage
              source={services.assetsService.getIcons("shareAndroid")}
              style={{ height: 24, width: 24 }}
            />
          </TouchableOpacity>
        </View>
        {account.welcomeHeadline != undefined &&
          account.welcomeHeadline !== "Longstring_HomepageDefaultTitle" &&
          account.welcomeText != undefined &&
          account.welcomeText !== "Longstring_HomepageDefaultDescription" ? (
          <View
            style={{
              alignItems: "flex-start",
              marginHorizontal: 16,
              marginTop: 32,
              marginBottom: 16,
            }}
          >
            {account.welcomeHeadline != undefined &&
              account.welcomeHeadline !== "Longstring_HomepageDefaultTitle" ? (
              <Text
                numberOfLines={0}
                style={[
                  { fontSize: 17.5, fontFamily: "Lato-Bold" },
                  { color: "#404040" },
                ]}
              >
                {account.welcomeHeadline}
              </Text>
            ) : null}
            {account.welcomeText != undefined &&
              account.welcomeText !== "Longstring_HomepageDefaultDescription" ? (
              <Text
                numberOfLines={0}
                style={[
                  { fontSize: 14, fontFamily: "Lato-Regular", marginTop: 16 },
                  { color: "#404040" },
                ]}
              >
                {account.welcomeText}
              </Text>
            ) : null}
          </View>
        ) : null}
        <View style={{ padding: 16 }}>
          <View style={{ margin: 0 }}>
            <Text
              style={{
                fontSize: 17.5,
                fontFamily: "Lato-Bold",
                color: "#404040",
              }}
            >
              Services
            </Text>
            {account.assistants.map((assistant, index) => (
              <Assistant key={index} assistant={assistant} account={account} />
            ))}
          </View>
        </View>
        <AccountActivation account={account} />
        <AccountContact account={account} />
      </ScrollView>
    </View>
  );
}

function Assistant({ account, assistant }) {
  const dispatch = useDispatch();
  const accessToken = useSelector(
    (state: RootState) => state.esy.auth.session.accessToken
  );
  const colorAbbr = getTextColor(account.mainColor);
  var abbr = "";
  if (assistant.name.length == 1) abbr = assistant.name.substring(0, 1);
  else abbr = assistant.name.substring(0, 2);
  const services = useContext(Services);
  // console.log(assistant.id, assistant.key)

  return (
    <View
      style={{
        marginTop: 12,
        padding: 16,
        borderRadius: 15,
        backgroundColor: "#FFFFFF",
        borderColor: "#e0e0e0",
        borderWidth: 1,
        flexDirection: "row",
        width: "100%",
      }}
    >
      <View
        style={{
          width: 56,
          justifyContent: "flex-start",
          alignItems: "center",
        }}
      >
        {assistant.logoUrl != null && assistant.logoUrl !== "" ? (
          <View style={{ width: "100%" }}>
            <FastImage
              source={{ uri: assistant.logoUrl }}
              style={{ height: 56, width: 56, borderRadius: 15 }}
            />
          </View>
        ) : (
          <View
            style={{
              borderRadius: 15,
              backgroundColor: account.mainColor
                ? account.mainColor
                : account.primaryColor,
              width: "100%",
              padding: 12,
            }}
          >
            <View
              style={{
                height: 32,
                width: 32,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text
                style={{
                  color: colorAbbr,
                  fontSize: 20,
                  fontFamily: "Lato-Bold",
                }}
              >
                {abbr}
              </Text>
            </View>
          </View>
        )}
      </View>
      <View style={{ flex: 1, flexDirection: "column", marginLeft: 16 }}>
        <Text
          style={{
            fontSize: 17.5,
            fontFamily: "Lato-Bold",
            color: "#404040",
            marginRight: 5,
            marginBottom: 8,
          }}
        >
          {assistant.name}
        </Text>
        {assistant.description != null && assistant.description !== "" ? (
          <Text
            style={{
              fontSize: 14,
              fontFamily: "Lato-Regular",
              color: "#404040",
              marginRight: 5,
              marginBottom: 8,
            }}
          >
            {assistant.description}
          </Text>
        ) : null}
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <TouchableOpacity
            style={{ flex: 1 }}
            onPress={() =>
              dispatch(loadWorkflow(account.id, assistant.id, accessToken))
            }
          >
            <Text
              // numberOfLines={0}
              style={{
                fontSize: 14,
                fontFamily: "Lato-Bold",
                color: "#404040",
                textDecorationLine: "underline",
                marginRight: 5,
              }}
            >
              {localization.language.ServiceProvider_LearnMore}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => dispatch(shareAssistant(assistant))}
            style={{ width: 30 }}
          >
            <FastImage
              source={services.assetsService.getIcons("shareAndroid")}
              style={{ height: 24, width: 24 }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

function AccountActivation({ account }) {
  const dispatch = useDispatch();
  const [accepted, setAccepted] = useState(false);

  if (account.isActivated) return null;

  return (
    <>
      <View
        style={{
          alignItems: "flex-start",
          justifyContent: "flex-start",
          flexDirection: "row",
          marginTop: 10,
          paddingHorizontal: 16,
          marginBottom: 30,
        }}
      >
        <CheckBox
          style={{ padding: 0, marginTop: 0 }}
          onValueChange={(value) => setAccepted(value)}
          value={accepted}
        />
        <View
          style={{
            flexDirection: "column",
            alignItems: "flex-start",
            flex: 1,
            marginLeft: 10,
          }}
        >
          {account.esyThingAgb ? (
            <View
              style={{
                marginBottom: 5,
                flexDirection: "row",
                flexWrap: "wrap",
              }}
            >
              <Text>
                {localization.language.ScreenAuthWelcomeAGB1}{" "}
                <Text
                  onPress={() => dispatch(previewAgbText(account))}
                  style={{ fontWeight: "bold", color: "#00a099" }}
                >
                  {localization.language.ScreenAuthWelcomeAGBLink}
                </Text>{" "}
                {localization.language.ScreenAuthWelcomeAGB2}{" "}
                <Text style={{ fontWeight: "bold" }}>
                  {account.name != undefined
                    ? account.name
                    : account.nameProvider}
                </Text>{" "}
                {localization.language.ScreenAuthWelcomeAGB3}
              </Text>
            </View>
          ) : null}
          {account.esyThingDatenschutz ? (
            <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
              <Text>
                {localization.language.ScreenAuthDatenschutz1}{" "}
                <Text
                  onPress={() => dispatch(previewDatenschutzText(account))}
                  style={{ fontWeight: "bold", color: "#00a099" }}
                >
                  {localization.language.ScreenAuthDatenschutzLink}
                </Text>{" "}
                {localization.language.ScreenAuthDatenschutz3}
              </Text>
            </View>
          ) : null}
        </View>
      </View>
      <Form.Field style={{ paddingHorizontal: 16 }}>
        <Button.Primary
          onPress={() => dispatch(activateAccount(account))}
          disabled={!accepted}
        >
          <Button.Primary.Text text={localization.language.Aktivieren} />
        </Button.Primary>
      </Form.Field>
      <View style={{ marginBottom: 10 }}></View>
    </>
  );
}
