import { useNavigation } from "@react-navigation/native";
import moment from "moment";
import { useMemo } from "react";
import { ScrollView, Text, View } from "react-native";
import {
  Button,
  DynamicFormPreview,
  Header,
  HeaderOffline,
  HeaderOnline,
  UnknownError,
} from "../../components";
import { SafeAreaView } from "../../components/safe-area-view.component";
import { AccountContext } from "../../context";
import { deserialize, localization } from "../../helpers";
import { Account, WorkflowProcess } from "../../models";

export function FormAnswerScreen({ route }: { route: any }) {
  const { workflowProcess, account } = (
    route.params ? deserialize(route.params) : {}
  ) as {
    workflowProcess: WorkflowProcess;
    account: Account;
  };
  const latestAnswer = useMemo(
    () =>
      workflowProcess.answers && workflowProcess.answers.length > 0
        ? workflowProcess.answers.sort(
            (a, b) =>
              b.answerCreationDate.getTime() - a.answerCreationDate.getTime()
          )[0]
        : null,
    [workflowProcess]
  );

  if (!workflowProcess || !workflowProcess.answers) return <UnknownError />;

  return (
    <AccountContext.Provider value={{ account }}>
      <View
        style={{
          flex: 1,
          backgroundColor: "#FFFFFF",
        }}
      >
        <SafeAreaView />
        <Header>
          <Header.Offline>
            <HeaderOffline showBackButton={true} showLogo={false}>
              <FormAnswerTitle
                workflowProcess={workflowProcess}
                account={account}
              />
            </HeaderOffline>
          </Header.Offline>
          <Header.Online>
            <HeaderOnline showBackButton={true} showLogo={false}>
              <FormAnswerTitle
                workflowProcess={workflowProcess}
                account={account}
              />
            </HeaderOnline>
          </Header.Online>
        </Header>
        <ScrollView>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginVertical: 16,
            }}
          >
            <Text
              style={{
                fontSize: 22,
                lineHeight: 32,
                fontWeight: "600",
                color: "#404040",
              }}
            >
              {workflowProcess.assistantName}
            </Text>
            <Text
              style={{
                fontSize: 14,
                lineHeight: 20,
                fontWeight: "600",
                color: "#219653",
              }}
            >
              {`Erfasst am ${moment(latestAnswer.answerCreationDate).format(
                "DD.MM.YY, HH:mm"
              )}`}
            </Text>
          </View>
          <DynamicFormPreview answer={latestAnswer} />
        </ScrollView>
      </View>
    </AccountContext.Provider>
  );
}

function FormAnswerTitle({
  workflowProcess,
  account,
}: {
  workflowProcess: WorkflowProcess;
  account: Account;
}) {
  return (
    <View
      style={{
        width: "80%",
        justifyContent: "center",
        marginLeft: 3,
        marginRight: 3,
      }}
    >
      <Text
        numberOfLines={1}
        style={{
          fontSize: 15,
          fontFamily: "Lato-Regular",
          color: "#404040",
        }}
      >
        {workflowProcess.assistantName}
      </Text>
      <Text
        numberOfLines={1}
        style={{
          fontSize: 10,
          fontFamily: "Lato-Regular",
          color: "#404040",
        }}
      >
        {localization.language.CreatorName}: {account.name}
      </Text>
    </View>
  );
}
