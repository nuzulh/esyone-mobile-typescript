import { Image, Text, TouchableOpacity, View, ScrollView } from "react-native";
import { AUTH_STACK, HOME_STACK, STACKS, deserialize, navigateToAction } from "../../helpers";
import { Account, RootState } from "../../models";
import { useDispatch, useSelector } from "react-redux";

export function FormSuccessScreen({ route }: { route: any; }) {
  const { account, title } = (
    route.params ? deserialize(route.params) : {}
  ) as { account: Account; title: string; };
  const dispatch = useDispatch();
  const isLoggedIn = useSelector(
    (state: RootState) => state.esy.auth.isLoggedIn
  );

  return (
    <>
      <View
        style={{
          flex: 1,
          paddingHorizontal: 24,
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
        }}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <View
            style={{
              width: 150,
              height: 150,
              // marginVertical: 50,
              justifyContent: "center",
              alignItems: "center",
              alignContent: "center",
              marginTop: 30,
            }}
          >
            <Image
              style={{ width: 150, height: 150, borderRadius: 150 / 2 }}
              source={{
                uri: account.primaryLogoUrl,
              }}
            />
          </View>
          <Text
            numberOfLines={0}
            style={{
              fontSize: 20,
              fontFamily: "Lato-Regular",
              color: "#ADADAD",
              lineHeight: 32,
              fontWeight: "400",
              alignSelf: "center",
              marginTop: 12,
            }}
          >
            {account.name}
          </Text>
          <Text
            style={{
              fontSize: 18,
              fontFamily: "Lato-Regular",
              color: "#404040",
              lineHeight: 22,
              fontWeight: "600",
              textAlign: "center",
              marginTop: 30,
              // textTransform: 'capitalize',
            }}
          >
            {title ? title : `Sie haben Ihre Antwort erfolgreich gesendet`}
          </Text>
          <View style={{ marginVertical: 32, width: "100%" }}>
            <TouchableOpacity
              onPress={() =>
                dispatch(
                  navigateToAction({
                    navigateTo: {
                      stack: isLoggedIn ? STACKS.HOME_STACK : STACKS.AUTH_STACK,
                      screen: isLoggedIn
                        ? HOME_STACK.HOME_SCREEN
                        : AUTH_STACK.SIGN_IN_SCREEN,
                    },
                  })
                )
              }
              style={{
                borderWidth: 1,
                backgroundColor: "#404040",
                borderColor: "#404040",
                borderRadius: 15,
                width: "30%",
                paddingVertical: 10,
                alignSelf: "center",
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: "Lato-Regular",
                  color: "white",
                  lineHeight: 28,
                  fontWeight: "600",
                  textAlign: "center",
                  textTransform: "capitalize",
                }}
              >
                Back
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </>
  );
}
