import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useNavigation } from "@react-navigation/native";
import { useMemo } from "react";
import {
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { DynamicForm } from "../../components";
import { SafeAreaView } from "../../components/safe-area-view.component";
import { deserialize, formatInitials, localization } from "../../helpers";
import { WorkflowProcess } from "../../models";
import { FilePreview } from "../../components/dynamic-form/preview/file-preview.component";
import { AccountContext } from "../../context";

export function FormProcessScreen({ route }: any) {
  const {
    account,
    workflowProcess,
    insurances,
    accessToken,
    files,
    uploadInfo,
    workflowFormular,
  } = route.params
      ? deserialize(route.params)
      : ({} as any);
  const logoUrl = useMemo(
    () => workflowProcess.headerLogoUrl || account.secondaryLogoUrl,
    []
  );

  return (
    <View style={{ flex: 1, backgroundColor: "#ffffff" }}>
      <SafeAreaView />
      <FormProcessHeader workflowProcess={workflowProcess} />
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{
          flex: 1,
          backgroundColor: "#FFFFFF",
        }}
      >
        <ScrollView
          contentContainerStyle={{ paddingBottom: 150 }}
          scrollEnabled={true}
        >
          <View style={{ paddingLeft: 24, paddingRight: 24, marginTop: 20 }}>
            <View
              style={{
                backgroundColor: account.mainColor
                  ? account.mainColor
                  : "white",
                width: "100%",
                height: 164,
                borderRadius: 10,
                justifyContent: "center",
                alignItems: "center",
                marginBottom: 10,
              }}
            >
              {logoUrl ? (
                <Image
                  style={{
                    width: "100%",
                    borderRadius: 10,
                    height: "100%",
                  }}
                  source={{
                    uri: logoUrl,
                  }}
                />
              ) : (
                <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                  {workflowProcess.assistantName
                    ? formatInitials(workflowProcess.assistantName)
                    : "AA"}
                </Text>
              )}
            </View>
            <Text
              style={{
                fontWeight: "400",
                color: "#404040",
                fontSize: 14,
                textAlign: "justify",
              }}
            >
              {workflowProcess.description || ""}
            </Text>
          </View>
          <AccountContext.Provider value={{ account }}>
            <FilePreview files={files} />
          </AccountContext.Provider>
          <DynamicForm
            account={account}
            insurances={insurances}
            workflowProcess={workflowProcess}
            accessToken={accessToken}
            uploadInfo={uploadInfo}
            workflowFormular={workflowFormular}
          />
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
}

function FormProcessHeader({
  workflowProcess,
}: {
  workflowProcess: WorkflowProcess;
}) {
  const navigation = useNavigation();

  return (
    <View
      style={[
        {
          padding: 10,
          height: 70,
          flexDirection: "row",
          shadowColor: "#000",
          shadowOffset: { width: 1, height: Platform.OS == "ios" ? 5 : 1 },
          shadowOpacity: Platform.OS == "ios" ? 0.1 : 0.4,
          shadowRadius: 3,
          elevation: Platform.OS == "ios" ? 1 : 5,
        },
        { backgroundColor: "#FFFFFF" },
      ]}
    >
      <TouchableOpacity
        style={{ width: "10%", justifyContent: "center", alignItems: "center" }}
        onPress={() => navigation.goBack()}
      >
        <FontAwesomeIcon icon={faArrowLeft} color={"#404040"} size={18} />
      </TouchableOpacity>
      <View style={{ width: "80%", justifyContent: "center" }}>
        <Text
          numberOfLines={1}
          style={{
            fontSize: 17,
            fontFamily: "Lato-Regular",
            color: "#404040",
            textAlign: "center",
            textTransform: "capitalize",
            fontWeight: "700",
          }}
        >
          {workflowProcess.assistantName}
        </Text>
      </View>
    </View>
  );
}
