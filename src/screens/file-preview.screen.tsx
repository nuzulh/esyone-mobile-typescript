import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useNavigation } from "@react-navigation/native";
import { useContext, useMemo } from "react";
import {
  Image,
  Platform,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
} from "react-native";
import { SafeAreaView } from "../components/safe-area-view.component";
import {
  deserialize,
  downloadAttachmentAction,
  localization,
} from "../helpers";
import { MetaFile, Services, VirtualMetaFile } from "../services";
import ImageZoom from "react-native-image-pan-zoom";
import { useFileReader, usePromise } from "../hooks";
import PDFView from "react-native-view-pdf";
import { useDispatch } from "react-redux";

export function FilePreviewScreen({ route }: any) {
  const {
    metaFile,
    hideDownload,
  } = (route.params ? deserialize(route.params) : {}) as {
    metaFile: MetaFile;
    hideDownload?: boolean;
  };
  // console.log(decodeURIComponent(metaFile.filePath));

  return (
    <View style={{ flex: 1, backgroundColor: "#F2F2F2" }}>
      <SafeAreaView />
      <FilePreviewHeader metaFile={metaFile} hideDownload={hideDownload} />
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          backgroundColor: "#030D120D",
        }}
      >
        <Viewer metaFile={metaFile} />
      </View>
    </View>
  );
}

function FilePreviewHeader({
  metaFile,
  hideDownload,
}: {
  metaFile: MetaFile;
  hideDownload?: boolean;
}) {
  const navigation = useNavigation();
  const services = useContext(Services);
  const dispatch = useDispatch();

  return (
    <View
      style={[
        {
          padding: 10,
          height: 70,
          flexDirection: "row",
          shadowColor: "#000",
          shadowOffset: { width: 1, height: Platform.OS == "ios" ? 5 : 1 },
          shadowOpacity: Platform.OS == "ios" ? 0.1 : 0.4,
          shadowRadius: 3,
          elevation: Platform.OS == "ios" ? 1 : 5,
        },
        { backgroundColor: "#FFFFFF" },
      ]}
    >
      <TouchableOpacity
        style={{ width: "10%", justifyContent: "center", alignItems: "center" }}
        onPress={() => navigation.goBack()}
      >
        <FontAwesomeIcon icon={faArrowLeft} color={"#404040"} size={18} />
      </TouchableOpacity>
      <View style={{ width: "75%", justifyContent: "center" }}>
        <Text
          numberOfLines={1}
          style={{ fontSize: 12, fontFamily: "Lato-Regular", color: "#404040" }}
        >
          {metaFile.fileName}
        </Text>
      </View>
      {!hideDownload ? (
        <TouchableOpacity
          style={{
            width: "15%",
            justifyContent: "center",
            alignItems: "flex-end",
          }}
          onPress={() => dispatch(downloadAttachmentAction(metaFile))}
        >
          <Image
            source={services.assetsService.getIcons("iconDownload")}
            style={{ height: 20, width: 20, marginHorizontal: 10 }}
          />
        </TouchableOpacity>
      ) : null}
    </View>
  );
}

function Viewer({ metaFile }: { metaFile: MetaFile; }) {
  const services = useContext(Services);

  if (metaFile.contentType.startsWith("image"))
    return <ImageViewer metaFile={metaFile} />;
  else if (metaFile.contentType === "application/pdf")
    return <PdfViewer metaFile={metaFile} />;
  else if (metaFile.contentType === "text/datenschutz")
    return (
      <TextContentViewer
        title={localization.language.ScreenAuthDatenschutz}
        metaFile={metaFile as VirtualMetaFile}
      />
    );
  else if (metaFile.contentType === "text/agb")
    return (
      <TextContentViewer
        title={localization.language.ScreenAuthAGB}
        metaFile={metaFile as VirtualMetaFile}
      />
    );
  else
    return (
      <View
        style={{
          width: "100%",
          height: "100%",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Image
          source={services.assetsService.getIcons("iconImage")}
          style={{ height: 50, width: 50, resizeMode: "contain" }}
        />
        <Text style={{ marginTop: 40 }}>
          {localization.language.ScreenPreview_NotAvailable}
        </Text>
      </View>
    );
}

function ImageViewer({ metaFile }) {
  const screenWidth = Dimensions.get("window").width;
  const screenHeight = Dimensions.get("window").height;
  const { content, error } = useFileReader(metaFile.filePath, "base64", [
    metaFile,
  ]);
  // const imageDimension = usePromise<{ width: number; height: number }>(
  //   () =>
  //     new Promise((resolve, reject) => {
  //       if (!content) return resolve(null);

  //       return Image.getSize(
  //         `data:image/png;base64,${content}`,
  //         (width, height) => resolve({ width, height }),
  //         (error) => reject(error)
  //       );
  //     }),
  //   { width: 0, height: 0 },
  //   [metaFile]
  // );
  // const scaledDimension = useMemo(() => {
  //   if (!imageDimension.result) return { width: 0, height: 0 };

  //   if (
  //     ((1.0 * imageDimension.result.height) / imageDimension.result.width) *
  //       screenWidth <
  //     0.85 * screenHeight
  //   )
  //     return {
  //       width: screenWidth,
  //       height:
  //         (imageDimension.result.height / imageDimension.result.width) *
  //         screenWidth,
  //     };
  //   else
  //     return {
  //       width:
  //         (imageDimension.result.width / imageDimension.result.height) *
  //         0.8 *
  //         screenHeight,
  //       height: 0.8 * screenHeight,
  //     };
  // }, [imageDimension]);
  const zoomProperties: any = useMemo(
    () => ({
      cropWidth: screenWidth,
      cropHeight: screenHeight - 80,
      // width: screenWidth,
      // height: screenHeight - 80,
    }),
    []
  );

  if (error || !content) return (
    <Text style={{ textAlign: "center" }}>
      {localization.language.FilePreviewPrepare}
    </Text>
  );

  // if (!content) return <Text>Loading</Text>;

  return (
    <View
      style={{
        flex: 1,
        marginTop: 0,
        padding: 0,
        alignContent: "center",
        justifyContent: "center",
      }}
    >
      <View style={{ alignItems: "center" }}>
        {/* <ImageZoom cropWidth={screenWidth} cropHeight={screenHeight - 80} imageWidth={0} imageHeight={0}>
          <Image
            source={{ uri: `data:image/png;base64,${content}` }}
            // source={require('../../../assets/icon/icon-file.png')}
            style={{
              resizeMode: "contain",
            }}
          />
        </ImageZoom> */}
        {/* <ImageZoom {...zoomProperties}> */}
        <Image
          source={{ uri: `data:image/png;base64,${content}` }}
          // source={require('../../../assets/icon/icon-file.png')}
          style={{
            width: screenWidth,
            height: screenHeight - 80,
            resizeMode: "contain",
          }}
        />
        {/* </ImageZoom> */}
      </View>
    </View>
  );
}

function PdfViewer({ metaFile }) {
  const services = useContext(Services);
  const { content, error } = useFileReader(metaFile.filePath, "base64", [
    metaFile,
  ]);

  if (error || !content) return (
    <Text style={{ textAlign: "center" }}>
      {localization.language.FilePreviewPrepare}
    </Text>
  );

  // if (!content) return <Text>Loading</Text>;

  return (
    // <ScrollView
    //   contentContainerStyle={{
    //     flex: 1,
    //     marginTop: 0,
    //     padding: 0,
    //     alignContent: "center",
    //     justifyContent: "center",
    //   }}
    //   showsVerticalScrollIndicator={true}
    // >
    <PDFView
      style={{ height: "100%" }}
      onError={(error) => services.logService.error(new Error(error.message))}
      onLoad={() =>
        services.logService.debug(
          `Render PDF ${metaFile.id} from base 64 data`
        )
      }
      resourceType="base64"
      resource={content}
    />
    // </ScrollView>
  );
}

function TextContentViewer({
  title,
  metaFile,
}: {
  title: string;
  metaFile: VirtualMetaFile;
}) {
  return (
    <View
      style={{
        height: "100%",
        alignContent: "center",
        justifyContent: "center",
        paddingTop: 0,
        backgroundColor: "white",
      }}
    >
      <ScrollView
        style={{
          paddingLeft: 20,
          paddingRight: 20,
          marginTop: 10,
          marginBottom: 20,
          flex: 1,
        }}
      >
        <Text
          style={{
            color: "#429f98",
            fontSize: 24,
            fontFamily: "Lato-Regular",
            textAlign: "center",
            paddingBottom: 20,
          }}
        >
          {title}
        </Text>
        <Text>{metaFile.content}</Text>
      </ScrollView>
    </View>
  );
}
