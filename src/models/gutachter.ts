export interface Gutachter {
  id: number;
  globalUniqeId: string;
  name: string;
  participantContactPersonFirstName?: string;
  participantContactPersonLastName?: string;
  contactPhone: string;
  foreignId?: string;
  maxUploadSizes?: number;
  externalMaxUploadSizes?: number;
  allowedSuffixes?: any;
  protocol?: any;
  leistungsversprechen: boolean;
  manMailAllowed: boolean;
  manFaxAllowed: boolean;
  type: number;
  canReceiveDamageReport: boolean;
  priority: number;
  secondaryForeignId?: string;
  preferredContacts?: string;
  hasNonDisclosureAgreement: boolean;
  mobile: string;
  eMail: string;
  street: string;
  postalCode: string;
  city: string;
}

export interface GutachterSpec {
  participants: Gutachter[];
  total: number;
  date: string;
}
