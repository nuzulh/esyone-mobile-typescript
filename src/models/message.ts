import { MetaFile } from "../services";

export interface Message {
  id: string;
  header: string;
  message: string;
  read: boolean;
  creatorId: string;
  creatorName: string;
  creationDate: Date;
  attachments?: MessageAttachment[];
  isFavorite: boolean;
  isOwnMessage: boolean;
  labels: string[];
  belegdatum?: Date;
}

export interface MessageAttachment {
  id: any;
  messageId?: string;
  fileName: string;
  metaFile?: MetaFile;
  // filePath?: string;
  creationDate: Date;
  // contentType: string;
  // size: number;
  // extension: string;
}
