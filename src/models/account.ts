import { Assistant } from "./assistant";
import { ExternalFormula } from "./forms";

export interface Account {
  id: any;
  name: string;
  shortName: string;
  contact: Contact | null;
  address: string;
  mobileNumber: string;
  externalFormulas: ExternalFormula[];
  mainColor: string;
  secondaryColor: string;
  categories: string[];
  esyThingAgb: string;
  esyThingDatenschutz: string;
  isActivated: boolean;
  assistants: Assistant[];
  autoResponderSetting: {
    isActivated: boolean;
    message: null;
  };
  imageToPDFConversionSetting: {
    isActivated: boolean;
    fileName: string;
  };
  uniqueAccountKey: string;
  isImageCompressionEnabled: boolean;
  isFilterOutSystemMessages: boolean;
  primaryLogoUrl: string;
  secondaryLogoUrl: string;
  welcomeHeadline: string;
  welcomeText: string;
}

export interface Contact {
  street: string;
  postalCode: string;
  city: string;
  phone: string;
  fax: string;
  website: string;
}

export declare type AccountsMap = {
  [key: string]: Account;
};
