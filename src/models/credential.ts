import { AccountsMap } from "./account";
import { Profile } from "./profile";

export interface Credential {
  userId: string;
  secret: string;
}

export interface OfflineCredential extends Credential {
  accessToken: string;
  profile: Profile;
  accounts: AccountsMap;
}
