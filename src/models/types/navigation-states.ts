export declare type NavigationState = {
  current: NavigationTarget;
};

export declare type NavigationRoute = {
  stack?: string;
  screen?: string;
  params?: any;
};

export declare type NavigationTarget = {
  goBack?: boolean;
  replace?: boolean;
  reset?: boolean;
  navigateTo: NavigationRoute | NavigationRoute[];
};
