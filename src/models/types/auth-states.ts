import { Account, AccountsMap } from "../account";
import { Profile } from "../profile";
import { Session } from "../session";

export declare type AuthState = {
  id: string;
  session: Session;
  profile: Profile;
  accounts: AccountsMap;
  isLoggedIn: boolean;
  isError: boolean;
  error: Error;
  errorMessage: string;
  // accountId: string;
  // userHashId: string;
  // accessToken: string;
  // login: boolean; // should login true or false
  // userStatus: number; // check is user already onboarding
};

/* export declare type AuthSession = {
  tokenType: string;
  accessToken: string;
  refreshToken: string;
  offlineCredential: string;
  idToken: string;
  login: boolean;
}; */

/* export declare type AuthProfile = {
  // authentication: string;
  // akteId: string;
  // isSmsAvailable: boolean;
  accountId: string;
  userId: string;
  email: string;
  // userStatus: string;
  // accountName: string;
  // accountShortName: string;
  // phone: string;
  firstName: string;
  lastName: string;
  pregivenUsername: string;
}; */
