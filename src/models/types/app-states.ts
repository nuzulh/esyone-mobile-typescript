import { ToastType } from "react-native-fast-toast";

export declare type ApplicationState = {
  currentState: "active" | "background" | "inactive" | "" | null;
  currentFlow: "notready" | "wait" | "ready";
  isOffline: boolean;
  isLoading: boolean;
  isLoadingBackground: boolean;
  isError: boolean;
  errorMessage: string;
  error: Error;
  toastRef: ToastType | null;
};
