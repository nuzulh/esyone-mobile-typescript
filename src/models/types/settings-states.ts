export declare type SettingsState = {
  language: string;
  isPushNotificationEnabled: boolean;
  isPushNotificationAsked: boolean;
  isBiometricEnabled: boolean;
  isBiometricSupported: boolean;
  isBiometricAsked: boolean;
  isOfflineLoginSupported: boolean;
  biometricType: string;
};
