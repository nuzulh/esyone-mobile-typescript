import { ResourceData } from "../../hooks";
import { Akte } from "../akte";

export declare type AktenState = {
  akten: ResourceData<Akte>;
  aktenAsList: Akte[];
  isFetching: boolean;
  lastUpdated: number;
};
