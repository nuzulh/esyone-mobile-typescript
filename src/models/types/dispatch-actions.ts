export declare type DispatchAction<T extends unknown> = {
  type: string;
  payload?: T;
};
