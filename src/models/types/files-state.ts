import { ResourceData } from "../../hooks";
import { MetaFile } from "../../services";

export declare type FilesState = {
  files: ResourceData<MetaFile>;
  filesAsList: MetaFile[];
  isFetching: boolean;
  error: Error;
};
