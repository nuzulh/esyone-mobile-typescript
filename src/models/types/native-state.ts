export declare type NativeState = {
  isCameraPermissionGranted: boolean;
  isCameraPermissionAsked: boolean;
};
