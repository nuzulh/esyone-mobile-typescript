// export interface FCMUnregisterAction

export interface FCMState {
  hasPermission: boolean;
  isRegistered: boolean;
  isEnabled: boolean;
  isPermissionAsked: boolean;
  token: string;
}
