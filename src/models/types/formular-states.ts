import { ResourceData } from "../../hooks";
import { WorkflowFormular } from "../workflow";

export declare type FormularState = {
  formular: ResourceData<WorkflowFormular>;
  lastUpdatedWorkflowId: string;
};
