export interface Session {
  tokenType: string;
  accessToken: string;
  refreshToken: string;
  offlineCredential: string;
  idToken: string;
  login: boolean;
}
