export interface Aufgabe {
  workflowId: string;
  formularName: string;
  assistantName: string;
  hasAnswer: boolean;
  answerCreationDate: Date;
  validUntil: Date;
  isClosed: boolean;
  formElements: any[];
  answers: any[];
}
