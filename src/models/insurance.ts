export interface Insurance {
  insuranceId: number;
  foreignId: string;
  name: string;
  maxUploadSize: {
    gdV2000_KFZ: string;
    eMail_RS: string;
    gdV2003_RECHNUNG: string;
    gdV2003_QUITTUNG: string;
  };
  allowedSuffixes: {
    gdV2000_KFZ: string;
    eMail_RS: string;
    gdV2003_RECHNUNG: string;
    gdV2003_QUITTUNG: string;
  };
}
