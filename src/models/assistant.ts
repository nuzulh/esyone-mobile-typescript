export interface Assistant {
  id: string;
  key: string;
  name: string;
  description: string;
  logo: string;
  assistantKey: string;
  url: string;
  logoUrl: string;
  headerLogoUrl: string;
  isMainService: boolean;
}
