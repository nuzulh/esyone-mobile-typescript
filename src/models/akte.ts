import { Aufgabe } from "./aufgabe";
import { Message, MessageAttachment } from "./message";

export interface Akte {
  id: string; // masking idHash
  rootId?: string;
  parentId?: string;
  aktenzeichen: number; // file number
  foreignReferenceHash: string; // related akte
  aktenkurzbezeichnung: string; // file abbreviation
  header: string;
  folderCount: number;
  totalUnreadMessage: number;
  creationDate: Date;
  lastMessage: string;
  lastMessageDate: Date;
  creatorId: string; // masking creatorIdHash
  creatorFirstName: string;
  totalUnreadMessageInFolder: number;
  creatorLastName: string;
  accountId: number;
  hasOpenAufgabe: boolean;
  lastState: AkteState;
  isEsyAkte: boolean;
  assignees?: AkteAssignee[];
  phases?: AktePhase[];
  children?: Akte[];
  serviceAkte?: boolean;
  permissionToReply?: boolean;
  attachments?: MessageAttachment[];
  messages?: Message[];
  aufgaben?: Aufgabe[];
}

export interface AkteState {
  name: string;
  statusText: string;
  occuredAt: Date;
  isDone: boolean;
  isInProgress: boolean;
  creationDate: Date;
}

export interface AkteAssignee {
  firstName: string;
  lastName: string;
  profilePictureUrl: string;
}

export interface AkteStatus {
  statusText: string;
  occuredAt: Date;
  isDone: boolean;
  isInProgress: boolean;
  creationDate: Date;
}

export interface AktePhase {
  name: string;
  states: AkteState[];
}
