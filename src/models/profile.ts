import { MetaFile } from "../services";

export interface Profile {
  accountId: string;
  userId: string;
  email: string;
  firstName: string;
  lastName: string;
  pregivenUsername: string;
  mobile: string;
  profilePicture: MetaFile;
}
