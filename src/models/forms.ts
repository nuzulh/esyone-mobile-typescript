export interface ExternalFormula {
  id: number;
  name: string;
}
