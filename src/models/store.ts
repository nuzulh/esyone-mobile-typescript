import {
  AktenState,
  ApplicationState,
  AuthState,
  FCMState,
  FilesState,
  KeyboardState,
  LinkingState,
  NativeState,
  SettingsState,
} from "./types";
import { FormularState } from "./types";
import { NavigationState } from "./types/navigation-states";

export interface EsyState {
  // currentState: "active" | "background" | "inactive" | "" | null;
  // currentLink: string;
  // currentFlow: "notready" | "wait" | "ready";
  appState: ApplicationState;
  auth: AuthState;
  fcm: FCMState;
  linking: LinkingState;
  settings: SettingsState;
  navigation: NavigationState;
  keyboard: KeyboardState;
  akten: AktenState;
  filesState: FilesState; // files cache
  nativeState: NativeState; // native permissions
  formularState: FormularState;
}

export interface RootState {
  legacy: any;
  esy: EsyState;
}

export declare type SelectStateFn = <T>(cb: (state: EsyState) => T) => T;
