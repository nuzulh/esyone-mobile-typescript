import { FileUploadInfo } from "../services";
import { Account } from "./account";

export interface WorkflowAdhoc {
  accessToken: string;
  accountId: number;
  workflowKey: string;
  workflowUser: string;
  workflowType: string;
}

export interface WorkflowAttachment {
  id: string;
  fileName: string;
}

export interface WorkflowProcess {
  workflowId: string;
  akteId: string;
  assistantName: string;
  showDatenschutz: boolean;
  showAgb: boolean;
  headerLogoUrl: string;
  assistantType: 0;
  assistantNotice: any;
  attachments: WorkflowAttachment[];
  formElement: WorkflowFormElement;
  answers?: WorkflowAnswer[];
}

export interface WorkflowFormular {
  workflowId: string;
  lastUpdated: number;
  fieldValues: {
    [key: string]: any;
  };
}

export interface WorkflowAnswer {
  answerCreationDate: Date;
  formValues: WorkflowFormElementAnswer[];
}

// export interface WorkflowAnswer {
//   workflowId: string;
//   answerCreationDate: Date;
//   assistantName: string;
//   formularName: string;
//   hasAnswer: boolean;
//   isClosed: boolean;
//   validUntil: Date;
//   answers:
// }

// export interface WorkflowProcessAnswerElement {

// }

export interface WorkflowProcessSubmission {
  account: Account;
  workflowProcess: WorkflowProcess;
  accessToken: string;
  data: any;
  uploadInfo?: FileUploadInfo;
}

export interface WorkflowFormElement {
  id: number;
  formElements: WorkflowFormElement[];
  text: string;
  value: any;
  requiredType: string;
  type: string;
  position: number;
  isVisible: boolean;
  styleSheet: string;
  style: string;
  size: number;
  maxLength: number;
  minLength: number;
  name: string;
  checked: boolean;
  matching: string;
  help: string;
  appliedValue: any;
  regEx: string;
  showButtons: boolean;
  showUploadField: boolean;
  showValidationText: boolean;
  isReadOnly: boolean;
  formId: any;
  htmlTemplate: string;
  ressourceKey: string;
  isRequired: boolean;
  account: number;
  formKey: string;
  aufgabeKey: string;
  isAutoMessageCreationEnabled: boolean;
  subject: string;
  message: string;
  isForExistingAkte: boolean;
}

export interface WorkflowFormElementAnswer {
  name: string;
  type: string;
  label: string;
  value: any;
}
