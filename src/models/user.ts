export interface User {
  id: string;
  guid: string;
  title: string;
  firstName: string;
  lastName: string;
  phone: string;
  street: string;
  postalCode: string;
  city: string;
  userRegistered: boolean;
  username: string;
  userStatus: number;
}

export interface UserSeed extends Partial<User> {
  accountId: number;
  authenticationKey: string;
  validUntil?: Date;
  agb?: string;
  datenschutz?: string;
  password?: string;
  accessToken?: string;
  accountName?: string;
}
