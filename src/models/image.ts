export interface ImageSpec {
  uri: string;
  headers?: any;
}
