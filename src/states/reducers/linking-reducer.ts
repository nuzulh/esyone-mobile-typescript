import { getDefaultState } from "../../helpers";
import { LINKING_ACTIONS } from "../../helpers/consts";
import { LinkingState } from "../../models";
import { DispatchAction } from "../../models/types/dispatch-actions";

export default function LinkingReducer(
  state: LinkingState = getDefaultState().linking,
  action: DispatchAction<Partial<LinkingState>>
): LinkingState {
  switch (action.type) {
    case LINKING_ACTIONS.MUTATION.LINKING_UPDATE:
      return {
        ...state,
        ...action.payload,
      };
    case LINKING_ACTIONS.MUTATION.LINKING_RESET:
      return getDefaultState().linking;
    default:
      return state;
  }
}
