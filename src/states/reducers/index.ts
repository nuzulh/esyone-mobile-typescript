import { combineReducers } from "redux";
import appStateReducer from "./app-state-reducer";
import fcmReducer from "./fcm-reducer";
import linkingReducer from "./linking-reducer";
import authReducer from "./auth-reducer";
import settingsReducer from "./settings-reducer";
import navigationReducer from "./navigation-reducer";
import { persistReducer } from "redux-persist";
import { defaultTransform, getDefaultPersistConfig } from "../../helpers";
import keyboardReducer from "./keyboard-reducer";
import aktenReducer from "./akten-reducer";
import filesReducer from "./files-reducer";
import nativeReducer from "./native-reducer";
import formularReducer from "./formular-reducer";

export const esyRootReducer = combineReducers({
  appState: appStateReducer,
  auth: persistReducer(
    getDefaultPersistConfig("esy_auth", {
      blacklist: ["isError", "error", "errorMessage"],
    }),
    authReducer
  ),
  fcm: persistReducer(
    getDefaultPersistConfig("esy_fcm", {
      blacklist: ["hasPermission", "isRegistered"],
    }),
    fcmReducer
  ),
  linking: linkingReducer,
  settings: persistReducer(
    getDefaultPersistConfig("esy_settings"),
    settingsReducer
  ),
  navigation: navigationReducer,
  keyboard: keyboardReducer,
  akten: persistReducer(
    {
      ...getDefaultPersistConfig("esy_akten"),
      transforms: [defaultTransform],
    },
    aktenReducer
  ),
  filesState: persistReducer(
    getDefaultPersistConfig("esy_files"),
    filesReducer
  ),
  nativeState: persistReducer(
    getDefaultPersistConfig("esy_native"),
    nativeReducer
  ),
  formularState: persistReducer(
    getDefaultPersistConfig("esy_formular"),
    formularReducer
  ),
});
