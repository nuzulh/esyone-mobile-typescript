import { getDefaultState } from "../../helpers";
import { APP_STATE_ACTIONS } from "../../helpers/consts";
import { ApplicationState } from "../../models";
import { DispatchAction } from "../../models/types/dispatch-actions";

export default function AppStateReducer(
  state: ApplicationState = getDefaultState().appState,
  action: DispatchAction<ApplicationState>
): ApplicationState {
  switch (action.type) {
    case APP_STATE_ACTIONS.MUTATION.APP_STATE_UPDATE:
      return {
        ...state,
        ...action.payload,
      };
    case APP_STATE_ACTIONS.MUTATION.APP_STATE_RESET:
      return getDefaultState().appState;
    /* case APP_STATE_ACTIONS.MUTATION.APP_STATE_START_LOADING:
      return {
        ...state,
        appState: {
          ...state.appState,
          isLoading: true,
        },
      };
    case APP_STATE_ACTIONS.MUTATION.APP_STATE_STOP_LOADING:
      return {
        ...state,
        appState: {
          ...state.appState,
          isLoading: false,
        },
      };
    case APP_STATE_ACTIONS.MUTATION.APP_STATE_SET_ERROR:
      return {
        ...state,
        appState: {
          ...state.appState,
          isError: true,
          errorMessage: action.payload.message,
          error: action.payload,
        },
      };
    case APP_STATE_ACTIONS.MUTATION.APP_STATE_CLEAR_ERROR:
      return {
        ...state,
        appState: {
          ...state.appState,
          isError: false,
          errorMessage: null,
          error: null,
        },
      }; */
    default:
      return state;
  }
}
