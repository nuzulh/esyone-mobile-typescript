import { FILE_ACTIONS, getDefaultState } from "../../helpers";
import { ResourceData } from "../../hooks";
import { DispatchAction, FilesState } from "../../models";
import { MetaFile } from "../../services";

export default function FilesReducer(
  oldState: FilesState = getDefaultState().filesState,
  action: DispatchAction<MetaFile | Error | boolean>
): FilesState {
  switch (action.type) {
    case FILE_ACTIONS.MUTATION.FILE_UPDATE:
      const metaFile = action.payload as MetaFile;

      // const files = oldState.files;
      // files[metaFile.id] = metaFile;
      const files = { ...oldState.files, [metaFile.id]: metaFile };

      return {
        files,
        filesAsList: transformToList(files),
        isFetching: oldState.isFetching,
        error: null,
      };
    case FILE_ACTIONS.MUTATION.FILE_UPDATE_FETCHING:
      return {
        ...oldState,
        isFetching: action.payload as boolean,
        error: (action.payload as boolean) ? null : oldState.error,
      };
    case FILE_ACTIONS.MUTATION.FILE_UPDATE_FETCH_ERROR:
      return {
        ...oldState,
        error: action.payload as Error,
      };
    case FILE_ACTIONS.MUTATION.FILE_REMOVE:
      const file = action.payload as MetaFile;
      delete oldState.files[file.id];

      return {
        files: oldState.files,
        filesAsList: transformToList(oldState.files),
        isFetching: oldState.isFetching,
        error: null,
      };
    case FILE_ACTIONS.MUTATION.FILE_RESET:
      return getDefaultState().filesState;
    default:
      return oldState;
  }
}

function transformToList(files: ResourceData<MetaFile>): MetaFile[] {
  const list = [];

  for (const key in files) {
    list.push(files[key]);
  }

  return list;
}
