import { AKTE_ACTIONS, getDefaultState } from "../../helpers";
import { ResourceData } from "../../hooks";
import { Akte, AktenState, DispatchAction } from "../../models";
import { MessageAttachment } from "../../models/message";

export default function AktenReducer(
  oldState: AktenState = getDefaultState().akten,
  action: DispatchAction<
    ResourceData<Akte> | MessageAttachment | boolean | Akte
  >
): AktenState {
  switch (action.type) {
    case AKTE_ACTIONS.MUTATION.AKTE_UPDATE_ALL:
      const oldAkten = oldState.akten;
      const newAkten = action.payload as ResourceData<Akte>;

      for (const key in newAkten) {
        newAkten[key] = {
          // ...oldAkten[key],
          ...newAkten[key],
          assignees: oldAkten[key] ? oldAkten[key].assignees : [],
          phases: oldAkten[key] ? oldAkten[key].phases : [],
          messages: oldAkten[key] ? oldAkten[key].messages : [],
          attachments: oldAkten[key] ? oldAkten[key].attachments : [],
          children: oldAkten[key] ? oldAkten[key].children : [],
          aufgaben: oldAkten[key] ? oldAkten[key].aufgaben : [],
        };
      }

      return {
        akten: newAkten,
        aktenAsList: transformToList(newAkten),
        isFetching: oldState.isFetching,
        lastUpdated: Date.now(),
      };
    case AKTE_ACTIONS.MUTATION.AKTE_UPDATE_FETCHING:
      return {
        ...oldState,
        isFetching: action.payload as boolean,
      };
    case AKTE_ACTIONS.MUTATION.AKTE_ADD:
      const newAkte = action.payload as Akte;
      oldState.akten[newAkte.id] = newAkte;
      oldState.aktenAsList = transformToList(oldState.akten);
      oldState.lastUpdated = Date.now();

      return {
        ...oldState,
      };
    case AKTE_ACTIONS.MUTATION.AKTE_UPDATE:
      const modifiedAkte = action.payload as Akte;
      // console.log(akte);
      // looking for akte in list
      if (oldState.akten[modifiedAkte.id]) {
        oldState.akten[modifiedAkte.id] = modifiedAkte;
        oldState.aktenAsList = transformToList(oldState.akten);
        oldState.lastUpdated = Date.now();
      }

      return {
        ...oldState,
      };
    case AKTE_ACTIONS.MUTATION.AKTE_RESET:
      return getDefaultState().akten;
    // case AKTE_ACTIONS.MUTATION.AKTE_UPDATE_ATTACHMENT:
    //   const attachment = action.payload as MessageAttachment;

    //   updateAttachment(oldState.akten, attachment);

    //   return {
    //     ...oldState,
    //     aktenAsList: transformToList(oldState.akten),
    //   };
    default:
      return oldState;
  }
}

function transformToList(akten: ResourceData<Akte>): Akte[] {
  const list = [];

  for (const key in akten) {
    list.push(akten[key]);
  }

  return list;
}

function transformToResource(akten: Akte[]): ResourceData<Akte> {
  const resource: ResourceData<Akte> = {};

  for (const akte of akten) {
    resource[akte.id] = akte;
  }

  return resource;
}

// function updateAttachment(
//   akten: ResourceData<Akte>,
//   attachment: MessageAttachment
// ) {
//   for (const key in akten) {
//     const akte = akten[key];

//     for (let a of akte.attachments)
//       if (a.id === attachment.id) {
//         a.fileName = attachment.fileName;
//         a.filePath = attachment.filePath;
//         a.contentType = attachment.contentType;
//         a.size = attachment.size;
//         a.extension = attachment.extension;

//         return;
//       }

//     if (akte.children && akte.children.length > 0)
//       updateAttachment(transformToResource(akte.children), attachment);
//   }
// }
