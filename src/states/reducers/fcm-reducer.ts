import { getDefaultState } from "../../helpers";
import { FCM_ACTIONS } from "../../helpers/consts";
import { FCMState } from "../../models";
import { DispatchAction } from "../../models/types/dispatch-actions";

export default function FCMReducer(
  state: FCMState = getDefaultState().fcm,
  action: DispatchAction<Partial<FCMState>>
): FCMState {
  switch (action.type) {
    case FCM_ACTIONS.MUTATION.FCM_UPDATE:
      return {
        ...state,
        ...action.payload,
      };
    case FCM_ACTIONS.MUTATION.FCM_RESET:
      return getDefaultState().fcm;
    default:
      return state;
  }
}
