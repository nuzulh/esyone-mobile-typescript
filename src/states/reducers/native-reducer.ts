import { getDefaultState, NATIVE_ACTIONS } from "../../helpers";
import { DispatchAction, NativeState } from "../../models";

export default function NativeReducer(
  oldState: NativeState = getDefaultState().nativeState,
  action: DispatchAction<Partial<NativeState>>
): NativeState {
  switch (action.type) {
    case NATIVE_ACTIONS.MUTATION.NATIVE_UPDATE:
      return {
        ...oldState,
        ...action.payload,
      };
    default:
      return oldState;
  }
}
