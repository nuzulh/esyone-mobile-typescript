import { getDefaultState, SETTINGS_ACTIONS } from "../../helpers";
import { DispatchAction, SettingsState } from "../../models";

export default function SettingsReducer(
  state: SettingsState = getDefaultState().settings,
  action: DispatchAction<Partial<SettingsState>>
): SettingsState {
  switch (action.type) {
    case SETTINGS_ACTIONS.MUTATION.SETTINGS_UPDATE:
      return {
        ...state,
        ...action.payload,
      };
    case SETTINGS_ACTIONS.MUTATION.SETTINGS_RESET:
      return getDefaultState().settings;
    default:
      return state;
  }
}
