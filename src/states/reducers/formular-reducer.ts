import { FORMULAR_ACTIONS, getDefaultState } from "../../helpers";
import { DispatchAction, WorkflowFormular } from "../../models";
import { FormularState } from "../../models/types";

export default function FormularReducer(
  state: FormularState = getDefaultState().formularState,
  action: DispatchAction<WorkflowFormular | string>
): FormularState {
  switch (action.type) {
    case FORMULAR_ACTIONS.MUTATION.FORMULAR_SAVE_DRAFT:
      const newFormular = action.payload as WorkflowFormular;
      const filteredFieldValues = {};

      Object.keys(newFormular.fieldValues).forEach((key) => {
        if (
          key === "agb" ||
          key === "datenschutz" ||
          key.includes("file") ||
          key.includes("signature") ||
          newFormular.fieldValues[key] === undefined ||
          newFormular.fieldValues[key] === null
        ) return;

        filteredFieldValues[key] = newFormular.fieldValues[key];
      });

      state.formular = { ...state.formular };
      state.formular[newFormular.workflowId] = {
        ...newFormular,
        lastUpdated: Date.now(),
        fieldValues: filteredFieldValues,
      };
      state.lastUpdatedWorkflowId = newFormular.workflowId;

      return { ...state };
    case FORMULAR_ACTIONS.MUTATION.FORMULAR_RESET:
      const workflowId = action.payload as string;
      delete state.formular[workflowId];

      return { ...state };
    case FORMULAR_ACTIONS.MUTATION.FORMULAR_RESET_ALL:
      return getDefaultState().formularState;
    default:
      return state;
  }
}
