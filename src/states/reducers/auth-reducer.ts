import { AUTH_ACTIONS, getDefaultState } from "../../helpers";
import { AuthState, DispatchAction } from "../../models";

export default function AuthReducer(
  state: AuthState = getDefaultState().auth,
  action: DispatchAction<Partial<AuthState>>
): AuthState {
  switch (action.type) {
    case AUTH_ACTIONS.MUTATION.AUTH_UPDATE:
      return {
        ...state,
        ...action.payload,
      };
    case AUTH_ACTIONS.MUTATION.AUTH_RESET:
      return getDefaultState().auth;
    case AUTH_ACTIONS.MUTATION.AUTH_UPDATE_PROFILE:
      return {
        ...state,
        profile: {
          ...state.profile,
          ...(action.payload.profile || {}),
        },
      };
    case AUTH_ACTIONS.MUTATION.AUTH_CLEAR_SESSION:
      return {
        ...state,
        session: {
          ...state.session,
          accessToken: null,
          refreshToken: null,
          login: false,
        },
        isLoggedIn: false,
      };
    default:
      return state;
  }
}
