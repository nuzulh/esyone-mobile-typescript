import { getDefaultState, NAVIGATION_ACTIONS } from "../../helpers";
import { DispatchAction } from "../../models";
import {
  NavigationState,
  NavigationTarget,
} from "../../models/types/navigation-states";

export default function NavigationReducer(
  state: NavigationState = getDefaultState().navigation,
  action: DispatchAction<NavigationTarget>
): NavigationState {
  switch (action.type) {
    case NAVIGATION_ACTIONS.MUTATION.NAVIGATION_UPDATE:
      return {
        ...state,
        current: action.payload,
      };
    case NAVIGATION_ACTIONS.MUTATION.NAVIGATION_RESET:
      return getDefaultState().navigation;
    default:
      return state;
  }
}
