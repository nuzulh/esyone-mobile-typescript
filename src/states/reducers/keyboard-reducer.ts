import { getDefaultState, KEYBOARD_ACTIONS } from "../../helpers";
import { DispatchAction, KeyboardState } from "../../models";

export default function KeyboardReducer(
  state: KeyboardState = getDefaultState().keyboard,
  action: DispatchAction<Partial<KeyboardState>>
): KeyboardState {
  switch (action.type) {
    case KEYBOARD_ACTIONS.MUTATION.KEYBOARD_UPDATE_VISIBILITY:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
}
