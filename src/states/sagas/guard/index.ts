import { all } from "redux-saga/effects";
import { GuardService, LogService } from "../../../services";
import { startEvaluateLink } from "./evaluate-link";

export function* startGuardSagas(
  logService: LogService,
  guardService: GuardService
) {
  yield all([startEvaluateLink(logService, guardService)]);
}
