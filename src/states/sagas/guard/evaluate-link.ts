import { call, put, takeLatest } from "redux-saga/effects";
import { LINKING_ACTIONS, navigateToAction } from "../../../helpers";
import { DispatchAction, LinkingState } from "../../../models";
import { EvaluationResult, GuardService, LogService } from "../../../services";

function createEvaluateLink(
  logService: LogService,
  guardService: GuardService
) {
  return function* (action: DispatchAction<LinkingState>) {
    logService.debug(`evaluate link ${action.payload.currentLink}`);
    const evaluationResult: EvaluationResult = yield call(
      guardService.evaluateWithLink,
      action.payload.currentLink
    );

    // console.log(
    //   "evaluationResult 2",
    //   JSON.stringify(evaluationResult, null, 2)
    // );

    yield put(evaluationResult);
    // const { isDenied, isPassed, ...navigationTarget } = evaluationResult;

    // if (isPassed || isDenied) return;

    // yield put(navigateToAction(navigationTarget));
  };
}

export function* startEvaluateLink(
  logService: LogService,
  guardService: GuardService
) {
  yield takeLatest(
    LINKING_ACTIONS.MUTATION.LINKING_UPDATE,
    createEvaluateLink(logService, guardService)
  );
}
