import { all } from "redux-saga/effects";
import {
  AccountsService,
  AktenService,
  AufgabeService,
  AuthService,
  EconsultService,
  FilesService,
  GuardService,
  LogService,
  PermissionService,
  TouchIdservice,
  UsersService,
} from "../../services";
import { startAccountSagas } from "./account";
import { startAktenSagas } from "./akten";
import { startAuthSagas } from "./auth";
import { startBootSagas } from "./boot";
import { startExternalSagas } from "./external";
import { startGuardSagas } from "./guard";

export function createEsySagas(
  logService: LogService,
  econsultService: EconsultService,
  guardService: GuardService,
  permissionService: PermissionService,
  touchIdService: TouchIdservice,
  aktenService: AktenService,
  aufgabeService: AufgabeService,
  filesService: FilesService,
  accountsService: AccountsService,
  authService: AuthService,
  usersService: UsersService
) {
  return function* () {
    logService.debug("Start sagas...");
    yield all([
      startBootSagas(
        logService,
        econsultService,
        guardService,
        permissionService,
        touchIdService
      ),
      startAccountSagas(logService, accountsService),
      startGuardSagas(logService, guardService),
      startExternalSagas(logService),
      startAktenSagas(
        logService,
        aktenService,
        aufgabeService,
        filesService,
        accountsService,
        authService,
        permissionService
      ),
      startAuthSagas(logService, guardService, usersService, authService, filesService),
    ]);
  };
}
