import { Linking, Platform } from "react-native";
import { LogService } from "../../../services";
import { openInbox } from "react-native-email-link";
import { all, call, takeEvery } from "redux-saga/effects";
import { LINKING_ACTIONS } from "../../../helpers";

function createOpenMailService(logService: LogService) {
  return function* () {
    logService.debug("opening external mail service");

    try {
      if (Platform.OS === "ios") {
        yield call({ context: Linking, fn: Linking.openURL }, "message:0");
      } else {
        yield call(openInbox);
      }
    } catch (err) {
      logService.error(err);
    }
  };
}

function createOpenSmsService(logService: LogService) {
  return function* () {
    logService.debug("opening external sms service");

    try {
      if (Platform.OS === "ios") {
        Linking.openURL("sms://e.syThing");
      } else {
        Linking.openURL("sms:e.syThing");
      }
    } catch (err) {
      logService.error(err);
    }
  };
}

export function* startOpenExternalServices(logService: LogService) {
  yield all([
    takeEvery(
      LINKING_ACTIONS.LINKING_OPEN_MAIL,
      createOpenMailService(logService)
    ),
    takeEvery(
      LINKING_ACTIONS.LINKING_OPEN_SMS,
      createOpenSmsService(logService)
    ),
  ]);
}
