import { Linking } from "react-native";
import { call, takeEvery } from "redux-saga/effects";
import { LINKING_ACTIONS } from "../../../helpers";
import { DispatchAction } from "../../../models";
import { LogService } from "../../../services";

function createOpenExternalLink(logService: LogService) {
  return function* (action: DispatchAction<string>) {
    if (!action.payload) return;
    logService.debug(`opening external link ${action.payload}`);

    yield call({ context: Linking, fn: Linking.openURL }, action.payload);
  };
}

export function* startOpenExternalLink(logService: LogService) {
  yield takeEvery(
    LINKING_ACTIONS.LINKING_OPEN_URL,
    createOpenExternalLink(logService)
  );
}
