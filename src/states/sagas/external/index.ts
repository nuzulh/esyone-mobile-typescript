import { all } from "redux-saga/effects";
import { LogService } from "../../../services";
import { startOpenExternalLink } from "./open-external-link";
import { startOpenExternalServices } from "./open-external-service";

export function* startExternalSagas(logService: LogService) {
  yield all([
    startOpenExternalLink(logService),
    startOpenExternalServices(logService),
  ]);
}
