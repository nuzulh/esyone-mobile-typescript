import { all } from "redux-saga/effects";
import {
  AuthService,
  FilesService,
  GuardService,
  LogService,
  UsersService,
} from "../../../services";
import { startCheckUsernameSaga } from "./check-username";
import { startLoadUserProfile } from "./load-user-profile";
import { startLogoutSaga } from "./logout";
import { startRequestPinSaga } from "./request-pin";
import { startRequestUsernameSaga } from "./request-username";
import { startResetPasswordSaga } from "./reset-password";
import { startSubmitBiometricLogin } from "./submit-biometric-login";
import { startSubmitLogin } from "./submit-login";
import { startSubmitPinSaga } from "./submit-pin";
import { startSubmitRegistrationSaga } from "./submit-registration";
import { startUpdateProfileImage } from "./change-user-profile";
import { startChangePasswordSaga } from "./change-password";
import { startCreateNewPasswordSaga } from "./create-new-password";
import { startRegistrationSaga } from "./start-registration";

export function* startAuthSagas(
  logService: LogService,
  guardService: GuardService,
  usersService: UsersService,
  authService: AuthService,
  filesService: FilesService
) {
  logService.debug("starting auth sagas");
  yield all([
    startSubmitLogin(logService, guardService),
    startSubmitBiometricLogin(logService, guardService),
    startLoadUserProfile(logService, usersService),
    startLogoutSaga(logService),
    startCheckUsernameSaga(logService, authService),
    startResetPasswordSaga(logService, authService),
    startChangePasswordSaga(logService, authService),
    startRegistrationSaga(logService, authService),
    startSubmitPinSaga(logService, authService),
    startRequestPinSaga(logService, authService),
    startRequestUsernameSaga(logService, authService),
    startSubmitRegistrationSaga(logService, authService),
    startUpdateProfileImage(logService, usersService, filesService),
    startCreateNewPasswordSaga(logService, authService),
  ]);
}
