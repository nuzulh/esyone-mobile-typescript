import { delay, put, takeLeading } from "redux-saga/effects";
import {
  authClearSessionAction,
  AUTH_ACTIONS,
  AUTH_STACK,
  disableFcmAction,
  hideLoadingAction,
  navigateToAction,
  showLoadingAction,
  STACKS,
} from "../../../helpers";
import { DispatchAction } from "../../../models";
import { LogService } from "../../../services";

function createLogout(logService: LogService) {
  return function* (action: DispatchAction<{ clearCached?: boolean }>) {
    logService.debug("logout from application");

    yield put(showLoadingAction());
    try {
      // unregister fcm
      if (action.payload.clearCached) {
        yield put(disableFcmAction());
        yield delay(2000);
      }

      yield put(authClearSessionAction());
      yield put(
        navigateToAction({
          navigateTo: {
            stack: STACKS.AUTH_STACK,
            screen: AUTH_STACK.SIGN_IN_SCREEN,
          },
        })
      );
    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startLogoutSaga(logService: LogService) {
  logService.debug("start logout saga");

  yield takeLeading(AUTH_ACTIONS.AUTH_LOGOUT, createLogout(logService));
}
