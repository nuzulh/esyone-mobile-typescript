import { put, select, takeLeading, call } from "redux-saga/effects";
import {
  authUpdateProfileAction,
  AUTH_ACTIONS,
  checkTokenIsValid,
  showLoadingAction,
  hideLoadingAction,
} from "../../../helpers";
import { AuthState, DispatchAction, Profile, RootState } from "../../../models";
import { LogService, UsersService } from "../../../services";
import { MetaFile } from "../../../services";
import { FileUploadResult } from "../../../services";
import { FilesService } from "../../../services";
import { Account } from "../../../models";

function createLoadUserProfile(
  logService: LogService,
  usersService: UsersService,
  filesService: FilesService
) {
  return function* (
    action: DispatchAction<{
      account?: Account;
      userId?: string;
      metaFiles?: MetaFile[];
    }>
  ) {
    const { account, userId, metaFiles } = action.payload;
    const authState: AuthState = yield select(
      (state: RootState) => state.esy.auth
    );
    const accountid = +authState.profile.accountId;

    logService.debug("process upload photo profile");

    yield put(showLoadingAction());
    try {
      if (action.payload) {
        const uploadResponse = yield filesService.uploadFiles(
          accountid,
          metaFiles
        );
        if (uploadResponse) {
          const fetchProfile = yield usersService.postProfilePicture(
            accountid.toString(),
            authState.profile.userId,
            uploadResponse[0].uploadId
          );
        }
      }

      const metaFile = yield usersService.getProfilePicture(
        authState.profile.accountId,
        authState.profile.userId
      );
      const profile: Partial<Profile> = {
        profilePicture: metaFile,
      };

      yield put(authUpdateProfileAction(profile));
      yield put(hideLoadingAction());
    } catch (error) {
      console.log(" eorr change photo profile", error);
    }
  };
}

export function* startUpdateProfileImage(
  logService: LogService,
  usersService: UsersService,
  filesService: FilesService
) {
  logService.debug("start upload photo profile");

  yield takeLeading(
    AUTH_ACTIONS.AUTH_UPDATE_PROFILE_IMAGE,
    createLoadUserProfile(logService, usersService, filesService)
  );
}
