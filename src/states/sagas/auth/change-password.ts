import { put, select, takeLeading } from "redux-saga/effects";
import {
  AUTH_ACTIONS,
  HOME_STACK,
  hideLoadingAction,
  localization,
  navigateToAction,
  parseApiReasonKey,
  showLoadingAction,
} from "../../../helpers";
import { DispatchAction, RootState } from "../../../models";
import { AuthService, LogService } from "../../../services";
import { Alert } from "react-native";

function createChangePassword(logService: LogService, authService: AuthService) {
  return function* (
    action: DispatchAction<{
      oldpassword: string;
      newpassword: string;
      newpasswordconfirmation: string;
    }>
  ) {
    logService.debug("changing user password");
    yield put(showLoadingAction());

    const userId = yield select(
      (state: RootState) => state.esy.auth.profile.userId
    );
    const {
      oldpassword,
      newpassword,
      newpasswordconfirmation,
    } = action.payload;

    try {
      yield authService.putUserChangePassword(
        userId,
        oldpassword,
        newpassword,
        newpasswordconfirmation,
      );

      Alert.alert("Password has been changed successfully");

      yield put(
        navigateToAction({
          navigateTo: {
            screen: HOME_STACK.HOME_SCREEN,
          },
        })
      );
    } catch (error) {
      logService.error(error);
      Alert.alert(localization.language[parseApiReasonKey(error)]);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startChangePasswordSaga(
  logService: LogService,
  authService: AuthService
) {
  logService.debug("starting change password saga");
  yield takeLeading(
    AUTH_ACTIONS.AUTH_CHANGE_PASSWORD,
    createChangePassword(logService, authService)
  );
}
