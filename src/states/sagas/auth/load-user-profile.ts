import { put, select, takeLeading } from "redux-saga/effects";
import {
  authUpdateProfileAction,
  AUTH_ACTIONS,
  checkTokenIsValid,
} from "../../../helpers";
import { AuthState, DispatchAction, Profile, RootState } from "../../../models";
import { LogService, UsersService } from "../../../services";

function createLoadUserProfile(
  logService: LogService,
  usersService: UsersService
) {
  return function* (
    action: DispatchAction<{ accountId?: string; userId?: string }>
  ) {
    const { accountId, userId } = action.payload;
    const accessToken = yield select(
      (state: RootState) => state.esy.auth.session.accessToken
    );

    if (!accessToken || !checkTokenIsValid(accessToken)) {
      logService.debug("no valid access token found");
      return;
    }

    const authState: AuthState = yield select(
      (state: RootState) => state.esy.auth
    );
    logService.debug(
      `load user profile for ${authState.profile.firstName} ${authState.profile.lastName}`
    );

    try {
      const metaFile = yield usersService.getProfilePicture(
        accountId || authState.profile.accountId,
        userId || authState.profile.userId
      );

      const profile: Partial<Profile> = {
        profilePicture: metaFile,
      };

      yield put(authUpdateProfileAction(profile));

      // console.log(userProfile);
    } catch (error) {
      logService.error(error);
    }
  };
}

export function* startLoadUserProfile(
  logService: LogService,
  usersService: UsersService
) {
  logService.debug("start load user profile");

  yield takeLeading(
    AUTH_ACTIONS.AUTH_LOAD_PROFILE,
    createLoadUserProfile(logService, usersService)
  );
}
