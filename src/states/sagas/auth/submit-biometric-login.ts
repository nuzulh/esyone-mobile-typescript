import { call, put, select, takeLeading } from "redux-saga/effects";
import {
  authFailedAction,
  authSucceedAction,
  AUTH_ACTIONS,
  BiometricNotSupported,
  hideLoadingAction,
  localization,
  showLoadingAction,
  startActivations,
} from "../../../helpers";
import { AuthState, DispatchAction, RootState } from "../../../models";
import { GuardService, LogService } from "../../../services";
import { Alert } from "react-native";

export function createSubmitBiometricLogin(
  logService: LogService,
  guardService: GuardService
) {
  return function* (action: DispatchAction<{ next?: any }>) {
    const { next } = action.payload;
    const appState = yield select((state: RootState) => state.esy.appState);
    const authState = yield select((state: RootState) => state.esy.auth);

    yield put(showLoadingAction());
    try {
      if (
        appState.isOffline ||
        !authState.session ||
        !authState.session.offlineCredential
      )
        throw new BiometricNotSupported();

      const loginResponse: AuthState = yield call(
        guardService.biometricLogin,
        authState.session.offlineCredential
      );

      yield put(authSucceedAction(loginResponse));

      if (next) yield put(next);
      else yield put(startActivations());
    } catch (error) {
      logService.error(error);
      Alert.alert(localization.language.LoginFailed, error.message);
      yield put(authFailedAction(error));
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startSubmitBiometricLogin(
  logService: LogService,
  guardService: GuardService
) {
  logService.debug("starting submit biometric login saga");
  yield takeLeading(
    AUTH_ACTIONS.AUTH_REQUEST_BIOMETRIC_LOGIN,
    createSubmitBiometricLogin(logService, guardService)
  );
}
