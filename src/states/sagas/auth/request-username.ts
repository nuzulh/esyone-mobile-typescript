import { Alert } from "react-native";
import { call, put, takeLeading } from "redux-saga/effects";
import {
  AUTH_ACTIONS,
  hideLoadingAction,
  localization,
  navigateToAction,
  REGISTRATION_STACK,
  showLoadingAction,
} from "../../../helpers";
import { DispatchAction, UserSeed } from "../../../models";
import { AuthService, LogService } from "../../../services";

function createRequestUsername(
  logService: LogService,
  authService: AuthService
) {
  return function* (action: DispatchAction<{ user: UserSeed }>) {
    logService.debug(`request username for ${action.payload.user.username}`);

    yield put(showLoadingAction());
    try {
      const isValid = yield call(
        authService.getIsUsernameAvailable,
        action.payload.user
      );

      if (isValid)
        yield put(
          navigateToAction({
            navigateTo: {
              screen: REGISTRATION_STACK.PASSWORD_SCREEN,
              params: {
                user: {
                  ...action.payload.user,
                },
              },
            },
          })
        );
      else
        Alert.alert(
          localization.language.ScreenRegistrationUsername_InvalidUsername
        );
    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startRequestUsernameSaga(
  logService: LogService,
  authService: AuthService
) {
  yield takeLeading(
    AUTH_ACTIONS.AUTH_REQUEST_USERNAME,
    createRequestUsername(logService, authService)
  );
}
