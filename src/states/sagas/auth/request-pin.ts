import { call, put, select, takeLeading } from "redux-saga/effects";
import {
  AUTH_ACTIONS,
  AUTH_STACK,
  hideLoadingAction,
  localization,
  navigateToAction,
  showLoadingAction,
  STACKS,
} from "../../../helpers";
import { DispatchAction, RootState, UserSeed } from "../../../models";
import { AuthService, LogService } from "../../../services";
import { ToastType } from "react-native-fast-toast";

function createRequestPin(logService: LogService, authService: AuthService) {
  return function* (
    action: DispatchAction<{ key: string; channel?: string; silent?: boolean; }>
  ) {
    logService.debug(`request pin ${action.payload.key}`);

    const { key, channel, silent } = action.payload;
    yield put(showLoadingAction());
    try {
      const userSeed: UserSeed = yield call(
        authService.getPinAuthentication,
        key
      );

      // console.log("authenticationKey", authenticationKey);
      yield call(authService.getPin, userSeed.authenticationKey, channel);

      const toastRef: ToastType = yield select(
        (state: RootState) => state.esy.appState.toastRef
      );
      if (toastRef) toastRef.show(localization.language.supportFormSent);

      if (!silent)
        yield put(
          navigateToAction({
            navigateTo: {
              stack: STACKS.AUTH_STACK,
              screen: AUTH_STACK.PIN_SCREEN,
              params: {
                id: key,
                user: userSeed,
              },
            },
          })
        );
    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startRequestPinSaga(
  logService: LogService,
  authService: AuthService
) {
  logService.debug("start request pin saga");

  yield takeLeading(
    AUTH_ACTIONS.AUTH_REQUEST_PIN,
    createRequestPin(logService, authService)
  );
}
