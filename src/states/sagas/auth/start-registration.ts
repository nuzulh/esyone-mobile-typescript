import { call, put, takeLeading } from "redux-saga/effects";
import {
  AUTH_ACTIONS,
  AUTH_STACK,
  hideLoadingAction,
  navigateToAction,
  REGISTRATION_STACK,
  showLoadingAction,
  STACKS,
} from "../../../helpers";
import { DispatchAction, UserSeed } from "../../../models";
import { AuthService, LogService } from "../../../services";

function createStartRegistration(logService: LogService, authService: AuthService) {
  return function* (
    action: DispatchAction<{ key: string; }>
  ) {
    logService.debug(`handle start registration ${action.payload.key}`);

    const { key } = action.payload;
    yield put(showLoadingAction());
    try {
      const userSeed: UserSeed = yield call(
        authService.getPinAuthentication,
        key
      );

      const resp = yield authService.postAdhocLogin(
        userSeed.authenticationKey
      );

      // console.log(resp);
      yield put(
        navigateToAction({
          navigateTo: {
            stack: STACKS.REGISTRATION_STACK,
            screen: REGISTRATION_STACK.STARTED_SCREEN,
            params: {
              id: key,
              user: {
                ...userSeed,
                accessToken: resp.accessToken,
              },
            },
          },
        })
      );

      // yield put(
      //   navigateToAction({
      //     navigateTo: {
      //       stack: STACKS.AUTH_STACK,
      //       screen: AUTH_STACK.PIN_SCREEN,
      //       params: {
      //         id: key,
      //         user: userSeed,
      //       },
      //     },
      //   })
      // );
    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startRegistrationSaga(
  logService: LogService,
  authService: AuthService
) {
  logService.debug("start registration saga");

  yield takeLeading(
    AUTH_ACTIONS.AUTH_START_REGISTRATION,
    createStartRegistration(logService, authService)
  );
}
