import { call, put, select, takeLeading } from "redux-saga/effects";
import {
  AUTH_ACTIONS,
  checkTokenIsValid,
  hideLoadingAction,
  localization,
  navigateToAction,
  parseApiReasonKey,
  REGISTRATION_STACK,
  showLoadingAction,
} from "../../../helpers";
import { DispatchAction, RootState, UserSeed } from "../../../models";
import { AuthService, LogService } from "../../../services";
import { Alert } from "react-native";

function createSubmitRegistration(
  logService: LogService,
  authService: AuthService
) {
  return function* (action: DispatchAction<{ key: string; user: UserSeed; }>) {
    logService.debug(`submit registration`);
    const appState = yield select(
      (state: RootState) => state.esy.appState
    );

    yield put(showLoadingAction());
    try {
      // console.log(
      //   "user",
      //   checkTokenIsValid(action.payload.user.accessToken),
      //   action.payload.user.accessToken
      // );
      const key = yield call(authService.getRegistration, action.payload.user);

      const isSuccess = yield call(
        authService.postRegistration,
        key,
        action.payload.user
      );

      if (!isSuccess) throw new Error("Registration failed");

      yield put(
        navigateToAction({
          navigateTo: {
            screen: REGISTRATION_STACK.CONGRATS_SCREEN,
            params: {
              user: action.payload.user,
            },
          },
        })
      );
    } catch (error) {
      logService.error(error);
      Alert.alert(localization.language[
        appState.isOffline
          ? "ErrorNoConnection"
          : parseApiReasonKey(error)
      ]);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startSubmitRegistrationSaga(
  logService: LogService,
  authService: AuthService
) {
  yield takeLeading(
    AUTH_ACTIONS.AUTH_REGISTER,
    createSubmitRegistration(logService, authService)
  );
}
