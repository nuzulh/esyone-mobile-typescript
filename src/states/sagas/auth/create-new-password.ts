import { call, put, takeLeading } from "redux-saga/effects";
import { DispatchAction } from "../../../models";
import { AuthService, LogService } from "../../../services";
import { AUTH_ACTIONS, AUTH_STACK, hideLoadingAction, localization, navigateToAction, parseApiReasonKey, showLoadingAction } from "../../../helpers";
import { Alert } from "react-native";

function createNewPassword(logService: LogService, authService: AuthService) {
  return function* (action: DispatchAction<{
    key: string;
    password: string;
  }>) {
    logService.debug(`reset password for key ${action.payload.key}`);

    yield put(showLoadingAction());
    try {
      yield authService.putResetPassword(
        action.payload.key,
        action.payload.password,
      );

      Alert.alert(localization.language.ResetPasswordSuccess);

      yield put(
        navigateToAction({
          navigateTo: {
            screen: AUTH_STACK.SIGN_IN_SCREEN,
          },
        })
      );
    } catch (error) {
      logService.error(error);

      Alert.alert(localization.language[parseApiReasonKey(error)]);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startCreateNewPasswordSaga(
  logService: LogService,
  authService: AuthService,
) {
  logService.debug("starting create new password saga");
  yield takeLeading(
    AUTH_ACTIONS.AUTH_CREATE_NEW_PASSWORD,
    createNewPassword(logService, authService),
  );
}
