import { Alert } from "react-native";
import { put, takeLeading } from "redux-saga/effects";
import {
  AUTH_ACTIONS,
  AUTH_STACK,
  hideLoadingAction,
  localization,
  navigateToAction,
  showLoadingAction,
} from "../../../helpers";
import { DispatchAction } from "../../../models";
import { AuthService, LogService } from "../../../services";

function createCheckUsername(logService: LogService, authService: AuthService) {
  return function* (action: DispatchAction<{ email: string }>) {
    logService.info(`check username for ${action.payload.email}`);

    yield put(showLoadingAction());
    try {
      yield authService.postCheckUsername(action.payload.email);

      Alert.alert(localization.language.ForgotUsername_Done);

      yield put(
        navigateToAction({
          navigateTo: {
            screen: AUTH_STACK.SIGN_IN_SCREEN,
          },
        })
      );
    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startCheckUsernameSaga(
  logService: LogService,
  authService: AuthService
) {
  logService.debug("starting check username saga");
  yield takeLeading(
    AUTH_ACTIONS.AUTH_CHECK_USERNAME,
    createCheckUsername(logService, authService)
  );
}
