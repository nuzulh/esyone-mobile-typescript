import { Alert } from "react-native";
import { call, put, select, takeLeading } from "redux-saga/effects";
import { esySelect } from "../../../effects";
import {
  localization,
  OfflineAuthenticationNotSupported,
  authSucceedAction,
  authFailedAction,
  showLoadingAction,
  hideLoadingAction,
  startActivations,
  AUTH_ACTIONS,
  parseApiReasonKey,
  clearFileCache,
  AKTE_ACTIONS,
  resetAllFormularDraft,
} from "../../../helpers";
import { ApplicationState, AuthState, DispatchAction, RootState } from "../../../models";
import { GuardService, LogService } from "../../../services";

function createSubmitLogin(logService: LogService, guardService: GuardService) {
  return function* (
    action: DispatchAction<{ username: string; password: string; next?: any; }>
  ) {
    const { username, password, next } = action.payload;
    const appState: ApplicationState = yield select(
      (state: RootState) => state.esy.appState);
    const authState: AuthState = yield select(
      (state: RootState) => state.esy.auth
    );

    yield put(showLoadingAction());
    try {
      if (
        appState.isOffline &&
        (!authState ||
          !authState.session ||
          !authState.session.offlineCredential)
      )
        throw new OfflineAuthenticationNotSupported();

      if (appState.isOffline) {
        const loginResponse: AuthState = yield call(
          guardService.offlineLogin,
          username,
          password,
          authState.session.offlineCredential
        );

        yield put(authSucceedAction(loginResponse));

        return;
      }

      const loginResponse: AuthState = yield call(
        guardService.login,
        username,
        password
      );

      if (
        (authState.profile && loginResponse.profile) &&
        (authState.profile.userId !== loginResponse.profile.userId)
      ) {
        yield put(clearFileCache());
        yield put({ type: AKTE_ACTIONS.MUTATION.AKTE_RESET });
        yield put(resetAllFormularDraft());
      }

      yield put(authSucceedAction(loginResponse));

      if (next) yield put(next);
      else yield put(startActivations());
    } catch (error) {
      logService.error(error);
      // const splittedError = error.message.split("\n");
      // const reasonKey = splittedError[splittedError.length - 1];
      Alert.alert(
        localization.language.SignIn_FailedLogin,
        localization.language[
        appState.isOffline
          ? "ErrorNoConnection"
          : parseApiReasonKey(error)
        ],
      );
      yield put(authFailedAction(error));
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startSubmitLogin(
  logService: LogService,
  guardService: GuardService
) {
  logService.debug("starting submit login saga");
  yield takeLeading(
    AUTH_ACTIONS.AUTH_REQUEST_LOGIN,
    createSubmitLogin(logService, guardService)
  );
}
