import { Alert } from "react-native";
import { put, takeLeading } from "redux-saga/effects";
import {
  AUTH_ACTIONS,
  AUTH_STACK,
  hideLoadingAction,
  localization,
  navigateToAction,
  showLoadingAction,
} from "../../../helpers";
import { DispatchAction } from "../../../models";
import { AuthService, LogService } from "../../../services";

function createResetPassword(logService: LogService, authService: AuthService) {
  return function* (action: DispatchAction<{ username: string; }>) {
    logService.debug(`check password for ${action.payload.username}`);

    yield put(showLoadingAction());
    try {
      yield authService.postResetPassword(action.payload.username);

      Alert.alert(localization.language.ForgotPassword_Done);

      yield put(
        navigateToAction({
          navigateTo: {
            screen: AUTH_STACK.SIGN_IN_SCREEN,
          },
        })
      );
    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startResetPasswordSaga(
  logService: LogService,
  authService: AuthService
) {
  logService.debug("starting reset password saga");
  yield takeLeading(
    AUTH_ACTIONS.AUTH_RESET_PASSWORD,
    createResetPassword(logService, authService)
  );
}
