import { put, select, takeLatest } from "redux-saga/effects";
import {
  AUTH_ACTIONS,
  hideLoadingAction,
  localization,
  navigateToAction,
  parseApiReasonKey,
  REGISTRATION_STACK,
  showLoadingAction,
  STACKS,
} from "../../../helpers";
import { DispatchAction, RootState, UserSeed } from "../../../models";
import { AuthService, LogService } from "../../../services";
import { Alert } from "react-native";

function createSubmitPin(logService: LogService, authService: AuthService) {
  return function* (
    action: DispatchAction<{ key: string; pin: string; user: UserSeed; }>
  ) {
    logService.debug(`submit pin ${action.payload.key} ${action.payload.pin}`);
    const { key, pin, user } = action.payload;
    const appState = yield select(
      (state: RootState) => state.esy.appState
    );

    yield put(showLoadingAction());
    try {
      const resp = yield authService.postAdhocLogin(
        user.authenticationKey,
        pin
      );

      // console.log(resp);
      yield put(
        navigateToAction({
          navigateTo: {
            stack: STACKS.REGISTRATION_STACK,
            screen: REGISTRATION_STACK.STARTED_SCREEN,
            params: {
              id: key,
              user: {
                ...action.payload.user,
                accessToken: resp.accessToken,
              },
            },
          },
        })
      );
    } catch (error) {
      logService.error(error);
      Alert.alert(localization.language[
        appState.isOffline
          ? "ErrorNoConnection"
          : parseApiReasonKey(error)
      ]);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startSubmitPinSaga(
  logService: LogService,
  authService: AuthService
) {
  logService.debug("starting submit pin saga");

  yield takeLatest(
    AUTH_ACTIONS.AUTH_REQUEST_PIN_LOGIN,
    createSubmitPin(logService, authService)
  );
}
