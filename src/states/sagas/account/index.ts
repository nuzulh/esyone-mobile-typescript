import { all } from "redux-saga/effects";
import { AccountsService, LogService, UsersService } from "../../../services";
import { startActivateAccount } from "./activate-account";
import { startShareAccount } from "./share-account";
import { startShareAssistant } from "./share-assistant";
import { startLoadAccounts } from "./load-accounts";

export function* startAccountSagas(
  logService: LogService,
  accountsService: AccountsService
  // usersService: UsersService
) {
  logService.debug("start account sagas");
  yield all([
    // startAccountSagas(logService, usersService),
    startShareAccount(logService),
    startShareAssistant(logService),
    startActivateAccount(logService, accountsService),
    startLoadAccounts(logService, accountsService),
  ]);
}
