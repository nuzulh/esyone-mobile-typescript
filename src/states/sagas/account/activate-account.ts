import { call, put, select, takeEvery } from "redux-saga/effects";
import {
  ACCOUNT_ACTIONS,
  authUpdateAction,
  fetchAllAccountAction,
  hideLoadingAction,
  showLoadingAction,
} from "../../../helpers";
import { Account, DispatchAction, RootState, AuthState } from "../../../models";
import { AccountsService, LogService } from "../../../services";

function createActivateAccount(
  logService: LogService,
  accountsService: AccountsService
) {
  return function* (action: DispatchAction<Account>) {
    logService.debug(`activate account ${action.payload.name}...`);

    const account = action.payload;
    const authState: AuthState = yield select(
      (state: RootState) => state.esy.auth
    );

    yield put(showLoadingAction());
    try {
      yield call(accountsService.postActivate, account.id);

      account.isActivated = true;

      authState.accounts[account.id] = { ...account };

      yield put(authUpdateAction({ ...authState }));
      yield put(fetchAllAccountAction());

    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startActivateAccount(
  logService: LogService,
  accountsService: AccountsService
) {
  logService.debug("start activate account saga...");

  yield takeEvery(
    ACCOUNT_ACTIONS.ACCOUNT_ACTIVATE,
    createActivateAccount(logService, accountsService)
  );
}
