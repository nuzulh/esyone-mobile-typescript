import { call, takeLatest } from "redux-saga/effects";
import { Assistant, DispatchAction } from "../../../models";
import { LogService } from "../../../services";
import { Share } from "react-native";
import { ACCOUNT_ACTIONS } from "../../../helpers";

function createShareAssistan(logService: LogService) {
  return function* (action: DispatchAction<Assistant>) {
    logService.debug(`share assistant ${action.payload.name}`);

    try {
      yield call(Share.share, {
        message: action.payload.url,
      });
    } catch (error) {
      logService.error(error);
    }
  };
}

export function* startShareAssistant(logService: LogService) {
  logService.debug("start sharing assistant saga");
  yield takeLatest(
    ACCOUNT_ACTIONS.ACCOUNT_SHARE_ASSISTANT,
    createShareAssistan(logService)
  );
}
