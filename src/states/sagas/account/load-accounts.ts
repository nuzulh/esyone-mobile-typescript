import { call, put, select, takeLeading } from "redux-saga/effects";
import { AccountsService, LogService } from "../../../services";
import {
  ACCOUNT_ACTIONS,
  authUpdateAction,
  hideLoadingBackgroundAction,
  showLoadingBackgroundAction,
} from "../../../helpers";
import { Account, AuthState, RootState } from "../../../models";

function createLoadAccounts(
  logService: LogService,
  accountsService: AccountsService,
) {
  return function* () {
    logService.debug("load all account");
    yield put(showLoadingBackgroundAction());

    const authState: AuthState = yield select(
      (state: RootState) => state.esy.auth
    );

    try {
      const { accounts } = yield accountsService.getAll();

      const newAccounts = {};

      // accounts.forEach((account: Account) => {
      //   newAccounts[`${account.id}`] = account;
      // });
      for (const account of accounts) {
        const accountPublicInfo = yield call(
          accountsService.getPublicInfo,
          account.id
        );
        newAccounts[`${account.id}`] = {
          ...account,
          ...accountPublicInfo,
        };
      }

      authState.accounts = newAccounts;

      yield put(authUpdateAction(authState));

    } catch (err) {
      logService.error(err);
    } finally {
      yield put(hideLoadingBackgroundAction());
    }
  };
}

export function* startLoadAccounts(
  logService: LogService,
  accountsService: AccountsService,
) {
  logService.debug("start load all account saga");
  yield takeLeading(
    ACCOUNT_ACTIONS.ACCOUNT_FETCH_ALL,
    createLoadAccounts(logService, accountsService)
  );
}
