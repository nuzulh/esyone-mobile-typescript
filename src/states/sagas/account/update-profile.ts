import { all, call, put, take, takeLatest } from "redux-saga/effects";
import {
  ACCOUNT_ACTIONS,
  authUpdateAction,
  AUTH_ACTIONS,
} from "../../../helpers";
import {
  fetchProfileAction,
  updateProfileAction,
} from "../../../helpers/actions/account-actions";
import { AuthState, DispatchAction } from "../../../models";
import { LogService, UsersService } from "../../../services";

function createUpdateProfile(
  logService: LogService,
  usersService: UsersService
) {
  return function* (action) {
    try {
      const id = action.payload;
      const result = yield call(usersService.getTask, id);

      // yield put(
      //   updateProfileAction({
      //     accountId: result.accountId,
      //     accountName: result.accountName,
      //     accountShortName: result.accountShortName,
      //     akteId: result.akteId,
      //     authentication: result.authentication,
      //     firstName: result.firstName,
      //     isSmsAvailable: result.isSmsAvailable,
      //     lastName: result.lastName,
      //     phone: result.phone,
      //     pregivenUsername: result.pregivenUsername,
      //     userId: result.userId,
      //     userStatus: result.userStatus,
      //   })
      // );
    } catch (err) {
      logService.error(err);
    }
  };
}

function* watchAuthSucceed(logService: LogService) {
  try {
    while (true) {
      const authAction: DispatchAction<AuthState> = yield take(
        AUTH_ACTIONS.RESULT.AUTH_SUCCEED
      );

      yield put(authUpdateAction(authAction.payload));
      // const { id } = authAction.payload;

      // yield put(fetchProfileAction(id));
    }
  } catch (err) {
    logService.error(err);
  }
}

export function* startUpdateProfile(
  logService: LogService,
  usersService: UsersService
) {
  logService.debug("start update profile saga");
  yield all([
    takeLatest(
      ACCOUNT_ACTIONS.ACCOUNT_FETCH_PROFILE,
      createUpdateProfile(logService, usersService)
    ),
    watchAuthSucceed(logService),
  ]);
}
