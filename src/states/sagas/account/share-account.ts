import { Share, ShareAction } from "react-native";
import { call, takeLatest } from "redux-saga/effects";
import { ACCOUNT_ACTIONS } from "../../../helpers";
import { Account, DispatchAction } from "../../../models";
import { LogService } from "../../../services";

function createShareAccount(logService: LogService) {
  return function* (action: DispatchAction<Account>) {
    logService.debug(`share account ${action.payload.name}`);

    try {
      yield call(Share.share, {
        message:
          "https://app.esy.one/page/" +
          action.payload.uniqueAccountKey,
      });
    } catch (error) {
      logService.error(error);
    }
  };
}

export function* startShareAccount(logService: LogService) {
  logService.debug("start sharing account saga");
  yield takeLatest(
    ACCOUNT_ACTIONS.ACCOUNT_SHARE,
    createShareAccount(logService)
  );
}
