import { call, put, select, takeLeading } from "redux-saga/effects";
import {
  AKTE_ACTIONS,
  hideLoadingAction,
  HOME_STACK,
  navigateToAction,
  reportFileFetchError,
  showLoadingAction,
  updateFileAction,
  updateFileFetchStatusAction,
} from "../../../helpers";
import { Akte, DispatchAction, RootState } from "../../../models";
import { MessageAttachment } from "../../../models/message";
import { FilesService, LogService, MetaFile } from "../../../services";

function createPreviewAttachment(
  logService: LogService,
  filesService: FilesService
) {
  return function* (
    action: DispatchAction<{ akte: Akte; attachment: MessageAttachment }>
  ) {
    const files = yield select(
      (state: RootState) => state.esy.filesState.files
    );
    const isOffline = yield select(
      (state: RootState) => state.esy.appState.isOffline
    );
    const { akte, attachment } = action.payload;
    logService.debug(
      `preview attachment ${attachment.id} ${attachment.fileName}`
    );

    if (attachment.metaFile) {
      yield put(
        navigateToAction({
          navigateTo: {
            screen: HOME_STACK.PREVIEW_SCREEN,
            params: {
              metaFile: attachment.metaFile,
            },
          },
        })
      );

      return;
    }

    yield put(showLoadingAction());

    try {
      // download attachment
      let file: MetaFile;

      if (!files[attachment.id] && !isOffline) {
        file = yield call(
          filesService.downloadFile,
          akte.accountId,
          attachment.id
        );

        // update attachment
        logService.debug(`get temporary file path ${file.filePath}`);

        yield put(updateFileAction(file));
      } else {
        file = files[attachment.id];

        logService.debug(`get cached file path ${file.filePath}`);
      }

      if (!file) throw new Error("file not found");

      yield put(
        navigateToAction({
          navigateTo: {
            screen: HOME_STACK.PREVIEW_SCREEN,
            params: {
              metaFile: file,
            },
          },
        })
      );
    } catch (err) {
      logService.error(err);
      yield put(reportFileFetchError(err));
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startPreviewAttachment(
  logService: LogService,
  filesService: FilesService
) {
  logService.debug("start preview attachment");

  yield takeLeading(
    AKTE_ACTIONS.AKTE_PREVIEW_ATTACHMENT,
    createPreviewAttachment(logService, filesService)
  );
}
