import { all } from "redux-saga/effects";
import {
  AccountsService,
  AktenService,
  AufgabeService,
  AuthService,
  FilesService,
  LogService,
  PermissionService,
} from "../../../services";
import { startLoadAkten } from "./load-akten";
import { startLoadAufgabe } from "./load-aufgabe";
import { startLoadSingleAkte } from "./load-single-akte";
import { startLoadWorkflow } from "./load-workflow";
import { startLoadWorkflowAnswer } from "./load-workflow-answer";
import { startPreviewAccount } from "./preview-account";
import { startPreviewAgb } from "./preview-agb";
import { startPreviewAkte } from "./preview-akte";
import { startPreviewAttachment } from "./preview-attachment";
import { startDownloadAttachment } from "./download-attachment";
import { startPreviewDatenschutz } from "./preview-datenschutz";
import { startPreviewFile } from "./preview-file";
import { startSubmitWorkflow } from "./submit-workflow";
import { startSubmitMessageSaga } from "./submit-message";
import { startToggleFavoriteMessage } from "./toggle-favorite-message";
import { startReadMessage } from "./read-message";
import { startWatchClearCache } from "./clear-cache";
import { startRedirectExternallinkSaga } from "./redirect-external-link";
import { startDownloadAkteSaga } from "./download-akte";

export function* startAktenSagas(
  logService: LogService,
  aktenService: AktenService,
  aufgabeService: AufgabeService,
  filesService: FilesService,
  accountsService: AccountsService,
  authService: AuthService,
  permissionService: PermissionService
) {
  yield all([
    startLoadAkten(logService, aktenService, aufgabeService),
    startPreviewAttachment(logService, filesService),
    startDownloadAttachment(logService, permissionService, filesService),
    startDownloadAkteSaga(logService, permissionService, aktenService),
    startLoadWorkflow(logService, aufgabeService, accountsService, authService, filesService),
    startLoadAufgabe(logService),
    startPreviewAccount(logService, accountsService),
    startPreviewAgb(logService),
    startPreviewDatenschutz(logService, accountsService),
    startSubmitWorkflow(logService, aufgabeService, filesService),
    startLoadSingleAkte(logService, aktenService, aufgabeService),
    startPreviewAkte(logService, aktenService, aufgabeService),
    startLoadWorkflowAnswer(logService, aufgabeService),
    startPreviewFile(logService, filesService),
    startSubmitMessageSaga(logService, filesService, aktenService),
    startToggleFavoriteMessage(logService, aktenService),
    startReadMessage(logService, aktenService),
    startWatchClearCache(logService),
    startRedirectExternallinkSaga(logService, permissionService),
  ]);
}
