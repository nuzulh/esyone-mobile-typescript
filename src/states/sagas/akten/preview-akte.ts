import { call, put, select, take, takeLatest } from "redux-saga/effects";
import {
  AKTE_ACTIONS,
  fetchSingleAkteAction,
  hideLoadingAction,
  HOME_STACK,
  navigateToAction,
  showLoadingAction,
  updateSingleAkteAction,
} from "../../../helpers";
import { ResourceData } from "../../../hooks";
import { Account, Akte, RootState } from "../../../models";
import { AktenService, AufgabeService, LogService } from "../../../services";
import { createFetchAkte } from "./load-single-akte";

function createPreviewAkte(
  logService: LogService,
  aktenService: AktenService,
  aufgabeService: AufgabeService
) {
  const fetchAkte = createFetchAkte(aktenService, aufgabeService);

  return function* (action: {
    type: string;
    payload: { account: Account; akte: Akte; initialScreen?: string };
  }) {
    let { account, akte, initialScreen } = action.payload;
    logService.debug(`preview akte ${akte.id}`);
    // console.log(akte.phases);

    if (
      !akte.phases ||
      akte.phases.length === 0 ||
      !akte.messages ||
      !akte.attachments ||
      !akte.assignees ||
      akte.assignees.length === 0 ||
      !akte.children ||
      !akte.aufgaben
    ) {
      yield put(showLoadingAction());

      try {
        akte = yield call(fetchAkte, account, akte);

        yield put(updateSingleAkteAction(akte));
      } catch (error) {
        logService.error(error);
      } finally {
        yield put(hideLoadingAction());
      }
    } else yield put(fetchSingleAkteAction(account, akte));

    yield put(
      navigateToAction({
        navigateTo: {
          // stack: STACKS.HOME_STACK,
          screen: HOME_STACK.MESSAGE_SCREEN,
          params: {
            provider: account,
            akte,
            initialTab: initialScreen,
          },
        },
      })
    );
    // const akten:ResourceData<Akte> = yield select((state:RootState) => state.esy.akten.akten);
  };
}

export function* startPreviewAkte(
  logService: LogService,
  aktenService: AktenService,
  aufgabeService: AufgabeService
) {
  logService.debug("start preview akte saga");
  yield takeLatest(
    AKTE_ACTIONS.AKTE_PREVIEW,
    createPreviewAkte(logService, aktenService, aufgabeService)
  );
}
