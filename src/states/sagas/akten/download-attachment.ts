import { all, call, put, select, takeLeading } from "redux-saga/effects";
import {
  AKTE_ACTIONS,
  downloadAttachmentAction,
  hideLoadingAction,
  localization,
  showLoadingAction,
  updateFileAction,
} from "../../../helpers";
import { Akte, DispatchAction, Message, RootState } from "../../../models";
import {
  FilesService,
  LogService,
  MetaFile,
  PermissionService,
} from "../../../services";
import { Alert, Platform } from "react-native";
import RNFS from "react-native-fs";
import RNFetchBlob from "rn-fetch-blob";
import moment from "moment";
import { useFileReader } from "../../../hooks";

function createDownloadAttachment(
  logService: LogService,
  permissionService: PermissionService
) {
  return function* (action: DispatchAction<{ metafile: MetaFile; }>) {
    const { metafile } = action.payload;

    const parsedFileName = metafile.fileName.replace(/"/g, "");

    logService.debug(`downloading attachment ${parsedFileName}...`);
    yield put(showLoadingAction());

    try {
      const isAllowed = yield call(permissionService.askFilesPermission);

      if (isAllowed) {
        if (Platform.OS === "ios") {
          RNFetchBlob.ios.previewDocument(metafile.filePath);
        } else {
          RNFetchBlob.fs
            .cp(
              metafile.filePath,
              `${RNFS.DownloadDirectoryPath}/${parsedFileName}`
            )
            .then(() => {
              logService.debug(
                `attachment ${parsedFileName} downloaded to ${RNFS.DownloadDirectoryPath}/${parsedFileName} successfully`
              );
              Alert.alert(localization.language.FileSaved);
            })
            .catch((err) => {
              Alert.alert(localization.language.DownloadFailed);
              throw new Error(err);
            });
        }
      }
    } catch (err) {
      logService.error(err);
      Alert.alert(localization.language.DownloadFailed);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

function createDownloadAttachmentFromMessage(
  logService: LogService,
  filesService: FilesService
) {
  return function* (action: DispatchAction<{ akte: Akte; message: Message; }>) {
    const { akte, message } = action.payload;
    const files = yield select(
      (state: RootState) => state.esy.filesState.files
    );
    const isOffline = yield select(
      (state: RootState) => state.esy.appState.isOffline
    );

    logService.debug(`downloading attachment from message ${message.id}...`);

    if (!message.attachments && message.attachments.length === 0) return;

    yield put(showLoadingAction());
    try {
      for (let attachment of message.attachments) {
        let file: MetaFile;

        if (!files[attachment.id] && !isOffline) {
          file = yield call(
            filesService.downloadFile,
            akte.accountId,
            attachment.id
          );

          // update attachment
          logService.debug(`get temporary file path ${file.filePath}`);

          yield put(updateFileAction(file));
        } else {
          file = files[attachment.id];

          logService.debug(`get cached file path ${file.filePath}`);
        }

        if (!file) throw new Error("file not found");

        yield put(downloadAttachmentAction(file));
      }
    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startDownloadAttachment(
  logService: LogService,
  permissionService: PermissionService,
  filesService: FilesService
) {
  logService.debug("start download attachment");

  yield all([
    takeLeading(
      AKTE_ACTIONS.AKTE_DOWNLOAD_ATTACHMENT,
      createDownloadAttachment(logService, permissionService)
    ),
    takeLeading(
      AKTE_ACTIONS.AKTE_DOWNLOAD_ATTACHMENT_FROM_MESSAGE,
      createDownloadAttachmentFromMessage(logService, filesService)
    ),
  ]);
}
