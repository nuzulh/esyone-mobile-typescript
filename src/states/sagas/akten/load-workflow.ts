import {
  all,
  call,
  put,
  select,
  takeLatest,
  takeLeading,
} from "redux-saga/effects";
import {
  checkTokenIsValid,
  hideLoadingAction,
  HOME_STACK,
  navigateToAction,
  parseFormTypeLength,
  PUBLIC_STACK,
  showLoadingAction,
  STACKS,
  updateFileAction,
  WORKFLOW_ACTIONS,
} from "../../../helpers";
import { Account, Insurance, RootState } from "../../../models";
import { WorkflowAdhoc, WorkflowFormular, WorkflowProcess } from "../../../models/workflow";
import {
  AccountsService,
  AufgabeService,
  AuthService,
  FileUploadInfo,
  FilesService,
  LogService,
  MetaFile,
  VirtualMetaFile,
} from "../../../services";
import { ResourceData } from "../../../hooks";

function createLoadWorkflow(
  logService: LogService,
  aufgabeService: AufgabeService,
  accountsService: AccountsService,
  authService: AuthService,
  filesService: FilesService
) {
  return function* (action: {
    type: string;
    payload: { accountId: number; workflowId: string; accessToken: string; };
  }) {
    const { accountId, workflowId, accessToken } = action.payload;
    const currentAccessToken = yield select(
      (state: RootState) => state.esy.auth?.session?.accessToken
    );
    const isValidToken = checkTokenIsValid(currentAccessToken);
    logService.debug(`load workflow ${workflowId}`);

    yield put(showLoadingAction());

    try {
      // get account
      const account: Account = yield call(
        accountsService.getPublicInfo,
        accountId
      );

      // get workflow
      const workflowProcess: WorkflowProcess = yield call(
        aufgabeService.getWorkflow,
        workflowId,
        accessToken
      );

      const insurances: Insurance[] = yield call(
        accountsService.getInsurances,
        accountId,
        accessToken
      );

      // get upload file info if formElements includes "file" type
      let uploadInfo: FileUploadInfo;
      if (
        parseFormTypeLength(
          workflowProcess.formElement.formElements,
          "file"
        ) > 0
      ) uploadInfo = yield call(
        filesService.getUploadInfo,
        accountId,
        accessToken
      );

      // get attachment files if exists
      let files: MetaFile[] = null;
      if (workflowProcess.attachments.length > 0) {
        const workflowAdhoc: WorkflowAdhoc = yield call(
          authService.postAdhocWorkflow,
          workflowId
        );
        // download attachments (WITH ADHOC TOKEN for this case)
        files = yield Promise.all(
          workflowProcess.attachments.map((attachment) =>
            filesService.downloadFile(
              accountId,
              attachment.id,
              workflowAdhoc.accessToken
            )
          )
        );
        // add attachments to file cache
        for (let i = 0; i < files.length; i++)
          yield put(updateFileAction(files[i]));
      }

      // get workflow formular field values draft
      const workflowFormular: ResourceData<WorkflowFormular> = yield select(
        (state: RootState) => state.esy.formularState.formular
      );

      // console.log(JSON.stringify(workflowFormular, null, 2));

      yield put(
        navigateToAction({
          navigateTo: {
            stack: isValidToken ? STACKS.HOME_STACK : STACKS.PUBLIC_STACK,
            screen: isValidToken
              ? HOME_STACK.FORMULA_SCREEN
              : PUBLIC_STACK.FORMULA_SCREEN,
            params: {
              accessToken: currentAccessToken ?? accessToken,
              account,
              workflowProcess,
              insurances,
              files,
              uploadInfo,
              workflowFormular: workflowFormular[workflowId],
            },
          },
        })
      );
    } catch (err) {
      // should navigate to error screen
      logService.error(err);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

function createResumeLoadWorkflow(
  logService: LogService,
  aufgabeService: AufgabeService,
  accountsService: AccountsService,
  authService: AuthService,
  filesService: FilesService
) {
  return function* (action: { type: string; payload: { workflowId: string; }; }) {
    const { workflowId } = action.payload;
    const accessToken = yield select(
      (state: RootState) => state.esy.auth?.session?.accessToken
    );
    const isValidToken = checkTokenIsValid(accessToken);
    logService.debug(`resume workflow ${workflowId}`);

    yield put(showLoadingAction());

    try {
      // adhoc workflow
      const workflowAdhoc: WorkflowAdhoc = yield call(
        authService.postAdhocWorkflow,
        workflowId
      );

      // get account
      const account: Account = yield call(
        accountsService.getPublicInfo,
        workflowAdhoc.accountId
      );

      // get workflow
      const workflowProcess: WorkflowProcess = yield call(
        aufgabeService.getWorkflow,
        workflowId,
        workflowAdhoc.accessToken
      );

      const insurances: Insurance[] = yield call(
        accountsService.getInsurances,
        workflowAdhoc.accountId,
        workflowAdhoc.accessToken
      );

      // get upload file info if formElements includes "file" type
      let uploadInfo: FileUploadInfo;
      if (
        parseFormTypeLength(
          workflowProcess.formElement.formElements,
          "file"
        ) > 0
      ) uploadInfo = yield call(
        filesService.getUploadInfo,
        workflowAdhoc.accountId,
        accessToken
      );

      yield put(
        navigateToAction({
          navigateTo: {
            stack: isValidToken ? STACKS.HOME_STACK : STACKS.PUBLIC_STACK,
            screen: isValidToken
              ? HOME_STACK.FORMULA_SCREEN
              : PUBLIC_STACK.FORMULA_SCREEN,
            params: {
              accessToken: accessToken ?? workflowAdhoc.accessToken,
              account,
              workflowProcess,
              insurances,
              uploadInfo,
            },
          },
        })
      );
    } catch (err) {
      // should navigate to error screen
      logService.error(err);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startLoadWorkflow(
  logService: LogService,
  aufgabeService: AufgabeService,
  accountsService: AccountsService,
  authService: AuthService,
  filesService: FilesService
) {
  logService.debug("start load workflow saga");

  yield all([
    takeLeading(
      WORKFLOW_ACTIONS.WORKFLOW_LOAD,
      createLoadWorkflow(
        logService,
        aufgabeService,
        accountsService,
        authService,
        filesService
      )
    ),
    takeLeading(
      WORKFLOW_ACTIONS.WORKFLOW_RESUME,
      createResumeLoadWorkflow(
        logService,
        aufgabeService,
        accountsService,
        authService,
        filesService
      )
    ),
  ]);
}
