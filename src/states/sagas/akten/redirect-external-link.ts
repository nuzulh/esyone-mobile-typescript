import { call, put, takeLeading } from "redux-saga/effects";
import { DispatchAction } from "../../../models";
import { LogService, PermissionService } from "../../../services";
import { AKTE_ACTIONS, HOME_STACK, navigateToAction } from "../../../helpers";

function createRedirectExternallink(
  logService: LogService,
  permissionService: PermissionService
) {
  return function* (action: DispatchAction<{
    url: string;
    title: string;
  }>) {
    const { url, title } = action.payload;
    logService.debug(`handle redirect external link ${url}`);

    const isAllowed = yield call(
      permissionService.askRedirectExternallink,
      url,
    );

    if (!isAllowed) {
      yield put(
        navigateToAction({ goBack: true })
      );
      return;
    }

    yield put(
      navigateToAction({
        navigateTo: {
          screen: HOME_STACK.WEBVIEW_SCREEN,
          params: { url, title },
        },
        replace: true,
      })
    );
  };
}

export function* startRedirectExternallinkSaga(
  logService: LogService,
  permissionService: PermissionService
) {
  logService.debug("start redirect external link saga");
  yield takeLeading(
    AKTE_ACTIONS.AKTE_REDIRECT_EXTERNAL_LINK,
    createRedirectExternallink(logService, permissionService)
  );
}
