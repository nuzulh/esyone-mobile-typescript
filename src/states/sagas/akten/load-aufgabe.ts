import { put, select, takeLatest } from "redux-saga/effects";
import {
  checkTokenIsValid,
  loadWorkflow,
  WORKFLOW_ACTIONS,
} from "../../../helpers";
import { Account, Aufgabe, RootState } from "../../../models";
import { LogService } from "../../../services";

function createLoadAufgabe(logService: LogService) {
  return function* (action: {
    type: string;
    payload: { account: Account; aufgabe: Aufgabe };
  }) {
    const { account, aufgabe } = action.payload;
    const accessToken = yield select(
      (state: RootState) => state.esy.auth?.session?.accessToken
    );

    logService.debug(`load aufgabe ${aufgabe.workflowId}`);

    if (!accessToken)
      return logService.error(new Error("no access token found"));
    if (!checkTokenIsValid(accessToken))
      return logService.error(new Error("access token is not valid"));

    yield put(loadWorkflow(account.id, aufgabe.workflowId, accessToken));
  };
}

export function* startLoadAufgabe(logService: LogService) {
  logService.debug("start load aufgabe saga");

  yield takeLatest(
    WORKFLOW_ACTIONS.WORKFLOW_LOAD_AUFGABE,
    createLoadAufgabe(logService)
  );
}
