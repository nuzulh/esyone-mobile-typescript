import { call, put, select, takeLeading } from "redux-saga/effects";
import { DispatchAction, RootState } from "../../../models";
import { AktenService, LogService, MetaFile, PermissionService } from "../../../services";
import { AKTE_ACTIONS, hideLoadingAction, localization, showLoadingAction } from "../../../helpers";
import { Alert, Platform } from "react-native";
import RNFetchBlob from "rn-fetch-blob";
import { ToastType } from "react-native-fast-toast";

function createDownloadAkte(
  logService: LogService,
  permissionService: PermissionService,
  aktenService: AktenService
) {
  return function* (action: DispatchAction<{
    accountId: number,
    akteId: string,
  }>) {
    const { accountId, akteId } = action.payload;

    logService.debug(`downloading akte ${akteId} as zip file...`);
    yield put(showLoadingAction());

    try {
      const toast: ToastType = yield select(
        (state: RootState) => state.esy.appState.toastRef
      );
      const isAllowed = yield call(permissionService.askFilesPermission);

      if (!isAllowed) throw new Error("permission is not permitted");

      const file: MetaFile = yield call(
        aktenService.getDownloadAkte,
        accountId,
        akteId
      );

      if (Platform.OS === "ios") RNFetchBlob.ios.previewDocument(file.filePath);
      else {
        yield RNFetchBlob.fs.mv(
          file.filePath,
          `${RNFetchBlob.fs.dirs.DownloadDir}/${file.fileName}`
        );

        logService.debug(`zip of akte ${akteId} downloaded to ${RNFetchBlob.fs.dirs.DownloadDir} successfully`);
        toast.show(localization.language.FileSaved);
      }

    } catch (error) {
      logService.error(error);
      Alert.alert(
        localization.language.DownloadFailed,
        error?.message
      );
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startDownloadAkteSaga(
  logService: LogService,
  permissionService: PermissionService,
  aktenService: AktenService
) {
  logService.debug("start download akte saga");

  yield takeLeading(
    AKTE_ACTIONS.AKTE_DOWNLOAD_AS_ZIP,
    createDownloadAkte(logService, permissionService, aktenService)
  );
}
