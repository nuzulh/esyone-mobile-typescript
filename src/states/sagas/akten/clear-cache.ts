import { call, put, select, takeEvery } from "redux-saga/effects";
import RNFetchBlob from "rn-fetch-blob";
import {
  FILE_ACTIONS,
  hideLoadingAction,
  removeFile,
  showLoadingAction,
} from "../../../helpers";
import { FilesState, RootState } from "../../../models";
import { LogService } from "../../../services";

function createClearCache(logService: LogService) {
  return function* () {
    logService.debug(`clear cache`);

    const filesState: FilesState = yield select(
      (state: RootState) => state.esy.filesState
    );

    yield showLoadingAction();
    for (const key in filesState.files) {
      const file = filesState.files[key];

      try {
        if (file.filePath) {
          logService.debug(`remove file ${file.filePath}`);

          // yield call(RNFetchBlob.fs.unlink, file.filePath);
          yield RNFetchBlob.fs.unlink(file.filePath);
          yield put(removeFile(file));
        }
      } catch (error) {
        logService.debug(`error while removing file ${file.filePath}`);
        logService.error(error);
      }
    }

    yield hideLoadingAction();
  };
}

export function* startWatchClearCache(logService: LogService) {
  logService.debug(`start watch clear cache`);

  yield takeEvery(FILE_ACTIONS.FILE_CLEAR_CACHE, createClearCache(logService));
}
