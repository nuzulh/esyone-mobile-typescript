import { all, call, put, select, takeLatest } from "redux-saga/effects";
import {
  ACCOUNT_ACTIONS,
  checkTokenIsValid,
  getDefaultHeader,
  hideLoadingAction,
  HOME_STACK,
  navigateToAction,
  PUBLIC_STACK,
  showLoadingAction,
  STACKS,
} from "../../../helpers";
import { Account, DispatchAction, RootState } from "../../../models";
import {
  AccountsService,
  LogService,
  MetaFile,
  VirtualMetaFile,
} from "../../../services";

function createPreviewDatenschutz(
  logService: LogService,
  accountService: AccountsService
) {
  const defaultHeaders = getDefaultHeader();

  return function* (action: DispatchAction<Account>) {
    const accessToken = yield select(
      (state: RootState) => state.esy.auth?.session?.accessToken
    );
    const isValidToken = checkTokenIsValid(accessToken);
    yield put(showLoadingAction());

    try {
      const datenschutz = yield call(
        accountService.getDatenschutz,
        action.payload.id
      );

      const metaFile: VirtualMetaFile = {
        id: null,
        accountId: action.payload.id,
        fileName: "Datenschutz.txt",
        filePath: null,
        contentType: "text/datenschutz",
        size: datenschutz.length,
        extension: "txt",
        content: datenschutz,
      };

      yield put(
        navigateToAction({
          navigateTo: {
            stack: isValidToken ? STACKS.HOME_STACK : STACKS.PUBLIC_STACK,
            screen: isValidToken
              ? HOME_STACK.PREVIEW_SCREEN
              : PUBLIC_STACK.PREVIEW_SCREEN,
            params: {
              metaFile,
              hideDownload: true,
            },
          },
        })
      );

    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

function createPreviewDatenschutzText(logService: LogService) {
  return function* (action: DispatchAction<Account>) {
    const accessToken = yield select(
      (state: RootState) => state.esy.auth?.session?.accessToken
    );
    const isValidToken = checkTokenIsValid(accessToken);
    yield put(showLoadingAction());

    try {
      const metaFile: VirtualMetaFile = {
        id: null,
        accountId: action.payload.id,
        fileName: "Datenschutz.txt",
        filePath: null,
        contentType: "text/datenschutz",
        size: action.payload.esyThingDatenschutz.length,
        extension: "txt",
        content: action.payload.esyThingDatenschutz,
      };

      yield put(
        navigateToAction({
          navigateTo: {
            stack: isValidToken ? STACKS.HOME_STACK : STACKS.PUBLIC_STACK,
            screen: isValidToken
              ? HOME_STACK.PREVIEW_SCREEN
              : PUBLIC_STACK.PREVIEW_SCREEN,
            params: {
              metaFile,
              hideDownload: true,
            },
          },
        })
      );

    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startPreviewDatenschutz(
  logService: LogService,
  accountService: AccountsService
) {
  logService.info("start preview datenschutz saga");
  yield all([
    takeLatest(
      ACCOUNT_ACTIONS.ACCOUNT_PREVIEW_DATENSCHUTZ,
      createPreviewDatenschutz(logService, accountService)
    ),
    takeLatest(
      ACCOUNT_ACTIONS.ACCOUNT_PREVIEW_DATENSCHUTZ_TEXT,
      createPreviewDatenschutzText(logService)
    ),
  ]);
}
