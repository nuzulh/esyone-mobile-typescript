import { call, put, takeEvery } from "redux-saga/effects";
import {
  AKTE_ACTIONS,
  hideLoadingBackgroundAction,
  showLoadingBackgroundAction,
  updateSingleAkteAction,
} from "../../../helpers";
import { Account, Akte, AktePhase } from "../../../models";
import {
  AktenService,
  AufgabeService,
  GetMessagesResult,
  LogService,
} from "../../../services";

export function createFetchAkte(
  aktenService: AktenService,
  aufgabeService: AufgabeService
) {
  return async function (account: Account, akte: Akte) {
    akte = await aktenService.getOne(account.id, akte.id);

    // get phases history
    const phases: AktePhase[] = await aktenService
      .getHistory(akte.accountId, akte.id)
      .catch(() => []);

    akte.phases = phases || [];
    // get messages
    const messagesResult: GetMessagesResult = await aktenService
      .getMessages(akte.accountId, akte.id, { size: 100000 })
      .catch(() => ({
        attachments: [],
        folders: [],
        messages: [],
        totalMessage: 0,
      }));

    akte.messages = messagesResult.messages
      // ? messagesResult.messages.reverse()
      ? messagesResult.messages
        .sort(
          (a, b) => b.creationDate.getTime() - a.creationDate.getTime()
        )
      : [];
    akte.attachments = messagesResult.attachments || [];
    akte.children = [];

    const fetchAkte = createFetchAkte(aktenService, aufgabeService);
    for (let folder of messagesResult.folders || []) {
      const child = await fetchAkte(account, { id: folder.id } as Akte).catch(
        () => null
      );
      if (child) akte.children.push(child);
    }

    // load aufgaben
    akte.aufgaben = await aufgabeService
      .getAll(akte.accountId, akte.id)
      .catch(() => []);

    return akte;
  };
}

function createLoadSingleAkte(
  logService: LogService,
  aktenService: AktenService,
  aufgabeService: AufgabeService
) {
  const fetchAkte = createFetchAkte(aktenService, aufgabeService);

  return function* (action: {
    type: string;
    payload: { account: Account; akte: Akte };
  }) {
    let { account, akte } = action.payload;

    logService.debug(`load akte ${account.id} ${akte.id}`);
    yield put(showLoadingBackgroundAction());
    try {
      akte = yield call(fetchAkte, account, akte);

      yield put(updateSingleAkteAction(akte));
    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingBackgroundAction());
    }
  };
}

export function* startLoadSingleAkte(
  logService: LogService,
  aktenService: AktenService,
  aufgabeService: AufgabeService
) {
  logService.debug("start load single akte saga");
  yield takeEvery(
    AKTE_ACTIONS.AKTE_FETCH,
    createLoadSingleAkte(logService, aktenService, aufgabeService)
  );
}
