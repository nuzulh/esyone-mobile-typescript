import { call, put, select, takeLeading } from "redux-saga/effects";
import {
  AKTE_ACTIONS,
  hideLoadingAction,
  HOME_STACK,
  navigateToAction,
  reportFileFetchError,
  showLoadingAction,
  updateFileAction,
  updateFileFetchStatusAction,
} from "../../../helpers";
import { Account, Akte, DispatchAction, RootState } from "../../../models";
import { MessageAttachment } from "../../../models/message";
import { FilesService, LogService, MetaFile } from "../../../services";

function createPreviewFile(logService: LogService, filesService: FilesService) {
  return function* (
    action: DispatchAction<{ account: Account; file: MetaFile; }>
  ) {
    const files = yield select(
      (state: RootState) => state.esy.filesState.files
    );
    const isOffline = yield select(
      (state: RootState) => state.esy.appState.isOffline
    );
    let { account, file } = action.payload;
    logService.debug(`preview file ${file.id} ${file.fileName}`);

    yield put(showLoadingAction());

    try {
      // download attachment
      // let file: MetaFile;

      if (!files[file.id] && !isOffline) {
        file = yield call(
          filesService.downloadFile,
          account.id,
          file.id && typeof file.id === "string"
            ? parseInt(file.id)
            : (file.id as number)
        );

        // update attachment
        logService.debug(`get temporary file path ${file.filePath}`);

        yield put(updateFileAction(file));
      } else {
        file = files[file.id];

        logService.debug(`get cached file path ${file.filePath}`);
      }

      if (!file) throw new Error("file not found");

      yield put(
        navigateToAction({
          navigateTo: {
            screen: HOME_STACK.PREVIEW_SCREEN,
            params: {
              metaFile: file,
            },
          },
        })
      );
    } catch (err) {
      logService.error(err);
      yield put(reportFileFetchError(err));
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startPreviewFile(
  logService: LogService,
  filesService: FilesService
) {
  logService.debug("start preview file saga");

  yield takeLeading(
    AKTE_ACTIONS.AKTE_PREVIEW_FILE,
    createPreviewFile(logService, filesService)
  );
}
