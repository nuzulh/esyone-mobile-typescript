import { call, put, select, takeLatest } from "redux-saga/effects";
import {
  checkTokenIsValid,
  fetchAllAkteAction,
  fetchSingleAkteAction,
  hideLoadingAction,
  HOME_STACK,
  localization,
  navigateToAction,
  parseApiReasonKey,
  PUBLIC_STACK,
  resetFormularDraft,
  showLoadingAction,
  STACKS,
  WORKFLOW_ACTIONS,
} from "../../../helpers";
import {
  DispatchAction,
  RootState,
  WorkflowProcessSubmission,
} from "../../../models";
import {
  AufgabeService,
  FilesService,
  LogService,
  MetaFile,
} from "../../../services";
import { ToastType } from "react-native-fast-toast";
import { Alert } from "react-native";

function createSubmitWorkflow(
  logService: LogService,
  aufgabeService: AufgabeService,
  filesService: FilesService
) {
  return function* (action: DispatchAction<WorkflowProcessSubmission>) {
    // console.log(action.payload.account);
    logService.debug(`submit workflow ${action.payload.workflowProcess.workflowId}`);
    const data = {
      formInput: action.payload.data,
    };
    const { uploadInfo } = action.payload;
    const toastRef: ToastType = yield select(
      (state: RootState) => state.esy.appState.toastRef
    );
    const currentAccessToken = yield select(
      (state: RootState) => state.esy.auth?.session?.accessToken
    );
    const isValidToken = checkTokenIsValid(currentAccessToken);

    if (!data.formInput) return;

    // console.log(JSON.stringify(data, null, 2));

    // return;

    yield put(showLoadingAction());
    try {
      // validate the sum of file sizes
      let countSize = 0;
      for (let key in data.formInput)
        if (key.startsWith("file")) {
          data.formInput[key].forEach((file) => {
            countSize += file.size;
          });
        }
      if (
        uploadInfo &&
        countSize / 1000 > uploadInfo.totalFileSizeLimitInKb
      ) {
        if (toastRef) toastRef.show(
          `The sum of file sizes in this form cannot be larger than ${uploadInfo.totalFileSizeLimitInKb / 1000} MB`
        );
        throw new Error("the sum of file sizes in this form are too large");
      }

      // check if there are any files to upload
      for (let key in data.formInput) {
        if (!key.startsWith("file")) continue;

        if (data.formInput[key].length == 0)
          throw new Error("no files to upload");
        if (data.formInput[key][0].tags == undefined)
          throw new Error("no tags found for file upload");
        if (data.formInput[key][0].tags.elementId == undefined)
          throw new Error("no elementId found for file upload");

        const elementId = data.formInput[key][0].tags.elementId;

        const uploadResponse = yield call(
          filesService.uploadFiles,
          action.payload.account.id,
          data.formInput[key],
          action.payload.accessToken
        );

        // console.log(uploadResponse);
        if (!uploadResponse) throw new Error("failed to upload file");
        delete data.formInput[key];

        if (key.includes("repeater")) data.formInput[
          `repeater${key.split("repeater").pop()}_formelement_${elementId}`
        ] = uploadResponse.map((result) => result.uploadId);

        else data.formInput[
          `formelement_${elementId}`
        ] = uploadResponse.map((result) => result.uploadId);
      }

      // console.log(JSON.stringify(data, null, 2));

      // return;

      const response = yield call(
        aufgabeService.postWorkflow,
        action.payload.workflowProcess.workflowId,
        action.payload.accessToken,
        data
      );

      // console.log(response);
      if (
        !response ||
        response.resultText == undefined ||
        response.resultText == null
      )
        throw new Error("failed to submit workflow");

      const akte = yield select(
        (state: RootState) =>
          state.esy.akten.akten[action.payload.workflowProcess.akteId]
      );

      if (akte) yield put(
        fetchSingleAkteAction(
          action.payload.account,
          akte
        )
      );

      if (isValidToken) yield put(fetchAllAkteAction());

      yield put(
        resetFormularDraft(action.payload.workflowProcess.workflowId)
      );

      yield put(
        navigateToAction({
          navigateTo: {
            stack: isValidToken ? STACKS.HOME_STACK : STACKS.PUBLIC_STACK,
            screen: isValidToken
              ? HOME_STACK.FORMULA_SCREEN_SUCCESS
              : PUBLIC_STACK.FORMULA_SCREEN_SUCCESS,
            params: {
              account: action.payload.account,
              title: response.resultText,
            },
          },
        })
      );
    } catch (error) {
      logService.error(error);
      Alert.alert(
        "",
        localization.language[parseApiReasonKey(error)]
      );
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startSubmitWorkflow(
  logService: LogService,
  aufgabeService: AufgabeService,
  filesService: FilesService
) {
  logService.debug("start submit workflow saga");
  yield takeLatest(
    WORKFLOW_ACTIONS.WORKFLOW_SUBMIT,
    createSubmitWorkflow(logService, aufgabeService, filesService)
  );
}
