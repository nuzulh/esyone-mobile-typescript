import { call, put, select, takeLatest, all } from "redux-saga/effects";
import RNFetchBlob from "rn-fetch-blob";
import {
  ACCOUNT_ACTIONS,
  applyAuthHeader,
  checkTokenIsValid,
  extractHeaders,
  getDefaultHeader,
  getPathFrom,
  hideLoadingAction,
  HOME_STACK,
  navigateToAction,
  previewFileAction,
  PUBLIC_STACK,
  showLoadingAction,
  STACKS,
} from "../../../helpers";
import { Account, DispatchAction, RootState } from "../../../models";
import { LogService, VirtualMetaFile } from "../../../services";

function createPreviewAgb(logService: LogService) {
  const defaultHeaders = getDefaultHeader();

  return function* (action: DispatchAction<Account>) {
    const accessToken = yield select(
      (state: RootState) => state.esy.auth?.session?.accessToken
    );
    const isValidToken = checkTokenIsValid(accessToken);

    yield put(showLoadingAction());

    try {
      const rnbFetch = RNFetchBlob.config({ fileCache: false });
      const res = yield call(
        rnbFetch.fetch,
        "GET",
        getPathFrom(
          `/cheetah/api/v2.0/e.consult.${action.payload.id}/account/agb?resultType=stream`
        ),
        {
          ...applyAuthHeader(defaultHeaders, accessToken),
        }
      );


      const path = `${RNFetchBlob.fs.dirs.DocumentDir}/esy_agb_cache_${action.payload.id}`;
      const content = yield res.base64();

      yield RNFetchBlob.fs.writeFile(path, content, "base64");

      const headers = extractHeaders(res.respInfo.headers);

      const metaFile = {
        id: null,
        accountId: action.payload.id,
        fileName: headers["content-disposition"]
          ? headers["content-disposition"].split("=")[1]
          : `${action.payload.id}-agb.pdf`,
        filePath: path,
        contentType: headers["content-type"],
        size: headers["content-length"],
        extension: headers["content-disposition"]
          ? headers["content-disposition"].split("=")[1].split(".")[1]
          : "pdf",
      };

      yield put(
        navigateToAction({
          navigateTo: {
            stack: isValidToken ? STACKS.HOME_STACK : STACKS.PUBLIC_STACK,
            screen: isValidToken
              ? HOME_STACK.PREVIEW_SCREEN
              : PUBLIC_STACK.PREVIEW_SCREEN,
            params: {
              metaFile,
              hideDownload: true,
            },
          },
        })
      );
    } catch (err) {
      logService.error(err);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

function createPreviewAgbText(logService: LogService) {
  return function* (action: DispatchAction<Account>) {
    const accessToken = yield select(
      (state: RootState) => state.esy.auth?.session?.accessToken
    );
    const isValidToken = checkTokenIsValid(accessToken);
    yield put(showLoadingAction());

    try {
      const metaFile: VirtualMetaFile = {
        id: null,
        accountId: action.payload.id,
        fileName: "AGB.txt",
        filePath: null,
        contentType: "text/agb",
        size: action.payload.esyThingAgb.length,
        extension: "txt",
        content: action.payload.esyThingAgb,
      };

      yield put(
        navigateToAction({
          navigateTo: {
            stack: isValidToken ? STACKS.HOME_STACK : STACKS.PUBLIC_STACK,
            screen: isValidToken
              ? HOME_STACK.PREVIEW_SCREEN
              : PUBLIC_STACK.PREVIEW_SCREEN,
            params: {
              metaFile,
              hideDownload: true,
            },
          },
        })
      );
    } catch (err) {
      logService.error(err);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startPreviewAgb(logService: LogService) {
  logService.info("start preview Agb saga");
  yield all([
    takeLatest(
      ACCOUNT_ACTIONS.ACCOUNT_PREVIEW_AGB,
      createPreviewAgb(logService)
    ),
    takeLatest(
      ACCOUNT_ACTIONS.ACCOUNT_PREVIEW_AGB_TEXT,
      createPreviewAgbText(logService)
    ),
  ]);
}
