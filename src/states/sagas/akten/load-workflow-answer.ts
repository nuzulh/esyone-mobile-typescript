import { call, put, select, takeLeading } from "redux-saga/effects";
import {
  hideLoadingAction,
  HOME_STACK,
  navigateToAction,
  showLoadingAction,
  WORKFLOW_ACTIONS,
} from "../../../helpers";
import {
  Account,
  DispatchAction,
  RootState,
  WorkflowAnswer,
  WorkflowProcess,
} from "../../../models";
import { AccountsService, AufgabeService, LogService } from "../../../services";

function createLoadWorkflowAnswer(
  logService: LogService,
  aufgabeService: AufgabeService
) {
  return function* (
    action: DispatchAction<{ workflowId: string; account: Account }>
  ) {
    const { workflowId, account } = action.payload;
    logService.debug(`load workflow answer ${workflowId}`);

    const accessToken = yield select(
      (state: RootState) => state.esy.auth.session.accessToken
    );

    yield put(showLoadingAction());
    try {
      const workflowProcess: WorkflowProcess = yield call(
        aufgabeService.getWorkflow,
        workflowId,
        accessToken
      );
      const workflowAnswers: WorkflowAnswer[] = yield call(
        aufgabeService.getWorkflowAnswers,
        workflowId,
        workflowProcess.akteId,
        account.id,
        accessToken
      );

      workflowProcess.answers = workflowAnswers;

      // console.log("workflowAnswers", workflowAnswers);
      yield put(
        navigateToAction({
          navigateTo: {
            screen: HOME_STACK.FORMULA_SCREEN_ANSWER,
            params: {
              workflowProcess,
              account,
            },
          },
        })
      );
    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startLoadWorkflowAnswer(
  logService: LogService,
  aufgabeService: AufgabeService
) {
  logService.debug("start load workflow answer saga");

  yield takeLeading(
    WORKFLOW_ACTIONS.WORKFLOW_LOAD_ANSWER,
    createLoadWorkflowAnswer(logService, aufgabeService)
  );
}
