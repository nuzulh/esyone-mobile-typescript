import { call, put, select, takeLatest } from "redux-saga/effects";
import {
  ACCOUNT_ACTIONS,
  checkTokenIsValid,
  filterNullAndEmpty,
  hideLoadingAction,
  HOME_STACK,
  navigateToAction,
  PUBLIC_STACK,
  showLoadingAction,
  STACKS,
} from "../../../helpers";
import { Account, DispatchAction, ImageSpec, RootState } from "../../../models";
import { AccountsService, LogService } from "../../../services";

function createPreviewAccount(
  logService: LogService,
  accountsService: AccountsService
) {
  return function* (action: DispatchAction<Account>) {
    const accessToken = yield select(
      (state: RootState) => state.esy.auth?.session?.accessToken
    );
    const isValidToken = checkTokenIsValid(accessToken);
    logService.debug(`preview account ${action.payload.name}`);

    yield put(showLoadingAction());
    try {
      // load public info
      const account: Account = yield call(
        accountsService.getPublicInfo,
        action.payload.id
      );

      // const mainLogo:ImageSpec = yield call(accountsService.getMainLogo, account.id);
      // const secondaryLogo:ImageSpec = yield call(accountsService.getSecondaryLogo, account.id);

      // // download main logo
      // const mainLogoUrl = yield call(RNF)

      yield put(
        navigateToAction({
          navigateTo: {
            stack: isValidToken ? STACKS.HOME_STACK : STACKS.PUBLIC_STACK,
            screen: isValidToken
              ? HOME_STACK.PROVIDER_SCREEN
              : PUBLIC_STACK.PROVIDER_SCREEN,
            params: {
              account: {
                ...action.payload,
                ...filterNullAndEmpty<any, any>(account),
              },
            },
          },
        })
      );
    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startPreviewAccount(
  logService: LogService,
  accountsService: AccountsService
) {
  logService.debug("start preview account");

  yield takeLatest(
    ACCOUNT_ACTIONS.ACCOUNT_PREVIEW,
    createPreviewAccount(logService, accountsService)
  );
}
