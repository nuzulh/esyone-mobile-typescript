import { Akte, DispatchAction, Message, RootState } from "../../../models";
import { AktenService, LogService } from "../../../services";
import { call, put, select, takeLeading } from "redux-saga/effects";
import {
  AKTE_ACTIONS,
  fetchSingleAkteAction,
  hideLoadingAction,
  showLoadingAction,
  updateSingleAkteAction,
} from "../../../helpers";

function createReadMessage(
  logService: LogService,
  aktenService: AktenService,
) {
  return function* (
    action: DispatchAction<{ akte: Akte; message: Message; }>
  ) {
    const { akte, message } = action.payload;
    const account = yield select(
      (state: RootState) => state.esy.auth.accounts[akte.accountId]
    );
    logService.debug(`read message ${message.id} from ${message.creatorName}`);

    yield put(showLoadingAction());
    try {
      const readResponse = yield call(
        aktenService.postReadMessage,
        akte.accountId,
        message,
      );

      akte.messages?.forEach((m) => {
        if (m.id === message.id) m.read = readResponse.isRead;
      });
      akte.messages = [...(akte.messages || [])];
      akte.totalUnreadMessage -= 1;

      yield put(updateSingleAkteAction({ ...akte }));
      yield put(fetchSingleAkteAction(account, akte));

    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startReadMessage(
  logService: LogService,
  aktenService: AktenService,
) {
  logService.debug("start read message saga");
  yield takeLeading(
    AKTE_ACTIONS.AKTE_READ_MESSAGE,
    createReadMessage(logService, aktenService)
  );
}
