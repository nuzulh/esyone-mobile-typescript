import { call, put, takeEvery } from "redux-saga/effects";
import {
  AKTE_ACTIONS,
  fetchSingleAkteAction,
  hideLoadingAction,
  navigateToAction,
  showLoadingAction,
  updateSingleAkteAction,
} from "../../../helpers";
import {
  Account,
  Akte,
  DispatchAction,
  Message,
  MessageAttachment,
} from "../../../models";
import {
  AktenService,
  FilesService,
  FileUploadResult,
  LogService,
  MetaFile,
} from "../../../services";
import { Alert } from "react-native";

function createSubmitMessage(
  logService: LogService,
  filesService: FilesService,
  aktenService: AktenService
) {
  return function* (
    action: DispatchAction<{
      message: Message;
      account: Account;
      akte: Akte;
      metaFiles?: MetaFile[];
    }>
  ) {
    const { message, account, akte, metaFiles } = action.payload;
    logService.debug(`send message to ${account.name} of ${message.header}`);

    yield put(showLoadingAction());
    try {
      // upload files if exists
      if (metaFiles && metaFiles.length > 0) {
        const uploadResponse: FileUploadResult[] = yield call(
          filesService.uploadFiles,
          account.id,
          metaFiles
        );
        // console.log(metaFiles.map((metaFile) => metaFile.fileName));
        // console.log(uploadResponse);

        message.attachments = uploadResponse.map(
          (result) =>
          ({
            id: result.uploadId,
            creationDate: new Date(),
            fileName: result.name,
            metaFile: metaFiles.find((f) => f.fileName === result.name),
          } as MessageAttachment)
        );
      }

      // send message
      const messageResponse = yield call(
        aktenService.postMessage,
        account.id,
        akte.id,
        message
      );

      message.id = messageResponse.Id;

      message.attachments?.forEach((attachment) => {
        attachment.messageId = message.id;
      });

      // console.log(messageResponse);

      akte.messages = [...(akte.messages || []), message];

      // update akte directly
      yield put(updateSingleAkteAction(akte));

      // update akte
      yield put(fetchSingleAkteAction(account, akte));

      // show responder message if activated
      if (account.autoResponderSetting.isActivated)
        Alert.alert("", account.autoResponderSetting.message);

      yield put(
        navigateToAction({
          goBack: true,
        })
      );
    } catch (error) {
      logService.error(error);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startSubmitMessageSaga(
  logService: LogService,
  filesService: FilesService,
  aktenService: AktenService
) {
  logService.debug("start submit message saga");
  yield takeEvery(
    AKTE_ACTIONS.AKTE_SUBMIT_MESSAGE,
    createSubmitMessage(logService, filesService, aktenService)
  );
}
