import { call, put, takeLeading } from "redux-saga/effects";
import {
  AKTE_ACTIONS,
  hideLoadingAction,
  showLoadingAction,
  updateSingleAkteAction,
} from "../../../helpers";
import { Akte, DispatchAction, Message } from "../../../models";
import { AktenService, LogService } from "../../../services";

function createToggleFavoriteMessage(
  logService: LogService,
  aktenService: AktenService
) {
  return function* (action: DispatchAction<{ akte: Akte; message: Message }>) {
    const { akte, message } = action.payload;
    logService.debug(`toggle favorite message ${message.id}...`);

    yield put(showLoadingAction());
    try {
      let response: any;
      if (message.isFavorite)
        response = yield call(
          aktenService.deleteFavoriteMessage,
          akte.accountId,
          message
        );
      else
        response = yield call(
          aktenService.postFavoriteMessage,
          akte.accountId,
          message
        );

      message.isFavorite = !message.isFavorite;

      akte.messages?.forEach((m) => {
        if (m.id === message.id) m.isFavorite = message.isFavorite;
      });

      yield put(updateSingleAkteAction(akte));

      // console.log(response);
    } catch (err) {
      logService.error(err);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export function* startToggleFavoriteMessage(
  logService: LogService,
  aktenService: AktenService
) {
  logService.debug("start toggle favorite message saga...");

  yield takeLeading(
    AKTE_ACTIONS.AKTE_TOGGLE_FAVORITE_MESSAGE,
    createToggleFavoriteMessage(logService, aktenService)
  );
}
