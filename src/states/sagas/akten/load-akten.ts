import {
  all,
  call,
  put,
  select,
  takeLatest,
  takeLeading,
} from "redux-saga/effects";
import {
  AKTE_ACTIONS,
  hideLoadingBackgroundAction,
  showLoadingBackgroundAction,
  updateAkteFetchStatus,
  updateAllAkteAction,
} from "../../../helpers";
import { ResourceData } from "../../../hooks";
import { Akte, AktenState, RootState } from "../../../models";
import {
  AktenService,
  AufgabeService,
  GetMessagesResult,
  LogService,
} from "../../../services";

function createLoadAkten(
  logService: LogService,
  aktenService: AktenService,
  aufgabeService: AufgabeService
) {
  // const loadSingleAkte = async function (accountId: number, akteId: string) {
  //   logService.debug(`load akte ${accountId} ${akteId}`);
  //   const akte: Akte = await aktenService.getOne(accountId, akteId);

  //   // get phases history
  //   const phases = await aktenService
  //     .getHistory(akte.accountId, akte.id)
  //     .catch(() => []);
  //   akte.phases = phases || [];
  //   // get messages
  //   const messagesResult: GetMessagesResult = await aktenService
  //     .getMessages(akte.accountId, akte.id, { size: 100000 })
  //     .catch(() => ({
  //       attachments: [],
  //       folders: [],
  //       messages: [],
  //       totalMessage: 0,
  //     }));

  //   akte.messages = messagesResult.messages || [];
  //   akte.attachments = messagesResult.attachments || [];
  //   akte.children = [];

  //   for (let folder of messagesResult.folders || []) {
  //     const child = await loadSingleAkte(akte.accountId, folder.id).catch(
  //       () => null
  //     );
  //     if (child) akte.children.push(child);
  //   }

  //   // load aufgaben
  //   akte.aufgaben = await aufgabeService
  //     .getAll(akte.accountId, akte.id)
  //     .catch(() => []);

  //   return akte;
  // };

  return function* (action: { type: string }) {
    // if (action.type === AKTE_ACTIONS.AKTE_FETCH_ALL) {
    //   const aktenState: AktenState = yield select(
    //     (state: RootState) => state.esy.akten
    //   );

    //   // console.log(aktenState.lastUpdated);

    //   if (Date.now() < aktenState.lastUpdated + 1000 * 60)
    //     return logService.debug("akten are up to date");
    // }

    logService.debug("load all akte");

    yield put(updateAkteFetchStatus(true));
    yield put(showLoadingBackgroundAction());
    try {
      const akten: ResourceData<Akte> = yield call(aktenService.getAll, {
        size: 100000,
      });

      // console.log("akten", JSON.stringify(akten, null, 2));

      // for (let key in akten) {
      //   let akte = akten[key];
      //   akte = yield call(loadSingleAkte, akte.accountId, akte.id);

      //   akten[key] = akte;
      // }

      yield put(updateAllAkteAction(akten));
    } catch (err) {
      // console.error(err.message);
      logService.error(err);
    } finally {
      yield put(updateAkteFetchStatus(false));
      yield put(hideLoadingBackgroundAction());
    }
  };
}

export function* startLoadAkten(
  logService: LogService,
  aktenService: AktenService,
  aufgabeService: AufgabeService
) {
  yield all([
    takeLeading(
      AKTE_ACTIONS.AKTE_FETCH_ALL,
      createLoadAkten(logService, aktenService, aufgabeService)
    ),
    takeLeading(
      AKTE_ACTIONS.AKTE_FETCH_ALL_FORCE,
      createLoadAkten(logService, aktenService, aufgabeService)
    ),
  ]);
}
