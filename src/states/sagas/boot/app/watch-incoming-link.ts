import { Linking } from "react-native";
import { eventChannel } from "redux-saga";
import { call, put, take } from "redux-saga/effects";
import {
  APP_STATE_ACTIONS,
  resetLinkingAction,
  updateLinkingAction,
} from "../../../../helpers";
import { LogService } from "../../../../services";

function createLinkChannel() {
  return eventChannel((emit) => {
    const subscription = Linking.addEventListener("url", ({ url }) =>
      emit(url)
    );
    return () => subscription.remove();
  });
}

export function* watchIncomingLink(logService: LogService) {
  yield take(APP_STATE_ACTIONS.APP_STATE_READY);

  logService.debug("start watching incoming links");

  let url = yield call(Linking.getInitialURL);

  if (url) yield put(updateLinkingAction(url));

  const chan = yield call(createLinkChannel);

  try {
    while (true) {
      url = yield take(chan);
      logService.debug(`handle external linking ${url}`);

      yield put(updateLinkingAction(url));
    }
  } catch (err) {
    yield put(resetLinkingAction());
    logService.error(err);
  }
}
