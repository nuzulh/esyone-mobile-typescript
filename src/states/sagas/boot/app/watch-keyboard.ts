import { Keyboard } from "react-native";
import { eventChannel } from "redux-saga";
import { call, put, take } from "redux-saga/effects";
import { updateKeyboardAction } from "../../../../helpers";
import { LogService } from "../../../../services";

function createKeyboardChannel() {
  return eventChannel((emit) => {
    const closeShowListener = Keyboard.addListener("keyboardDidShow", () => {
      emit(true);
      // setKeyboardVisible(true); // or some other action
    });

    const closeHideListener = Keyboard.addListener("keyboardDidHide", () => {
      emit(false);
      // setKeyboardVisible(false); // or some other action
    });

    return () => {
      closeShowListener.remove();
      closeHideListener.remove();
    };
  });
}

export function* watchKeyboard(logService: LogService) {
  const chan = yield call(createKeyboardChannel);

  try {
    logService.debug("start watching keyboard");
    while (true) {
      const isVisible = yield take(chan);
      yield put(updateKeyboardAction(isVisible));
    }
  } catch (err) {
    logService.error(err);
  }
}
