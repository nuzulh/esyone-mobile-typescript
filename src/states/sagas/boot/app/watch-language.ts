import { call, takeLatest } from "redux-saga/effects";
import { SETTINGS_ACTIONS } from "../../../../helpers";
import { DispatchAction, SettingsState } from "../../../../models";
import { LogService } from "../../../../services";
import localization from "../../../../helpers/localization";

function createLanguageHandler(logService: LogService) {
  return function* (action: DispatchAction<Partial<SettingsState>>) {
    if (!action.payload.language) return;

    yield call(
      { context: localization.language, fn: localization.language.setLanguage },
      action.payload.language
    );
    logService.debug(`update language to ${action.payload.language}`);
  };
}

export function* watchLanguageUpdate(logService: LogService) {
  logService.debug("start watching language update");
  yield takeLatest(
    SETTINGS_ACTIONS.MUTATION.SETTINGS_UPDATE,
    createLanguageHandler(logService)
  );
}
