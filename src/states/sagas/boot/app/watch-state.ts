import { eventChannel } from "redux-saga";
import { Alert, AppState } from "react-native";
import { call, put, select, take } from "redux-saga/effects";
import { LogService } from "../../../../services";
import {
  APP_STATE_ACTIONS,
  authLogoutAction,
  checkTokenIsValid,
  resetAppStateAction,
  updateAppStateAction,
} from "../../../../helpers";
import { navigationRef } from "../../../../components";
import { RootState } from "../../../../models";

function createAppStateChannel() {
  return eventChannel((emit) => {
    emit(AppState.currentState);

    const subs = AppState.addEventListener("change", (newAppState) =>
      emit(newAppState)
    );

    return () => subs.remove();
  });
}

export function* watchAppState(logService: LogService) {
  const chan = yield call(createAppStateChannel);
  let prevState: string;

  // do checking after boot
  yield take(APP_STATE_ACTIONS.APP_STATE_READY);

  try {
    logService.debug("start watching app state");
    while (true) {
      const state = yield take(chan);
      logService.debug(`Update app state to ${state}`);

      if (state === "active" && prevState === "background") {
        // check access token
        const accessToken = yield select(
          (state: RootState) => state.esy.auth?.session?.accessToken
        );
        if (accessToken && !checkTokenIsValid(accessToken))
          yield put(authLogoutAction());
      }
      /* yield put({
        type: APP_STATE_ACTIONS.MUTATION.APP_STATE_UPDATE,
        payload: state,
      }); */

      // console.log("current route", navigationRef.current?.getCurrentRoute());

      // if (state === "active" && !navigationRef.current?.getCurrentRoute())
      // Alert.alert("No navigation ref", "No navigation ref");
      prevState = state;
      yield put(updateAppStateAction({ currentState: state }));
    }
  } catch (err) {
    logService.error(err);
  } finally {
    yield put(resetAppStateAction());
  }
}
