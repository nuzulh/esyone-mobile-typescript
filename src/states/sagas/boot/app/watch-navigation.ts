import { CommonActions, StackActions } from "@react-navigation/native";
import {
  delay,
  put,
  select,
  takeLatest,
  takeLeading,
} from "redux-saga/effects";
import { navigationRef } from "../../../../components";
import {
  authLogoutAction,
  AUTH_STACK,
  checkTokenIsValid,
  NAVIGATION_ACTIONS,
  serialize,
} from "../../../../helpers";
import {
  DispatchAction,
  NavigationTarget,
  RootState,
} from "../../../../models";
import { LogService } from "../../../../services";

function createNavigationHandler(logService: LogService) {
  return function* (action: DispatchAction<NavigationTarget>) {
    if (!navigationRef.current) {
      logService.debug(
        "cannot handle incoming navigation because no navigation ref"
      );

      return;
    }

    // check session
    const accessToken = yield select(
      (state: RootState) => state.esy.auth?.session?.accessToken
    );
    if (accessToken && !checkTokenIsValid(accessToken)) {
      yield put(authLogoutAction());
      return;
    }

    if (action.payload.goBack) {
      logService.debug("handle incoming navigation go back");
      navigationRef.goBack();
      return;
    }

    if (Array.isArray(action.payload.navigateTo)) {
      if (action.payload.reset) {
        navigationRef.dispatch(
          CommonActions.reset({
            index: action.payload.navigateTo.length - 1,
            routes: action.payload.navigateTo.map((route) => ({
              name: route.screen,
              params: route.params,
            })),
          })
        );
      }

      return;
    }

    let { stack, screen, params } = action.payload.navigateTo;
    params = params ? serialize(params) : params;

    logService.debug(`handle incoming navigation ${stack} ${screen}`);
    // console.log("navigation state", navigationRef.getState());
    if (action.payload.replace) {
      navigationRef.dispatch(StackActions.replace(screen, params));
      return;
    }

    if (stack) navigationRef.navigate(stack, { screen, params });
    else navigationRef.dispatch(StackActions.push(screen, params));

    // navigationRef.navigate(
    //   stack || screen,
    //   stack && screen ? { screen } : params,
    //   stack && screen ? params : null
    // );
  };
}

export function* watchNavigation(logService: LogService) {
  logService.debug("start watching navigation update");
  yield takeLeading(
    NAVIGATION_ACTIONS.MUTATION.NAVIGATION_UPDATE,
    createNavigationHandler(logService)
  );
}
