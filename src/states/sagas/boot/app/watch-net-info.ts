import NetInfo, { NetInfoState } from "@react-native-community/netinfo";
import { eventChannel } from "redux-saga";
import { call, put, take } from "redux-saga/effects";
import { updateAppStateAction } from "../../../../helpers";
import { LogService } from "../../../../services";

function createNetInfoChannel() {
  return eventChannel((emit) => {
    const close = NetInfo.addEventListener((state) => emit(state));

    return () => close();
  });
}

export function* watchNetInfo(logService: LogService) {
  const chan = yield call(createNetInfoChannel);

  try {
    logService.debug("start watching net info");
    while (true) {
      const info: NetInfoState = yield take(chan);
      logService.debug(
        `update offline status to ${!(
          info.isConnected && info.isInternetReachable
        )}`
      );

      yield put(
        updateAppStateAction({
          isOffline: !(info.isConnected && info.isInternetReachable),
        })
      );
    }
  } catch (err) {
    logService.error(err);
  }
}
