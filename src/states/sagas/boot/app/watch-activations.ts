import { put, select, takeLatest } from "redux-saga/effects";
import {
  ACTIVATION_STACK,
  authLoadProfileAction,
  enableFcmAction,
  fetchAllAkteAction,
  HOME_STACK,
  navigateToAction,
  SETTINGS_ACTIONS,
  STACKS,
} from "../../../../helpers";
import { RootState, SettingsState } from "../../../../models";
import { LogService } from "../../../../services";

function createActivationHandler(logService: LogService) {
  return function* () {
    const settings: SettingsState = yield select(
      (state: RootState) => state.esy.settings
    );

    // check biometrics
    if (!settings.isBiometricAsked) {
      logService.debug("start biometric activation");

      yield put(
        navigateToAction({
          navigateTo: {
            stack: STACKS.ACTIVATION_STACK,
            screen: ACTIVATION_STACK.BIOMETRIC_SCREEN,
          },
        })
      );

      return;
    }

    if (!settings.isPushNotificationAsked) {
      logService.debug("start push notification activation");

      yield put(
        navigateToAction({
          navigateTo: {
            stack: STACKS.ACTIVATION_STACK,
            screen: ACTIVATION_STACK.PUSHNOTIF_SCREEN,
          },
        })
      );

      return;
    }

    // check if fcm should be enabled
    if (settings?.isPushNotificationEnabled) yield put(enableFcmAction());

    // console.log(settings);
    yield put(fetchAllAkteAction());
    yield put(
      navigateToAction({
        navigateTo: {
          stack: STACKS.HOME_STACK,
          screen: HOME_STACK.HOME_SCREEN,
        },
      })
    );

    yield put(authLoadProfileAction());
  };
}

export function* watchActivation(logService: LogService) {
  logService.debug("start watch activation");

  yield takeLatest(
    SETTINGS_ACTIONS.SETTINGS_START_ACTIVATIONS,
    createActivationHandler(logService)
  );
}
