import { all } from "redux-saga/effects";
import { LogService } from "../../../../services";
import { watchLanguageUpdate } from "./watch-language";
import { watchIncomingLink } from "./watch-incoming-link";
import { watchNavigation } from "./watch-navigation";
import { watchAppState } from "./watch-state";
import { watchNetInfo } from "./watch-net-info";
import { watchKeyboard } from "./watch-keyboard";
import { watchActivation } from "./watch-activations";

export function* startAppSagas(logService: LogService) {
  logService.debug("starting app sagas");
  yield all([
    watchIncomingLink(logService),
    watchAppState(logService),
    watchNavigation(logService),
    watchLanguageUpdate(logService),
    watchNetInfo(logService),
    watchKeyboard(logService),
    watchActivation(logService),
  ]);
}
