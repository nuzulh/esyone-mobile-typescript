import { call, delay, put, select, takeLeading } from "redux-saga/effects";
import {
  appReadyAction,
  AUTH_STACK,
  BOOT_ACTIONS,
  checkTokenIsValid,
  navigateToAction,
  setCameraPermissionIsAskedAction,
  setCameraPermissionIsGrantedAction,
  STACKS,
  startActivations,
  updateAppStateAction,
  updateBiometricSupport,
  updateLanguageAction,
} from "../../../helpers";
import { fetchProfileAction } from "../../../helpers/actions/account-actions";
import {
  askFcmPermissionAction,
  enableFcmAction,
  setPermissionIsAskedAction,
} from "../../../helpers/actions/fcm-actions";
import {
  ApplicationState,
  AuthState,
  FCMState,
  NativeState,
  RootState,
  SettingsState,
} from "../../../models";
import {
  EvaluationResult,
  GuardService,
  LogService,
  PermissionService,
  TouchIdservice,
} from "../../../services";

export function createBootInit(
  logService: LogService,
  guardService: GuardService,
  permissionService: PermissionService,
  touchIdService: TouchIdservice
) {
  return function* () {
    logService.debug("initiate boot application...");
    // const currentState = yield select();
    // logService.debug("get current state " + JSON.stringify(currentState));
    /* const appState: ApplicationState = yield select(
    (state: RootState) => state.esy.appState
  ); */
    const authStatus: AuthState = yield select(
      (state: RootState) => state.esy.auth
    );
    const appSettings: SettingsState = yield select(
      (state: RootState) => state.esy.settings
    );
    const fcmState: FCMState = yield select(
      (state: RootState) => state.esy.fcm
    );
    const nativeState: NativeState = yield select(
      (state: RootState) => state.esy.nativeState
    );

    // set default language
    yield put(updateLanguageAction(appSettings.language));

    // check if offline login is supported
    const biometricType = yield call(touchIdService.isSupported);
    if (biometricType) yield put(updateBiometricSupport(true, biometricType));
    else yield put(updateBiometricSupport(false, null));

    // ask notification permission
    if (!fcmState.isPermissionAsked) {
      yield delay(1000);
      // const isAllowed = yield call(
      //   permissionService.askPushNotificationPermission
      // );

      // if (isAllowed) yield put(askFcmPermissionAction());

      yield put(askFcmPermissionAction());
      yield put(setPermissionIsAskedAction());
    }

    if (!nativeState.isCameraPermissionAsked) {
      yield delay(1000);
      const isAllowed = yield call(permissionService.askCameraPermission);

      if (isAllowed) yield put(setCameraPermissionIsGrantedAction(true));

      yield put(setCameraPermissionIsAskedAction(true));
    }

    // check if app is ready
    // if (appState.currentFlow === "ready") return;

    // console.log(checkTokenIsValid(authStatus?.session?.accessToken));
    // check if user has logged in
    if (
      !authStatus ||
      !authStatus.session ||
      !authStatus.session.accessToken ||
      !checkTokenIsValid(authStatus?.session?.accessToken)
    ) {
      const evaluationResult: EvaluationResult = yield call(
        guardService.evaluate
      );
      /* console.error(
      "evaluation result",
      checkTokenIsValid(authStatus?.session?.accessToken),
      authStatus?.session?.accessToken,
      evaluationResult
    ); */
      yield delay(1000);
      yield put(evaluationResult);

      /* return yield put(
      updateAppStateAction({
        currentFlow: "ready",
        isLoading: false,
      })
    ); */
      // return;
    } else {
      yield delay(1000);
      yield put(startActivations());
      // yield put(
      //   navigateToAction({
      //     navigateTo: {
      //       stack: STACKS.AUTH_STACK,
      //       screen: AUTH_STACK.BIOMETRIC_SCREEN,
      //     },
      //   })
      // );
    }

    // get current user profile
    // yield put(fetchProfileAction(authStatus.id));

    // make application ready
    yield put(
      updateAppStateAction({
        currentFlow: "ready",
        isLoading: false,
        toastRef: global["toast"],
      })
    );

    yield put(appReadyAction());
  };
}

export function* startBootInit(
  logService: LogService,
  guardService: GuardService,
  permissionService: PermissionService,
  touchIdService: TouchIdservice
) {
  yield takeLeading(
    BOOT_ACTIONS.BOOT_START,
    createBootInit(logService, guardService, permissionService, touchIdService)
  );
}
