import { LocalizedStringsMethods } from "react-native-localization";
import { all } from "redux-saga/effects";
import {
  EconsultService,
  GuardService,
  LogService,
  PermissionService,
  TouchIdservice,
} from "../../../services";
import { startAppSagas } from "./app";
import { startBootInit } from "./boot-init";
import { startFcmSagas } from "./fcm";

export function* startBootSagas(
  logService: LogService,
  econsultService: EconsultService,
  guardService: GuardService,
  permissionService: PermissionService,
  touchIdService: TouchIdservice
) {
  logService.debug("starting boot sagas");
  yield all([
    startAppSagas(logService),
    startFcmSagas(logService, econsultService),
    startBootInit(logService, guardService, permissionService, touchIdService),
  ]);
}
