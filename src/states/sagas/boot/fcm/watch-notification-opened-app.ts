import { eventChannel } from "redux-saga";
import messaging, {
  FirebaseMessagingTypes,
} from "@react-native-firebase/messaging";
import { call, put, take } from "redux-saga/effects";
import { LogService } from "../../../../services";
import { APP_STATE_ACTIONS, FCM_ACTIONS, handleMessage } from "../../../../helpers";

function createNotificationChannel() {
  const messageApp = messaging();

  return eventChannel((emit) => {
    messageApp.getInitialNotification().then((message) =>
      message && emit(message)
    );
    const close = messageApp.onNotificationOpenedApp((message) =>
      emit(message)
      // console.log("notification opened app", message)
    );

    return () => close();
  });
}

export default function* watchNotificationOpenedApp(logService: LogService) {
  yield take(APP_STATE_ACTIONS.APP_STATE_READY);
  const chan = yield call(createNotificationChannel);

  try {
    while (true) {
      const message: FirebaseMessagingTypes.RemoteMessage = yield take(chan);
      logService.debug(`(opened app notification) ${JSON.stringify(message)}`);

      yield put(handleMessage(message));
      yield put({ type: FCM_ACTIONS.EVENT.FCM_OPENED_APP, payload: message });
    }
  } catch (err) {
    logService.error(err);
  }
}
