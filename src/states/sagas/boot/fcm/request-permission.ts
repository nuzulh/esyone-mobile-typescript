import { call, put, takeLatest } from "redux-saga/effects";
import messaging from "@react-native-firebase/messaging";

import { FCM_ACTIONS } from "../../../../helpers/consts";
import {
  permissionDisabledAction,
  permissionEnabledAction,
} from "../../../../helpers";
import { LogService } from "../../../../services";

function createRequestPermissionsHandler(logService: LogService) {
  return function* () {
    try {
      const fcm = messaging();
      const result = yield call([fcm, fcm.requestPermission]);
      const enabled =
        result === messaging.AuthorizationStatus.AUTHORIZED ||
        messaging.AuthorizationStatus.PROVISIONAL;

      if (enabled) yield put(permissionEnabledAction());
      else yield put(permissionDisabledAction());
    } catch (err) {
      logService.error(err);
    }
  };
}

// on boot do this once
export default function* requestPermissions(logService: LogService) {
  yield takeLatest(
    FCM_ACTIONS.FCM_INIT_PERMISSIONS,
    createRequestPermissionsHandler(logService)
  );

  /* const result = yield call(messaging().requestPermission);
  const enabled =
    result === messaging.AuthorizationStatus.AUTHORIZED ||
    messaging.AuthorizationStatus.PROVISIONAL;

  if (enabled) yield put(permissionEnabledAction());
  else yield put(permissionDisabledAction()); */
}
