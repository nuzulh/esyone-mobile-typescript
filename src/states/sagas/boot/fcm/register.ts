import { all, call, put, select, take } from "redux-saga/effects";
import messaging from "@react-native-firebase/messaging";
import { AUTH_ACTIONS, FCM_ACTIONS } from "../../../../helpers/consts";
import { EconsultService } from "../../../../services/api/econsult/econsult.service";
import { LogService } from "../../../../services";
import {
  AuthState,
  DispatchAction,
  RootState,
  SettingsState,
} from "../../../../models";
import { eventChannel } from "redux-saga";
import {
  hideLoadingAction,
  registerFailAction,
  registerSuccessAction,
  showLoadingAction,
  updatePushNotificationSupport,
  updateTokenAction,
} from "../../../../helpers";

function createTokenRefreshChannel() {
  const messageApp = messaging();

  return eventChannel((emit) => {
    const close = messageApp.onTokenRefresh((token) => emit(token));

    return () => close();
  });
}

function createRegisterHandler(
  logService: LogService,
  econsultService: EconsultService
) {
  const messageApp = messaging();

  return function* (authStatus: AuthState) {
    try {
      if (!messageApp.isDeviceRegisteredForRemoteMessages)
        yield call(messageApp.registerDeviceForRemoteMessages);

      const token = yield messageApp.getToken();

      const result = yield call(
        econsultService.postNotificationToken,
        authStatus.profile.accountId,
        authStatus.profile.userId,
        token
      );

      if (!(result.status == 200 || result.status == 204))
        throw new Error(
          "fcm token registration failed for unknown reason, check backend message"
        );

      yield put({ type: FCM_ACTIONS.RESULT.FCM_REGISTER_SUCCEED });
      yield put(registerSuccessAction());
      yield put(updateTokenAction(token));
    } catch (error) {
      yield put({ type: FCM_ACTIONS.RESULT.FCM_REGISTER_FAILED });
      yield put(registerFailAction());
      logService.error(error);
    }
  };
}

function createUnregisterHandler(
  logService: LogService,
  econsultService: EconsultService
) {
  const messageApp = messaging();

  return function* () {
    const authState = yield select((state: RootState) => state.esy.auth);

    try {
      if (!messageApp.isDeviceRegisteredForRemoteMessages) return;

      const token = yield messageApp.getToken();

      if (!token) return;

      yield call(
        econsultService.deleteNotificationToken,
        authState.profile.accountId,
        authState.profile.userId,
        token
      );

      // yield messageApp.unregisterDeviceForRemoteMessages();

      // yield put(registerFailAction());
    } catch (error) {
      // console.log("error disini", error.message);
      logService.error(error);
    }
  };
}

function* watchAuthSucceed(
  logService: LogService,
  econsultService: EconsultService
) {
  const register = createRegisterHandler(logService, econsultService);

  try {
    while (true) {
      const authAction: DispatchAction<AuthState> = yield take(
        AUTH_ACTIONS.RESULT.AUTH_SUCCEED
      );
      const settings: SettingsState = yield select(
        (state: RootState) => state.esy.settings
      );

      if (settings.isPushNotificationEnabled)
        yield call(register, authAction.payload);
    }
  } catch (err) {
    logService.error(err);
  }
}

function* watchFcmEnabled(
  logService: LogService,
  econsultService: EconsultService
) {
  const register = createRegisterHandler(logService, econsultService);

  try {
    while (true) {
      yield take(FCM_ACTIONS.FCM_REGISTER);
      // logService.info("fcm enabled");

      yield put(updatePushNotificationSupport(true));
      const authStatus = yield select((state: RootState) => state.esy.auth);

      yield put(showLoadingAction());
      try {
        yield call(register, authStatus);
      } catch (error) {
        logService.error(error);
      } finally {
        yield put(hideLoadingAction());
      }
    }
  } catch (err) {
    logService.error(err);
  }
}

function* watchFcmDisabled(
  logService: LogService,
  econsultService: EconsultService
) {
  const unregister = createUnregisterHandler(logService, econsultService);

  try {
    while (true) {
      yield take(FCM_ACTIONS.FCM_UNREGISTER);
      // logService.info("fcm disabled");

      yield put(updatePushNotificationSupport(false));

      yield put(showLoadingAction());
      try {
        yield call(unregister);
      } catch (error) {
        logService.error(error);
      } finally {
        yield put(hideLoadingAction());
      }
    }
  } catch (err) {
    logService.error(err);
  }
}

function* watchTokenUpdate(logService: LogService) {
  const chan = yield call(createTokenRefreshChannel);

  try {
    while (true) {
      const token = yield take(chan);
      yield put(updateTokenAction(token));
    }
  } catch (err) {
    logService.error(err);
  }
}

export default function* register(
  logService: LogService,
  econsultService: EconsultService
) {
  yield all([
    watchAuthSucceed(logService, econsultService),
    watchFcmEnabled(logService, econsultService),
    watchFcmDisabled(logService, econsultService),
    watchTokenUpdate(logService),
  ]);
}
