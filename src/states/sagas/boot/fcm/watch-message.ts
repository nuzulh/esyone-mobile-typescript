import { eventChannel } from "redux-saga";
import messaging, {
  FirebaseMessagingTypes,
} from "@react-native-firebase/messaging";
import { call, put, select, take } from "redux-saga/effects";
import { APP_STATE_ACTIONS, FCM_ACTIONS } from "../../../../helpers/consts";
import { LogService } from "../../../../services";
import { handleMessage, localization } from "../../../../helpers";
import { Alert } from "react-native";
import { RootState } from "../../../../models";

function createNotificationChannel() {
  const messageApp = messaging();

  return eventChannel((emit) => {
    messageApp.setBackgroundMessageHandler(async (message) =>
    // console.log("background message", message)
    // emit(message)
    {}
    );
    const close = messageApp.onMessage(async (message) => emit(message));

    return () => close();
  });
}

export default function* watchNotification(logService: LogService) {
  yield take(APP_STATE_ACTIONS.APP_STATE_READY);
  const chan = yield call(createNotificationChannel);

  try {
    while (true) {
      const message: FirebaseMessagingTypes.RemoteMessage = yield take(chan);
      const appState = yield select(
        (state: RootState) => state.esy.appState.currentState
      );

      if (appState === "active") {
        const toastRef = yield select(
          (state: RootState) => state.esy.appState.toastRef
        );
        if (toastRef) toastRef.show(
          localization.language.Notification_ForegroundNew
        );
        return;
      }
      // console.log(message);
      // Alert.alert(
      //   localization.language.ScreenProfile_PushNotification,
      //   `${message.notification.title} - ${message.notification.body}`
      // );
      yield put(handleMessage(message));
      yield put({ type: FCM_ACTIONS.EVENT.FCM_NEW_MESSAGE, payload: message });
    }
  } catch (err) {
    logService.error(err);
  }
}
