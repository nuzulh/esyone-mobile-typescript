import { FirebaseMessagingTypes } from "@react-native-firebase/messaging";
import { Account, Akte, DispatchAction, RootState } from "../../../../models";
import { LogService } from "../../../../services";
import { put, select, takeLeading } from "redux-saga/effects";
import {
  FCM_ACTIONS,
  hideLoadingAction,
  loadWorkflow,
  previewAkte,
  showLoadingAction,
} from "../../../../helpers";

function createHandleMessage(logService: LogService) {
  return function* (action: DispatchAction<{
    message: FirebaseMessagingTypes.RemoteMessage;
  }>) {
    logService.debug(`handle message ${JSON.stringify(action.payload.message)}`);
    yield put(showLoadingAction());

    try {
      const message = action.payload.message;

      const account: Account = yield select(
        (state: RootState) => state.esy.auth?.accounts[message.data.accountId]
      );
      const akte: Akte = yield select(
        (state: RootState) => state.esy.akten.akten[message.data.akteId]
      );
      const accessToken = yield select(
        (state: RootState) => state.esy.auth?.session?.accessToken
      );

      switch (message.data.type) {
        case "Message":
          yield put(previewAkte(account, akte, "Messages"));
          break;
        case "Task":
          yield put(
            loadWorkflow(
              parseInt(message.data.accountId),
              message.data.payload.replace(
                "https://app.esy.one/externalform/",
                ""
              ),
              accessToken
            )
          );
          break;
        case "Status":
          yield put(previewAkte(account, akte, "CaseDetail"));
          break;
        default:
          break;
      }
    } catch (err) {
      logService.error(err);
    } finally {
      yield put(hideLoadingAction());
    }
  };
}

export default function* startHandleMessageSaga(
  logService: LogService,
) {
  logService.debug("start handle message saga...");
  yield takeLeading(
    FCM_ACTIONS.EVENT.FCM_HANDLE_MESSAGE,
    createHandleMessage(logService),
  );
}
