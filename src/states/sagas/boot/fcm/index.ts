import { all } from "redux-saga/effects";
import { EconsultService, LogService } from "../../../../services";
import register from "./register";
import requestPermissions from "./request-permission";
import unregister from "./unregister";
import watchNotification from "./watch-message";
import watchNotificationOpenedApp from "./watch-notification-opened-app";
import startHandleMessageSaga from "./handle-message";

export function* startFcmSagas(
  logService: LogService,
  econsultService: EconsultService
) {
  logService.debug("starting fcm sagas");
  yield all([
    register(logService, econsultService),
    requestPermissions(logService),
    unregister(logService, econsultService),
    watchNotification(logService),
    watchNotificationOpenedApp(logService),
    startHandleMessageSaga(logService),
  ]);
}
