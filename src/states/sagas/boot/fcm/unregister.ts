import { call, put, select, takeLeading } from "redux-saga/effects";
import messaging from "@react-native-firebase/messaging";
import { FCM_ACTIONS } from "../../../../helpers/consts";
import { EconsultService } from "../../../../services/api/econsult/econsult.service";
import { LogService } from "../../../../services";
import { AuthState, RootState } from "../../../../models";
import { updatePushNotificationSupport } from "../../../../helpers";

function createUnregisterHandler(
  logService: LogService,
  econsultService: EconsultService
) {
  const messageApp = messaging();

  return function* () {
    try {
      yield call(messageApp.unregisterDeviceForRemoteMessages);
      const authStatus: AuthState = yield select(
        (state: RootState) => state.esy.auth
      );
      if (!authStatus) throw new Error("no authentication result found");

      const token = yield messageApp.getToken();

      yield call(
        econsultService.deleteNotificationToken,
        authStatus.profile.accountId,
        authStatus.profile.userId,
        token
      );

      yield put(updatePushNotificationSupport(false));
      yield put({ type: FCM_ACTIONS.RESULT.FCM_UNREGISTER_SUCCEED });
    } catch (err) {
      yield put({
        type: FCM_ACTIONS.RESULT.FCM_UNREGISTER_FAILED,
        payload: err,
      });
      logService.error(err);
    }
  };
}

export default function* unregister(
  logService: LogService,
  econsultService: EconsultService
) {
  const unregister = createUnregisterHandler(logService, econsultService);

  yield takeLeading(FCM_ACTIONS.FCM_UNREGISTER, unregister);
}
