// import jose from "jose/dist/browser";
import JWT from "expo-jwt";

import { COLORS } from "../../helpers/colors";
import { parseLinkingUrl } from "../../helpers/parser";
import {
  AUTH_STACK,
  HOME_STACK,
  REGISTRATION_STACK,
  SPLASH_STACK,
  STACKS,
  WELCOME_STACK,
} from "../../helpers/screens";
import {
  Account,
  AccountsMap,
  AuthState,
  EsyState,
  OfflineCredential,
  Profile,
  SelectStateFn,
  Session,
} from "../../models";
import { AccountsService } from "../api";
import { AuthService } from "../api/auth/auth.service";
import { EconsultService } from "../api/econsult/econsult.service";
import { LogService } from "../log";
import { EvaluationResult, GuardService } from "./guard.service";
import accountsDto from "../../helpers/dto/accounts-dto";
import {
  AuthenticationFailed,
  AVAILABLE_KEYS,
  BiometricNotSupported,
  checkTokenIsValid,
  config,
  decodeToken,
  loadWorkflow,
  navigateToAction,
  NO_ACTIONS,
  NotAuthorized,
  OfflineAuthenticationFailed,
  previewAccount,
  resumeWorkflow,
  startActivations,
  startRegistrationAction,
} from "../../helpers";
import { TouchIdservice } from "../native";
import { useEsyState } from "../../hooks";

// const DEFAULT_KEY = "mandant";

export function createGuardService(
  logService: LogService,
  authService: AuthService,
  accountService: AccountsService,
  econsultService: EconsultService,
  touchIdService: TouchIdservice,
  select: SelectStateFn
): GuardService {
  const evaluate = createEvaluate(
    logService,
    authService,
    accountService,
    econsultService,
    select
  );

  const evaluateAdhocWorkflow = createEvaluateAdhocWorkflow(
    logService,
    authService
  );

  const evaluateWithLink = createEvaluateWithLink(
    logService,
    authService,
    accountService,
    econsultService,
    select,
  );

  const login = createLogin(authService, accountService);

  const offlineLogin = createOfflineLogin();

  const biometricLogin = createBiometricLogin(
    touchIdService,
    authService,
    accountService
  );

  return {
    login,
    offlineLogin,
    biometricLogin,
    evaluate,
    evaluateAdhocWorkflow,
    evaluateWithLink,
  };
}

function createLogin(
  authService: AuthService,
  accountsService: AccountsService
) {
  const findAccounts = createFindAccounts(accountsService);

  return async function (
    username: string,
    password: string
  ): Promise<AuthState> {
    // do login to api
    const authResult = await authService.postLogin<any>(username, password);
    const session: Partial<Session> = {
      accessToken: authResult.accessToken,
      tokenType: authResult.tokenType,
      // offlineCredential:
    };

    const profile: Partial<Profile> = {
      accountId: authResult.accountId,
      email: authResult.email,
      firstName: authResult.firstName,
      lastName: authResult.lastName,
      pregivenUsername: username,
      userId: authResult.userId,
    };

    const accounts = await findAccounts(authResult.accessToken);

    for (const account of Object.values(accounts)) {
      const accountPublicInfo = await accountsService.getPublicInfo(account.id);
      accounts[`${account.id}`] = {
        ...account,
        ...accountPublicInfo,
      };
    }

    const offlineCredential: OfflineCredential = {
      accessToken: authResult.accessToken,
      userId: username,
      secret: password,
      profile: profile as Profile,
      accounts,
    };

    // console.log("ini jose coyyy", jose, jose.UnsecuredJWT);

    session.offlineCredential = JWT.encode(
      {
        iss: config.get(AVAILABLE_KEYS.DOMAIN_NAME),
        aud: config.get(AVAILABLE_KEYS.BASE_URL),
        sub: username,
        exp: Date.now() / 1000 + 86400 * 14,
        ...offlineCredential,
      },
      config.get(AVAILABLE_KEYS.JWT_KEY)
    );

    return {
      isLoggedIn: true,
      session: session as Session,
      profile: profile as Profile,
      accounts,
    } as AuthState;
  };
}

function createOfflineLogin() {
  return async function (
    username: string,
    password: string,
    offlineCredentialHash: string
  ): Promise<AuthState> {
    if (!offlineCredentialHash) throw new OfflineAuthenticationFailed();

    const offlineCredential: OfflineCredential = decodeToken<OfflineCredential>(
      offlineCredentialHash
    );

    if (
      !(
        username === offlineCredential.userId &&
        password === offlineCredential.secret
      )
    )
      throw new OfflineAuthenticationFailed();

    const decodedAccessToken = decodeToken<any>(offlineCredential.accessToken);
    const newAccessTokenJwt = {
      ...decodedAccessToken,
      exp: Date.now() / 1000 + 10800,
      offline: true,
    };

    const session: Partial<Session> = {
      accessToken: JWT.encode(
        newAccessTokenJwt,
        config.get(AVAILABLE_KEYS.JWT_KEY)
      ),
      tokenType: "Bearer",
    };

    return {
      isLoggedIn: true,
      session: session as Session,
      profile: offlineCredential.profile,
      accounts: offlineCredential.accounts,
    } as AuthState;
  };
}

function createBiometricLogin(
  touchIdService: TouchIdservice,
  authService: AuthService,
  accountsService: AccountsService
) {
  const login = createLogin(authService, accountsService);

  return async function (offlineCredentialHash): Promise<AuthState> {
    if (!offlineCredentialHash) throw new AuthenticationFailed();
    if (!(await touchIdService.isSupported()))
      throw new BiometricNotSupported();

    const isSuccess = await touchIdService.authenticate().catch(() => false);

    if (!isSuccess) throw new AuthenticationFailed();

    const offlineCredential: OfflineCredential = decodeToken<OfflineCredential>(
      offlineCredentialHash
    );

    return await login(offlineCredential.userId, offlineCredential.secret);
  };
}

function createFindAccounts(accountService: AccountsService) {
  return async function (accessToken: string): Promise<AccountsMap> {
    // const output = {} as AccountsMap
    const { accounts } = await accountService.getAll({ accessToken });
    // console.log(accounts);
    return accounts.reduce(
      (acc, item) => ({
        ...acc,
        [item.id]: accountsDto.fromCheetahResponse(item),
      }),
      {} as AccountsMap
    );
  };
}

function createEvaluate(
  logService: LogService,
  authService: AuthService,
  accountService: AccountsService,
  econsultService: EconsultService,
  select: SelectStateFn
) {
  const evaluateNoLink = createEvaluateNoLink();
  const evaluateWithLink = createEvaluateWithLink(
    logService,
    authService,
    accountService,
    econsultService,
    select,
  );

  return async function (currentLink?: string) {
    if (!currentLink)
      return await evaluateNoLink(select((state: EsyState) => state.auth));

    return await evaluateWithLink(currentLink);
  };
}

function createEvaluateWithLink(
  logService: LogService,
  authService: AuthService,
  accountService: AccountsService,
  econsultService: EconsultService,
  select: SelectStateFn,
) {
  const evaluatePageLink = createEvaluatePageLink(accountService);
  const evaluateExternalFrageLink = createEvaluateExternalFrageLink(
    logService,
    authService,
    accountService,
    econsultService
  );
  const evaluateAufgabeLink = createEvaluateAufgabeLink(
    logService,
    authService
  );

  return async function (currentLink: string): Promise<EvaluationResult> {
    logService.debug(`Processing link ${currentLink}`);
    const parseResult = parseLinkingUrl(currentLink); //parseIncomingUrl(currentLink);
    /* const key = parseResult.value.substring(
      parseResult.value.lastIndexOf("/") + 1,
      parseResult.value.length
    ); */
    // console.log("ini parse result", parseResult);

    switch (parseResult.target) {
      case "page":
        return await evaluatePageLink(parseResult.key);
      case "aufgabe":
        return await evaluateAufgabeLink(parseResult.key);
      case "externalfrage":
        return await evaluateExternalFrageLink(
          parseResult.providerName,
          parseResult.key
        );
      case "start":
        // return navigateToAction({
        //   navigateTo: {
        //     stack: STACKS.WELCOME_STACK,
        //     screen: WELCOME_STACK.WELCOME_SCREEN,
        //     params: {
        //       id: parseResult.key,
        //     },
        //   },
        // });
        return startRegistrationAction(parseResult.key);
      case "activate":
        const accounts = select((state) => state.auth.accounts);

        if (!accounts) return navigateToAction({
          navigateTo: {
            stack: STACKS.AUTH_STACK,
            screen: AUTH_STACK.SIGN_IN_SCREEN,
          },
        });

        if (Object.keys(accounts).includes(parseResult.key))
          return previewAccount(accounts[parseResult.key]);

        return { type: NO_ACTIONS.DO_NOTHING };
      case "reset-password":
        return navigateToAction({
          navigateTo: {
            stack: STACKS.AUTH_STACK,
            screen: AUTH_STACK.RESET_PASSWORD_SCREEN,
            params: {
              key: parseResult.key,
            },
          },
        });
      default:
        return navigateToAction({
          navigateTo: {
            stack: STACKS.HOME_STACK,
          },
        });
    }
  };
}

function createEvaluateAdhocWorkflow(
  logService: LogService,
  authService: AuthService
) {
  return async function (workflowKey: string) {
    // console.log("auth service", authService);
    try {
      const adhocWorkflowResult = await authService.postAdhocWorkflow(
        workflowKey
      );

      // console.log("ini adhoc workflow result", adhocWorkflowResult);
      const { accountId, accessToken } = adhocWorkflowResult;

      if (!accessToken)
        throw new NotAuthorized("local://", {}, "No Access Token");

      return loadWorkflow(accountId, workflowKey, accessToken);

      // return navigateToAction({
      //   navigateTo: {
      //     stack: STACKS.HOME_STACK,
      //     screen: SPLASH_STACK.FORMULA_SCREEN,
      //     params: {
      //       providerId: accountId,
      //       key: workflowKey,
      //       accessToken,
      //     },
      //   },
      // });
    } catch (err) {
      if (err instanceof NotAuthorized)
        return navigateToAction({
          // replace: true,
          navigateTo: {
            stack: STACKS.AUTH_STACK,
            screen: AUTH_STACK.SIGN_IN_SCREEN,
            params: {
              // item: key,
              dispatch: resumeWorkflow(workflowKey),
            },
          },
        });
      else logService.error(err);
    }
  };
}

function createEvaluateExternalFrageLink(
  logService: LogService,
  authService: AuthService,
  accountService: AccountsService,
  econsultService: EconsultService
) {
  const evaluateAdhocWorkflow = createEvaluateAdhocWorkflow(
    logService,
    authService
  );

  return async function (
    providerName: string,
    key: string
  ): Promise<EvaluationResult> {
    try {
      const uniqueAccountKeyResult = await accountService.getUniqueAccountKey(
        providerName
      );
      const externalRequestKeyResult =
        await econsultService.getExternalRequestKey<any>(
          uniqueAccountKeyResult.accountId,
          key
        );
      let workflowKey = externalRequestKeyResult.workflowKey;

      return await evaluateAdhocWorkflow(workflowKey);
    } catch (err) {
      logService.error(err);
    }

    return Promise.reject();
  };
}

function createEvaluateAufgabeLink(
  logService: LogService,
  authService: AuthService
) {
  const evaluateAdhocWorkflow = createEvaluateAdhocWorkflow(
    logService,
    authService
  );

  return async function (key: string): Promise<EvaluationResult> {
    return await evaluateAdhocWorkflow(key);
  };
}

function createEvaluatePageLink(accountService: AccountsService) {
  return async function (uniqueName: string): Promise<EvaluationResult> {
    const uniqueAccountKeyResult = await accountService.getUniqueAccountKey(
      uniqueName
    );
    const { accountId } = uniqueAccountKeyResult;

    const account: Partial<Account> = {
      id: accountId,
    };

    return previewAccount(account as Account);

    // const publicInfoResult = await accountService.getPublicInfo(accountId);
    // return navigateToAction({
    //   navigateTo: {
    //     stack: STACKS.HOME_STACK,
    //     screen: HOME_STACK.PROVIDER_SCREEN,
    //     params: {
    //       item: publicInfoResult,
    //       id: accountId,
    //     },
    //   },
    // });
  };
}

function createEvaluateNoLink() {
  return async function (authStatus: AuthState): Promise<EvaluationResult> {
    // if (!authStatus || authStatus.id == null || authStatus.login)
    /* if (
      authStatus &&
      (!authStatus.session ||
        !authStatus.session.accessToken ||
        authStatus.session.login)
    ) */
    const isValidToken = checkTokenIsValid(authStatus?.session?.accessToken);

    if (
      authStatus &&
      authStatus.profile &&
      (!authStatus.session || !authStatus.session.accessToken || !isValidToken)
    )
      return navigateToAction({
        navigateTo: {
          stack: STACKS.AUTH_STACK,
          screen: AUTH_STACK.SIGN_IN_SCREEN,
        },
      });
    else if (!authStatus || !authStatus.profile)
      return navigateToAction({
        navigateTo: {
          stack: STACKS.ONBOARDING_STACK,
          screen: AUTH_STACK.ON_BOARDING_SCREEN,
        },
      });
    // else if (
    //   authStatus &&
    //   authStatus.session &&
    //   authStatus.session.accessToken &&
    //   !isValidToken(authStatus.session.accessToken)
    // )
    //   return {
    //     navigateTo: {
    //       stack: STACKS.AUTH_STACK,
    //       screen: AUTH_STACK.SIGN_IN_SCREEN,
    //     },
    //   };
    else if (
      authStatus &&
      authStatus.session &&
      authStatus.session.accessToken &&
      isValidToken
    )
      return startActivations();
    // return navigateToAction({
    //   navigateTo: {
    //     stack: STACKS.HOME_STACK,
    //     // screen: AUTH_STACK.UNAUTHORIZED,
    //   },
    // });
    else
      return navigateToAction({
        navigateTo: {
          stack: STACKS.AUTH_STACK,
          screen: AUTH_STACK.SIGN_IN_SCREEN,
        },
      });
  };
}
