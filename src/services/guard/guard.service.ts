import { AuthState, DispatchAction, NavigationTarget } from "../../models";

export declare type EvaluationResult = DispatchAction<any>;
// {
//   isDenied?: boolean;
//   isPassed?: boolean;
//   /* navigateTo?: {
//     stack: string;
//     screen?: string;
//     params?: any;
//   }; */
// } & Partial<DispatchAction<any>>;

export interface GuardService {
  login(username: string, password: string): Promise<AuthState>;
  offlineLogin(
    username: string,
    password: string,
    offlineCredential: string
  ): Promise<AuthState>;
  biometricLogin(offlineCredential: string): Promise<AuthState>;
  evaluate(currentLink?: string): Promise<EvaluationResult>;
  evaluateAdhocWorkflow(workflowKey: string): Promise<EvaluationResult>;
  evaluateWithLink(currentLink: string): Promise<EvaluationResult>;
}
