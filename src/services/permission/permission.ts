import { Alert, PermissionsAndroid, Platform } from "react-native";
import { PermissionService } from "./permission.service";
import localization from "../../helpers/localization";
import { LogService } from "../log";

export function createPermissionService(
  logService: LogService
): PermissionService {
  const askPushNotificationPermission = () =>
    new Promise<boolean>((resolve) => {
      Alert.alert(
        localization.language.Notification_AskHeader,
        "",
        [
          {
            text: localization.language.Notification_AskNo,
            onPress: () => {
              // TODO: use state
              // AsyncStorage.setItem('pushNotification', '0');
              resolve(false);
            },
            style: "cancel",
          },
          {
            text: localization.language.Notification_AskYes,
            onPress: () => {
              // newly activated, ScreenDashboard onload needs to handle this appropriately
              // TODO: use state
              // AsyncStorage.setItem('pushNotification', '2');
              resolve(true);
            },
          },
        ],
        { cancelable: false }
      );
    });

  const askCameraPermission = async () => {
    if (Platform.OS !== "android") return true;

    try {
      const grant = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "App Camera Permission",
          message: "App needs access to your camera",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK",
        }
      );

      if (grant === PermissionsAndroid.RESULTS.GRANTED) return true;
      else return false;
    } catch (err) {
      logService.error(err);
      return false;
    }
  };

  const askFilesPermission = async () => {
    if (Platform.OS !== "android") return true;
    if (Number(Platform.constants["Release"]) >= 13) return true;

    try {
      const grant = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      ]);

      if (grant["android.permission.WRITE_EXTERNAL_STORAGE"] === "granted" &&
        grant["android.permission.READ_EXTERNAL_STORAGE"] === "granted") return true;
      else return false;
    } catch (err) {
      logService.error(err);
      return false;
    }
  };

  const askRedirectExternallink = async (url: string) =>
    new Promise<boolean>((resolve) => {
      Alert.alert(
        "",
        localization
          .language
          .RedirectExternalLink_AskMessage
          .replace("{{Link}}", url),
        [
          {
            text: localization.language.RedirectExternalLink_AskNo,
            onPress: () => {
              resolve(false);
            },
            style: "cancel",
          },
          {
            text: localization.language.RedirectExternalLink_AskYes,
            onPress: () => {
              resolve(true);
            },
          },
        ],
        { cancelable: false }
      );
    });

  return {
    askPushNotificationPermission,
    askCameraPermission,
    askFilesPermission,
    askRedirectExternallink,
  };
}
