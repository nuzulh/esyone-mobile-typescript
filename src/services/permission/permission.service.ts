export interface PermissionService {
  askPushNotificationPermission(): Promise<boolean>;
  askCameraPermission(): Promise<boolean>;
  askFilesPermission(): Promise<boolean>;
  askRedirectExternallink(url: string): Promise<boolean>;
}
