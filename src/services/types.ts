import {
  AccountsService,
  AktenService,
  AufgabeService,
  AuthService,
  EconsultService,
  FilesService,
  UsersService,
} from "./api";
import { AssetsService } from "./assets";
import { GuardService } from "./guard";
import { LogService } from "./log";
import { TouchIdservice } from "./native/touch-id";
import { PermissionService } from "./permission";

export declare type ServicesContext = {
  logService: LogService;
  accountService: AccountsService;
  authService: AuthService;
  econsultService: EconsultService;
  guardService: GuardService;
  usersService: UsersService;
  permissionService: PermissionService;
  touchIdService: TouchIdservice;
  aktenService: AktenService;
  aufgabeService: AufgabeService;
  filesService: FilesService;
  assetsService: AssetsService;
};
