export interface LogService {
  error(err: Error): void;
  debug(message: any): void;
  info(message: any): void;
}
