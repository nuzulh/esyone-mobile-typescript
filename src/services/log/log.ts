import { LogService } from "./log.service";

export function createLogService(): LogService {
  return {
    error(err) {
      console.error(
        err.stack
          ? `${err.message}\n${err.stack}`
          : err.message
          ? err.message
          : err
      );
    },
    debug(message) {
      console.debug(message);
    },
    info(message) {
      console.log(message);
    },
  };
}
