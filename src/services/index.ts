import { createContext } from "react";
import { SelectStateFn } from "../models";
import {
  createRemoteAccountService,
  createRemoteAktenService,
  createRemoteAufgabeService,
  createRemoteAuthService,
  createRemoteFilesService,
  createRemoteUsersService,
} from "./api";
import { createRemoteEconsultService } from "./api/econsult/econsult.remote";
import { AssetsService } from "./assets";
import { createGuardService } from "./guard";
import { createLogService } from "./log";
import { createTouchIdService } from "./native/touch-id";
import { createPermissionService } from "./permission";
import { ServicesContext } from "./types";

export * from "./api";
export * from "./guard";
export * from "./log";
export * from "./permission";
export * from "./native";
export * from "./assets";

export const Services = createContext<ServicesContext>({} as ServicesContext);

export function createServices(
  // getToken: () => string,
  // getAuthStatus: () => Promise<AuthState>
  select: SelectStateFn,
  assetsService: AssetsService
): ServicesContext {
  const logService = createLogService();
  const accountService = createRemoteAccountService(select);
  const authService = createRemoteAuthService(select);
  const econsultService = createRemoteEconsultService(select);
  const usersService = createRemoteUsersService(select);
  const permissionService = createPermissionService(logService);
  const touchIdService = createTouchIdService();
  const aktenService = createRemoteAktenService(select);
  const aufgabeService = createRemoteAufgabeService(select);
  const filesService = createRemoteFilesService(select);
  const guardService = createGuardService(
    logService,
    authService,
    accountService,
    econsultService,
    touchIdService,
    select
  );

  return {
    logService,
    accountService,
    authService,
    econsultService,
    guardService,
    usersService,
    permissionService,
    touchIdService,
    aktenService,
    aufgabeService,
    filesService,
    assetsService,
  };
}
