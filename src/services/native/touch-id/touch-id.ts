import { TouchIdservice } from "./touch-id.service";
import TouchID from "react-native-touch-id";
import { localization } from "../../../helpers";

const authenticationOptionalConfig = {
  title: localization.language.SignIn_BiometricFingerprint /* Android */,
  imageColor: "#e00606" /* Android */,
  imageErrorColor: "#ff0000" /* Android */,
  sensorDescription:
    localization.language.SignIn_BiometricTouchSensor /* Android */,
  sensorErrorDescription:
    localization.language.SignIn_BiometricFailed /* Android */,
  cancelText: localization.language.SignIn_BiometricCancel /* Android */,
  fallbackLabel:
    localization.language
      .SignIn_BiometricTryAgain /* iOS (if empty, then label is hidden) */,
  passcodeFallback: true,
};

export function createTouchIdService(): TouchIdservice {
  const isSupported = () =>
    TouchID.isSupported({
      unifiedErrors: false,
    })
      .then((type) => type)
      .catch(() => null);

  const authenticate = () =>
    TouchID.authenticate(
      "Touch ID Register",
      authenticationOptionalConfig
    ).then(() => true);

  return {
    isSupported,
    authenticate,
  };
}
