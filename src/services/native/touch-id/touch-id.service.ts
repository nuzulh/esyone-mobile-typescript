export interface TouchIdservice {
  isSupported(): Promise<string>; // return biometry type
  authenticate(): Promise<boolean>; // credential id
}
