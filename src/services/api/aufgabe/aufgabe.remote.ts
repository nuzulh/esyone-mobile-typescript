import {
  applyAuthHeader,
  aufgabenDto,
  execFetch,
  GET,
  getDefaultHeader,
  getPathFrom,
  POST,
  workflowDto,
} from "../../../helpers";
import { Aufgabe, SelectStateFn } from "../../../models";
import { WorkflowAnswer, WorkflowProcess } from "../../../models/workflow";
import { AufgabeService } from "./aufgabe.service";

export function createRemoteAufgabeService(
  select: SelectStateFn
): AufgabeService {
  const request = execFetch(getPathFrom("/cheetah"));
  const defaultHeaders = getDefaultHeader();

  const getAll = (accountId: number, akteId: string): Promise<Aufgabe[]> =>
    request(
      GET(`/api/e.consult.${accountId}/assistants/processes/${akteId}`, null, {
        headers: applyAuthHeader(
          defaultHeaders,
          select((state) => state.auth?.session?.accessToken)
        ),
      })
    )
      .then((response) => {
        // console.log(response);
        return response.json();
      })
      .then((data) =>
        data.map((item) => aufgabenDto.fromCheetahResponse(item))
      );

  const getWorkflow = (
    workflowId: string,
    accessToken: string
  ): Promise<WorkflowProcess> =>
    request(
      GET(
        `/api/assistants/process`,
        {
          workflowKey: workflowId,
        },
        {
          headers: applyAuthHeader(defaultHeaders, accessToken),
        }
      )
    )
      .then((res) => res.json())
      .then((data) => ({ ...data, workflowId }));

  const getWorkflowAnswers = (
    workflowID: string,
    akteId: string,
    accountId: number,
    accessToken: string
  ): Promise<WorkflowAnswer[]> =>
    request(
      GET(
        `/api/e.consult.${accountId}/assistants/processes/${akteId}/workflow/${workflowID}`,
        null,
        {
          headers: applyAuthHeader(defaultHeaders, accessToken),
        }
      )
    )
      .then((res) => res.json())
      .then((data) => data.answers)
      // .then((data) => {
      //   console.log(data);
      //   return [];
      // })
      .then((data) =>
        data.map((item) => workflowDto.fromCheetahResponse(item))
      );

  const postWorkflow = (
    workflowId: string,
    accessToken: string,
    data: any
  ): Promise<any> =>
    request(
      POST(
        `/api/assistants/process`,
        data,
        { workflowKey: workflowId },
        {
          headers: applyAuthHeader(defaultHeaders, accessToken),
        }
      )
    )
      // .then(tap(async (res) => console.log(res)))
      .then((res) => res.json());

  return {
    getAll,
    getWorkflow,
    getWorkflowAnswers,
    postWorkflow,
  };
}
