import { Aufgabe } from "../../../models";
import { WorkflowAnswer, WorkflowProcess } from "../../../models/workflow";

export interface AufgabeService {
  getAll(accountId: number, akteId: string): Promise<Aufgabe[]>;
  getWorkflow(
    workflowId: string,
    accessToken: string
  ): Promise<WorkflowProcess>;
  getWorkflowAnswers(
    workflowID: string,
    akteId: string,
    accountId: number,
    accessToken: string
  ): Promise<WorkflowAnswer[]>;
  postWorkflow(
    workflowId: string,
    accessToken: string,
    data: any
  ): Promise<any>;
}
