export interface MetaFile {
  id: number | string;
  accountId: number | string;
  fileName: string;
  filePath: string;
  contentType: string;
  size: number;
  extension: string;
  tags?: { [key: string]: any; };
}

export interface VirtualMetaFile extends MetaFile {
  content: string;
}

export interface FileUploadResult {
  name: string;
  uploadId: string;
}

export interface FileUploadInfo {
  maxUploadSizeInKB: number;
  totalFileSizeLimitInKb: number;
  defaultDisallowedUploadFormats: string;
}

export interface FilesService {
  readFile(file: MetaFile, encoding: string): Promise<VirtualMetaFile>;
  downloadFile(
    accountId: number,
    fileId: number | string,
    accessToken?: string
  ): Promise<MetaFile>;
  uploadFiles(
    accountid: number,
    files: Partial<MetaFile>[],
    accessToken?: string
  ): Promise<FileUploadResult[]>;
  getUploadInfo(
    accountId: number,
    accessToken?: string
  ): Promise<FileUploadInfo>;
}
