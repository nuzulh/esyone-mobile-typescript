import { SelectStateFn } from "../../../models";
import RNFetchBlob from "rn-fetch-blob";
import RNFS from "react-native-fs";
import {
  applyAuthHeader,
  execFetch,
  extractHeaders,
  GET,
  getDefaultHeader,
  getPathFrom,
  POST_FORM,
} from "../../../helpers";
import {
  FilesService,
  FileUploadResult,
  MetaFile,
  VirtualMetaFile,
} from "./files.service";

export function createRemoteFilesService(select: SelectStateFn): FilesService {
  const request = execFetch(getPathFrom("/cheetah"));
  const defaultHeaders = getDefaultHeader();

  const readFile = (
    file: MetaFile,
    encoding: string
  ): Promise<VirtualMetaFile> =>
    RNFetchBlob.fs.readFile(file.filePath, encoding as any).then((content) => ({
      ...file,
      content,
    }));

  const downloadFile = (
    accountId: number,
    fileId: number,
    accessToken?: string
  ): Promise<MetaFile> => {
    // console.log(
    //   getPathFrom(
    //     `/cheetah/api/v2.0/e.consult.${accountId}/files/download/${fileId}?resultType=stream`
    //   )
    // );

    return RNFetchBlob.config({
      fileCache: false,
    })
      .fetch(
        "GET",
        getPathFrom(
          `/cheetah/api/v2.0/e.consult.${accountId}/files/download/${fileId}?resultType=base64`
        ),
        {
          ...applyAuthHeader(
            defaultHeaders,
            accessToken
              ? accessToken
              : select((state) => state.auth?.session?.accessToken)
          ),
        }
      )
      .then((res) =>
        (async () => {
          const headers = extractHeaders(res.respInfo.headers);
          const [filename, mime] = headers["content-type"].split("/");
          const path = `${RNFetchBlob.fs.dirs.DocumentDir}/esy_file_cache_${fileId}.${mime}`;
          const content = await res.text(); //res.base64();
          const splittedFilename = headers["content-disposition"]?.split("=")[1]?.split(".") || ["unknown"];
          const extension = splittedFilename[splittedFilename.length - 1].replace("\"", "");

          await RNFetchBlob.fs.writeFile(path, content, "base64");

          return {
            id: fileId,
            accountId,
            fileName: headers["content-disposition"]
              ? headers["content-disposition"]
                ?.split("=")[1]
                ?.replace(/^["'](.+(?=["']$))["']$/, '$1') || null
              : `${fileId}`,
            filePath: path,
            contentType: headers["content-type"],
            size: headers["content-length"],
            extension,
          };
        })()
      );
  };

  const uploadFiles = async (
    accountId: number,
    files: Partial<MetaFile>[],
    accessToken?: string
  ): Promise<FileUploadResult[]> => {
    const formData = new FormData();

    for (let file of files) {
      const uri = file.filePath; //.replace("file://", "");
      formData.append("files", {
        uri,
        type: file.contentType,
        name: file.fileName,
      } as any);
    }

    const result = await request(
      POST_FORM(
        `/api/v2.0/e.consult.${accountId}/files/upload`,
        formData,
        null,
        {
          headers: applyAuthHeader(
            {
              ...defaultHeaders,
              "Content-Type": "multipart/form-data",
            },
            select((state) =>
              accessToken ? accessToken : state.auth?.session?.accessToken
            )
          ),
        }
      )
    )
      .then((res) => res.json())
      .then(({ files }) =>
        files.map((file) => ({ name: file.name, uploadId: file.fileuploadId }))
      );

    return result;
  };

  const getUploadInfo = async (
    accountId: number,
    accessToken: string
  ) => await request(
    GET(
      `/api/v2.0/e.consult.${accountId}/files/upload/info`,
      null,
      {
        headers: applyAuthHeader(
          defaultHeaders,
          select((state) =>
            accessToken ? accessToken : state.auth?.session?.accessToken
          )
        ),
      }
    )
  ).then((res) => res.json());

  return {
    readFile,
    downloadFile,
    uploadFiles,
    getUploadInfo,
  };
}
