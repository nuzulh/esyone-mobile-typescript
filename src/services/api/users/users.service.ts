import { MetaFile } from "../files";

export interface UsersService {
  getTask<T extends unknown>(id: string): Promise<T>;
  getCheckUsername<T extends unknown>(
    userId: string,
    username: string
  ): Promise<T>;
  getProfilePicture(
    accountId: string,
    userId: string,
    debug?: boolean
  ): Promise<MetaFile>;
  getProfilePictureOfOther(
    accountId: string,
    userId: string,
    debug?: boolean
  ): Promise<MetaFile>;
  postProfilePicture(accountId: string, userId: string, fileId: string);
}
