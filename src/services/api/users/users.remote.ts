import RNFetchBlob from "rn-fetch-blob";
import {
  applyAuthHeader,
  execFetch,
  extractHeaders,
  GET,
  getDefaultHeader,
  getPathFrom,
} from "../../../helpers";
import { SelectStateFn } from "../../../models";
import { UsersService } from "./users.service";

export function createRemoteUsersService(select: SelectStateFn): UsersService {
  const request = execFetch(getPathFrom("/cheetah"));
  const defaultHeaders = getDefaultHeader();

  const getTask = (id: string) =>
    request(
      GET(`/api/v2.0/user/task/${id}`, null, {
        headers: defaultHeaders,
      })
    ).then((res) => res.json());

  const getCheckUsername = (userId: string, username: string) =>
    request(
      GET(
        `/api/v2.0/user/${userId}/checkUsername/${encodeURIComponent(
          username
        )}`,
        null,
        {
          headers: defaultHeaders,
        }
      )
    ).then((res) => res.json());

  const getProfilePicture = (accountId: string, userId: string) => {
    return RNFetchBlob.config({
      fileCache: true,
    })
      .fetch(
        "GET",
        getPathFrom(
          `/cheetah/api/v2.0/e.consult.${accountId}/user/${userId}/profile/picture?resultType=stream`
        ),
        {
          ...applyAuthHeader(
            defaultHeaders,
            select((state) => state.auth?.session?.accessToken)
          ),
        }
      )
      .then((res) =>
        (async () => {
          const path = res.path();
          //`${RNFetchBlob.fs.dirs.DocumentDir}/esy_file_cache_user_${userId}`;
          // const content = await res.text(); //res.base64();
          // const content = await res.readFile("base64");
          // console.log("content", accountId, userId, content);
          // console.log(
          //   `/api/v2.0/e.consult.${accountId}/user/${userId}/profile/picture?resultType=stream`,
          //   content
          // );
          // await RNFetchBlob.fs.writeFile(path, content, "base64");
          // console.log("path", path);

          // console.log(res.respInfo);
          const headers = extractHeaders(res.respInfo.headers);

          return {
            id: userId,
            accountId,
            fileName: headers["content-disposition"]
              ? headers["content-disposition"].split("=")[1]
              : `${userId}.png`,
            filePath: path,
            contentType: headers["content-type"],
            size: headers["content-length"],
            extension: headers["content-disposition"]
              ? headers["content-disposition"].split("=")[1].split(".")[1]
              : "png",
          };
        })()
      );
  };

  const postProfilePicture = (
    accountId: string,
    userId: string,
    fileId: string
  ) => {
    return RNFetchBlob.config({
      fileCache: true,
    })
      .fetch(
        "POST",
        getPathFrom(
          `/cheetah/api/v2.0/e.consult.${accountId}/user/${userId}/profile/picture`
        ),
        {
          ...applyAuthHeader(
            defaultHeaders,
            select((state) => state.auth?.session?.accessToken)
          ),
        },
        '{"UploadId":"' + fileId + '"}'
      )
      .then((res) =>
        (async () => {
          const path = res.path();
          const headers = extractHeaders(res.respInfo.headers);

          return {
            id: userId,
            accountId,
            fileName: headers["content-disposition"]
              ? headers["content-disposition"].split("=")[1]
              : `${userId}.png`,
            filePath: path,
            contentType: headers["content-type"],
            size: headers["content-length"],
            extension: headers["content-disposition"]
              ? headers["content-disposition"].split("=")[1].split(".")[1]
              : "png",
          };
        })()
      );
  };

  const getProfilePictureOfOther = (accountId: string, userId: string) => {
    return RNFetchBlob.config({
      fileCache: true,
    })
      .fetch(
        "GET",
        getPathFrom(
          `/cheetah/api/v2.0/e.consult.${accountId}/user/${userId}/profile/picture/other?resultType=stream`
        ),
        {
          ...applyAuthHeader(
            defaultHeaders,
            select((state) => state.auth?.session?.accessToken)
          ),
        }
      )
      .then((res) =>
        (async () => {
          const path = res.path();
          //`${RNFetchBlob.fs.dirs.DocumentDir}/esy_file_cache_user_${userId}`;
          // const content = await res.text(); //res.base64();
          // const content = await res.readFile("base64");
          // console.log("content", accountId, userId, content);
          // console.log(
          //   `/api/v2.0/e.consult.${accountId}/user/${userId}/profile/picture?resultType=stream`,
          //   content
          // );
          // await RNFetchBlob.fs.writeFile(path, content, "base64");
          // console.log("path", path);

          // console.log(res.respInfo);
          const headers = extractHeaders(res.respInfo.headers);

          return {
            id: userId,
            accountId,
            fileName: headers["content-disposition"]
              ? headers["content-disposition"].split("=")[1]
              : `${userId}.png`,
            filePath: path,
            contentType: headers["content-type"],
            size: headers["content-length"],
            extension: headers["content-disposition"]
              ? headers["content-disposition"].split("=")[1].split(".")[1]
              : "png",
          };
        })()
      );
  };

  return {
    getTask,
    getCheckUsername,
    getProfilePicture,
    getProfilePictureOfOther,
    postProfilePicture,
  };
}
