// import { Account } from "../../../models/account";

import { Account, GutachterSpec, ImageSpec, Insurance } from "../../../models";

export interface AccountsService {
  getAll(options?: { accessToken?: string; }): Promise<{ accounts: any[]; }>;
  getPublicInfo(accountId: string | number): Promise<Partial<Account>>;
  getUniqueAccountKey(name: string): Promise<any>;
  getMainLogo(accountId: string | number): Promise<ImageSpec>;
  getSecondaryLogo(accountId: string | number): Promise<ImageSpec>;
  getInsurances(accountId: number, accessToken?: string): Promise<Insurance[]>;
  getDatenschutz(accountId: number): Promise<string>;
  getGutachter(accountId: number, postalCode: string): Promise<GutachterSpec>;
  postActivate(accountId: number): Promise<void>;
}
