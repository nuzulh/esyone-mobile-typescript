import {
  accountsDto,
  applyAuthHeader,
  execFetch,
  GET,
  getPathFrom,
  POST,
} from "../../../helpers";
import { getDefaultHeader } from "../../../helpers/remote/get-default-header";
import { ImageSpec, SelectStateFn } from "../../../models";
import { AccountsService } from "./accounts.service";

export function createRemoteAccountService(
  select: SelectStateFn
): AccountsService {
  const request = execFetch(getPathFrom("/cheetah"));
  const defaultHeaders = getDefaultHeader();

  const getAll = (options?: { accessToken?: string; }) =>
    request(
      GET("/api/v2.0/accounts", null, {
        headers: {
          Authorization:
            "Bearer " +
            select((state) =>
              options?.accessToken
                ? options?.accessToken
                : state.auth?.session?.accessToken
            ),
          ...defaultHeaders,
        },
      })
    ).then((res) => res.json());

  const getPublicInfo = (accountId: string) =>
    request(
      GET(`/api/account/${accountId}/publicinfo`, null, {
        headers: {
          ...defaultHeaders,
        },
      })
    )
      .then((res) => res.json())
      .then((res) => accountsDto.fromPublicInfoResponse(accountId, res));

  const getUniqueAccountKey = (name: string) =>
    request(
      GET(`/api/account/uniqueAccountKey/${name}`, null, {
        headers: {
          ...defaultHeaders,
        },
      })
    ).then((res) => res.json());

  const getMainLogo = (
    accountId: string,
    options?: { accessToken?: string; }
  ) => {
    return new Promise<ImageSpec>((resolve) =>
      resolve({
        uri: getPathFrom(
          `/cheetah/api/v2.0/e.consult.${accountId}/accounts/logo?resultType=stream`
        ),
        headers: {
          Authorization:
            "Bearer " +
            select((state) =>
              options?.accessToken
                ? options?.accessToken
                : state.auth?.session?.accessToken
            ),
          ...defaultHeaders,
        },
      })
    );
  };

  const getSecondaryLogo = (
    accountId: string,
    options?: { accessToken?: string; }
  ) => {
    return new Promise<ImageSpec>((resolve) =>
      resolve({
        uri: getPathFrom(
          `/cheetah/api/v2.0/e.consult.${accountId}/accounts/secondaryLogo?resultType=stream`
        ),
        headers: {
          Authorization:
            "Bearer " +
            select((state) =>
              options?.accessToken
                ? options?.accessToken
                : state.auth?.session?.accessToken
            ),
          ...defaultHeaders,
        },
      })
    );
  };

  const getInsurances = (accountId: number, accessToken?: string) =>
    request(
      GET(`/api/v2.0/e.consult.${accountId}/insurances/rs`, null, {
        headers: applyAuthHeader(
          defaultHeaders,
          accessToken
            ? accessToken
            : select((state) => state.auth?.session?.accessToken)
        ),
      })
    ).then((res) => res.json());

  const getDatenschutz = (accountId: number) =>
    request(
      GET(`/api/v2.0/e.consult.${accountId}/account/datenschutz`, null, {
        headers: applyAuthHeader(
          defaultHeaders,
          select((state) => state.auth?.session?.accessToken)
        ),
      })
    )
      .then((res) => res.json())
      .then((res) => res.datenschutz);

  const postActivate = (accountId: number) =>
    request(
      POST(
        `/api/v2.0/e.consult.${accountId}/account/activate`,
        {
          AgbAndDatenschutzAccepted: "true",
        },
        null,
        {
          headers: applyAuthHeader(
            defaultHeaders,
            select((state) => state.auth?.session?.accessToken)
          ),
        }
      )
    ).then((res) => {});

  const getGutachter = (accountId: number, postalCode: string) =>
    request(
      GET(
        `/api/e.consult.${accountId}/participants/${postalCode}`,
        null,
        {
          headers: applyAuthHeader(
            defaultHeaders,
            select((state) => state.auth?.session?.accessToken)
          ),
        }
      )
    ).then((res) => res.json());

  return {
    getAll,
    getPublicInfo,
    getUniqueAccountKey,
    getMainLogo,
    getSecondaryLogo,
    getInsurances,
    getDatenschutz,
    postActivate,
    getGutachter,
  };
}
