import { ResourceData } from "../../../hooks";
import { Akte, AktePhase } from "../../../models";
import { Message, MessageAttachment } from "../../../models/message";
import { MetaFile } from "../files";

export declare type GetWithPagingOptions = {
  offset?: number;
  size?: number;
};

export declare type GetAllAktenOptions = GetWithPagingOptions;

export declare type GetMessagesResult = {
  messages: Message[];
  totalMessage: number;
  folders: Pick<Akte, "id">[];
  attachments: MessageAttachment[];
};

export interface AktenService {
  getAll(options?: GetAllAktenOptions): Promise<ResourceData<Akte>>;
  getOne(accountId: number, akteId: string): Promise<Akte>;
  getHistory(accountId: number, akteId: string): Promise<AktePhase[]>;
  getMessages(
    accountId: number,
    akteId: string,
    options?: GetWithPagingOptions
  ): Promise<GetMessagesResult>;
  getDownloadAkte(accountId: number, akteId: string): Promise<MetaFile>;
  postMessage(
    accountId: number,
    akteId: string,
    message: Message
  ): Promise<Message>;
  postReadMessage(accountId: number, message: Message): Promise<any>;
  postFavoriteMessage(accountId: number, message: Message): Promise<any>;
  deleteFavoriteMessage(accountId: number, message: Message): Promise<any>;
}
