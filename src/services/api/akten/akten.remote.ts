import RNFetchBlob from "rn-fetch-blob";
import {
  aktenDto,
  applyAuthHeader,
  attachmentsDto,
  DELETE,
  execFetch,
  extractHeaders,
  GET,
  getDefaultHeader,
  getPathFrom,
  localization,
  messagesDto,
  POST,
} from "../../../helpers";
import { ResourceData } from "../../../hooks";
import { Akte, Message, SelectStateFn } from "../../../models";
import {
  AktenService,
  GetAllAktenOptions,
  GetMessagesResult,
  GetWithPagingOptions,
} from "./akten.service";
import { MetaFile } from "../files";

export function createRemoteAktenService(select: SelectStateFn): AktenService {
  const request = execFetch(getPathFrom("/cheetah"));
  const defaultHeaders = getDefaultHeader();

  const getAll = (options?: GetAllAktenOptions): Promise<ResourceData<Akte>> =>
    request(
      GET(
        "/api/v2.0/akten",
        {
          size: 100000,
        },
        {
          headers: applyAuthHeader(
            defaultHeaders,
            select((state) => state.auth?.session?.accessToken)
          ),
        }
      )
    )
      .then((res) => res.json())
      .then((data) => {
        // console.log("akten", JSON.stringify(data, null, 2));
        const tmp: ResourceData<Akte> = {};

        for (let accountId in data) {
          const akten = data[accountId];

          for (let akte of akten)
            tmp[akte.idHash] = aktenDto.fromCheetahResponse(akte);
        }

        return tmp;
      });

  const getOne = (
    accountId: number,
    akteId: string,
    options?: { accessToken?: string; }
  ): Promise<Akte> =>
    request(
      GET(`/api/v2.0/e.consult.${accountId}/akten/${akteId}`, null, {
        headers: applyAuthHeader(
          defaultHeaders,
          select((state) =>
            options?.accessToken
              ? options?.accessToken
              : state.auth?.session?.accessToken
          )
        ),
      })
    )
      .then((res) => res.json())
      .then((data) => aktenDto.fromCheetahResponse(data));

  const getHistory = (accountId: number, akteId: string) =>
    request(
      GET(
        `/api/v2.0/e.consult.${accountId}/akten/${akteId}/esyaktehistory`,
        null,
        {
          headers: applyAuthHeader(
            defaultHeaders,
            select((state) => state.auth?.session?.accessToken)
          ),
        }
      )
    )
      .then((res) => res.json())
      .then(({ stateGroups }) => stateGroups);

  const getMessages = (
    accountId: number,
    akteId: string,
    options?: GetWithPagingOptions
  ): Promise<GetMessagesResult> =>
    request(
      GET(
        `/api/v2.0/e.consult.${accountId}/akten/timeline/${akteId}`,
        {
          Size: options?.size,
        },
        {
          headers: applyAuthHeader(
            defaultHeaders,
            select((state) => state.auth?.session?.accessToken)
          ),
        }
      )
    )
      .then((res) => res.json())
      .then(({ messages, folders, files }) => ({
        messages: (messages.timeline || []).map((item) =>
          messagesDto.fromCheetahResponse(item)
        ),
        totalMessage: messages.totalCount,
        attachments: (files || []).map((item) =>
          attachmentsDto.fromCheetahResponse(item)
        ),
        folders: folders || [],
      }));

  const getDownloadAkte = (
    accountId: number,
    akteId: string
  ): Promise<MetaFile> => RNFetchBlob
    .config({ fileCache: false })
    .fetch(
      "GET",
      getPathFrom(
        `/cheetah/api/v2.0/e.consult.${accountId}/akten/${akteId}/download?resultType=base64`
      ),
      {
        ...applyAuthHeader(
          defaultHeaders,
          select((state) => state.auth?.session?.accessToken)
        ),
      }
    )
    .then(async (res) => {
      if (res.respInfo.respType === "json") throw new Error(
        localization.language.DownloadAkteFailed
      );

      const headers = extractHeaders(res.respInfo.headers);
      const extension = headers["content-type"].split("/")[1];
      const fileName = headers["content-disposition"]
        ?.split("=")[1]
        ?.replace(/"/g, "")
        ?.replace(":", ".")
        || `akte_${akteId}.zip`;
      const filePath = `${RNFetchBlob.fs.dirs.DocumentDir}/esy_file_cache_${akteId}.${extension}`;
      const content = await res.text();

      await RNFetchBlob.fs.writeFile(filePath, content, "base64");

      return {
        id: akteId,
        accountId,
        fileName,
        filePath,
        contentType: headers["content-type"],
        size: headers["content-length"],
        extension,
      };
    });

  const postMessage = (
    accountId: number,
    akteId: string,
    message: Message
  ): Promise<Message> =>
    request(
      POST(
        `/api/v2.0/e.consult.${accountId}/akten/${akteId}/messages`,
        {
          Header: message.header,
          TextContent: message.message,
          UploadIds: message.attachments?.map((item) => item.id),
        },
        null,
        {
          headers: applyAuthHeader(
            defaultHeaders,
            select((state) => state.auth?.session?.accessToken)
          ),
        }
      )
    )
      .then((res) => res.json())
      .then((data) => {
        // console.log("postMessage", data);
        return data;
      });

  const postReadMessage = (
    accountId: number,
    message: Message
  ) =>
    request(
      POST(
        `/api/v2.0/e.consult.${accountId}/messages/${message.id}/read`,
        null,
        null,
        {
          headers: applyAuthHeader(
            defaultHeaders,
            select((state) => state.auth?.session?.accessToken)
          ),
        }
      )
    ).then((res) => res.json());

  const postFavoriteMessage = (
    accountId: number,
    message: Message
  ): Promise<any> =>
    request(
      POST(
        `/api/v2.0/e.consult.${accountId}/messages/${message.id}/favorite`,
        null,
        null,
        {
          headers: applyAuthHeader(
            defaultHeaders,
            select((state) => state.auth?.session?.accessToken)
          ),
        }
      )
    ).then((res) => res.json());

  const deleteFavoriteMessage = (
    accountId: number,
    message: Message
  ): Promise<any> =>
    request(
      DELETE(
        `/api/v2.0/e.consult.${accountId}/messages/${message.id}/favorite`,
        null,
        null,
        {
          headers: applyAuthHeader(
            defaultHeaders,
            select((state) => state.auth?.session?.accessToken)
          ),
        }
      )
    ).then((res) => res.json());

  return {
    getAll,
    getOne,
    getHistory,
    getMessages,
    getDownloadAkte,
    postMessage,
    postReadMessage,
    postFavoriteMessage,
    deleteFavoriteMessage,
  };
}
