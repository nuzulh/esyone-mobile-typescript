import {
  applyAuthHeader,
  DELETE,
  execFetch,
  GET,
  getDefaultHeader,
  getPathFrom,
  POST,
} from "../../../helpers";
import { SelectStateFn } from "../../../models";
import { EconsultService } from "./econsult.service";

export function createRemoteEconsultService(
  select: SelectStateFn
): EconsultService {
  const request = execFetch(getPathFrom("/cheetah"));
  const defaultHeaders = getDefaultHeader();
  const getToken = () => select((state) => state.auth?.session?.accessToken);

  const getAkten = (accountId: string, akteId: string) =>
    request(
      GET(`/api/v2.0/e.consult.${accountId}/akten/${akteId}`, null, {
        headers: applyAuthHeader(defaultHeaders, getToken()),
      })
    ).then((res) => res.json());

  const postNotificationToken = (
    accountId: string,
    userIdHash: string,
    fcmToken: string
  ) =>
    request(
      POST(
        `/api/v2.0/e.consult.${accountId}/user/${userIdHash}/notification/tokens`,
        {
          DeviceToken: fcmToken,
        },
        null,
        {
          headers: applyAuthHeader(defaultHeaders, getToken()),
        }
      )
    ).then((res) => ({ status: res.status, content: res.json() }));

  const deleteNotificationToken = (
    accountId: string,
    userIdHash: string,
    fcmToken: string
  ) =>
    request(
      DELETE(
        `/api/v2.0/e.consult.${accountId}/user/${userIdHash}/notification/tokens`,
        {
          DeviceToken: fcmToken,
        },
        null,
        {
          headers: applyAuthHeader(defaultHeaders, getToken()),
        }
      )
    ).then((res) => res.json());

  const getExternalRequestKey = (accountId: string, key: string) =>
    request(
      GET(`/api/e.consult.${accountId}/externalRequestKey/${key}`, null, {
        headers: defaultHeaders,
      })
    ).then((res) => res.json());

  const postAssistantProcess = (
    accountId: string,
    workflowKey: string,
    data: any
  ) =>
    request(
      POST(
        `/api/e.consult.${accountId}/assistants/process`,
        data,
        { workflowKey },
        {
          headers: applyAuthHeader(defaultHeaders, getToken()),
        }
      )
    ).then((res) => res.json());

  const getAssistantProcess = (accountId: string, workflowKey: string) =>
    request(
      GET(
        `/api/e.consult.${accountId}/assistants/process`,
        { workflowKey },
        {
          headers: applyAuthHeader(defaultHeaders, getToken()),
        }
      )
    ).then((res) => res.json());

  const getAssistantProcesses = (accountId: string, akteIdHash: string) =>
    request(
      GET(
        `/api/e.consult.${accountId}/assistants/processes/${akteIdHash}`,
        null,
        {
          headers: applyAuthHeader(defaultHeaders, getToken()),
        }
      )
    ).then((res) => res.json());

  const getAssistantProcessWorkflow = (
    accountId: string,
    processIdHash: string,
    workflowKey: string
  ) =>
    request(
      GET(
        `/api/e.consult.${accountId}/assistants/processes/${processIdHash}/workflow/${workflowKey}`,
        null,
        {
          headers: applyAuthHeader(defaultHeaders, getToken()),
        }
      )
    ).then((res) => res.json());

  const getInsurances = (accountId: string) =>
    request(
      GET(`/api/v2.0/e.consult.${accountId}/insurances/rs`, null, {
        headers: applyAuthHeader(defaultHeaders, getToken()),
      })
    ).then((res) => res.json());

  const getAccountAgb = (accountId: string) =>
    request(
      GET(
        `/api/v2.0/e.consult.${accountId}/account/agb`,
        {
          resultType: "base64",
        },
        {
          headers: applyAuthHeader(defaultHeaders, getToken()),
        }
      )
    ).then((res) => res.json());

  const getAccountDatenschutz = (accountId: string) =>
    request(
      GET(`/api/v2.0/e.consult.${accountId}/account/datenschutz`, null, {
        headers: applyAuthHeader(defaultHeaders, getToken()),
      })
    ).then((res) => res.json());

  const postAccountActivate = (accountId: string) =>
    request(
      POST(
        `/api/v2.0/e.consult.${accountId}/account/activate`,
        {
          AgbAndDatenschutzAccepted: "true",
        },
        null,
        {
          headers: applyAuthHeader(defaultHeaders, getToken()),
        }
      )
    ).then((res) => res.json());

  const getAktenTimeline = (
    accountId: string,
    akteIdHash: string,
    options?: {
      size?: number;
      offset?: number;
      order?: string;
      column?: string;
    }
  ) =>
    request(
      GET(
        `/api/v2.0/e.consult.${accountId}/akten/timeline/${akteIdHash}`,
        options,
        {
          headers: applyAuthHeader(defaultHeaders, getToken()),
        }
      )
    ).then((res) => res.json());

  const getUserRegister = (accountId: string, userId: string) =>
    request(
      GET(`/api/v2.0/e.consult.${accountId}/user/${userId}/register`, null, {
        headers: applyAuthHeader(defaultHeaders, getToken()),
      })
    ).then((res) => res.json());

  const postUserRegister = (accountId: string, userId: string, data: any) =>
    request(
      POST(
        `/api/v2.0/e.consult.${accountId}/user/${userId}/register`,
        data,
        null,
        {
          headers: applyAuthHeader(defaultHeaders, getToken()),
        }
      )
    ).then((res) => res.json());

  return {
    getAkten,
    postNotificationToken,
    deleteNotificationToken,
    getExternalRequestKey,
    postAssistantProcess,
    getAssistantProcess,
    getAssistantProcesses,
    getAssistantProcessWorkflow,
    getInsurances,
    getAccountAgb,
    getAccountDatenschutz,
    postAccountActivate,
    getAktenTimeline,
    getUserRegister,
    postUserRegister,
  };
}
