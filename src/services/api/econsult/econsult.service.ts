// import { Akte } from "../../../models/akte";

export interface EconsultService {
  getAkten(accountId: string, akteId: string): Promise<any[]>;
  postNotificationToken<T extends unknown>(
    accountId: string,
    userIdHash: string,
    fcmToken: string
  ): Promise<any>;
  deleteNotificationToken<T extends unknown>(
    accountId: string,
    userIdHash: string,
    fcmToken: string
  ): Promise<T>;
  getExternalRequestKey<T extends unknown>(
    accountId: string,
    key: string
  ): Promise<T>;
  postAssistantProcess<T extends unknown>(
    accountId: string,
    workflowKey: string,
    data: any
  ): Promise<T>;
  getAssistantProcess<T extends unknown>(
    accountId: string,
    workflowKey: string
  ): Promise<T>;
  getAssistantProcesses<T extends unknown>(
    accountId: string,
    akteIdHash: string
  ): Promise<T[]>;
  getAssistantProcessWorkflow<T extends unknown>(
    accountId: string,
    processIdHash: string,
    workflowKey: string
  ): Promise<T>;
  getInsurances<T extends unknown>(accountId: string): Promise<T[]>;
  getAccountAgb<T extends unknown>(accountId: string): Promise<T>;
  getAccountDatenschutz<T extends unknown>(accountId: string): Promise<T>;
  postAccountActivate<T extends unknown>(accountId: string): Promise<T>;
  getAktenTimeline<T extends unknown>(
    accountId: string,
    akteIdHash: string,
    options?: {
      size?: number;
      offset?: number;
      order?: string;
      column?: string;
    }
  ): Promise<T[]>;
  getUserRegister<T extends unknown>(
    accountId: string,
    userId: string
  ): Promise<T>;
  postUserRegister<T extends unknown>(
    accountId: string,
    userId: string,
    data: any
  ): Promise<T>;
}
