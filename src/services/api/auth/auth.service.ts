import { User, UserSeed, WorkflowAdhoc } from "../../../models";

export interface AuthService {
  getPinAuthentication(key: string): Promise<User>;
  getPin(key: string, channel?: string): Promise<void>;
  getIsUsernameAvailable(seed: UserSeed): Promise<boolean>;
  getRegistration(user: UserSeed): Promise<string>;
  postLogin<T extends unknown>(username: string, password: string): Promise<T>;
  postAdhocLogin<T extends unknown>(key: string, password?: string): Promise<T>;
  postAdhocWorkflow(key: string): Promise<WorkflowAdhoc>;
  postCheckUsername(email: string): Promise<boolean>;
  postResetPassword(username: string): Promise<boolean>;
  postRegistration(key: string, user: UserSeed): Promise<boolean>;
  putUserChangePassword(
    userId: string,
    oldpassword: string,
    newpassword: string,
    newpasswordconfirmation: string
  ): Promise<any>;
  putResetPassword(key: string, password: string): Promise<boolean>;
}
