import {
  applyAuthHeader,
  execFetch,
  GET,
  getPathFrom,
  POST,
  PUT,
} from "../../../helpers";
import { getDefaultHeader } from "../../../helpers/remote/get-default-header";
import { SelectStateFn, UserSeed } from "../../../models";
import { AuthService } from "./auth.service";

export function createRemoteAuthService(select: SelectStateFn): AuthService {
  const request = execFetch(getPathFrom("/cheetah"));
  const defaultHeaders = getDefaultHeader();
  const getToken = () => select((state) => state.auth?.session?.accessToken);

  const getPinAuthentication = (key: string) =>
    request(
      GET(`/api/v2.0/user/task/${key}`, null, {
        headers: defaultHeaders,
      })
    )
      .then((res) => res.json())
      .then((data) => ({
        ...data,
        id: data.userId,
        guid: data.userGuid,
        agb: data.esyThingAgb,
        datenschutz: data.esyThingDatenschutz,
        authenticationKey: data.authentication,
      }));

  const getPin = (key: string, channel: string = "Email") =>
    request(
      GET(
        `/api/v2.0/authenticate/adhoc/pin/${key}`,
        { preferredChannel: channel },
        {
          headers: defaultHeaders,
        }
      )
    ).then(() => {});

  const getIsUsernameAvailable = (seed: UserSeed) =>
    request(
      GET(
        `/api/v2.0/user/${seed.id}/checkUsername/${encodeURIComponent(
          seed.username
        )}`,
        null,
        {
          headers: defaultHeaders,
        }
      )
    )
      .then((res) => res.json())
      .then((data) => data.isValid);

  const getRegistration = (seed: UserSeed) =>
    request(
      GET(
        `/api/v2.0/e.consult.${seed.accountId}/user/${seed.id}/register`,
        null,
        {
          headers: applyAuthHeader(defaultHeaders, seed.accessToken),
        }
      )
    )
      .then((res) => res.json())
      .then((data) => data.asyncTaskKey);

  const postLogin = (username: string, password: string) =>
    request(
      POST(
        "/api/authenticate/login",
        {
          Username: username,
          Password: password,
        },
        null,
        {
          headers: defaultHeaders,
        }
      )
    ).then((res) => res.json());

  const postAdhocLogin = (key: string, password?: string) =>
    request(
      POST("/api/v2.0/authenticate/adhoc", { key, password }, null, {
        headers: defaultHeaders,
      })
    ).then((res) => res.json());

  const postAdhocWorkflow = (key: string) =>
    request(
      POST("/api/authenticate/adhoc/workflow", { key }, null, {
        headers: applyAuthHeader(
          defaultHeaders,
          select((state) => state.auth?.session?.accessToken)
        ),
      })
    ).then((res) => res.json());

  const postCheckUsername = (email: string) =>
    request(
      POST(
        "/api/v2.0/user/username",
        { email },
        null,
        {
          headers: defaultHeaders,
        }
      )
    ).then(() => true);

  const postResetPassword = (username: string) =>
    request(
      POST(
        "/api/v2.0/user/password",
        { username },
        null,
        {
          headers: defaultHeaders,
        }
      )
    ).then(() => true);

  const postRegistration = (key: string, seed: UserSeed) =>
    request(
      POST(
        `/api/v2.0/e.consult.${seed.accountId}/user/${seed.id}/register`,
        {
          username: seed.username,
          password: seed.password,
          passwordConfirmation: seed.password,
          key: key,
          accountId: seed.accountId,
          AgbsAccepted: "true",
        },
        null,
        {
          headers: applyAuthHeader(defaultHeaders, seed.accessToken),
        }
      )
    ).then(() => true);

  const putUserChangePassword = (
    userId: string,
    oldpassword: string,
    newpassword: string,
    newpasswordconfirmation: string,
  ) =>
    request(
      PUT(
        `/api/v2.0/user/${userId}/changepassword`,
        {
          oldpassword,
          newpassword,
          newpasswordconfirmation,
        },
        null,
        {
          headers: applyAuthHeader(defaultHeaders, getToken()),
        }
      )
    ).then((res) => res.json());

  const putResetPassword = (key: string, password: string) =>
    request(
      PUT(
        `/api/v2.0/user/password/${key}`,
        { password },
        null,
        { headers: defaultHeaders },
      )
    ).then(() => true);

  return {
    getPinAuthentication,
    getPin,
    getIsUsernameAvailable,
    getRegistration,
    postLogin,
    postAdhocLogin,
    postAdhocWorkflow,
    postCheckUsername,
    postResetPassword,
    postRegistration,
    putUserChangePassword,
    putResetPassword,
  };
}
