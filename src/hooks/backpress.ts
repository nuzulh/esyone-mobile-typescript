import { useEffect } from "react";
import { BackHandler } from "react-native";

export function useBackpress(cb: () => boolean) {
  const triggerBackpress = () => {
    cb();
  };

  useEffect(() => {
    const handler = BackHandler.addEventListener("hardwareBackPress", () =>
      cb()
    );

    return () => handler.remove();
  }, []);

  return {
    triggerBackpress,
  };
}
