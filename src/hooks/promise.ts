import { useContext, useEffect, useState } from "react";
import { Services } from "../services";

export function usePromise<T>(
  callFuture: () => Promise<T>,
  initialValue?: T,
  dependencies?: any[]
) {
  const services = useContext(Services);
  const [result, setResult] = useState(initialValue);
  const [error, setError] = useState();

  useEffect(() => {
    callFuture()
      .then(setResult)
      .catch((error) => {
        setError(error);
        services.logService.error(error);
      });

    return () => {
      setResult(initialValue);
      setError(null);
    };
  }, dependencies);

  return { result, error };
}
