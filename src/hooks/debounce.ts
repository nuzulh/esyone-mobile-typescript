import { useEffect } from "react";

export function useDebounce(cb: () => void, ms: number, dependencies: any[]) {
  useEffect(() => {
    const t = setTimeout(() => cb(), ms);

    return () => {
      clearTimeout(t);
    };
  }, dependencies);
}
