import { useContext, useEffect, useState } from "react";
import RNFetchBlob from "rn-fetch-blob";
import { Services } from "../services";

export function useFileReader(
  path: string,
  encoding: string = "base64",
  dependencies?: any[]
) {
  const services = useContext(Services);
  const [content, setContent] = useState<string>(null);
  const [error, setError] = useState<Error>(null);

  useEffect(() => {
    if (!path) return;

    setContent(null);
    setError(null);

    RNFetchBlob.fs
      .readFile(path.replace("file://", ""), encoding as any)
      .then(setContent)
      .catch((error) => {
        setError(error);
        services.logService.error(error);
      });
  }, dependencies || []);

  return { content, error };
}
