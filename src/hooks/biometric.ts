import { useCallback, useContext, useReducer } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  BiometricNotSupported,
  hideLoadingAction,
  showLoadingAction,
} from "../helpers";
import { RootState } from "../models";
import { Services } from "../services";

declare type BiometricStatus = {
  isInProgress: boolean;
  isSupported: boolean;
  isSucces: boolean;
  isError: boolean;
  error: Error;
};

const defaultBiometricStatus: BiometricStatus = {
  isInProgress: false,
  isSupported: false,
  isSucces: false,
  isError: false,
  error: null,
};

declare type DispatchStatusAction = {
  type: "start" | "success" | "failed" | "update" | "callback";
  payload?: BiometricStatus | Error | Function | null;
};

export function useBiometric(
  initialValue?: Pick<BiometricStatus, "isSupported">
) {
  const dispatch = useDispatch();
  const settings = useSelector((state: RootState) => state.esy.settings);
  const [biometricStatus, dispatchStatus] = useReducer(biometricReducer, {
    ...defaultBiometricStatus,
    ...{ isSupported: settings.isBiometricSupported },
    ...(initialValue || {}),
  });
  const tryValidate = useCallback(createValidate(), []);

  const validate = (cb?: (status: BiometricStatus) => void) => {
    dispatchStatus({ type: "start" });
    dispatch(showLoadingAction());
    tryValidate()
      .then(() => dispatchStatus({ type: "success" }))
      .catch((err) => dispatchStatus({ type: "failed", payload: err }))
      .finally(() => {
        if (!cb) return;

        dispatchStatus({ type: "callback", payload: cb });
        dispatch(hideLoadingAction());
      });
  };

  return { biometricStatus, validate };
}

function createValidate() {
  const services = useContext(Services);
  const authState = useSelector((state: RootState) => state.esy.auth);

  return async function () {
    if (
      !authState ||
      !authState.session ||
      !authState.session.offlineCredential ||
      !services.touchIdService.isSupported()
    )
      throw new BiometricNotSupported();

    const isSuccess = await services.touchIdService.authenticate();

    return isSuccess;
  };
}

function biometricReducer(
  oldState: BiometricStatus,
  action: DispatchStatusAction
): BiometricStatus {
  switch (action.type) {
    case "start":
      return {
        ...defaultBiometricStatus,
        isInProgress: true,
      };
    case "success":
      return {
        ...defaultBiometricStatus,
        isSucces: true,
        isInProgress: false,
      };
    case "failed":
      return {
        ...defaultBiometricStatus,
        isInProgress: false,
        isError: true,
        error: action.payload as Error,
      };
    case "update":
      return {
        ...defaultBiometricStatus,
        ...action.payload,
      };
    case "callback":
      const cb = action.payload as Function;
      cb(oldState);
      return oldState;
    default:
      return oldState;
  }
}
