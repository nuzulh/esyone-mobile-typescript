import { useMemo } from "react";
import { useSelector } from "react-redux";
import { RootState, WorkflowFormElement } from "../models";

export function useAnmeldeValue(element: WorkflowFormElement) {
  const profile = useSelector((state: RootState) => state.esy.auth?.profile);
  const defaultValue = useMemo(() => {
    if (!profile) return null;

    switch (element.name) {
      case "m_vorname":
        return profile.firstName;

      case "m_name":
        return profile.lastName;

      case "m_email":
        return profile.email;

      case "m_handy":
        return profile.mobile;

      default:
        return null;
    }
  }, []);

  return defaultValue;
}
