import { useContext, useMemo } from "react";
import { ParseShape } from "react-native-parsed-text";
import { useDispatch } from "react-redux";
import { openExternalLinkAction } from "../helpers";
import { Services } from "../services";

export function useMessageParser() {
  const services = useContext(Services);
  const dispatch = useDispatch();
  const specs = useMemo<ParseShape[]>(
    () => [
      {
        pattern: /https:\/\/app\.esything\.io\/externalform\/.*/,
        style: {
          color: "#0000FF",
          textDecorationLine: "underline",
        },
        onPress: (url: string) =>
          dispatch(services.guardService.evaluateWithLink(url)),
      },
      {
        pattern: /https:\/\/app\.esything\.io\/proudly-presents\/.*/,
        style: {
          color: "#0000FF",
          textDecorationLine: "underline",
        },
        onPress: (url: string) =>
          dispatch(
            services.guardService.evaluateWithLink(
              url.replace("proudly-presents", "externalform")
            )
          ),
      },
      {
        pattern: /app\.esything\.io\/externalform\/.*/,
        style: {
          color: "#0000FF",
          textDecorationLine: "underline",
        },
        onPress: (url: string) =>
          dispatch(services.guardService.evaluateWithLink(url)),
      },
      {
        pattern: /app\.esything\.io\/proudly-presents\/.*/,
        style: {
          color: "#0000FF",
          textDecorationLine: "underline",
        },
        onPress: (url: string) =>
          dispatch(
            services.guardService.evaluateWithLink(
              url.replace("proudly-presents", "externalform")
            )
          ),
      },
      {
        type: "url",
        style: {
          color: "#0000FF",
          textDecorationLine: "underline",
        },
        onPress: (url: string) => dispatch(openExternalLinkAction(url)),
      },
      {
        pattern: /0[0-9]{10,15}/,
        style: {
          color: "#0000FF",
          textDecorationLine: "underline",
        },
        onPress: (phone: string) =>
          dispatch(openExternalLinkAction(`tel:${phone}`)),
      },
      {
        type: "phone",
        style: {
          color: "#0000FF",
          textDecorationLine: "underline",
        },
        onPress: (phone: string) =>
          dispatch(openExternalLinkAction(`tel:${phone}`)),
      },
      {
        type: "email",
        style: {
          color: "#0000FF",
          textDecorationLine: "underline",
        },
        onPress: (email: string) =>
          dispatch(openExternalLinkAction(`mailto:${email}`)),
      },
    ],
    []
  );

  return {
    specs,
  };
}
