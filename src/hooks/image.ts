import { useState } from "react";
import { useSelector } from "react-redux";
import { ImageSpec, RootState } from "../models";
import { usePromise } from "./promise";

export function useImage(
  imageFactory: () => Promise<ImageSpec>,
  fallbackImage?: ImageSpec,
  dependencies?: any[]
) {
  const accessToken = useSelector((state:RootState) => state.esy.auth?.session?.accessToken)
  const imageSpec = usePromise(() => imageFactory(), fallbackImage, dependencies ? [accessToken, ...dependencies] : [accessToken]);

  return { imageUri: imageSpec.result?.uri, headers: imageSpec.result?.headers, error: imageSpec.error };
}
