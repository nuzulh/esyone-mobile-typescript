import { useSelector } from "react-redux";
import { EsyState, RootState } from "../models";

export function useEsyState<T>(callback: (_: EsyState) => T) {
  const result = useSelector((state: RootState) => callback(state.esy));

  return result;
}
