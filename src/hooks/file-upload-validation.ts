import { useContext, useEffect, useMemo, useState } from "react";
import { FileUploadInfo, MetaFile, Services } from "../services";
import { useDispatch } from "react-redux";
import { Account } from "../models";
import { hideLoadingAction, localization, showLoadingAction } from "../helpers";

export function useFileUploadValidation(
  account: Account,
  files: MetaFile[],
  dependencies?: any[]
) {
  const dispath = useDispatch();
  const services = useContext(Services);
  const [uploadInfo, setUploadInfo] = useState<FileUploadInfo | null>(null);

  useEffect(() => {
    if (uploadInfo) return;

    dispath(showLoadingAction());
    services.filesService.getUploadInfo(account.id)
      .then((res) => setUploadInfo(res))
      .catch((err) => services.logService.error(err))
      .finally(() => dispath(hideLoadingAction()));
  }, []);

  const results = useMemo(
    () => ({
      passedFiles: files.filter(
        (file) => file.size / 1000 <= uploadInfo.maxUploadSizeInKB &&
          !uploadInfo.defaultDisallowedUploadFormats.includes(file.extension)
      ),
      filesErrorMessages: files.map(
        (file) => {
          if (file.size / 1000 > uploadInfo.maxUploadSizeInKB)
            return localization.language.FileTooLarge
              .replace("{{fileName}}", file.fileName)
              .replace("{{maxSize}}", `${uploadInfo.maxUploadSizeInKB / 1000}`);
          if (uploadInfo.defaultDisallowedUploadFormats.includes(file.extension))
            return localization.language.InvalidFileExtension
              .replace("{{fileName}}", file.fileName);
        }
      ).filter((f) => !!f),
      totalFileSize: files.reduce(
        (a, b) => a + (b.size / 1000),
        0
      )
    }),
    dependencies ? dependencies : [files]
  );

  return { ...results, uploadInfo };
}
