import { useEffect, useReducer } from "react";

export declare type ApiHookOptions<T> = {
  initialValue?: T;
  dependencies?: any[];
  cache?: {
    key: string;
    isEnabled: boolean;
  };
};

export declare type ApiHookState<T> = {
  result: T;
  error: Error;
  isError: boolean;
  isInProgress: boolean;
  isSuccess: boolean;
};

declare type DispatchAction = {
  type: "fetch" | "success" | "error";
  payload?: any;
};

const defaultState: ApiHookState<any> = {
  result: null,
  error: null,
  isError: false,
  isInProgress: false,
  isSuccess: false,
};

export function useApi<T>(
  apiCall: () => Promise<T>,
  options?: ApiHookOptions<T>
): ApiHookState<T> {
  const [state, dispatch] = useReducer(apiReducer, {
    ...defaultState,
    result: options.initialValue,
  } as ApiHookState<T>);

  useEffect(() => {
    dispatch({ type: "fetch" });
    apiCall()
      .then((result) => dispatch({ type: "success", payload: result }))
      .catch((error) => dispatch({ type: "error", payload: error }));
  }, options?.dependencies || []);

  return {
    result: state.result as T,
    error: state.error,
    isError: state.isError,
    isInProgress: state.isInProgress,
    isSuccess: state.isSuccess,
  };
}

function apiReducer<T>(
  oldState: ApiHookState<T>,
  action: DispatchAction
): ApiHookState<T> {
  switch (action.type) {
    case "fetch":
      return {
        ...defaultState,
        ...oldState,
        isInProgress: true,
        isSuccess: false,
        isError: false,
        error: null,
      };
    case "success":
      return {
        ...defaultState,
        ...oldState,
        isInProgress: false,
        isSuccess: true,
        isError: false,
        error: null,
        result: action.payload as T,
      };
    case "error":
      return {
        ...defaultState,
        ...oldState,
        isInProgress: false,
        isSuccess: false,
        isError: true,
        error: action.payload as Error,
      };
    default:
      return oldState;
  }
}
