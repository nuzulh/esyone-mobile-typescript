import { useCallback, useContext, useReducer } from "react";
import { useSelector } from "react-redux";
import {
  BiometricNotSupported,
  EmptyUsernamePassword,
  OfflineAuthenticationNotSupported,
} from "../helpers";
import { AuthState, RootState } from "../models";
import { Services } from "../services";

declare type AuthStatus = {
  username: string;
  password: string;
  isInProgress: boolean;
  isError: boolean;
  isSuccess: boolean;
  error: Error;
  result: AuthState;
};

declare type DispatchStatusAction = {
  type: "start" | "success" | "failed" | "update" | "callback";
  payload?: AuthState | AuthStatus | Error | Function | null;
};

const defaultAuthStatus: AuthStatus = {
  username: null,
  password: null,
  isInProgress: false,
  isError: false,
  isSuccess: false,
  error: null,
  result: null,
};

export function useAuth(
  initialValue?: Pick<AuthStatus, "username" | "password">
) {
  const [authStatus, dispatchStatus] = useReducer(authStatusReducer, {
    ...defaultAuthStatus,
    ...(initialValue || {}),
  });
  const tryLogin = useCallback(createLogin(), []);
  const tryBiometricLogin = useCallback(createBiometricLogin(), []);

  const setUsername = (username: string) =>
    dispatchStatus({ type: "update", payload: { username } as AuthStatus });

  const setPassword = (password: string) =>
    dispatchStatus({ type: "update", payload: { password } as AuthStatus });

  const login = (cb?: () => void) => {
    dispatchStatus({ type: "start" });
    tryLogin(authStatus.username, authStatus.password)
      .then((authState) =>
        dispatchStatus({ type: "success", payload: authState })
      )
      .catch((err) => dispatchStatus({ type: "failed", payload: err }))
      .finally(() => {
        if (!cb) return;

        dispatchStatus({ type: "callback", payload: cb });
      });
  };

  const biometricLogin = (cb?: () => void) => {
    dispatchStatus({ type: "start" });
    tryBiometricLogin()
      .then((authState) =>
        dispatchStatus({ type: "success", payload: authState })
      )
      .catch((err) => dispatchStatus({ type: "failed", payload: err }))
      .finally(() => {
        if (!cb) return;

        dispatchStatus({ type: "callback", payload: cb });
      });
  };

  return {
    authStatus,
    login,
    biometricLogin,
    setUsername,
    setPassword,
  };
}

function createLogin() {
  const services = useContext(Services);
  const appState = useSelector((state: RootState) => state.esy.appState);
  const authState = useSelector((state: RootState) => state.esy.auth);

  return async function (username: string, password: string) {
    if (!username || username === "" || !password || password === "")
      throw new EmptyUsernamePassword();

    if (
      appState.isOffline &&
      (!authState || !authState.session || !authState.session.offlineCredential)
    )
      throw new OfflineAuthenticationNotSupported();

    if (appState.isOffline)
      return await services.guardService.offlineLogin(
        username,
        password,
        authState.session.offlineCredential
      );

    return await services.guardService.login(username, password);
  };
}

function createBiometricLogin() {
  const services = useContext(Services);
  const appState = useSelector((state: RootState) => state.esy.appState);
  const authState = useSelector((state: RootState) => state.esy.auth);

  return async function () {
    if (
      appState.isOffline ||
      !authState.session ||
      !authState.session.offlineCredential
    )
      throw new BiometricNotSupported(); // cannot do biometric login in offline mode

    return await services.guardService.biometricLogin(
      authState.session.offlineCredential
    );
  };
}

function authStatusReducer(
  oldState: AuthStatus,
  action: DispatchStatusAction
): AuthStatus {
  // console.log(authState);

  switch (action.type) {
    case "start":
      return {
        ...defaultAuthStatus,
        isInProgress: true,
        isSuccess: false,
        isError: false,
        result: null,
      };
    case "success":
      return {
        ...defaultAuthStatus,
        isInProgress: false,
        isSuccess: true,
        result: action.payload as AuthState,
      };
    case "failed":
      return {
        ...defaultAuthStatus,
        username: oldState.username,
        password: oldState.password,
        isInProgress: false,
        isError: true,
        error: action.payload as Error,
      };
    case "update":
      return {
        ...oldState,
        ...action.payload,
      };
    case "callback":
      const cb = action.payload as Function;
      cb(oldState);
      return oldState;
    default:
      return oldState;
  }
}
