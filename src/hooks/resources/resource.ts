import { useCallback, useContext, useEffect, useMemo, useReducer } from "react";
import { AVAILABLE_KEYS, config } from "../../helpers";
import { Services } from "../../services";

export declare type ResourceData<T> = {
  [key: string]: T;
};

export declare type ResourceFetcher<T> = (
  type: "get" | "add" | "update" | "remove",
  params?: any,
  data?: any
) => Promise<ResourceData<T>>;

export declare type ResourceState<T extends unknown> = {
  data: ResourceData<T>;
  dataAsList: T[];
  offset: number;
  size: number;
  isInProgress: boolean;
  isSuccess: boolean;
  isError: boolean;
  error: Error;
};

export declare type ResourceHook<T extends unknown> = ResourceState<T> & {
  fetch(page?: number): void;
  fetchNext(): void;
  fetchPrev(): void;
  add(item: T): void;
  update(item: T): void;
  remove(item: T): void;
};

export declare type DispatchResourceAction<T extends unknown> = {
  type: "start" | "success" | "failed" | "offset";
  payload?: ResourceData<T> | number | Error;
};

const defaultState: ResourceState<any> = {
  data: {},
  dataAsList: [],
  offset: 0,
  size: 50,
  isInProgress: false,
  isSuccess: false,
  isError: false,
  error: null,
};

export declare type ResourceHookOptions = {
  defaultPage: number;
  defaultSize: number;
  cache?: {
    key: string;
    isEnabled: boolean;
  };
};

export function useResource<T extends unknown>(
  fetcher: ResourceFetcher<T>,
  options: ResourceHookOptions
): ResourceHook<T> {
  options = options || {
    defaultPage: 0,
    defaultSize: parseInt(config.get(AVAILABLE_KEYS.API_PAGE_SIZE)),
  };

  const [resourceStatus, dispatch] = useReducer(resourceReducer, {
    ...defaultState,
    offset: options.defaultPage * options.defaultSize,
    size: options.defaultSize,
  });

  const fetch = (page?: number) => {
    page = page < 0 ? 0 : page;
    const offset = page ? page * resourceStatus.size : resourceStatus.offset;
    dispatch({ type: "start" });
    fetcher("get", {
      offset: offset,
      size: resourceStatus.size,
    })
      .then((result) => dispatch({ type: "success", payload: result }))
      .then(() => dispatch({ type: "offset", payload: offset }))
      .catch((err) => dispatch({ type: "failed", payload: err }));
  };

  const fetchNext = () =>
    fetch(resourceStatus.offset / resourceStatus.size + 1);

  const fetchPrev = () =>
    fetch(resourceStatus.offset / resourceStatus.size - 1);

  const add = (item: T) => {
    dispatch({ type: "start" });
    fetcher("add", null, item)
      .then((result) => dispatch({ type: "success", payload: result }))
      .catch((err) => dispatch({ type: "failed", payload: err }));
  };

  const update = (item: T) => {
    dispatch({ type: "start" });
    fetcher("update", null, item)
      .then((result) => dispatch({ type: "success", payload: result }))
      .catch((err) => dispatch({ type: "failed", payload: err }));
  };

  const remove = (item: T) => {
    dispatch({ type: "start" });
    fetcher("remove", null, item)
      .then((result) => dispatch({ type: "success", payload: result }))
      .catch((err) => dispatch({ type: "failed", payload: err }));
  };

  return {
    data: resourceStatus.data as ResourceData<T>,
    dataAsList: resourceStatus.dataAsList as T[],
    offset: resourceStatus.offset,
    size: resourceStatus.size,
    error: resourceStatus.error,
    isError: resourceStatus.isError,
    isSuccess: resourceStatus.isSuccess,
    isInProgress: resourceStatus.isInProgress,
    fetch,
    fetchNext,
    fetchPrev,
    add,
    update,
    remove,
  };
}

function resourceReducer<T extends unknown>(
  oldState: ResourceState<T>,
  action: DispatchResourceAction<T>
): ResourceState<T> {
  switch (action.type) {
    case "start":
      return {
        ...defaultState,
        ...oldState,
        isInProgress: true,
        isSuccess: false,
        isError: false,
        error: null,
      };
    case "success":
      const data = oldState.data;
      const dataAsList = [];
      let newData = action.payload as ResourceData<T>;
      // console.log("new data", newData);

      if (data) for (let key in newData) data[key] = newData[key];

      for (let key in data) dataAsList.push(data[key]);

      return {
        ...defaultState,
        ...oldState,
        data,
        dataAsList,
        isInProgress: false,
        isSuccess: true,
        isError: false,
        error: null,
      };
    case "failed":
      return {
        ...defaultState,
        ...oldState,
        isInProgress: false,
        isSuccess: false,
        isError: true,
        error: action.payload as Error,
      };
    case "offset":
      return {
        ...oldState,
        offset: action.payload as number,
      };
    default:
      return oldState;
  }
}
