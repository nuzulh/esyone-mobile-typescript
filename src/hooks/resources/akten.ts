import { useContext, useEffect, useMemo } from "react";
import { Akte } from "../../models";
import { Services } from "../../services";
import {
  ResourceData,
  ResourceHook,
  ResourceHookOptions,
  useResource,
} from "./resource";

//   const sortByDate = dataDummyAkten.sort(function (a, b) {
//     return new Date(b.lastMessageDate) - new Date(a.lastMessageDate);
//   });
//   const sortUnReadMessage = sortByDate.sort(function (c, d) {
//     return d.totalUnreadMessage - c.totalUnreadMessage;
//   });

export function useAkten(options?: ResourceHookOptions): ResourceHook<Akte> {
  const fetcher = createFetcher();
  const aktenHook = useResource(fetcher, options);
  const dataAsList = useMemo(
    () =>
      aktenHook.dataAsList
        .sort(
          (a, b) => b.lastMessageDate.getTime() - a.lastMessageDate.getTime()
        )
        .sort((a, b) => b.totalUnreadMessage - a.totalUnreadMessage),
    [aktenHook.dataAsList]
  );

  // initiate first load
  useEffect(() => {
    aktenHook.fetch(0);
  }, []);

  return {
    ...aktenHook,
    dataAsList,
  };
}

function createFetcher() {
  const services = useContext(Services);

  return async function (
    type: "get" | "add" | "update" | "remove",
    params?: any,
    data?: any
  ): Promise<ResourceData<Akte>> {
    switch (type) {
      case "get":
        return await services.aktenService.getAll(params);
      default:
        throw new Error("method not supported");
    }
  };
}
