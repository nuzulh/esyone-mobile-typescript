import { createContext } from "react";
import { Account } from "../models";

export const AccountContext = createContext({} as { account?: Account });
