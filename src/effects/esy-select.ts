import { select } from "redux-saga/effects";
import { RootState, EsyState } from "../models";

export function* esySelect<T>(callback: (_: EsyState) => T) {
  return yield select((state: RootState) => callback(state.esy));
}
