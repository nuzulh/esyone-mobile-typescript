export const STACKS = {
  SPLASH_STACK: "Splash",
  ONBOARDING_STACK: "OnBoarding",
  WELCOME_STACK: "Welcome",
  AUTH_STACK: "Auth",
  REGISTRATION_STACK: "Registration",
  HOME_STACK: "Home",
  ACTIVATION_STACK: "Activation",
  PUBLIC_STACK: "Public",
};

export const WELCOME_STACK = {
  WELCOME_SCREEN: "ScreenWelcome",
};

export const SHARED_STACKS = {
  FORMULA_SCREEN: "ScreenFormula",
  FORMULA_SCREEN_SUCCESS: "ScreenFormulaSuccess",
  FORMULA_SCREEN_ANSWER: "ScreenFormulaAnswer",
  PROVIDER_SCREEN: "ScreenProviderDetail",
  PREVIEW_SCREEN: "ScreenPreview",
  WEBVIEW_SCREEN: "ScreenWebview",
};

export const SPLASH_STACK = {
  // PROVIDER_DETAIL_SCREEN: "ScreenProviderDetail",
  // FORMULA_SCREEN: "ScreenFormula",
  SPLASH_SCREEN: "ScreenSplash",
};

export const AUTH_STACK = {
  SIGN_IN_SCREEN: "ScreenSignIn",
  FORGOT_SCREEN: "ScreenForgot",
  FORGOT_USERNAME_SCREEN: "ScreenForgotUsername",
  FORGOT_PASSWORD_SCREEN: "ScreenForgotPassword",
  // BIOMETRIC_SCREEN: "ScreenSignInActivatedBiometric",
  // PUSHNOTIF_SCREEN: "ScreenSignInPushNotif",
  ON_BOARDING_SCREEN: "ScreenOnBoarding",
  UNAUTHORIZED: "ScreenUnauthorized",
  // GAIN_ACCESS_SCREEN: "ScreenGainAccess",
  PIN_SCREEN: "ScreenPin",
  RESET_PASSWORD_SCREEN: "ScreenResetPassword",
};

export const REGISTRATION_STACK = {
  STARTED_SCREEN: "ScreenRegistrationStarted",
  USERNAME_SCREEN: "ScreenRegistrationUsername",
  PASSWORD_SCREEN: "ScreenRegistrationPassword",
  CONGRATS_SCREEN: "ScreenRegistrationCongrats",
  ...SHARED_STACKS,
};

export const HOME_STACK = {
  HOME_SCREEN: "ScreenHome",
  MESSAGE_SCREEN: "ScreenMessage",
  COMPOSE_MESSAGE_SCREEN: "ScreenComposeMessage",
  CHANGE_PASSWORD_SCREEN: "ScreenChangePassword",
  ...SHARED_STACKS,
};

export const ACTIVATION_STACK = {
  BIOMETRIC_SCREEN: "ScreenBiometricActivation",
  PUSHNOTIF_SCREEN: "ScreenPushNotifActivation",
};

export const PUBLIC_STACK = {
  ...SHARED_STACKS,
};
