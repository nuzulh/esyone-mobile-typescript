import * as R from "ramda";
import config, { AVAILABLE_KEYS } from "./config";
import { ApiError, InvalidPIN, NotAuthorized } from "./errors";

export const getPath = (host: string) => (basePath: string) =>
  `${host}${basePath}`;

export const getPathFrom = getPath(
  config.getOrElse(AVAILABLE_KEYS.BASE_URL, "'https://secure.webakte.de")
);

export const downloadFrom = (path: string) => {
  const targetUrl = getPathFrom(path);

  window.open(targetUrl);
};

export const BuildFileData = (key: string, file: any) => {
  const fd = new FormData();
  fd.append(key, file);

  return fd;
};

function normalizeQuery(params: any) {
  for (const key in params) {
    if (!params[key]) delete params[key];
    else if (typeof params[key] === "object")
      params[key] = JSON.stringify(params[key]);
  }

  return params;
}

const applyOptions = R.mergeDeepLeft<any>({
  headers: {
    "Content-Type": "application/json",
    // Authorization: `Bearer ${getToken()}`,
  },
});

const applyUrl = (path: string) => new URL(path);

const applyParams = (path: string, params: any): URL | string =>
  R.compose(
    // R.assoc("search", new URLSearchParams(normalizeQuery(params)).toString()),
    // R.tap(
    //   (url) =>
    //     (url.search = new URLSearchParams(normalizeQuery(params)).toString())
    // ),
    (url) => {
      const queryString = new URLSearchParams(
        normalizeQuery(params)
      ).toString();
      // console.log(params, normalizeQuery(params), url.toString());
      return `${url.href}${queryString ? `?${queryString}` : ""}`;
    },
    applyUrl
  )(path);

const applyData = (
  method: string,
  data: any,
  contentType: string = "application/json"
) =>
  R.compose(
    R.mergeDeepLeft<any>({
      method,
      headers: {
        "Content-Type": contentType,
      },
      body: data,
    }),
    applyOptions
  );

export const GET = (path: string, params?: any, options: any = {}) => ({
  url: { path, params },
  options: applyOptions(options),
});

export const POST = (
  path: string,
  data?: any,
  params?: any,
  options: any = {}
) => ({
  url: { path, params },
  options: applyData("POST", JSON.stringify(data))(options),
});

export const POST_FORM = (
  path: string,
  data?: any,
  params?: any,
  options: any = {}
) => ({
  url: { path, params },
  options: {
    method: "POST",
    body: data,
    ...options,
  },
});

export const PUT = (
  path: string,
  data?: any,
  params?: any,
  options: any = {}
) => ({
  url: { path, params },
  options: applyData("PUT", JSON.stringify(data))(options),
});

export const DELETE = (
  path: string,
  data?: any,
  params?: any,
  options: any = {}
) => ({
  url: { path, params },
  options: applyData("DELETE", JSON.stringify(data))(options), //applyOptions(R.mergeDeepRight(options, { method: "DELETE" })),
});

export const execFetch =
  (basePath: string) =>
    ({ url, options }) => {
      const { debug, ...httpOptions } = options;
      if (debug) {
        console.log(
          "execFetch",
          url.params
            ? applyParams(`${basePath}${url.path}`, url.params).toString()
            : applyUrl(`${basePath}${url.path}`).toString(),
          url.params
        );
      }

      return fetch(
        url.params
          ? applyParams(`${basePath}${url.path}`, url.params).toString()
          : applyUrl(`${basePath}${url.path}`).toString(),
        httpOptions
      ).then((res) => {
        if (res.status == 404) throw new Error(`Not found: ${res.url}`);
        if (res.status == 401 || res.status == 423)
          return res.text().then((v) => {
            throw new NotAuthorized(
              res.url,
              {
                params: url.params
                  ? applyParams(`${basePath}${url.path}`, url.params).toString()
                  : applyUrl(`${basePath}${url.path}`).toString(),
                ...(httpOptions || {}),
              },
              JSON.parse(v).reasonKey || "UnknownError"
            );
          });

        if (res.status == 410)
          throw new InvalidPIN(
            `PIN is invalid or not found: ${res.url}\n${JSON.stringify(res)}`
          );

        if (res.status != 200 && res.status != 201 && res.status != 204)
          return res.text().then((v) => {
            throw new ApiError(
              res.status,
              url.params
                ? applyParams(`${basePath}${url.path}`, url.params).toString()
                : applyUrl(`${basePath}${url.path}`).toString(),
              httpOptions || {},
              v || "Unknown API Error"
            );
          });

        if (res.status == 204)
          return new Response(JSON.stringify({}), {
            status: 200,
            statusText: "OK",
          });

        // return res
        //   .text()
        //   .then((v) => ({ status: res.status, content: v } as any));

        // return { status: res.status, content: res } as any;
        return res;
      });
      // .then(({ status, content, ...attrs }) => {
      //   if (status != 200 && status != 201) {
      //     console.log("content error", status, content);
      //     // console.error(content, attrs);
      //     throw new Error(content?.message || content);
      //   }

      //   return content;
      // });
    };
