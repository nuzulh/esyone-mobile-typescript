import JWT from "expo-jwt";
import Base64 from "crypto-js/enc-base64";
import Utf8 from "crypto-js/enc-utf8";

export function checkTokenIsValid(accessToken: string): boolean {
  if (!accessToken) return false;

  try {
    const decodedAccessToken = decodeToken<any>(accessToken);
    const currentTime = Date.now() / 1000;
    // console.log(decodedAccessToken.exp, currentTime);
    // console.log(currentTime < decodedAccessToken.exp);
    // console.log("currentTime", currentTime, "expTime", decodedAccessToken.exp);
    // if (currentTime > decodedAccessToken.exp) return false;

    return currentTime < decodedAccessToken.exp;
  } catch (err) {
    // console.error(err);
    return false;
  }
}

export function decodeToken<T extends unknown>(accessToken: string): T {
  if (!accessToken) return null;

  try {
    const tokens = accessToken.split(".");

    return JSON.parse(Base64.parse(tokens[1]).toString(Utf8));
  } catch (err) {
    return null;
  }
}
