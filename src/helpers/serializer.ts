export function serialize(object: any) {
  if (!object) return null;

  if (Array.isArray(object)) return object.map((value) => serialize(value));

  if (typeof object !== "object") return object;

  const tmp = {};
  for (let key in object) {
    let field = object[key];

    if (!field) continue;

    if (Array.isArray(field)) field = field.map((value) => serialize(value));
    else if (typeof field === "function") continue;
    else if (field instanceof Date)
      field = {
        type: "Date",
        value: field.getTime(),
      };
    else if (typeof field === "object") field = serialize(field);

    tmp[key] = field;
  }

  return tmp;
}

export function deserialize(object: any) {
  if (!object) return null;

  if (Array.isArray(object)) return object.map((value) => deserialize(value));

  if (typeof object !== "object") return object;

  if (object.type === "Date") return new Date(object.value);

  const tmp = {};
  for (let key in object) {
    let field = object[key];

    if (!field) continue;

    if (Array.isArray(field)) field = field.map((value) => deserialize(value));
    else if (typeof field === "function") continue;
    else if (typeof field === "object" && field.type === "Date")
      field = new Date(field.value);
    else if (typeof field === "object") field = deserialize(field);

    tmp[key] = field;
  }

  return tmp;
}
