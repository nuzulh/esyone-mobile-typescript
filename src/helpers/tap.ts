export function tap<T>(logger: (input: T) => Promise<void>) {
  return function (input: T) {
    return logger(input).then(() => input);
  };
}
