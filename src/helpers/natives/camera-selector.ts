import { Platform } from "react-native";
import { launchCamera } from "react-native-image-picker";
import { MetaFile } from "../../services";

export function selectCamera(
  cb: (files: Partial<MetaFile>[], error: Error) => void
) {
  launchCamera(
    {
      mediaType: "photo",
      quality: 1.0,
      maxWidth: 1200,
      maxHeight: 1200,
      saveToPhotos: true,
      cameraType: "back",
    },
    (response) => {
      if (response.didCancel) {
        cb(null, new Error("User cancelled camera"));
      } else if (response.errorCode === "camera_unavailable") {
        cb(null, new Error("Camera not available on device"));
      } else if (response.errorCode === "permission") {
        cb(null, new Error("Permission not satisfied"));
      } else if (response.errorCode === "others") {
        cb(null, new Error(response.errorMessage));
      } else if (!response.assets || response.assets.length === 0) {
        cb(null, new Error("No file selected"));
      } else {
        const metaFiles: Partial<MetaFile>[] = [
          {
            id: Date.now(),
            filePath: response.assets[0].uri,
            fileName: response.assets[0].fileName,
            size: response.assets[0].fileSize,
            contentType: response.assets[0].type,
            extension: response.assets[0].fileName.split(".").pop(),
          },
        ];
        cb(metaFiles, null);
      }
    }
  );
}
