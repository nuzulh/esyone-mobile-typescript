import { launchImageLibrary } from "react-native-image-picker";
import { MetaFile } from "../../services";

export function selectPhotos(
  cb: (files: Partial<MetaFile>[], error: Error) => void
) {
  launchImageLibrary(
    {
      mediaType: "photo",
      quality: 1.0,
      maxWidth: 1200,
      maxHeight: 1200,
      selectionLimit: 0,
    },
    (response) => {
      if (response.didCancel) {
        cb(null, new Error("User cancelled image picker"));
      } else if (response.errorCode) {
        cb(null, new Error(response.errorMessage));
      } else {
        const metaFiles = response.assets.map((file, index) => ({
          id: Date.now() + index,
          filePath: file.uri,
          fileName: file.fileName,
          size: file.fileSize,
          contentType: file.type,
          extension: file.fileName.split(".").pop(),
        }));

        cb(metaFiles, null);
      }
    }
  );
}
