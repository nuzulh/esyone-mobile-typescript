import { MetaFile } from "../../services";
import DocumentPicker from "react-native-document-picker";

export function selectFiles(
  cb: (files: Partial<MetaFile>[], error: Error) => void
) {
  DocumentPicker.pickMultiple({
    presentationStyle: "formSheet",
    copyTo: "documentDirectory",
    type: [DocumentPicker.types.allFiles],
    mode: "import",
  })
    .then((files) => {
      // console.log(files);
      const metaFiles = files.map((file) => ({
        id: Date.now(),
        filePath: file.fileCopyUri,
        fileName: file.name,
        size: file.size,
        contentType: file.type,
        extension: file.name.split(".").pop(),
      }));

      cb(metaFiles, null);
    })
    .catch((error) => {
      cb(null, error);
    });
}
