import moment from "moment";
import localization from "./localization";

export function formatDateAndTimeCasesDetail(lang, objectDate) {
  if (lang == undefined || lang == "de") {
    var dataDate = moment(objectDate).format("DD.MM.YYYY");
    var dataTime = moment(objectDate).format("HH.mm");
    return String(dataDate + "  " + dataTime);
  } else {
    var dataDate = moment(objectDate).format("M/D/YYYY");
    var dataTime = moment(objectDate).format("hh:mm A");
    return String(dataDate + "  " + dataTime);
  }
}

export function formatInitials(name) {
  if (name == undefined) {
    return "";
  }
  var initials = name.match(/\b\w/g) || [];
  initials = ((initials.shift() || "") + (initials.pop() || "")).toUpperCase();
  return initials;
}

export function formatTimelineDate(lang: string, targetDate: Date | string) {
  var currentDateEn = moment().format("M/D/YYYY");
  var currentDateDe = moment().format("DD.MM.YYYY");
  // console.log('---- FormatTimelineDate: lang: ' + lang);
  if (lang == undefined || lang == "de") {
    var dataDate = moment(targetDate).format("DD.MM.YYYY");
    var dataTime = moment(targetDate).format("HH:mm");
    return String(
      (currentDateDe == dataDate ? localization.language.today : dataDate) +
        ", " +
        dataTime +
        " Uhr"
    );
  } else {
    var dataDate = moment(targetDate).format("M/D/YYYY");
    var dataTime = moment(targetDate).format("hh:mm A");
    return String(
      (currentDateEn == dataDate ? localization.language.today : dataDate) +
        ", " +
        dataTime
    );
  }
}

export function formatNumber(number) {
  if (number == undefined) {
    return "";
  }
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

export function extractHeaders(headers) {
  const result = {};
  if (!headers) return result;
  for (const key in headers) {
    result[key.toLowerCase()] = headers[key];
  }
  return result;
}

export function formatMobileNumber(number: string) {
  if (number == undefined) {
    return "";
  }
  return number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  // if (!phoneNumberText) return "";

  // let cleaned = phoneNumberText.replace(/\D/g, "");
  // cleaned = parseInt(cleaned).toString();
  // const match = cleaned.match(/^(\d{3})(\d+)$/);

  // if (match) return match[1] + " " + match[2];

  // return "";
}

// export function formatMobileNumber
