import { compose, isEmpty, pickBy } from "ramda";

export const filterNull = pickBy((val) => !!val);

export const filterEmpty = pickBy((val) => !isEmpty(val));

export const filterNullAndEmpty = compose(filterNull, filterEmpty);
