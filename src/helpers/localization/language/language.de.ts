export default {
  //Auth-Screen
  TypeUsername: "Benutzername",
  TypePassword: "Passwort",

  //Forgot-Username-Screen

  TitleHeaderForgotUsername: "Benutzername vergessen",
  TitleHeaderForgotPassword: "Passwort vergessen",
  PlaceHolderForgotUsername: "Benutzername",

  //Change-Password-Screen

  CurrentPassword: "Altes Passwort",
  NewPassword: "Neues Passwordt",
  ConfirmNewPassword: "Passwort bestätigen",
  ChangePassword: "Passwort speichern",
  PasswordConfirmRequired: "Eine Passwortbestätigung ist erforderlich",

  //Registration

  PasswordDontMatch: "Passwörter stimmen nicht überein",
  PasswordRequired: "Passwort wird benötigt",

  // Tabs-Main
  TabHome: "Home",
  TabCases: "Akten",
  TabProviders: "Account",
  TabAccount: "Einstellungen",
  TabExplore: "Entdecken",

  //error-Mesasage
  erorFull: "Anfrage ist voll",
  ErrorNoConnection: "Keine Internetverbindung",

  // Tabs-Akten
  TabAktenNew: "Nachricht schreiben",
  TabAktenEmpty: "Es sind keine Nachrichten vorhanden",
  TabAktenUpdates: "Nachrichten",
  TabAktenData: "Dateien",
  TabAktenFavorites: "Favoriten",
  TabAktenDataEmpty: "Es sind keine Dateien vorhanden",
  TabAktenFavoriteEmpty: "Es sind keine Favoriten vorhanden",
  TabAktenFolderEmpty: "Keine Ordner vorhanden",
  TabAktenFolders: "Ordner",
  TabAktenCasesDetail: "Übersicht",
  TabAktenTask: "Aufgaben",
  CasesDetailAccount: "Account",
  CasesDetailEditor: "Bearbeiter",

  Scroll: "Scrollen",

  // Status
  CaseReported: "Fall Gemeldet",
  CaseUnderReview: "Fall Wird Geprüft",
  CaseInProgress: "Fall in Bearbeitung",
  CaseClosed: "Fall Abgeschlossen",

  //Status Description
  YourCaseHasBentSent: "Ihr Fall wurde erfolgreich versendet",
  ClientAccPending: "Mandatsannahme steht aus",
  YourCaseHasBentAcc: "Ihr Fall wurde angenommen",
  YourCaseWillbe: "Ihr Fall wird von uns bearbeitet ",

  //Warning Card
  YourHelpIsNeeded: "Ihre Mithilfe wird benötigt",
  InOrdertoBeable:
    "Um ihren Fall weiterverfolgen zu können benotigen wir noch eine angaben von Ihnen",
  ToYourTask: "Zu Ihrer Aufgabe",

  // Aufabe list :
  Untill: "bis",
  Expired: "deaktiviert",
  Submittedat: "erfasst em",

  //Aufabe status
  All: "Alle",
  OpenAufgabe: "Erfasst",
  ExpiredAufgabe: "Abgelaufen",
  OpenAufabe: "Offen",
  Closed: "Erfasst",
  // Expired: 'Abgelaufen',

  //Sign in screen

  UsernameisRequired: "Benutzername ist erforderlich",
  PasswordisRequired: "Passwort ist erforderlich",

  //Biometric

  failedMessageBiometric:
    "Authentifizierungsversuch fehlgeschlagen, bitte überprüfen Sie Ihre userId und secret",

  // Add aattachemenct

  Addattachment: "Anhang hinzufügen",
  Files: "Dateien",
  Gallery: "Galerie",
  Camera: "Kamera",
  AddFilesOrImage: "Dateien oder Bilder hinzufügen",
  FilePreviewPrepare: "Dateivorschau wird vorbereitet.",

  country: "deutsch",
  Enable: "verwenden",
  Phone: "Tel.",
  Mail: "E-Mail",
  Address: "Adresse",
  Start: "Startseite",
  Support: "Support",
  Feedback: "Feedback",
  // 'Usersettings' : 'Benutzereinstellungen',
  ScreenSetting_ChangePassword: "Neues Passwort vergeben",
  ScreenSetting_ClearCache: "Cache löschen",
  ScreenSetting_PrivacyPolicy: "Datenschutz",
  ScreenSetting_TermsAndConditions: "AGB",
  ScreenSetting_Imprint: "Impressum",
  ScreenSetting_Logout: "Ausloggen",

  Username: "Benutzername",
  Password: "Passwort",
  LogIn: "Anmelden",
  Me: "Ich",
  TimeHourSuffix: " Uhr",
  on: "vom",
  today: "heute",
  messages: "Nachrichten",
  documents: "Dokumente",
  images: "Bilder",
  favorites: "Favoriten",
  showMore: "mehr anzeigen",
  showLess: "weniger anzeigen",
  attachments: "Dateianhänge",
  detailsOfYourCase: "Details zu Ihrem Fall",
  Today: "Heute",
  LastWeek: "Letzte Woche",
  LastMonth: "Letzter Monat",
  Older: "Älter",
  AccustomedBy: "Es betreut Sie",
  CreatedOn: "Angelegt am",
  Created: "Angelegt",
  Privacy: "Datenschutz",
  PrivacyNotes: "Datenschutzhinweise",
  PrivacyNote: "Datenschutzhinweis",
  Imprint: "Impressum",
  Download: "Herunterladen",
  Print: "Drucken",
  PrintThisMessage: "Diese Nachricht drucken",
  DownloadThisMessage: "Diese Nachricht herunterladen",
  WriteAMessageHere: "Schreiben Sie eine Nachricht...",
  Send: "Absenden",
  Abort: "Abbrechen",
  UploadFile: "Datei hochladen",
  TakePicture: "Foto machen",
  andXmore: "und {{remaining}} weitere",
  show: "anzeigen",
  New: "Neu",
  AttachmentLoading: "Anhang wird geladen",
  GeneratingPDF: "PDF wird erzeugt",
  pleaseWait: "bitte warten",
  LoggingIn: "Anmeldung",
  MessageSending: "Nachricht wird gesendet...",
  UploadingFiles: "Anhänge werden verarbeitet",
  Initialize: "Anwendung wird gestartet",
  Unauthorised: "Zugriff verweigert",
  MessageSent: "Nachricht wurde gesendet",
  LoginFailed: "Benutzername oder Passwort nicht korrekt",
  SessionExpired:
    "Ihre Sitzung ist abgelaufen, bitte melden Sie sich erneut an.",
  AccessDenied: "Zugriff verweigert",
  TaskNotFound: "Keine Daten gefunden",
  CheckingPermissions: "Berechtigungen werden geprüft",
  NoMessagesFound: "Es sind keine Nachrichten vorhanden",
  NoDocumentsFound: "Es sind keine Dokumente vorhanden",
  NoFavoritesFound: "Es sind keine Favoriten vorhanden",
  EmptyTimelineMessage:
    "Hier werden allen Nachrichten angezeigt, die Sie gelesen oder an die Kanzlei gesendet haben.",
  EmptyDocumentsMessage:
    "Hier werden Dokumente aus Nachrichten angezeigt, die Sie gelesen oder an die Kanzlei gesendet haben.",
  EmptyFavoritesMessage:
    "Über das Stern-Symbol können Sie sich Nachrichten als Favoriten setzen, um diese schneller wiederzufinden. Der Stern ist oben rechts innerhalb einer Nachricht positioniert.",
  PleaseEnterData: "Bitte geben Sie einen Text ein oder machen Sie ein Foto.",
  GetPin: "PIN anfordern",
  PinSending: "PIN wird gesendet",
  EnterPinHere: "PIN hier eingeben",
  ApiErrorVirusFound: "Ein Virus wurde entdeckt. Vorgang wurde abgebrochen.",
  ApiError_VirusFound: "Ein Virus wurde entdeckt. Vorgang wurde abgebrochen.",
  ApiError_Unauthorize_Login_Attempt_Exceeded:
    "Zu viele ungültige Versuche, bitte versuchen Sie es in 30 Minuten noch einmal.",
  ApiError_Unauthorize_Wrong_Username_or_Password:
    "Zugangsdaten nicht korrekt, bitte versuchen Sie es erneut.",
  ApiError_Unauthorize_Expired_Token:
    "Ihre Sitzung ist abgelaufen, bitte melden Sie sich erneut an.",
  ApiError_Unauthorize_Token_Invalid:
    "Das gesendete Authentifizierungstoken ist nicht korrekt. Bitte versuchen Sie sich erneut anzumelden.",
  ApiError_Unauthorize_Registration_Failed: "Registrierung fehlgeschlagen",
  ApiError_Unauthorize_Forbidden_Access:
    "Zugriff verweigert. Bitte melden Sie sich erneut an.",

  ApiError_Unauthorize_Username_Taken: "Benutzername ist schon vergeben",
  ApiError_Unauthorize_Weak_Password:
    "Bitte wählen Sie ein stärkeres Passwort!",
  ApiError_Unauthorize_Passwords_Not_Matching:
    "Beide eingegebenen neuen Passwörter müssen gleich sein!",
  ApiError_Unauthorize_Passwords_Registration_Failed:
    "Registrierung fehlgeschlagen.",

  ApiError_Task_Expired:
    "Dieser Link ist nicht mehr gültig. Bitte wenden Sie sich an Ihre Kanzlei, um einen neuen Link zu erhalten.",
  ApiError_Unauthorize_Pin_Expired:
    "Diese PIN ist nicht mehr gültig. Bitte fordern Sie eine neue PIN an.",
  AccountProductLimitReached: "Zur Zeit können Sie keine Dateien hochladen. Bitte wenden Sie sich an Ihren Anwalt.",
  PleaseEnterPin: "Bitte PIN eingeben",
  LoadOlderMessages: "Ältere Nachrichten laden",
  LoadOlderDocument: "Ältere Dokumente laden",
  ApplicationBlocked: "Anwendung gesperrt!",
  NextPossibleRetry: "Nächstmöglicher Anmeldungsversuch in:",
  ContactForm: "Kontaktformular",
  ContactInformation: "Kontakt",
  Subject: "Betreff",
  YourRequest: "Ihre Anfrage",
  SubjectExample: 'z.B. "meine Supportanfrage"',
  YourRequestHere: "geben Sie hier Ihre Anfrage ein",
  TechnicalSupport: "Technischer Support",
  DownloadTeamViewer: "TeamViewer herunterladen",
  WithTheDownloadOfTheTeamViewerSoftwareLONG:
    "Mit dem Download der TeamViewer-Software bestätige ich die unten stehenden Fernwartungsbedingungen zum Online-Support.",
  YouAreAboutToEstablishLONG:
    "Sie sind im Begriff, eine gemeinsame PC-Sitzung mit einem Support-Mitarbeiter der e.Consult AG herzustellen. Bitte beachten Sie, dass Ihnen dieser Service nur während unserer Geschäftszeiten nach telefonischer Absprache mit unseren Support-Mitarbeitern zur Verfügung steht.",
  GotQuestionsLONG:
    "Haben Sie Fragen oder möchten Sie uns ein Anliegen mitteilen?",
  OurServiceTeamLONG:
    "Unser Service-Team steht Ihnen montags bis freitags von 9:00 Uhr bis 17:00 Uhr gerne zur Verfügung!",
  UserSettingsPageDescriptionLONG:
    "Die Sicherheitseinstellungen bestimmen das Sicherheitsniveau für Ihre Arbeit. Das Niveau 'einfach' sichert den Zugriff per PIN ab. Bei Einstellung des Niveau 'hoch' verwenden Sie anstelle von PIN Benutzername und Passwort.",
  SecurityLevel: "Sicherheitsniveau",
  YourCurrentSecurityLevel: "Ihr aktuelles Sicherheitsniveau",
  lowJustPin: "niedrig (nur PIN)",
  mediumUsername: "hoch (Benutzername und Passwort)",
  RaiseSecurityLevel: "Sicherheitsniveau erhöhen",
  UserSettings: "Benutzereinstellungen",
  ChooseUsername: "Benutzername wählen",
  ChoosePassword: "Passwort wählen",
  ConfirmPassword: "Passwort bestätigen",
  ReenterPassword: "Passwort erneut eingeben",
  PasswordStrength: "Passwortstärke",
  validationIsEmpty: "Pflichtfeld",
  validationPasswordMinlength: "Passwort Mindestlänge: 8 Zeichen",
  validationPasswordBlacklisted: "Passwort nicht erlaubt",
  validationPasswordUsernameIncluded: "Benutzername enthalten",
  validationDoesntMatch: "Felder stimmen nicht überein",
  validationPasswordCriterias: "Weniger als 3 Kriterien erfüllt",
  SupportFormSending: "Anfrage wird gesendet",
  supportFormSent: "Anfrage wurde übermittelt",
  validationNotChecked: "Pflichtfeld, bitte anhaken",
  IHaveReadAndAcceptedTheAGB: "Ich habe die AGB gelesen und akzeptiert",
  goOn: "weiter",
  showAgbNow: "AGB jetzt anzeigen",
  providesTheseAgbPleaseAcceptLONG:
    "stellt AGB zur Verfügung. Bitte akzeptieren Sie diese AGB, um fortzufahren.",
  TermsAndConditions: "Allgemeine Geschäftsbedinungen",
  CheckingForTermsAndConditions: "Überprüfe, ob Kanzlei AGB hinterlegt hat...",
  DownloadAgb: "AGB herunterladen",
  loading: "wird geladen",
  DownloadFile: "Datei herunterladen",
  FileXnotPreviewable: 'Keine Vorschau für die Datei "{{fileName}}" möglich.',
  passwordMinLength: "Passwort muss mindestens 8 Zeichen lang sein.",
  includesLowerCaseCharacter:
    "Mindestens ein Kleinbuchstabe muss enthalten sein.",
  includesUpperCaseCharacter:
    "Mindestens ein Großbuchstabe muss enthalten sein.",
  includesSpecialCharacter: "Mindestens ein Sonderzeichen muss enthalten sein.",
  includesNumericCharacter: "Mindestens eine Zahl muss enthalten sein.",
  passwordNotBlacklisted: "Password darf nicht trivial sein (z.B.“12345678“).",
  usernameNotIncluded:
    "Password darf nicht den von Ihnen gewählten Benutzernamen enthalten.",
  passwordCriteriaHeading: "Ihr Passwort muss 3 dieser 4 Kriterien erfüllen:",
  passwordLengthCriteriaHeading: "Außerdem muss Ihr Passwort...",
  passwordSpecialCriteriaHeading: "Ihr Passwort darf außerdem...",
  PleaseEnterYourUsernameLONG:
    "Bitte geben Sie Ihren Benutzernamen ein. Sie erhalten eine Nachricht mit einen Link, um Ihr Passwort zu erneuern.",
  PleaseEnterYourEmailOrPhoneLONG:
    "Bitte geben Sie Ihre E-Mail-Adresse oder Telefonnummer ein. Wir senden Ihnen eine Nachricht mit Ihrem Benutzernamen.",
  EmailOrPhone: "E-Mail-Adresse oder Telefonnummer",
  ForgotCredentials: "Zugangsdaten vergessen?",
  FindOutUsername: "Benutzername erfahren",
  ResetPassword: "Passwort neu vergeben",
  ResetPasswordSuccess: "Das Passwort wurde erfolgreich geändert.",
  UsernameSending: "Benutzername wird gesendet",
  MessageBeingCreated: "Nachricht wird erzeugt",
  UnknownError: "Unbekannter Fehler",
  ResettingPassword: "Passwort wird neu gesetzt",
  PleaseEnterYourNewPasswordLONG:
    "Geben Sie Ihr neues Passwort ein und bestätigen Sie dieses noch einmal. Das Passwort muss aus mindestens 8 Zeichen bestehen.",
  BackToLogin: "Zurück zur Anmeldung",
  ForgotCredentialsLONG:
    "Wenn Sie Ihre Zugangsdaten vergessen haben, können Sie diese mit den nachstehenden Funktionen wieder herstellen. Verwenden Sie hierzu die Buttons und folgen Sie den weiteren Schritten und Anweisungen.",
  ForgotCredentialsNotAvailableLONG:
    "Dieser Benutzer ist noch nicht vollständig registriert, daher existiert noch kein Benutzername bzw. Passwort.",
  PasswordLinkWillBeSent:
    "Nachricht wurde gesendet. Bitte folgen Sie dem enthaltenen Link, um Ihr Passwort zu erneuern.",
  UsernameWillBeSent: "Eine Nachricht mit Ihrem Benutzernamen wurde gesendet.",
  RequestPinAgain: "PIN erneut anfordern",
  LetsGo: "Los geht's",
  GainAccess: "Zugang erhalten",
  LoginViaSMS: "Anmelden mit SMS",
  LoginViaEmail: "Anmelden mit E-Mail",
  LoginViaUsername: "Anmelden",
  back: "zurück",
  EnterPin: "PIN eingeben",
  downloadingAttachment: "Anhang wird heruntergeladen...",
  downloadingMessage: "Nachricht wird heruntergeladen...",
  PleaseNotePasswordTipps:
    "Bitte beachten Sie die Hinweise zur Passwortsicherheit.",
  NotFound: "Kein Inhalt gefunden",
  SomethingWentWrongLONG: "Für die eingegebene URL wurde kein Inhalt gefunden.",
  "1hour": "1 Stunde",
  "2hours": "2 Stunden",
  "6hours": "6 Stunden",
  "12hours": "12 Stunden",
  "24hours": "24 Stunden",
  "3days": "3 Tage",
  "1week": "1 Woche",
  "3month": "3 Monate",
  EmailAddress: "E-Mail Adresse",
  Phonenumber: "Telefonnummer",
  EnterEmail: "E-Mail Adresse hier eingeben",
  EnterPhonenumber: "Telefonnummer hier eingeben",
  SendPinAsync: "PIN in separater Nachricht senden",
  viaSms: "via SMS",
  viaEmail: "via E-Mail",
  ForwardMessage: "Nachricht weiterleiten",
  Forward: "Weiterleiten",
  ValidUntil: "Gültigkeitsdauer des Links",
  SmartlinkDescriptionStep1LONG:
    "Wählen Sie bitte den Versandweg für die Nachricht aus.",
  SmartlinkDescriptionStep2EmailLONG:
    "Geben Sie bitte die E-Mail-Adresse des Empfängers an und bestimmen Sie, wie lange der Link für die Nachricht aufrufbar sein soll.",
  SmartlinkDescriptionStep2SmsLONG:
    "Geben Sie bitte die Mobilfunknummer des Empfängers an und bestimmen Sie, wie lange der Link für die Nachricht aufrufbar sein soll.",
  SmartlinkSending: "Nachricht wird weitergeleitet...",
  unknownError: "unbekannter Fehler",
  LogOut: "Abmelden",
  SmartlinkSent: "Nachricht wurde weitergeleitet",
  PinNotSent: "PIN konnte nicht gesendet werden.",
  FileTooLarge: "Die Datei '{{fileName}}' ist zu groß. Dateien dürfen nicht größer als {{maxSize}} MB sein",
  InvalidFileExtension: "'{{fileName}}' kann nicht hochgeladen werden, da die Datei eine nicht erlaubte Erweiterung aufweist",

  SplashText1: "Einfach.\nSchnell.\nSicher.\nEffizient.",
  SplashText2: "Wir\nbeschleunigen\nIhr Business.",
  SplashText3: "e.syOne",
  SplashPhaseOneFooterText:
    "Footer text dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor",
  SplashPhaseTwoDescriptionGeneric:
    "Aus Sicherheitsgründen und zur eindeutigen Identifizierung fordere bitte einen PIN an.\n\nPIN einfach und bequem per E-Mail oder SMS anfordern und anmelden.",
  SplashPhaseTwoDescriptionEmail:
    "Aus Sicherheitsgründen und zur eindeutigen Identifizierung fordere bitte einen PIN an.",
  SplashPhaseTwoDescriptionUsername:
    "Ihr Business wartet darauf beschleunigt zu werden. Melden Sie sich einfach und bequem mit Ihren altbekannten WebAkte-Zugangsdaten an.",
  SplashPhaseThreeDescription:
    "Bitte tragen Sie hier die per E-Mail oder SMS erhaltene 4-stellige PIN ein.",

  StaticInfoDescription:
    "Die Funktionen der App stehen Ihnen zur Verfügung, sobald Ihre Kanzlei diese aktiviert hat.\n\nBitte wenden Sie sich an Ihre Kanzlei.",
  Register: "Registrierung",
  PinWrong: "Falsche PIN",
  notYou: "nicht Sie?",
  Cancel: "Abbrechen",
  Alert: "Achtung!",
  DoYouWantToLogout: "Sie werden sich ausgeloggt",
  Continue: "Weiter",
  GreatFromnowonyoucanloginwithyour: "Super! Sie können sich jetzt mit",
  Nothanksmaybelater: "Nein danke, vielleicht später",
  Loginquickerwith: "Schneller einloggen mit",
  PleaseactivateTouchIDFaceIDonthisdevice:
    "Bitte aktivieren Sie Touch ID / Face ID auf diesem Gerät",
  verwendenloginwith: "Einloggen mit",
  einloggen: "einloggen.",
  LanguageSetting: "Sprache",
  Account: "Profil",
  PrivacyPolicy: "Datenschutz",
  AboutUs: "über uns",
  ContactUs: "Kontakt",

  //onboarding
  YouwillreceiveanSMSorEmailfromyourlawyer:
    "Sie erhalten eine SMS oder E-Mail von Ihrem Anwalt",
  FollowtheSmartlinkintheSMSorEmail:
    "Folgen Sie dem Smartlink in der SMS oder E-Mail",
  Thatsitnowyoucouldcommunicatewithyourlawyer:
    "Das war's - das e.syOne  ist für Sie startklar",
  ToYourSmartlink: "Weiter mit Einladung.",
  Doesnthavelawyer: "Noch kein Anwalt?",
  AlreadyHaveLogin: "Bereits registriert? Melden Sie sich direkt an.",
  Anyquestions: "Noch Fragen?",
  MoreatwwwesyMandantde: "Mehr unter esything.io",
  Opensmartlinkwith: "gehe zu Smartlink",
  Open: "öffnen",

  //after app authentication
  welcometo: "Herzlich willkommen zum",
  GetStarted: "Los geht`s",
  Hello: "Hallo",
  ActivateYourBiometricDefault:
    "Authentifizierung via\nFingerabdruck oder\nGesichtserkennung\ngewünscht?",
  ActivateYourBiometricFingerabdruck:
    "Authentifizierung via\nFingerabdruck\ngewünscht?",
  ActivateYourBiometricnGesichtserkennung:
    "Authentifizierung via\nGesichtserkennung\ngewünscht?",
  Aktivieren: "Aktivieren",
  NoThanks: "Nein, danke",
  SignUpNow: "Registrieren Sie sich jetzt!",
  andagaindirectaccesstoyouronlineAkte:
    "Und erhalte sofort Zugang zu deinen\nAkten",
  UsernamehasbeentakenTryanother: "Benutzername ist bereits vorhanden",
  Next: "weiter",
  JustOneMoreStepConfirmYourPassword:
    "Nur noch ein schritt -\nPasswort vergeben.",
  Passworddosentmatch: "Das Passwort stimmt nicht überein!",
  Congratulation: "Herzlichen Glückwunsch!",
  EnjoyEasyAndSecureCommunicationWith: "Jetzt einfach, schnell\nund sicher mit",
  EnjoyEasyAndSecureCommunicationWith2: "kommunizieren",
  Home: "zurück",

  ScreenAuthWelcomeAGB1: "Ich habe die",
  ScreenAuthWelcomeAGBLink: "AGB",
  ScreenAuthWelcomeAGB2: "von",
  ScreenAuthWelcomeAGB3: "gelesen Und bin damit einverstanden!",
  ScreenAuthDatenschutz1: "Informieren Sie sich gerne weiter unter dem",
  ScreenAuthDatenschutzLink: "Datenschutzhinweis",
  ScreenAuthDatenschutz3: "der Kanzlei!",
  ScreenAuthDatenschutz: "Datenschutzhinweis",
  ScreenAuthAGB: "AGB",

  SelectLanguage: "Sprache auswählen",
  SelectaPhoto: "Bilder auswählen",
  TakePhoto: "Bild aufnehmen",
  ChoosefromLibrary: "von Bibliothek auswählen",

  Thepasswordmusthaveatleast:
    "Ihr Passwort muss 3 dieser 4 Kriterien erfüllen: ",
  Itmustcontainanuppercaseletter:
    "Mindestens ein Kleinbuchstabe muss enthalten sein.",
  Itmustcontainanlowercaseletter:
    "Mindestens ein Großbuchstabe muss enthalten sein. ",
  Itmustcontainannumber: "Mindestens ein Sonderzeichen muss enthalten sein. ",
  Itmustcontainanumlautspecialcharacter:
    "Mindestens eine Zahl muss enthalten sein.",
  Thepasswordmustnotbeatrivialpassword: "Außerdem muss Ihr Passwort:",
  LengthPassword: "Passwort muss mindestens 8 Zeichen lang sein.",

  Passwordhint: "Passworthinweis",
  PasswordIsIncorrect:
    "Passworteingabe ist nicht korrekt! Bitte folgen Sie den Passworthinweis.",
  YouHaveNoCases: "Keine Akten vorhanden",
  Read: "Nachricht lesen",
  Close: "Schließen",
  CreateOn: "Erstellt am",
  IntoThisFile: "in diese Akte",
  InSubFolder: "in Unterordnern",
  News: "Ungelesene Nachrichten",

  // Additional Registration: Biometric
  ScreenRegistrationActivationBiometric_ButtonNo: "Nein, danke.",
  ScreenRegistrationActivationBiometric_ButtonYes: "Aktivieren.",

  // Additional Registration: Push Notification
  ScreenRegistrationActivationPushNotif: "Push-Benachrichtigung aktivieren?",
  ScreenRegistrationActivationPushNotif_ButtonNo: "Nein, danke.",
  ScreenRegistrationActivationPushNotif_ButtonYes: "Aktivieren.",
  ScreenRegistrationActivationPrivacyPolicy: "Datenschutzrichtlinien des Entwicklers",

  // Register Username
  ScreenRegistrationUsername_InvalidUsername:
    "Der Benutzername ist bereits vergeben.",
  ScreenRegistrationUsername_PlaceHolder: "Benutzername eingeben",

  // common
  Overview: "Überblick",
  Activate: "Aktivieren",

  // forgot usrname/password instructions
  ForgotUsername_ButtonHeader:
    "Benutzername vergessen? Benutzernamen per E-Mail anfordern.",
  ForgotUsername_ButtonText: "Benutzername anfordern",
  ForgotPassword_ButtonHeader: "Passwort vergessen? Neues Passwort anfordern",
  ForgotPassword_ButtonText: "Passwort neu vergeben",

  ForgotPassword_Header: "Passwort vergessen",
  ForgotPassword_Message:
    "Bitte geben Sie Ihren Benutzernamen ein. Sie erhalten einen Link per E-Mail, um Ihr Passwort zu erneuern.",
  ForgotPassword_Done:
    "Der Link zum Zurücksetzen des Passworts wurde an Ihre E-Mail gesendet",
  ForgotUsername_Header: "Benutzername vergessen",
  ForgotUsername_Message:
    "Bitte geben Sie Ihre E-Mail-Adresse ein. Wir werden Ihnen Ihren Benutzernamen zuschicken. ",
  ForgotUsername_Done: "Benutzername wurde an Ihre E-Mail gesendet",
  Forgot_Submit: "Senden",

  // Sign in page
  SignIn_Header: "Anmelden",
  SignIn_Message:
    "Melden Sie sich mit Ihrem Benutzernamen und Passwort an. Falls Sie bereits bei WebAkte registriert sind, verwenden Sie bitte diesen Benutzernamen und dieses Passwort, um sich einzuloggen.",
  SignIn_Forgot: "Benutzername oder Passwort vergessen?",
  SignIn_LogIn: "Einloggen",
  SignIn_Username: "Benutzername",
  SignIn_Password: "Passwort",
  SignIn_EmptyUsername: "Bitte geben Sie Ihren Benutzernamen ein",
  SignIn_EmptyPassword: "Bitte geben Sie Ihr Passwort ein",
  SignIn_BiometricFingerprint: "Authentifizierung erforderlich",
  SignIn_BiometricTouchSensor: "Fingerabdruck",
  SignIn_BiometricCancel: "ABBRECHEN",
  SignIn_BiometricFailed: "Fehler",
  SignIn_BiometricWasCanceled: "Authentifizierung abgebrochen",
  SignIn_BiometricTryAgain: "Nochmal versuchen",
  SignIn_FailedLogin: "Nochmal versuchen",

  // Additionals
  CreatorName: "Ersteller",
  LetsGetStarted: "Herzlich willkommen!",
  // New: 'Neu',
  Search: "suche",
  Offline: "Sie sind offline", // NEED TRANSLATION
  DownloadFailed: "Dateidownload fehlgeschlagen", // NEED TRANSLATION
  FileSaved: "Dateien werden im Downloadverzeichnis gespeichert", // NEED TRANSLATION
  Retake: "Neuaufnahme",
  UsePhoto: "Fertig",
  Folder: "Ordner",

  // ScreenProvider
  ScreenProvider_Header: "Provider",
  ScreenProvider_AllProvider: "Alle provider",

  // Dashboard
  Dashboard_Hello: "Guten Tag",
  Dashboard_Latest: "Neueste Nachrichten",
  Dashboard_Provider: "Account",

  // Message Reply Header
  ScreenMessage_ReplyHeaderPrefix: "Antwort von",
  ScreenMessage_AddFavorites: "Nachricht zu Favoriten hinzugefügt",
  ScreenMessage_RemoveFavorites: "Nachricht aus Favoriten entfernt",
  ScreenMessage_AddCaption: "Beschriftung hinzufügen",
  ScreenMessage_SendFail: "Senden fehlgeschlagen",
  ScreenMessage_LabelSubject: "Bezeichnung",
  ScreenMessage_LabelMessage: "Nachrichtentext",

  // ScreenPreview
  ScreenPreview_NotAvailable:
    "Dateivorschau für diesen Dateityp ist nicht möglich", // NEED TRANSLATION

  //ScreenMessages
  ScreePlaceholderAddSubject: "Bitte geben Sie hier die Bezeichnung",
  ScreePlaceholderMessage: "Bitte geben Sie hier den Nachrichtentext ein...",

  // ScreenPIN
  ScreenPIN_Header: "PIN eingeben",
  ScreenPIN_Message:
    "Bitte geben Sie hier Ihre 6-stellige PIN ein, die Sie per E-Mail oder SMS erhalten haben",
  ScreenPIN_QuestionReceived: "Keinen PIN erhalten?",
  ScreenPIN_RequestNew: "Neuen PIN anfordern",

  // ScreenProfile
  ScreenProfile_Settings: "Einstellungen",
  ScreenProfile_Language: "Sprache",
  ScreenProfile_Biometric: "Biometrische Anmeldung",
  ScreenProfile_Logout: "Ausloggen",
  ScreenProfile_PushNotification: "Push-Benachrichtigung",
  ScreenProfile_PrivacyPolicy: "Datenschutz",
  ScreenProfile_TermsAndConditions: "AGB",
  ScreenProfile_Imprint: "Impressum",
  ScreenProfile_ClearCache: "Cache löschen", // NEED TRANSLATION

  ScreenProfile_LogoutPrompt: "e.syOne abmelden?",
  ScreenProfile_LogoutPrompt_DataClear: "",
  ScreenProfile_LogoutPrompt_Yes: "Ja, abmelden",
  ScreenProfile_LogoutPrompt_No: "Abbrechen",

  ScreenProfile_ClearCachePrompt: "Cache löschen?", // NEED TRANSLATION
  ScreenProfile_ClearCachePrompt_Yes: "Löschen", // NEED TRANSLATION
  ScreenProfile_ClearCachePrompt_No: "Abbrechen", // NEED TRANSLATION

  ScreenProfile_ChangeProfilePicture: "Profilbild ändern",
  ScreenProfile_ChangePassword: "Neues Passwort vergeben",
  ScreenProfile_ChangePasswordMessage:
    "Geben Sie Ihr altes Passwort und dann Ihr neues Passwort ein",
  ScreenProfile_CurrentPassword: "Altes Passwort",
  ScreenProfile_NewPassword: "Neues Passwort",
  ScreenProfile_ConfirmNewPassword: "Passwort bestätigen",
  ScreenProfile_ChangePasswordOK: "Passwort speichern",

  // Notification
  Notification_AskHeader: "Push-Benachrichtigung?",
  Notification_AskMessage: "",
  Notification_AskYes: "Aktivieren",
  Notification_AskNo: "Deaktivieren",
  Notification_ForegroundNew: "Sie haben eine neue Nachricht(en)",

  RedirectExternalLink_AskMessage: "Sie werden auf die Webseite '{{Link}}' weitergeleitet. Bitte beachten Sie, dass auf dieser Seite Cookies und datenschutzrelevante Elemente aktiviert werden können. Möchten Sie fortfahren?",
  RedirectExternalLink_AskYes: "Ja",
  RedirectExternalLink_AskNo: "Nein",

  // v3.1.0
  Dashboard_ViewAllAkten: "alle Akten",

  // v3.2.0
  ServiceProvider_LearnMore: "zum Online-Formular",
  ToastClipboard: "In die Zwischenablage kopiert",

  // v3.2.1
  Post_ScanningVirus: "Auf Viren prüfen",
  Post_GeneratingPDF: "In PDF umwandeln",
  Post_MayTakeAWhile: "Dies kann eine Weile dauern.",
  Post_Message: "Ihre Nachricht wurde gesendet",

  //Explore
  Title_MenuExport: "Entdecken",

  UnreadMessage: "ungelesene Nachricht",
  SearchCases: "Aktensuche",
  SearchFiles: "Datei durchsuchen",
  SearchFolders: "Ordner durchsuchen",
  DownloadAkteFailed: "Akte kann nicht heruntergeladen werden. Bitte wenden Sie sich an Ihren Anwalt.",
};
