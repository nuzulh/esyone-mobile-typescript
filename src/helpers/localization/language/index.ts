import LocalizedStrings from "react-native-localization";
import en from "./language.en";
import de from "./language.de";

// TODO: use file not to static code
const language = new LocalizedStrings({
  en,
  de,
});

export default language;
