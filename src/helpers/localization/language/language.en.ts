export default {
  //Auth-Screen
  TypeUsername: "Type your username here",
  TypePassword: "Type your password here",

  //Forgot-Passwrod

  TitleHeaderForgotUsername: "Forgot Username",
  TitleHeaderForgotPassword: "Forgot Password",
  PlaceHolderForgotUsername: "Username",

  //Change-Password-Screen

  CurrentPassword: "Current password",
  NewPassword: "New password",
  ConfirmNewPassword: "Confirm new password",
  ChangePassword: "Set new Password",
  PasswordConfirmRequired: "Password Confirmation is required",

  //Registration

  PasswordDontMatch: "Passwords don't match",
  PasswordRequired: "Password is required",

  // Tabs-Main
  TabHome: "Home",
  TabCases: "Cases",
  TabProviders: "Account",
  TabAccount: "Settings",
  TabExplore: "Explore",

  //error-Mesasage
  erorFull: "Request is full",
  ErrorNoConnection: "No Internet Connection",

  // Tabs-Akten
  TabAktenNew: "New Message",
  TabAktenEmpty: "You have no messages yet",
  TabAktenUpdates: "Messages",
  TabAktenData: "Files",
  TabAktenFavorites: "Favorites",
  TabAktenDataEmpty: "No Files",
  TabAktenFavoriteEmpty: "You don't have any favorites yet!",
  TabAktenFolderEmpty: "No Folders",
  TabAktenFolders: "Folders",
  TabAktenCasesDetail: "Overview",
  TabAktenTask: "Task",
  CasesDetailAccount: "Account",
  CasesDetailEditor: "Collaborator",
  Scroll: "Scroll",

  // Status
  CaseReported: "Case Reported",
  CaseUnderReview: "Case Under Review",
  CaseInProgress: "Case in Progress",
  CaseClosed: "Case Closed",

  //Status Description
  YourCaseHasBentSent: "Your case has been send successfully",
  ClientAccPending: "Client acceptance pending",
  YourCaseHasBentAcc: "Your case has been accepted",
  YourCaseWillbe: "Your case will be processed by us",

  //Warning Card
  YourHelpIsNeeded: "Your help is needed",
  InOrdertoBeable:
    "In order to be able to follow up on your case, we need further information from you",
  ToYourTask: "To your task",

  // Aufabe list :
  Untill: "Untill",
  Expired: "Expired",
  Submittedat: "Submitted at",

  //Aufabe status
  All: "All",
  OpenAufgabe: "Open",
  ExpiredAufgabe: "Expired",
  OpenAufabe: "Open",
  Closed: "Closed",

  // Expired: 'Expired',

  //Explore
  Title_MenuExport: "Explore",

  //Sign in screen

  UsernameisRequired: "Username is required",
  PasswordisRequired: "Password is required",

  //Biometric

  failedMessageBiometric:
    "Fail to attempt authentication, please check your userId and secret",

  // Add aattachemenct

  Addattachment: "Add attachment",
  Files: "Files",
  Gallery: "Gallery",
  Camera: "Camera",
  AddFilesOrImage: "Add files or image",
  FilePreviewPrepare: "File preview is being prepared.",

  country: "english",
  Enable: "Enable",
  Phone: "Phone",
  Mail: "Email",
  Address: "Address",
  Start: "Start",
  Support: "Support",
  Feedback: "Feedback",
  // 'Usersettings' : 'User settings',
  ScreenSetting_ChangePassword: "Change password",
  ScreenSetting_ClearCache: "Clear cached data",
  ScreenSetting_PrivacyPolicy: "Privacy policy",
  ScreenSetting_TermsAndConditions: "Terms and conditions",
  ScreenSetting_Imprint: "Imprint",
  ScreenSetting_Logout: "Log out",

  Username: "Username",
  Password: "Password",
  LogIn: "Log in",
  Me: "Me",
  TimeHourSuffix: "",
  on: "on",
  today: "today",
  messages: "Messages",
  documents: "Documents",
  images: "Images",
  favorites: "Favorites",
  showMore: "show more",
  showLess: "show less",
  attachments: "Attachments",
  detailsOfYourCase: "Details of your case",
  Today: "Today",
  LastWeek: "Last week",
  LastMonth: "Last month",
  Older: "Older",
  AccustomedBy: "Accustomed by",
  CreatedOn: "Created on",
  Created: "Created",
  Privacy: "Privacy",
  PrivacyNotes: "Privacy notes",
  PrivacyNote: "Privacy note",
  Imprint: "Imprint",
  Download: "Download",
  Print: "Print",
  PrintThisMessage: "Print this message",
  DownloadThisMessage: "Download this message",
  WriteAMessageHere: "Write a message...",
  Send: "Send",
  Abort: "Abort",
  UploadFile: "Upload file",
  TakePicture: "Take a picture",
  andXmore: "and {{remaining}} more",
  show: "show",
  New: "New",
  AttachmentLoading: "Loading attachment",
  GeneratingPDF: "Generating PDF",
  pleaseWait: "Please Wait",
  LoggingIn: "Logging in",
  MessageSending: "Sending message...",
  UploadingFiles: "Uploading files",
  Initialize: "Starting application",
  Unauthorised: "Access denied",
  MessageSent: "Message has been sent",
  LoginFailed: "Wrong username or password",
  SessionExpired: "This session has expired. Please log in again.",
  AccessDenied: "Access denied",
  TaskNotFound: "No data found",
  CheckingPermissions: "Checking permissions",
  NoMessagesFound: "No messages found",
  NoDocumentsFound: "No documents found",
  NoFavoritesFound: "No favourites found",
  EmptyTimelineMessage:
    "All messages that you sent or received will be displayed here.",
  EmptyDocumentsMessage:
    "All attachments belonging to messages that you sent or received will be displayed here.",
  EmptyFavoritesMessage:
    "You can mark messages as favourite with the star icon to access them faster. This icon can be found in the upper right corner of a message within the timeline.",
  PleaseEnterData: "Please enter your text or take a picture.",
  GetPin: "Request PIN",
  PinSending: "Sending PIN",
  EnterPinHere: "Enter PIN here",
  ApiErrorVirusFound: "A Virus has been found. Process has been canceled.",
  ApiError_VirusFound: "A Virus has been found. Process has been canceled.",
  ApiError_Unauthorize_Login_Attempt_Exceeded:
    "Too many failed log in attempts. Please try again in 30 minutes.",
  ApiError_Unauthorize_Wrong_Username_or_Password:
    "Wrong username or password, please try again.",
  ApiError_Unauthorize_Expired_Token:
    "The last session has expired, please log in again.",
  ApiError_Unauthorize_Token_Invalid:
    "The token that was sent is not valid. Please try to log in again.",
  ApiError_Unauthorize_Registration_Failed: "Registration failed.",
  ApiError_Unauthorize_Forbidden_Access: "Access denied. Please log in again.",

  ApiError_Unauthorize_Username_Taken: "This username is already taken.",
  ApiError_Unauthorize_Weak_Password: "Please choose a stronger Password!",
  ApiError_Unauthorize_Passwords_Not_Matching: "Passwords must match.",
  ApiError_Unauthorize_Passwords_Registration_Failed: "Registration failed.",

  ApiError_Task_Expired: "This link is not valid anymore.",
  ApiError_Unauthorize_Pin_Expired:
    "This PIN is not valid anymore. Please request a new one.",
  AccountProductLimitReached: "You cannot upload files at the moment. Please contact your lawyer.",
  PleaseEnterPin: "Please enter the PIN here",
  LoadOlderMessages: "Load older messages",
  LoadOlderDocument: "Load older documents",
  ApplicationBlocked: "Application is locked!",
  NextPossibleRetry: "Next possible log in attempt:",
  ContactForm: "Contact form",
  ContactInformation: "Contact",
  Subject: "Subject",
  YourRequest: "Your request",
  SubjectExample: 'e.g. "My support request"',
  YourRequestHere: "enter your concern here",
  TechnicalSupport: "Technical support",
  DownloadTeamViewer: "Download TeamViewer",
  SecurityLevel: "Security level",
  YourCurrentSecurityLevel: "Your current security level",
  lowJustPin: "low (PIN only)",
  mediumUsername: "high (username and password)",
  RaiseSecurityLevel: "Raise security level",
  UserSettings: "Settings",
  ChooseUsername: "Choose username",
  ChoosePassword: "Choose password",
  ConfirmPassword: "Confirm password",
  ReenterPassword: "Reenter password",
  PasswordStrength: "password strength",
  validationIsEmpty: "Required field",
  validationPasswordMinlength: "password minimum length: 8 characters",
  validationPasswordBlacklisted: "password is blacklisted",
  validationPasswordUsernameIncluded: "password contains username",
  validationDoesntMatch: "password and password confirmation don't match",
  validationPasswordCriterias: "Less than 3 criterias met",
  SupportFormSending: "Sending request",
  supportFormSent: "Request was sent",
  validationNotChecked: "please check this required field",
  IHaveReadAndAcceptedTheAGB:
    "I have read and agree to the terms and conditions",
  goOn: "go on",
  showAgbNow: "Show terms and conditions",
  providesTheseAgbPleaseAcceptLONG:
    "provides terms and conditions. Please accept them to go on.",
  TermsAndConditions: "Terms and conditions",
  CheckingForTermsAndConditions: "Checking for terms and conditions.",
  DownloadAgb: "Download terms and conditions",
  loading: "loading",
  DownloadFile: "Download file",
  FileXnotPreviewable: 'No preview for the file "{{fileName}}" possible.',
  passwordMinLength: "contain at least 8 characters.",
  includesLowerCaseCharacter: "Must contain at least one lower case character.",
  includesUpperCaseCharacter: "Must contain at least one upper case character.",
  includesSpecialCharacter: "Must contain at least special character.",
  includesNumericCharacter: "Must contain at least one number",
  passwordNotBlacklisted: "be trivial (e.g. “12345678“).",
  usernameNotIncluded: "contain the chosen username.",
  passwordCriteriaHeading: "Your password must meet at least 3 of 4 criterias:",
  passwordLengthCriteriaHeading: "Furthermore your password must...",
  passwordSpecialCriteriaHeading: "Your password cannot...",
  EmailOrPhone: "Email address or phone number",
  ForgotCredentials: "Forgot your Username or Password?",
  FindOutUsername: "Find out username",
  ResetPassword: "Reset password",
  ResetPasswordSuccess: "The password has been changed successfully.",
  UsernameSending: "Username was sent",
  MessageBeingCreated: "Creating message",
  UnknownError: "Unknown error",
  ResettingPassword: "Resetting password",
  BackToLogin: "Back to log in",
  UsernameWillBeSent: "A message containing your username has been sent.",
  RequestPinAgain: "Request a new PIN",
  LetsGo: "Let's go",
  GainAccess: "Gain access",
  LoginViaSMS: "Log in via SMS",
  LoginViaEmail: "Log in via E-Mail",
  LoginViaUsername: "Log in",
  back: "back",
  EnterPin: "Enter PIN",
  downloadingAttachment: "Downloading attachments...",
  downloadingMessage: "Downloading message...",
  NotFound: "Nothing here!",
  "1hour": "1 hour",
  "2hours": "2 hours",
  "6hours": "6 hours",
  "12hours": "12 hours",
  "24hours": "24 hours",
  "3days": "3 days",
  "1week": "1 week",
  "3month": "3 month",
  EmailAddress: "Email address",
  Phonenumber: "Phone number",
  EnterEmail: "Enter the Email address here",
  EnterPhonenumber: "Enter the phone number here",
  SendPinAsync: "Send PIN in separate message",
  viaSms: "via SMS",
  viaEmail: "via Email",
  ForwardMessage: "Forward message",
  Forward: "Forward",
  ValidUntil: "Validity period of the created link",
  SmartlinkSending: "Forwarding message...",
  unknownError: "unknown error",
  LogOut: "Log out",
  SmartlinkSent: "Message has been forwarded",
  PinNotSent: "PIN cannot be sent.",
  FileTooLarge: "The file '{{fileName}}' is too large. Each file cannot be larger than {{maxSize}} MB",
  InvalidFileExtension: "'{{fileName}}' cannot be uploaded because the file has an extension that is not allowed",

  SmartlinkDescriptionStep1LONG:
    "Please choose your prefered communication channel for messages.",
  SmartlinkDescriptionStep2EmailLONG:
    "Please enter the mail address of the recipient and define how long the given message link should be valid.",
  SmartlinkDescriptionStep2SmsLONG:
    "Please enter the mobile number of the recipient and define how long the given message link should be valid.",
  SomethingWentWrongLONG: "For the named URL there are no contents available.",
  PleaseNotePasswordTipps: "Please note the details about password security.",
  ForgotCredentialsLONG:
    "In case you forgot your password the following function allows you to start the restore procedure. Please click the buttons below and follow the instructions.",
  ForgotCredentialsNotAvailableLONG:
    "This user account is not completely registered yet. Because of this there are currently no username or password available.",
  PasswordLinkWillBeSent:
    "Message was sent. Please follow the link you will receive to renew your password.",
  PleaseEnterYourNewPasswordLONG:
    "Enter your new password and confirm it once more. The password has to be at least eight characters long.",
  PleaseEnterYourUsernameLONG:
    "Enter your username. You will receive a link to renew your password.",
  PleaseEnterYourEmailOrPhoneLONG:
    "Please enter you Email address or mobile number. You will receive a message with your username.",
  WithTheDownloadOfTheTeamViewerSoftwareLONG:
    "By downloading the TeamViewer software you are agreeing the Terms of Remote maintenance and support services.",
  YouAreAboutToEstablishLONG:
    "You are establishing an online session with one of the support team members from e.Consult AG. Please note that this servive is only available during our office hours and with an appointment schedule made by our service and support team,",
  GotQuestionsLONG:
    "Are there questions or do you want to give us some feedback?",
  OurServiceTeamLONG:
    "Our service and support team is available from monday to friday 9 am until 5 pm CET.",
  UserSettingsPageDescriptionLONG:
    "The security settings are defining the security level of your software access. The level 'low' is securing the information access by PIN code. The setting  'high' allows you to work with username and password in order to secure your account.",

  SplashText1: "Easy.\nFast.\nSafe.\nEfficient.",
  SplashText2: "We\naccelerate\nyour business",
  SplashText3: "WebAkte\nClientView",
  SplashPhaseOneFooterText:
    "Footer text dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor",
  SplashPhaseTwoDescriptionGeneric:
    "Benefit from the advantages of „ClienView“ and take your communication to the next level.\n\nRequest your PIN easily and conveniently by email or by SMS message.",
  SplashPhaseTwoDescriptionEmail:
    "For your security, please request a PIN to start using this service.",
  SplashPhaseTwoDescriptionUsername:
    "Your business is waiting to be accelerated. Easy and convenient login with your known WebAkte-Credentials.",
  SplashPhaseThreeDescription:
    "Please enter the 4-digit PIN received by email or SMS message.",

  StaticInfoDescription:
    "The application functionality will be available as soon as your lawyer shared your related resources.\n\nPlease contact your lawyer.",
  Register: "Register",
  PinWrong: "The PIN is incorrect",
  notYou: "not You?",
  Cancel: "Cancel",
  Alert: "Alert",
  DoYouWantToLogout: "Do you want to logout?",
  Continue: "Continue",
  GreatFromnowonyoucanloginwithyour:
    "Great! From now on you can login with your",
  Nothanksmaybelater: "No thanks, maybe later",
  Loginquickerwith: "Login quicker with",
  PleaseactivateTouchIDFaceIDonthisdevice:
    "Please activate Touch ID / Face ID on this device",
  verwendenloginwith: "Enable login with",
  einloggen: "einloggen",
  LanguageSetting: "Language Setting",
  Account: "Account",
  PrivacyPolicy: "Privacy policy",
  AboutUs: "About Us",
  ContactUs: "Contact Us",

  //onboarding
  YouwillreceiveanSMSorEmailfromyourlawyer:
    "You will receive an SMS or Email\nfrom your lawyer",
  FollowtheSmartlinkintheSMSorEmail:
    "Follow the Smartlink in the SMS\nor Email",
  Thatsitnowyoucouldcommunicatewithyourlawyer:
    "That's it - now you could\ncommunicate with your lawyer",
  ToYourSmartlink: "Continue with Invitation",
  Doesnthavelawyer: "Don't have lawyer?",
  AlreadyHaveLogin: "Have an account? Log in directly",
  Anyquestions: "Any questions?",
  MoreatwwwesyMandantde: "More at esything.io",
  Opensmartlinkwith: "Open smartlink with",
  Open: "Open",

  //after app authentication
  welcometo: "Welcome to",
  GetStarted: "Get Started",
  Hello: "Hello",
  ActivateYourBiometricDefault:
    "Activate your biometric\nauthentication via\nfingerprint or face\nrecognition",
  ActivateYourBiometricFingerabdruck:
    "Activate your biometric\nauthentication via\nfingerprint",
  ActivateYourBiometricnGesichtserkennung:
    "Activate your biometric\nauthentication via\nface recognition",
  Aktivieren: "Enable",
  NoThanks: "No, thanks",
  SignUpNow: "Sign up now!",
  andagaindirectaccesstoyouronlineAkte:
    "and again direct access to your\nonline Akte.",
  UsernamehasbeentakenTryanother: "Username is already taken",
  Next: "Next",
  JustOneMoreStepConfirmYourPassword:
    "Just one more step -\nConfirm your password",
  Passworddosentmatch: "Password dosen't match.",
  Congratulation: "Congratulations!",
  EnjoyEasyAndSecureCommunicationWith:
    "Enjoy easy and secure\ncommunication with",
  EnjoyEasyAndSecureCommunicationWith2: "",
  Home: "Home",

  ScreenAuthWelcomeAGB1: "I have read the",
  ScreenAuthWelcomeAGBLink: "Terms and conditions",
  ScreenAuthWelcomeAGB2: "of the",
  ScreenAuthWelcomeAGB3: "and I agree!",
  ScreenAuthDatenschutz1: "Find out more under the",
  ScreenAuthDatenschutzLink: "Data Protection Notice",
  ScreenAuthDatenschutz3: "of the law firm!",
  ScreenAuthDatenschutz: "Privacy policy",
  ScreenAuthAGB: "Terms and conditions",

  SelectLanguage: "Select Language",
  SelectaPhoto: "Select a Photo",
  TakePhoto: "Take Photo...",
  ChoosefromLibrary: "Choose from Library...",

  Thepasswordmusthaveatleast: "Your password must meet 3 of these 4 criteria:",
  Itmustcontainanuppercaseletter:
    "At least one lowercase letter must be included. ",
  Itmustcontainanlowercaseletter:
    "At least one capital letter must be included.",
  Itmustcontainannumber: "At least one special character must be included.",
  Itmustcontainanumlautspecialcharacter:
    "At least one number must be included.",
  Thepasswordmustnotbeatrivialpassword: "In addition for password length:",
  LengthPassword: "Password must be at least 8 characters long.",

  Passwordhint: "Password hint",
  PasswordIsIncorrect:
    "Password is incorrect! Please follow the password hint.",
  YouHaveNoCases: "You have no cases",
  Read: "read more",
  // today: 'today',
  Close: "close",
  CreateOn: "Created on",
  IntoThisFile: "Into this file",
  InSubFolder: "In Sub Folder",
  News: "News",

  // Additional Registration: Biometric
  ScreenRegistrationActivationBiometric_ButtonNo: "No, thanks",
  ScreenRegistrationActivationBiometric_ButtonYes: "Enable",

  // Additional Registration: Push Notification
  ScreenRegistrationActivationPushNotif:
    "Would you like to enable Push notifications?",
  ScreenRegistrationActivationPushNotif_ButtonNo: "No, thanks",
  ScreenRegistrationActivationPushNotif_ButtonYes: "Enable",
  ScreenRegistrationActivationPrivacyPolicy: "Developer privacy policy",

  // Register Username
  ScreenRegistrationUsername_InvalidUsername:
    "The username is invalid. Please enter another.",
  ScreenRegistrationUsername_PlaceHolder: "Enter Username",

  // common
  Overview: "Overview",
  Activate: "Activate",

  // forgot usrname/password instructions
  ForgotPassword_ButtonHeader:
    "Forgot your password? Request a password reset delivered to your e-mail",
  ForgotPassword_ButtonText: "Request Password Reset",
  ForgotUsername_ButtonHeader:
    "Forgot your username? Request an e-mail with your username",
  ForgotUsername_ButtonText: "Request Username",

  ForgotPassword_Header: "Forgot Password",
  ForgotPassword_Message:
    "Please enter your username. You will receive an email with a link to edit your password.",
  ForgotPassword_Done:
    "The link to reset your password has been sent to your email",
  ForgotUsername_Header: "Forgot Username",
  ForgotUsername_Message:
    "Please enter your email address. We will send you an email with your username.",
  ForgotUsername_Done: "Username has been sent to your email",
  Forgot_Submit: "Submit",

  // Sign in page
  SignIn_Header: "Log In",
  SignIn_Message:
    "Log in with your username and password. If you already have an account at Secure WebAkte, please use the account's username and password to log in.",
  SignIn_Forgot: "Having trouble signing in? ",
  SignIn_LogIn: "Log In",
  SignIn_Username: "Username",
  SignIn_Password: "Password",
  SignIn_EmptyUsername: "Please enter a username",
  SignIn_EmptyPassword: "Please enter your password",
  SignIn_BiometricFingerprint: "Authentication Required",
  SignIn_BiometricTouchSensor: "Touch sensor",
  SignIn_BiometricCancel: "Cancel",
  SignIn_BiometricFailed: "Failed",
  SignIn_BiometricWasCanceled: "Authentication was canceled",
  SignIn_BiometricTryAgain: "Try Again",
  SignIn_FailedLogin: "Failed to Login",

  // Additionals
  CreatorName: "Creator",
  LetsGetStarted: "Let's get started!",
  // New: 'New',
  Search: "search",
  Offline: "You are offline",
  DownloadFailed: "File download failed", // NEED TRANSLATION
  FileSaved: "File saved to your downloads folder", // NEED TRANSLATION
  Retake: "Retake",
  UsePhoto: "Apply",
  Folder: "Folder",

  // ScreenProvider
  ScreenProvider_Header: "Provider",
  ScreenProvider_AllProvider: "All providers",

  // Dashboard
  Dashboard_Hello: "Hello",
  Dashboard_Latest: "Latest Updates",
  Dashboard_Provider: "Account",

  // Message Reply Header
  ScreenMessage_ReplyHeaderPrefix: "Answer from",
  ScreenMessage_AddFavorites: "Message added to Favorites",
  ScreenMessage_RemoveFavorites: "Message removed from Favorites",
  ScreenMessage_AddCaption: "Add caption",
  ScreenMessage_SendFail: "Failed to send message",
  ScreenMessage_LabelSubject: "Add subject",
  ScreenMessage_LabelMessage: "Message",

  // ScreenPreview
  ScreenPreview_NotAvailable: "Cannot preview this file type",

  //ScreenMessages
  ScreePlaceholderAddSubject: "Subject",
  ScreePlaceholderMessage: "Type your messages..",

  // ScreenPIN
  ScreenPIN_Header: "Enter PIN",
  ScreenPIN_Message:
    "Please enter the 6-digit PIN received by email or SMS Message",
  ScreenPIN_QuestionReceived: "Haven't received a PIN?",
  ScreenPIN_RequestNew: "Request new PIN",

  // ScreenProfile
  ScreenProfile_Biometric: "Biometric login",
  ScreenProfile_Logout: "Log out",
  ScreenProfile_Settings: "Settings",
  ScreenProfile_Language: "Language",
  ScreenProfile_PushNotification: "Push notifications",
  ScreenProfile_PrivacyPolicy: "Privacy policy",
  ScreenProfile_TermsAndConditions: "Terms and conditions",
  ScreenProfile_Imprint: "Imprint",
  ScreenProfile_ClearCache: "Clear cached data",

  ScreenProfile_LogoutPrompt: "Sign out from e.syOne?",
  ScreenProfile_LogoutPrompt_DataClear: "",
  ScreenProfile_LogoutPrompt_Yes: "Yes, sign out",
  ScreenProfile_LogoutPrompt_No: "Cancel",

  ScreenProfile_ClearCachePrompt: "Clear cached data?",
  ScreenProfile_ClearCachePrompt_Yes: "Yes",
  ScreenProfile_ClearCachePrompt_No: "No",

  ScreenProfile_ChangeProfilePicture: "Change your profile picture",
  ScreenProfile_ChangePassword: "Change password",
  ScreenProfile_ChangePasswordMessage:
    "Enter your old password and then your new one",
  ScreenProfile_CurrentPassword: "Current password",
  ScreenProfile_NewPassword: "New password",
  ScreenProfile_ConfirmNewPassword: "Confirm new password",
  ScreenProfile_ChangePasswordOK: "Set new password",

  Notification_AskHeader: "Enable push notifications?",
  Notification_AskMessage: "",
  Notification_AskYes: "Yes, enable",
  Notification_AskNo: "No",
  Notification_ForegroundNew: "You have a new message",

  RedirectExternalLink_AskMessage: "You will be redirected to the website '{{Link}}'. Please keep in mind that this website might activate cookies and privacy-related features. Do you wish to continue?",
  RedirectExternalLink_AskYes: "Yes",
  RedirectExternalLink_AskNo: "No",

  // v3.1.0
  Dashboard_ViewAllAkten: "View All Cases",

  // v3.2.0
  ServiceProvider_LearnMore: "Online-Form",
  ToastClipboard: "Copied to clipboard",

  // v3.2.1
  Post_ScanningVirus: "Scanning for virus",
  Post_GeneratingPDF: "Converting to PDF",
  Post_MayTakeAWhile: "This will only take a moment",
  Post_Message: "Sending your message",

  UnreadMessage: "unread message",
  SearchCases: "Search case",
  SearchFiles: "Search file",
  SearchFolders: "Search folder",
  DownloadAkteFailed: "File cannot be downloaded. Please contact your lawyer.",
};
