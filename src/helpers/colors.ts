export const COLORS = {
  EXTERNAL_FORM: "#429F98",
};

export function getTextColor(bgColor: string) {
  try {
    const R = Number("0x" + bgColor.substring(1, 3));
    const G = Number("0x" + bgColor.substring(3, 5));
    const B = Number("0x" + bgColor.substring(5, 7));
    const Y = (R + R + R + B + G + G + G + G) >> 3;
    // console.log("----- R:" + R + " G:" + G + " B:" + B + " --- Y:" + Y);
    if (Y < 130) {
      return "#FFFFFF";
    } else {
      return "#000000";
    }
  } catch (e) {
    return "#FFFFFF";
  }
}
