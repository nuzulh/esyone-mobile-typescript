import { WorkflowFormElement } from "../models";

// TODO: optimize this
export function parseIncomingUrl(incomingURL: string) {
  const noProtocolURL = incomingURL.replace(/.*?:\/\//g, "");
  // console.log("----- ParseIncomingURL: " + incomingURL);
  const tokens = noProtocolURL.split("/", 3);
  if (tokens.length == 2 && tokens[1].startsWith("dashboard?")) {
    const accountId = tokens[1].replace("dashboard?accid=", "");
    // console.log(
    //   "----- ParseIncomingURL: new account activation id=" + accountId
    // );
    return { type: 1, value: accountId };
  } else if (tokens.length == 3) {
    const newUrlID = tokens[2];
    // console.log("----- ParseIncomingURL: returning new URL ID: " + newUrlID);
    if (tokens[1] === "externalform" || tokens[1] === "proudly-presents") {
      // console.log("----- external form parsed");
      return { type: 3, value: incomingURL };
    }
    if (tokens[1] === "reset-password") {
      // console.log("----- reset-password parsed");
      return { type: 4, value: incomingURL };
    }
    if (tokens[1] === "page") {
      // console.log("----- externe afrage parsed");
      // route to webview for now
      return { type: 3, value: incomingURL };
    }
    return { type: 2, value: newUrlID };
  } else {
    // console.log("----- ParseIncomingURL: Unable to parse " + incomingURL);
    return { type: 0, value: "" };
  }
}

export declare type ParsedUrl = {
  domain?: string;
  target?: "page" | "externalfrage" | "aufgabe" | "start" | "activate" | "reset-password" | "unknown";
  providerName?: string;
  key?: string;
};

export function parseLinkingUrl(incomingURL: string) {
  const noProtocolURL = incomingURL.replace(/.*?:\/\//g, "");
  const tokens = noProtocolURL.split("/");

  return parse({}, tokens, 0);
}

function parse(acc: ParsedUrl, tokens: string[], index): ParsedUrl {
  if (index == 0)
    return parse({ ...acc, domain: tokens[index] }, tokens, index + 1);
  else if (index == 1) {
    if (tokens[index].includes("?"))
      return parse(
        { ...acc, domain: tokens[0] },
        [tokens[0], ...tokens[index].split("?")],
        index,
      );
    switch (tokens[index]) {
      case "page":
        if (tokens.length === 3)
          return {
            ...acc,
            target: "page",
            providerName: tokens[index + 1],
            key: tokens[index + 1],
          };
        else
          return parse(
            { ...acc, providerName: tokens[index + 1] },
            tokens,
            index + 2
          );
      case "externalform":
        return { ...acc, target: "aufgabe", key: tokens[index + 1] };
      case "start":
        return { ...acc, target: "start", key: tokens[index + 1] };
      case "dashboard":
        return {
          ...acc,
          target: "activate",
          key: tokens[index + 1]
            .substring(6, tokens[index + 1].indexOf("&")),
        };
      case "reset-password":
        return { ...acc, target: "reset-password", key: tokens[index + 1] };
      default:
        return { ...acc, target: "unknown" };
    }
  }
  else if (index == 3)
    switch (tokens[index]) {
      case "guidance":
        return { ...acc, target: "externalfrage", key: tokens[index + 1] };
      default:
        return { ...acc, target: "unknown" };
    }
  else return { ...acc, target: "unknown" };
}

export function parseInitial(name) {
  if (!name) {
    return "?";
  }
  const u = name.toUpperCase();
  const split = u.split(" ");
  if (split.length == 1) {
    return split[0].charAt(0);
  } else {
    return split[0].charAt(0) + split[split.length - 1].charAt(0);
  }
}

export function parseStatus(statusText: string) {
  switch (statusText) {
    case "EsyAkte_StatusDescription_ESYAKTEFALLGEMELDET_success":
      return "lhr Fall wurde erfolgreich versendet";
    case "EsyAkte_StatusDescription_ESYAKTEFALLBEARBEITUNG_success":
      return "Ihr Fall wird von uns bearbeitet.";
    case "EsyAkte_StatusDescription_ESYAKTEFALLABGESCHLOSSEN_inProgress":
      return "Dieser Fall wurde bereits beendet";
    case "EsyAkte_StatusDescription_ESYAKTEFALLGEPRUFT_inProgress":
      return "Mandatsannahme steht aus";
    case "EsyAkte_StatusDescription_ESYAKTEFALLGEPRUFT_success":
      return "Ihr Fall wurde angenommen";
    default:
      return statusText;
  }
}

export function parseMobileNumber(phoneNumberText: string) {
  if (!phoneNumberText) return "";

  let cleaned = phoneNumberText.replace(/\D/g, "");
  cleaned = parseInt(cleaned).toString();
  const match = cleaned.match(/^(\d{4})(\d+)$/);

  if (match) return match[1] + " " + match[2];

  return "";
}

export function parseApiReasonKey(error: Error): string {
  const splittedError = error.message.split("\n");
  const reason = splittedError[splittedError.length - 1];

  if (reason.startsWith("{"))
    return JSON.parse(reason)["reasonKey"] ||
      JSON.parse(reason)["Reason"];
  if (reason.startsWith("ApiError")) return reason;

  return "UnknownError";
}

export function parseFormTypeLength(
  formElements: WorkflowFormElement[],
  type: string
): number {
  let count: number = 0;
  formElements.forEach((f) => {
    if (f.type === type) count += 1;
    if (f.type === "repeater")
      f.formElements[0].formElements.forEach((fRepeater) => {
        if (fRepeater.type === type) count += 1;
      });
  });
  return count;
}
