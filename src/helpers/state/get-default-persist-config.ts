import AsyncStorage from "@react-native-async-storage/async-storage";
import { createTransform } from "redux-persist";
import { deserialize, serialize } from "../serializer";

export declare type PersistConfigOptions = {
  blacklist?: string[];
  whitelist?: string[];
};

export const defaultTransform = createTransform(
  (inboundState, key) => serialize(inboundState),
  (outboundState, key) => deserialize(outboundState)
);

export function getDefaultPersistConfig(
  key: string,
  options?: PersistConfigOptions
) {
  return {
    key,
    storage: AsyncStorage,
    blacklist: options?.blacklist,
    whitelist: options?.whitelist,
  };
}
