import { EsyState } from "../../models";

export function getDefaultState(): EsyState {
  return {
    appState: {
      currentFlow: "notready",
      currentState: "active",
      isError: false,
      isLoading: false,
      isLoadingBackground: false,
      isOffline: false,
      errorMessage: null,
      error: null,
      toastRef: null,
    },
    auth: {
      id: null,
      profile: null,
      session: null,
      accounts: null,
      isLoggedIn: false,
      isError: false,
      error: null,
      errorMessage: null,
    },
    fcm: {
      hasPermission: false,
      isRegistered: false,
      isEnabled: false,
      isPermissionAsked: false,
      token: null,
    },
    settings: {
      language: "de",
      isPushNotificationEnabled: false,
      isPushNotificationAsked: false,
      isBiometricEnabled: false,
      isBiometricSupported: false,
      isBiometricAsked: false,
      isOfflineLoginSupported: true,
      biometricType: null,
    },
    linking: {
      currentLink: null,
    },
    navigation: {
      current: null,
    },
    keyboard: {
      isVisible: false,
    },
    akten: {
      akten: {},
      aktenAsList: [],
      isFetching: false,
      lastUpdated: 0,
    },
    filesState: {
      files: {},
      filesAsList: [],
      isFetching: false,
      error: null,
    },
    nativeState: {
      isCameraPermissionAsked: false,
      isCameraPermissionGranted: false,
    },
    formularState: {
      formular: {},
      lastUpdatedWorkflowId: null,
    },
  };
}
