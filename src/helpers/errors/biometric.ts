import {
  AuthenticationFailed,
  OfflineAuthenticationNotSupported,
} from "./auth";

export class BiometricNotSupported extends Error {
  constructor() {
    super("Biometric not supported on this device");
  }
}

export class BiometricAuthenticationFailed extends AuthenticationFailed {
  constructor() {
    super("Biometric authentication failed");
  }
}

export class BiometricOfflineAuthenticationFailed extends OfflineAuthenticationNotSupported {
  constructor() {
    super();
  }
}
