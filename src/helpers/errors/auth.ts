import localization from "../localization";

export class EmptyUsernamePassword extends Error {
  constructor() {
    super(
      "Not valid username or password. Username or password cannot be empty"
    );
  }
}

export class AuthenticationFailed extends Error {
  constructor(message?: string) {
    super(message || localization.language.failedMessageBiometric);
  }
}

export class OfflineAuthenticationNotSupported extends AuthenticationFailed {
  constructor() {
    super(
      "Fail to attempt authentication, offline authentication is not supported"
    );
  }
}

export class OfflineAuthenticationFailed extends AuthenticationFailed {
  constructor() {
    super(
      "Fail to attempt offline authentication, please check your userId and secret"
    );
  }
}

export class InvalidPIN extends Error {
  constructor(message?: string) {
    super(message || "Invalid PIN");
  }
}

export class ApiError extends Error {
  constructor(status: number, url: string, options: any, message?: string) {
    super(
      `Error execute API: [${status}]\n\t{${url}}\n\t${JSON.stringify(
        options
      )}\t\n${message}`
    );
  }
}

export class NotAuthorized extends ApiError {
  constructor(url: string, options: any, message?: string) {
    super(401, url, options, message || "Not authorized");
    // super(message || "Not authorized");
  }
}
