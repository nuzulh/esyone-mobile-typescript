export class FormulaRequireLogin extends Error {
  constructor() {
    super("Formula requires login");
  }
}

export class FormulaRequirePIN extends Error {
  constructor() {
    super("Formula requires PIN");
  }
}
