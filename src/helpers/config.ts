import * as R from "ramda";

export enum AVAILABLE_KEYS {
  BASE_URL = "BASE_URL",
  API_KEY = "API_KEY",
  APPLICATION_KEY = "APPLICATION_KEY",
  DOMAIN_NAME = "DOMAIN_NAME",
  JWT_KEY = "JWT_KEY",
  API_PAGE_SIZE = "API_PAGE_SIZE",
  APPLICATION_VERSION = "APPLICATION_VERSION",
  LICENCES = "LICENCES",
  DEVELOPER_PRIVACY_POLICY_URL = "DEVELOPER_PRIVACY_POLICY_URL",
}

export declare type Config = {
  [key: string]: string;
};

export declare type ConfigCache = {
  config: Config;
};

// create singleton object
const configCache: ConfigCache = {
  config: {
    BASE_URL: "https://secure.webakte.de",
    // BASE_URL: "https://ecostage01.e-consult.de",
    API_KEY: "",
    APPLICATION_KEY: "",
    DOMAIN_NAME: "https://app.esything.io",
    JWT_KEY: "mandant",
    API_PAGE_SIZE: "50",
    APPLICATION_VERSION: "4.2.4",
    LICENCES: `Lato Font Family: Copyright (c) 2010-2011 by tyPoland Lukasz Dziedzic (team@latofonts.com) with Reserved Font Name "Lato". Licensed under the SIL Open Font License, Version 1.1.\n\nMaterial Icons: (c) Google Inc. Licensed under the Apache License Version 2.0.`,
    DEVELOPER_PRIVACY_POLICY_URL: "https://esy.one/index.php/home-obs/datenschutzerklaerung/",
  },
};

export function applyConfig(data: Config) {
  configCache.config = R.mergeDeepRight(configCache.config, data);
}

export default {
  get(key: AVAILABLE_KEYS): string | null {
    return configCache.config[key.toString()];
  },

  getOrElse(key: AVAILABLE_KEYS, altValue: string): string {
    return configCache.config[key.toString()] || altValue;
  },
};
