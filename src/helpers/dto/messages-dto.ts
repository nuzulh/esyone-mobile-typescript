import { Message } from "../../models/message";
import attachmentsDto from "./attachments-dto";

function fromCheetahResponse(responseItem): Message {
  const { creationDate, belegdatum, attachments, ...messageAttributes } =
    responseItem;

  return {
    ...messageAttributes,
    creatorId: responseItem.creatorIdHash,
    creationDate: creationDate && new Date(creationDate),
    belegdatum: belegdatum && new Date(belegdatum),
    attachments:
      attachments &&
      attachments.map((attachment) =>
        attachmentsDto.fromCheetahResponse(attachment)
      ),
  };
}

export default {
  fromCheetahResponse,
};
