import { Account } from "../../models";

function fromCheetahResponse(responseItem): Account {
  return {
    ...responseItem,
  };
}

function fromPublicInfoResponse(id: any, responseItem): Partial<Account> {
  // console.log("ini dari account-dto");
  // console.log(responseItem);
  return {
    id,
    name: responseItem.accountName,
    shortName: responseItem.accountShortName,
    uniqueAccountKey: responseItem.accountUniqueKey,
    primaryLogoUrl: responseItem.primaryLogoUrl,
    secondaryLogoUrl: responseItem.secondaryLogoUrl,
    mainColor: responseItem.primaryColor,
    secondaryColor: responseItem.secondaryColor,
    welcomeHeadline: responseItem.welcomeHeadline,
    welcomeText: responseItem.welcomeText,
    contact: responseItem.contact,
    assistants: responseItem.assistants,
    esyThingAgb: responseItem.esyThingAgb,
    esyThingDatenschutz: responseItem.esyThingDatenschutz,
  };
}

export default {
  fromCheetahResponse,
  fromPublicInfoResponse,
};
