import { Aufgabe } from "../../models";

function fromCheetahResponse(responseItem): Aufgabe {
  const { answerCreationDate, validUntil, ...aufgabeAttributes } = responseItem;

  return {
    ...aufgabeAttributes,
    answerCreationDate: answerCreationDate && new Date(answerCreationDate),
    validUntil: validUntil && new Date(validUntil),
  };
}

export default {
  fromCheetahResponse,
};
