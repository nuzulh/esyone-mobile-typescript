function fromCheetahResponse(responseItem) {
  const { creationDate, ...attachmentAttributes } = responseItem;

  return {
    ...attachmentAttributes,
    creationDate: creationDate && new Date(creationDate),
  };
}

export default {
  fromCheetahResponse,
};
