import moment from "moment";
import { Akte, AkteState } from "../../models";

function fromCheetahResponse(responseItem): Akte {
  const {
    idHash,
    creatorIdHash,
    aktenzeichen,
    creationDate,
    lastMessageDate,
    lastState: lastStateResponseItem,
    ...akteAttributes
  } = responseItem;

  let lastState: AkteState = null;
  if (lastStateResponseItem) {
    const {
      occuredAt,
      creationDate: lastStateCreationDate,
      ...lastStateAttributes
    } = lastStateResponseItem;
    lastState = {
      ...lastStateAttributes,
      occuredAt: moment(occuredAt, "dddd, D. MMMM YYYY, HH:mm", "de").toDate(),
      creationDate: moment(
        lastStateCreationDate,
        "DD.MM.YYYY HH:mm:ss"
      ).toDate(),
    };
  }

  return {
    ...akteAttributes,
    id: idHash,
    creatorId: creatorIdHash,
    aktenzeichen:
      aktenzeichen && !isNaN(aktenzeichen) ? parseInt(aktenzeichen) : null,
    creationDate: moment(creationDate).toDate(),
    lastMessageDate: moment(lastMessageDate).toDate(),
    lastState,
  };
}

export default {
  fromCheetahResponse,
};
