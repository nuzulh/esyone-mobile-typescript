export { default as accountsDto } from "./accounts-dto";
export { default as aktenDto } from "./akten-dto";
export { default as aufgabenDto } from "./aufgaben-dto";
export { default as messagesDto } from "./messages-dto";
export { default as attachmentsDto } from "./attachments-dto";
export { default as workflowDto } from "./workflow-dto";
