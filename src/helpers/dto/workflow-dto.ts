import moment from "moment";
import { WorkflowAnswer, WorkflowFormElementAnswer } from "../../models";
import { MetaFile } from "../../services";
import { formatMobileNumber, formatNumber } from "../formatter";

function fromCheetahResponse(item: any): WorkflowAnswer {
  const answerCreationDate = item.creationDate
    ? new Date(item.creationDate)
    : null;

  const formValues = item.answerFormElements
    ? item.answerFormElements.map((element) => parseFormElementAnswer(element))
    : [];

  return {
    answerCreationDate,
    formValues,
  };
}

function parseFormElementAnswer(element: any): WorkflowFormElementAnswer {
  switch (element.type) {
    case "date":
      return {
        name: element.name,
        type: element.type,
        label: element.description,
        value: moment(element.answer, "DD.MM.YYYY").toDate(),
      };
    case "time":
      return {
        name: element.name,
        type: element.type,
        label: element.description,
        value: moment(element.answer, "hh:mm").toDate(),
      };
    case "number":
      return {
        name: element.name,
        type: element.type,
        label: element.description,
        value: formatNumber(element.answer),
      };
    case "mobilenumber":
      return {
        name: element.name,
        type: element.type,
        label: element.description,
        value: formatMobileNumber(element.answer),
      };
    case "file":
      const files: MetaFile[] = element.answer
        ? element.answer.map((answer) => {
            const file: Partial<MetaFile> = {
              id: answer.entityId,
              fileName: `${answer.name}.${answer.extension}`,
            };

            return file;
          })
        : null;

      return {
        name: element.name,
        type: element.type,
        label: element.description,
        value: files,
      };
    case "signature":
      const signatures: MetaFile[] = element.answer
        ? element.answer.map((answer) => {
            const file: Partial<MetaFile> = {
              id: answer.entityId,
              fileName: `${answer.name}.${answer.extension}`,
            };

            return file;
          })
        : null;

      return {
        name: element.name,
        type: element.type,
        label: element.description,
        value: signatures,
      };
    case "checkbox":
      return {
        name: element.name,
        type: element.type,
        label: element.description,
        value:
          typeof element.answer === "boolean"
            ? element.answer
            : element.answer === "true"
            ? true
            : false,
      };
    default:
      return {
        name: element.name,
        type: element.type,
        label: element.description,
        value: element.answer,
      };
  }
}

export default {
  fromCheetahResponse,
};
