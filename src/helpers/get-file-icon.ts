import { AssetsService } from "../services";

export function createGetFileIcon(assetService: AssetsService) {
  return function (name) {
    if (!name) return assetService.getIcons("iconAttachment");

    const extension = name.substring(name.lastIndexOf(".") + 1);
    // console.log('getting file icon for ' + name);
    switch (extension) {
      case "pdf":
        // console.log('-> PDF');
        return assetService.getIcons("iconPdf"); //require("./assets/icon/icon-pdf.png");
      case "png":
        return assetService.getIcons("iconImage");
      case "jpg":
        return assetService.getIcons("iconImage");
      case "jpeg":
        return assetService.getIcons("iconImage");
      case "docx" || "doc":
        return assetService.getIcons("iconDocx");
      default:
        // console.log('-> generic');
        return assetService.getIcons("iconAttachment");
    }
  };
}
