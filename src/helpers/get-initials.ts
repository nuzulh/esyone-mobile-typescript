export function getInitials(text: string) {
  if (!text) {
    return "?";
  }
  const u = text.toUpperCase();
  const split = u.split(" ");
  if (split.length == 1) {
    return split[0].charAt(0);
  } else {
    return split[0].charAt(0) + split[split.length - 1].charAt(0);
  }
}
