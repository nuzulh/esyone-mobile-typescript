export function validatePhoneNumber(phoneNumber: string) {
  if (!phoneNumber) return null;

  // Remove all non-digit characters from the phone number
  const digitsPhoneNumber = phoneNumber.replace(/\D/g, "");

  if (digitsPhoneNumber.length < 11) return null;

  // Regular expression to match phone numbers
  const regex = /^(\+|\d)[0-9]{6,15}$/;

  // Test the phone number against the regular expression
  const isValid = regex.test(digitsPhoneNumber);

  // Return the validated phone number with a plus sign in front if it's valid, or null if it's not valid
  return isValid ? `+${digitsPhoneNumber}` : null;
}

export function validateNumber(text: any) {
  const number = parseFloat(text);
  return !isNaN(number) && isFinite(text) ? number : null;
}

export function validateEmail(email: string) {
  if (!email) return null;

  // Regular expression to match email addresses
  const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  // Test the email address against the regular expression
  const isValid = regex.test(email);

  // Return the validated email address if it's valid, or null if it's not valid
  return isValid ? email : null;
}
