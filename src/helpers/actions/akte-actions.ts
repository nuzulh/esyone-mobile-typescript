import { ResourceData } from "../../hooks";
import { Account, Akte } from "../../models";
import { Message, MessageAttachment } from "../../models/message";
import { MetaFile } from "../../services";
import { AKTE_ACTIONS } from "../consts";

export function fetchAllAkteAction() {
  return {
    type: AKTE_ACTIONS.AKTE_FETCH_ALL,
  };
}

export function fetchSingleAkteAction(account: Account, akte: Akte) {
  return {
    type: AKTE_ACTIONS.AKTE_FETCH,
    payload: {
      account,
      akte,
    },
  };
}

export function previewAkte(
  account: Account,
  akte: Akte,
  initialScreen?: string
) {
  return {
    type: AKTE_ACTIONS.AKTE_PREVIEW,
    payload: {
      account,
      akte,
      initialScreen,
    },
  };
}

export function previewAttachmentAction(
  akte: Akte,
  attachment: MessageAttachment
) {
  return {
    type: AKTE_ACTIONS.AKTE_PREVIEW_ATTACHMENT,
    payload: {
      akte,
      attachment,
    },
  };
}

export function previewFileAction(account: Account, file: MetaFile) {
  return {
    type: AKTE_ACTIONS.AKTE_PREVIEW_FILE,
    payload: {
      account,
      file,
    },
  };
}

export function downloadAttachmentAction(metafile: MetaFile) {
  return {
    type: AKTE_ACTIONS.AKTE_DOWNLOAD_ATTACHMENT,
    payload: { metafile },
  };
}

export function downloadAttachmentFromMessage(akte: Akte, message: Message) {
  return {
    type: AKTE_ACTIONS.AKTE_DOWNLOAD_ATTACHMENT_FROM_MESSAGE,
    payload: { akte, message },
  };
}

export function downloadAkteAsZip(accountId: number, akteId: string) {
  return {
    type: AKTE_ACTIONS.AKTE_DOWNLOAD_AS_ZIP,
    payload: { accountId, akteId },
  };
}

export function updateAllAkteAction(akten: ResourceData<Akte>) {
  return {
    type: AKTE_ACTIONS.MUTATION.AKTE_UPDATE_ALL,
    payload: akten,
  };
}

export function updateSingleAkteAction(akte: Akte) {
  return {
    type: AKTE_ACTIONS.MUTATION.AKTE_UPDATE,
    payload: akte,
  };
}

export function updateAkteFetchStatus(isFetching: boolean) {
  return {
    type: AKTE_ACTIONS.MUTATION.AKTE_UPDATE_FETCHING,
    payload: isFetching,
  };
}

export function updateAttachmentAction(attachment: MessageAttachment) {
  return {
    type: AKTE_ACTIONS.MUTATION.AKTE_UPDATE_ATTACHMENT,
    payload: attachment,
  };
}

export function submitMessageAction(
  message: Message,
  account: Account,
  akte: Akte,
  metaFiles?: MetaFile[]
) {
  return {
    type: AKTE_ACTIONS.AKTE_SUBMIT_MESSAGE,
    payload: {
      message,
      account,
      akte,
      metaFiles,
    },
  };
}

export function toggleFavoriteMessage(akte: Akte, message: Message) {
  return {
    type: AKTE_ACTIONS.AKTE_TOGGLE_FAVORITE_MESSAGE,
    payload: {
      akte,
      message,
    },
  };
}

export function readMessageAction(akte: Akte, message: Message) {
  return {
    type: AKTE_ACTIONS.AKTE_READ_MESSAGE,
    payload: {
      akte,
      message,
    },
  };
}

export function redirectExternallinkAction(url: string, title: string) {
  return {
    type: AKTE_ACTIONS.AKTE_REDIRECT_EXTERNAL_LINK,
    payload: {
      url,
      title,
    },
  };
}
