import {
  AuthState,
  DispatchAction,
  Profile,
  Session,
  UserSeed,
} from "../../models";
import { MetaFile } from "../../services";
import { AUTH_ACTIONS } from "../consts";

export function authRequestLoginAction(
  username: string,
  password: string,
  next?: any
) {
  return {
    type: AUTH_ACTIONS.AUTH_REQUEST_LOGIN,
    payload: {
      username,
      password,
      next,
    },
  };
}

export function authRequestBiometricLoginAction(next?: any) {
  return {
    type: AUTH_ACTIONS.AUTH_REQUEST_BIOMETRIC_LOGIN,
    payload: {
      next,
    },
  };
}

export function authUpdateProfileImage(metaFiles?: MetaFile[]) {
  return {
    type: AUTH_ACTIONS.AUTH_UPDATE_PROFILE_IMAGE,
    payload: {
      metaFiles,
    },
  };
}

export function authSucceedAction(
  authState: Partial<AuthState>
): DispatchAction<Partial<AuthState>> {
  return {
    type: AUTH_ACTIONS.MUTATION.AUTH_UPDATE,
    payload: {
      ...authState,
      isLoggedIn: true,
      isError: false,
      error: null,
      errorMessage: null,
    },
  };
}

export function authFailedAction(
  error: Error
): DispatchAction<Partial<AuthState>> {
  return {
    type: AUTH_ACTIONS.MUTATION.AUTH_UPDATE,
    payload: {
      isLoggedIn: false,
      isError: true,
      error,
      errorMessage: error.message,
    },
  };
}

export function authUpdateAction(
  authState: Partial<AuthState>
): DispatchAction<Partial<AuthState>> {
  return {
    type: AUTH_ACTIONS.MUTATION.AUTH_UPDATE,
    payload: authState,
  };
}

export function authLoadProfileAction(accountId?: string, userId?: string) {
  return {
    type: AUTH_ACTIONS.AUTH_LOAD_PROFILE,
    payload: {
      accountId,
      userId,
    },
  };
}

export function authUpdateProfileAction(profile: Partial<Profile>) {
  return {
    type: AUTH_ACTIONS.MUTATION.AUTH_UPDATE_PROFILE,
    payload: {
      profile,
    },
  };
}

export function authClearSessionAction() {
  return {
    type: AUTH_ACTIONS.MUTATION.AUTH_CLEAR_SESSION,
  };
}

export function authLogoutAction(clearCached: boolean = false) {
  return {
    type: AUTH_ACTIONS.AUTH_LOGOUT,
    payload: {
      clearCached,
    },
  };
}

export function authCheckUsernameAction(email: string) {
  return {
    type: AUTH_ACTIONS.AUTH_CHECK_USERNAME,
    payload: {
      email,
    },
  };
}

export function authResetPasswordAction(username: string) {
  return {
    type: AUTH_ACTIONS.AUTH_RESET_PASSWORD,
    payload: {
      username,
    },
  };
}

export function authChangePasswordAction(
  oldpassword: string,
  newpassword: string,
  newpasswordconfirmation: string
) {
  return {
    type: AUTH_ACTIONS.AUTH_CHANGE_PASSWORD,
    payload: {
      oldpassword,
      newpassword,
      newpasswordconfirmation,
    },
  };
}

export function startRegistrationAction(key: string) {
  return {
    type: AUTH_ACTIONS.AUTH_START_REGISTRATION,
    payload: { key },
  };
}

export function authRequestPin(
  key: string,
  channel?: string,
  silent?: boolean
) {
  return {
    type: AUTH_ACTIONS.AUTH_REQUEST_PIN,
    payload: {
      key,
      channel,
      silent,
    },
  };
}

export function authRequestPinLoginAction(
  key: string,
  pin: string,
  user: UserSeed
) {
  return {
    type: AUTH_ACTIONS.AUTH_REQUEST_PIN_LOGIN,
    payload: {
      key,
      pin,
      user,
    },
  };
}

export function authRequestUsername(user: UserSeed) {
  return {
    type: AUTH_ACTIONS.AUTH_REQUEST_USERNAME,
    payload: {
      user,
    },
  };
}

export function authSubmitRegistrationAction(key: string, user: UserSeed) {
  return {
    type: AUTH_ACTIONS.AUTH_REGISTER,
    payload: {
      key,
      user,
    },
  };
}

export function authCreateNewPassword(key: string, password: string) {
  return {
    type: AUTH_ACTIONS.AUTH_CREATE_NEW_PASSWORD,
    payload: {
      key,
      password,
    },
  };
}
