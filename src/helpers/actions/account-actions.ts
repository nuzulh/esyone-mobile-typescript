import { Account, Assistant, Profile } from "../../models";
import { ACCOUNT_ACTIONS } from "../consts";

export function fetchProfileAction(id: string) {
  return {
    type: ACCOUNT_ACTIONS.ACCOUNT_FETCH_PROFILE,
    payload: id,
  };
}

export function updateProfileAction(profile: Profile) {
  return {
    type: ACCOUNT_ACTIONS.MUTATION.ACCOUNT_UPDATE_PROFILE,
    payload: profile,
  };
}

export function previewAccount(account: Account) {
  return {
    type: ACCOUNT_ACTIONS.ACCOUNT_PREVIEW,
    payload: account,
  };
}

export function previewAgb(account: Account) {
  return {
    type: ACCOUNT_ACTIONS.ACCOUNT_PREVIEW_AGB,
    payload: account,
  };
}

export function previewDatenschutz(account: Account) {
  return {
    type: ACCOUNT_ACTIONS.ACCOUNT_PREVIEW_DATENSCHUTZ,
    payload: account,
  };
}

export function previewAgbText(account: Account) {
  return {
    type: ACCOUNT_ACTIONS.ACCOUNT_PREVIEW_AGB_TEXT,
    payload: account,
  };
}

export function previewDatenschutzText(account: Account) {
  return {
    type: ACCOUNT_ACTIONS.ACCOUNT_PREVIEW_DATENSCHUTZ_TEXT,
    payload: account,
  };
}

export function shareAccount(account: Account) {
  return {
    type: ACCOUNT_ACTIONS.ACCOUNT_SHARE,
    payload: account,
  };
}

export function shareAssistant(assistant: Assistant) {
  return {
    type: ACCOUNT_ACTIONS.ACCOUNT_SHARE_ASSISTANT,
    payload: assistant,
  };
}

export function activateAccount(account: Account) {
  return {
    type: ACCOUNT_ACTIONS.ACCOUNT_ACTIVATE,
    payload: account,
  };
}

export function fetchAllAccountAction() {
  return {
    type: ACCOUNT_ACTIONS.ACCOUNT_FETCH_ALL,
  };
}
