import { FirebaseMessagingTypes } from "@react-native-firebase/messaging";
import { DispatchAction, FCMState } from "../../models";
import { FCM_ACTIONS } from "../consts";

export function askFcmPermissionAction() {
  return {
    type: FCM_ACTIONS.FCM_INIT_PERMISSIONS,
  };
}

export function enableFcmAction() {
  return {
    type: FCM_ACTIONS.FCM_REGISTER,
  };
}

export function disableFcmAction() {
  return {
    type: FCM_ACTIONS.FCM_UNREGISTER,
  };
}

export function updateTokenAction(
  token: string,
  isEnabled: boolean = true
): DispatchAction<Partial<FCMState>> {
  return {
    type: FCM_ACTIONS.MUTATION.FCM_UPDATE,
    payload: {
      isEnabled: isEnabled,
      token,
    },
  };
}

export function registerSuccessAction(): DispatchAction<Partial<FCMState>> {
  return {
    type: FCM_ACTIONS.MUTATION.FCM_UPDATE,
    payload: {
      isRegistered: true,
    },
  };
}

export function registerFailAction(): DispatchAction<Partial<FCMState>> {
  return {
    type: FCM_ACTIONS.MUTATION.FCM_UPDATE,
    payload: {
      isRegistered: false,
      isEnabled: false,
      token: null,
    },
  };
}

export function permissionEnabledAction(): DispatchAction<Partial<FCMState>> {
  return {
    type: FCM_ACTIONS.MUTATION.FCM_UPDATE,
    payload: {
      hasPermission: true,
    },
  };
}

export function permissionDisabledAction(): DispatchAction<Partial<FCMState>> {
  return {
    type: FCM_ACTIONS.MUTATION.FCM_UPDATE,
    payload: {
      hasPermission: false,
    },
  };
}

export function setPermissionIsAskedAction(): DispatchAction<
  Partial<FCMState>
> {
  return {
    type: FCM_ACTIONS.MUTATION.FCM_UPDATE,
    payload: {
      isPermissionAsked: true,
    },
  };
}

export function handleMessage(
  message: FirebaseMessagingTypes.RemoteMessage,
) {
  return {
    type: FCM_ACTIONS.EVENT.FCM_HANDLE_MESSAGE,
    payload: { message },
  };
}
