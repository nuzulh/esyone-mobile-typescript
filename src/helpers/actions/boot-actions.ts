import { BOOT_ACTIONS } from "../consts";

export function startApplicationBoot() {
  return {
    type: BOOT_ACTIONS.BOOT_START,
  };
}
