import { DispatchAction, WorkflowFormular } from "../../models";
import { FORMULAR_ACTIONS } from "../consts";

export function saveFormularDraft(
  workflowFormular: Partial<WorkflowFormular>
): DispatchAction<Partial<WorkflowFormular>> {
  return {
    type: FORMULAR_ACTIONS.MUTATION.FORMULAR_SAVE_DRAFT,
    payload: workflowFormular,
  };
}

export function resetFormularDraft(
  workflowId: string
): DispatchAction<string> {
  return {
    type: FORMULAR_ACTIONS.MUTATION.FORMULAR_RESET,
    payload: workflowId,
  };
}

export function resetAllFormularDraft() {
  return {
    type: FORMULAR_ACTIONS.MUTATION.FORMULAR_RESET_ALL,
  };
}
