import { MetaFile } from "../../services";
import { FILE_ACTIONS } from "../consts";

export function updateFileAction(file: MetaFile) {
  return {
    type: FILE_ACTIONS.MUTATION.FILE_UPDATE,
    payload: file,
  };
}

export function updateFileFetchStatusAction(isFetching: boolean) {
  return {
    type: FILE_ACTIONS.MUTATION.FILE_UPDATE_FETCHING,
    payload: isFetching,
  };
}

export function reportFileFetchError(error: Error) {
  return {
    type: FILE_ACTIONS.MUTATION.FILE_UPDATE_FETCH_ERROR,
    payload: error,
  };
}

export function removeFile(file: MetaFile) {
  return {
    type: FILE_ACTIONS.MUTATION.FILE_REMOVE,
    payload: file,
  };
}

export function resetFileState() {
  return {
    type: FILE_ACTIONS.MUTATION.FILE_RESET,
  };
}

export function clearFileCache() {
  return {
    type: FILE_ACTIONS.FILE_CLEAR_CACHE,
  };
}
