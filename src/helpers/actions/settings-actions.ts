import { DispatchAction, SettingsState } from "../../models";
import { SETTINGS_ACTIONS } from "../consts";

export function updateLanguageAction(
  countryId: string
): DispatchAction<Partial<SettingsState>> {
  return {
    type: SETTINGS_ACTIONS.MUTATION.SETTINGS_UPDATE,
    payload: {
      language: countryId,
    },
  };
}

export function updateBiometricSupport(
  isSupported: boolean,
  biometricType: string
): DispatchAction<Partial<SettingsState>> {
  return {
    type: SETTINGS_ACTIONS.MUTATION.SETTINGS_UPDATE,
    payload: {
      isBiometricSupported: isSupported,
      biometricType,
    },
  };
}

export function updateBiometricSettings(
  isEnabled: boolean
): DispatchAction<Partial<SettingsState>> {
  return {
    type: SETTINGS_ACTIONS.MUTATION.SETTINGS_UPDATE,
    payload: {
      isBiometricEnabled: isEnabled,
      isBiometricAsked: true,
    },
  };
}

export function updatePushNotificationSupport(
  isEnabled: boolean
): DispatchAction<Partial<SettingsState>> {
  return {
    type: SETTINGS_ACTIONS.MUTATION.SETTINGS_UPDATE,
    payload: {
      isPushNotificationEnabled: isEnabled,
      isPushNotificationAsked: true,
    },
  };
}

export function startActivations() {
  return {
    type: SETTINGS_ACTIONS.SETTINGS_START_ACTIVATIONS,
  };
}
