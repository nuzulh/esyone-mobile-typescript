import { put } from "redux-saga/effects";
import { DispatchAction } from "../../models";
import { NavigationTarget } from "../../models/types/navigation-states";
import { NAVIGATION_ACTIONS } from "../consts";

export declare type NavigationSpecs = {
  navigateTo: {
    stack: string;
    screen?: string;
    params?: any;
  };
};

export function* dispatchNavigation(specs: NavigationSpecs) {
  yield put({
    type: NAVIGATION_ACTIONS.MUTATION.NAVIGATION_UPDATE,
    payload: specs,
  });
}

export function navigateToAction(
  target: Partial<NavigationTarget>
): DispatchAction<Partial<NavigationTarget>> {
  return {
    type: NAVIGATION_ACTIONS.MUTATION.NAVIGATION_UPDATE,
    payload: target,
  };
}
