import { NO_ACTIONS } from "../consts";

export function doNothingAction() {
  return {
    type: NO_ACTIONS.DO_NOTHING,
  };
}
