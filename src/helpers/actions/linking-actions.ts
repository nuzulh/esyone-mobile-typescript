import { DispatchAction, LinkingState } from "../../models";
import { LINKING_ACTIONS } from "../consts";

export function updateLinkingAction(
  newLink: string
): DispatchAction<Partial<LinkingState>> {
  return {
    type: LINKING_ACTIONS.MUTATION.LINKING_UPDATE,
    payload: {
      currentLink: newLink,
    },
  };
}

export function resetLinkingAction() {
  return {
    type: LINKING_ACTIONS.MUTATION.LINKING_RESET,
  };
}

export function openExternalLinkAction(url: string): DispatchAction<string> {
  return {
    type: LINKING_ACTIONS.LINKING_OPEN_URL,
    payload: url,
  };
}

export function openExternalMailAction(payload?: any): DispatchAction<any> {
  return {
    type: LINKING_ACTIONS.LINKING_OPEN_MAIL,
    payload,
  };
}

export function openExternalSMSAction(payload?: any): DispatchAction<any> {
  return {
    type: LINKING_ACTIONS.LINKING_OPEN_SMS,
    payload,
  };
}
