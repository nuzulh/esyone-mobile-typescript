import {
  Account,
  Aufgabe,
  DispatchAction,
  WorkflowProcess,
  WorkflowProcessSubmission,
} from "../../models";
import { FileUploadInfo } from "../../services";
import { WORKFLOW_ACTIONS } from "../consts";

export function loadWorkflow(
  accountId: number,
  workflowId: string,
  accessToken: string
) {
  return {
    type: WORKFLOW_ACTIONS.WORKFLOW_LOAD,
    payload: {
      accountId,
      workflowId,
      accessToken,
    },
  };
}

export function resumeWorkflow(workflowId: string) {
  return {
    type: WORKFLOW_ACTIONS.WORKFLOW_RESUME,
    payload: {
      workflowId,
    },
  };
}

export function loadAufgabe(provider: Account, aufgabe: Aufgabe) {
  return {
    type: WORKFLOW_ACTIONS.WORKFLOW_LOAD_AUFGABE,
    payload: {
      account: provider,
      aufgabe,
    },
  };
}

export function submitWorkflow(
  account: Account,
  workflowProcess: WorkflowProcess,
  accessToken: string,
  data: any,
  uploadInfo: FileUploadInfo
): DispatchAction<WorkflowProcessSubmission> {
  return {
    type: WORKFLOW_ACTIONS.WORKFLOW_SUBMIT,
    payload: {
      account,
      workflowProcess,
      accessToken,
      data,
      uploadInfo,
    },
  };
}

export function loadWorkflowAnswer(
  workflowId: string,
  account: Account
): DispatchAction<{ workflowId: string; account: Account; }> {
  return {
    type: WORKFLOW_ACTIONS.WORKFLOW_LOAD_ANSWER,
    payload: {
      workflowId,
      account,
    },
  };
}
