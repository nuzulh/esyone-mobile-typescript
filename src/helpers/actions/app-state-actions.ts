import { ApplicationState, DispatchAction } from "../../models";
import { APP_STATE_ACTIONS } from "../consts";

export function updateAppStateAction(appState: Partial<ApplicationState>) {
  return {
    type: APP_STATE_ACTIONS.MUTATION.APP_STATE_UPDATE,
    payload: appState,
  };
}

export function reportAppError(
  error: Error
): DispatchAction<Partial<ApplicationState>> {
  return {
    type: APP_STATE_ACTIONS.MUTATION.APP_STATE_UPDATE,
    payload: {
      isError: true,
      error,
      errorMessage: error.message,
    },
  };
}

export function clearAppError(): DispatchAction<Partial<ApplicationState>> {
  return {
    type: APP_STATE_ACTIONS.MUTATION.APP_STATE_UPDATE,
    payload: {
      isError: false,
      error: null,
      errorMessage: null,
    },
  };
}

export function resetAppStateAction() {
  return {
    type: APP_STATE_ACTIONS.MUTATION.APP_STATE_RESET,
  };
}

export function showLoadingAction() {
  return {
    type: APP_STATE_ACTIONS.MUTATION.APP_STATE_UPDATE,
    payload: {
      isLoading: true,
    },
  };
}

export function hideLoadingAction() {
  return {
    type: APP_STATE_ACTIONS.MUTATION.APP_STATE_UPDATE,
    payload: {
      isLoading: false,
    },
  };
}

export function showLoadingBackgroundAction() {
  return {
    type: APP_STATE_ACTIONS.MUTATION.APP_STATE_UPDATE,
    payload: {
      isLoadingBackground: true,
    },
  };
}

export function hideLoadingBackgroundAction() {
  return {
    type: APP_STATE_ACTIONS.MUTATION.APP_STATE_UPDATE,
    payload: {
      isLoadingBackground: false,
    },
  };
}

export function appReadyAction() {
  return {
    type: APP_STATE_ACTIONS.APP_STATE_READY,
  };
}
