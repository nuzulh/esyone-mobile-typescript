import { DispatchAction, KeyboardState } from "../../models";
import { KEYBOARD_ACTIONS } from "../consts";

export function updateKeyboardAction(
  isVisible: boolean
): DispatchAction<Partial<KeyboardState>> {
  return {
    type: KEYBOARD_ACTIONS.MUTATION.KEYBOARD_UPDATE_VISIBILITY,
    payload: {
      isVisible,
    },
  };
}
