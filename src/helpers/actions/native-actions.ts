import { DispatchAction, NativeState } from "../../models";
import { NATIVE_ACTIONS } from "../consts";

export function setCameraPermissionIsAskedAction(
  isAsked: boolean
): DispatchAction<Partial<NativeState>> {
  return {
    type: NATIVE_ACTIONS.MUTATION.NATIVE_UPDATE,
    payload: {
      isCameraPermissionAsked: isAsked,
    },
  };
}

export function setCameraPermissionIsGrantedAction(isGranted: boolean) {
  return {
    type: NATIVE_ACTIONS.MUTATION.NATIVE_UPDATE,
    payload: {
      isCameraPermissionGranted: isGranted,
    },
  };
}
