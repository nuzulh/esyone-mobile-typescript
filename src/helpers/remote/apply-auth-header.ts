import { checkTokenIsValid } from "../crypt";

export function applyAuthHeader(header: any, token?: string) {
  if (!token) return header;
  if (!checkTokenIsValid(token)) return header;

  return {
    ...header,
    Authorization: `Bearer ${token}`,
  };
}
