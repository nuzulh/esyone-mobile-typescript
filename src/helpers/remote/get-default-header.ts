import config, { AVAILABLE_KEYS } from "../config";

export function getDefaultHeader() {
  return {
    "Content-Type": "application/json",
    "X-API-Key": config.getOrElse(AVAILABLE_KEYS.API_KEY, ""),
    "X-Application-Key": config.getOrElse(AVAILABLE_KEYS.APPLICATION_KEY, ""),
  };
}
