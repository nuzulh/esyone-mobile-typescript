# e.Syone Mobile Typescript Components

This package contains logical components and screens for e.syOne mobile application. You need esyone-mobile to run these components and screens.

## Requirements

- Node Js >= 16
- Yarn
- Npm

## Development Setup

*note: make sure you have access to e-consult gitlab repository

```sh
git clone -b development https://gitlab.e-consult.eco/esything/clients/esyone-mobile-typescript.git
cd esyone-mobile-typescript
yarn install
```

### Build init for the first time

```sh
yarn build: init
```

### Build after making changes to modify esyone-mobile package

```sh
yarn build
```